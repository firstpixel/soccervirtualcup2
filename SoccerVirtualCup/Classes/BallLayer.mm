//
//  BallLayer.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import "BallLayer.h"
#import "GameCredit.h"
#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "AppDelegate.h"
#import "AfterGameMatchsScene.h"
#import "TournamentScene.h"
#import "DataHolder.h"
#import "MatchDrawScene.h"
#import "MatchDrawSceneSingle.h"
#import "MatchWinnerScene.h"
#import "MatchLoserScene.h"
#import "SimpleAudioEngine.h"
#import "DeviceSettings.h"
#import "RootViewController.h"
#import "GameConfig.h"


#ifdef __IPHONE_4_1
#import "GameKit/GameKit.h"
#endif
#import "Flurry.h"

BallLayer * ballLayer;

#define kBannerAdjustY 12


//main cocos2d rendering loop, this has to be streamlined
// e.g dont add logs here :/

static void eachShape(void *ptr, void* unused){
    
	cpShape *shape = (cpShape*) ptr;
	CCSprite* sprite = (CCSprite*)shape->data;
	
	if(sprite.tag== 1 || sprite.tag == 2 || sprite.tag == 3 ||
	   sprite.tag== 4 || sprite.tag == 5 || sprite.tag == 6 ||
	   sprite.tag == 7){
		
		cpBody *body = shape->body;

		[sprite setPosition: ADJUST_CCP(ccp( body->p.x, body->p.y))];
		if(sprite.tag==7){
			//[sprite setRotation: (float) CC_RADIANS_TO_DEGREES( -body->a )];
        }
		
	}
}



static int ballCollisionNew(cpArbiter *arb,cpSpace *space, void *data)
{
	cpShape *a, *b; 
	cpArbiterGetShapes(arb, &a, &b);
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	CCSprite* sprite1 = (CCSprite*)a->data;
	CCSprite* sprite2 = (CCSprite*)b->data;
	BallLayer *ballLayer = (BallLayer *)data;	
	if(sprite1.tag==7 ){
		if((sprite1.tag==7 && sprite2.tag==601) || (sprite1.tag==7 && sprite2.tag==602) || (sprite1.tag==7 && sprite2.tag==603) || (sprite1.tag==7 && sprite2.tag==604)){
			//NSLog(@"BALL HIT CORNER ADD IMPULSE!! %i",sprite2.tag);
			//NSLog(@"******************************************************************************");
			if(sprite2.tag==601)[ballLayer setCornerImpulse:1];
			if(sprite2.tag==602)[ballLayer setCornerImpulse:2];
			if(sprite2.tag==603)[ballLayer setCornerImpulse:3];
			if(sprite2.tag==604)[ballLayer setCornerImpulse:4];
			return 1;
		}
		if((sprite1.tag==7 && sprite2.tag==1001) || (sprite1.tag==7 && sprite2.tag==1002) || (sprite1.tag==7 && sprite2.tag==1003) 
		   || (sprite1.tag==7 && sprite2.tag==1004) || (sprite1.tag==7 && sprite2.tag==1005) || (sprite1.tag==7 && sprite2.tag==1006)){
			//NSLog(@"BALL HIT WALL ADD IMPULSE!! %i",sprite2.tag);
			//NSLog(@"******************************************************************************");
			if(sprite2.tag==1001)[ballLayer setWallImpulse:1];
			if(sprite2.tag==1002)[ballLayer setWallImpulse:2];
			if(sprite2.tag==1003)[ballLayer setWallImpulse:2];
			if(sprite2.tag==1004)[ballLayer setWallImpulse:3];
			if(sprite2.tag==1005)[ballLayer setWallImpulse:4];
			if(sprite2.tag==1006)[ballLayer setWallImpulse:4];
			return 1;
		}
		if((sprite1.tag==7 && sprite2.tag==101) || (sprite1.tag==7 && sprite2.tag==102) || (sprite1.tag==7 && sprite2.tag==103) || (sprite1.tag==7 && sprite2.tag==104)){
			if( [mySingleton  golAnimationPlaying]==NO  ){
				if((sprite1.tag==7 && sprite2.tag==103))[ballLayer golScored:2];
				if((sprite1.tag==7 && sprite2.tag==104))[ballLayer golScored:1];
			}
			return 0;
		}else{
			[ballLayer ballAudio];
			return 1;
		}
	}else{
		return 1;
	}
}


@implementation BallLayer

@synthesize hasSent;

@synthesize highscores;
@synthesize leaderboardMatchWinner;
@synthesize scoredPoint;
@synthesize teamFactor1;
@synthesize teamFactor2;
@synthesize leaderboardPlayTime;

@synthesize leaderboardAddicted;
@synthesize leaderboardPoints;
@synthesize leaderboardGoalScored;
@synthesize leaderboardGoalConceded;
@synthesize leaderboardGoalDifference;
@synthesize leaderboardWins;
@synthesize leaderboardLoss;
@synthesize leaderboardDraws;

@synthesize elasticIterations;
@synthesize ballMass;
@synthesize playerMass;
@synthesize kSlide;

@synthesize cornerImpulse;
@synthesize wallImpulse;
@synthesize ballFrameNumber;
@synthesize diffPosy;
@synthesize diffPosx;
@synthesize oldpy,oldpx;
//@synthesize gkHelper=_gkHelper;
//
// Called by NSTimer fire, hides the game sprite
//
- (void)removeSpriteItsYourTurn:(NSTimer *)timer {
    [self removeSpriteGlowP1];
    [self removeSpriteGlowP2];
    [self removeChild:itsYourTurn cleanup:YES];
	itsYourTurn=nil;
}

- (void)addSpriteGlowP1{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    
    if([mySingleton isP1]==YES && glow1Sprite==nil){
        NSLog(@"GLOW 1");
        [self removeSpriteGlowP2];
        glow1Sprite = [CCSprite spriteWithSpriteFrameName:@"clock24.png"];
        glow2Sprite = [CCSprite spriteWithSpriteFrameName:@"clock24.png"];
        glow3Sprite = [CCSprite spriteWithSpriteFrameName:@"clock24.png"];
        
        [glow1Sprite setPosition:ccp(p1Sprite.contentSize.width /2 ,p1Sprite.contentSize.height /2 + 1)];
        [glow2Sprite setPosition:ccp(p2Sprite.contentSize.width /2 ,p2Sprite.contentSize.height /2 + 1)];
        [glow3Sprite setPosition:ccp(p3Sprite.contentSize.width /2 ,p3Sprite.contentSize.height /2 + 1)];
        
        [p1Sprite addChild:glow1Sprite z:-1];
        [p2Sprite addChild:glow2Sprite z:-1];
        [p3Sprite addChild:glow3Sprite z:-1];
        
        id scale1To = [CCScaleTo actionWithDuration:0.5f scale:1.4f];
        id scale1Back = [CCScaleTo actionWithDuration:0.5f scale:0.7];
        id scale2To = [CCScaleTo actionWithDuration:0.5f scale:1.4f];
        id scale2Back = [CCScaleTo actionWithDuration:0.5f scale:0.7];
        id scale3To = [CCScaleTo actionWithDuration:0.5f scale:1.4f];
        id scale3Back = [CCScaleTo actionWithDuration:0.5f scale:0.7];
        
        id seq1 = [CCSequence actions: scale1To, scale1Back, scale1To, scale1Back, scale1To, scale1Back, scale1To, scale1Back, scale1To, scale1Back, scale1To, scale1Back, scale1To, scale1Back, scale1To, scale1Back, scale1To, scale1Back, scale1To, scale1Back, nil];
        id seq2 = [CCSequence actions: scale2To, scale2Back, scale2To, scale2Back, scale2To, scale2Back, scale2To, scale2Back, scale2To, scale2Back, scale2To, scale2Back, scale2To, scale2Back, scale2To, scale2Back, scale2To, scale2Back, scale2To, scale2Back, nil];
        id seq3 = [CCSequence actions: scale3To, scale3Back, scale3To, scale3Back, scale3To, scale3Back, scale3To, scale3Back, scale3To, scale3Back, scale3To, scale3Back, scale3To, scale3Back, scale3To, scale3Back, scale3To, scale3Back, scale3To, scale3Back, nil];
        
        [glow1Sprite runAction:seq1];
        glow1Sprite.blendFunc = (ccBlendFunc){GL_ONE, GL_ONE};
        [glow2Sprite runAction:seq2];
        glow2Sprite.blendFunc = (ccBlendFunc){GL_ONE, GL_ONE};
        [glow3Sprite runAction:seq3];
        glow3Sprite.blendFunc = (ccBlendFunc){GL_ONE, GL_ONE};
    }
    
    
}

- (void)addSpriteGlowP2{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    
    if([mySingleton isP2]==YES && glow4Sprite==nil){
        NSLog(@"GLOW 2");
        [self removeSpriteGlowP1];
        glow4Sprite = [CCSprite spriteWithSpriteFrameName:@"clock24.png"];
        glow5Sprite = [CCSprite spriteWithSpriteFrameName:@"clock24.png"];
        glow6Sprite = [CCSprite spriteWithSpriteFrameName:@"clock24.png"];
        
        [glow4Sprite setPosition:ccp(p4Sprite.contentSize.width /2 ,p4Sprite.contentSize.height /2 + 1)];
        [glow5Sprite setPosition:ccp(p5Sprite.contentSize.width /2 ,p5Sprite.contentSize.height /2 + 1)];
        [glow6Sprite setPosition:ccp(p6Sprite.contentSize.width /2 ,p6Sprite.contentSize.height /2 + 1)];
        
        [p4Sprite addChild:glow4Sprite z:-1];
        [p5Sprite addChild:glow5Sprite z:-1];
        [p6Sprite addChild:glow6Sprite z:-1];
        
        id scale4To = [CCScaleTo actionWithDuration:0.5f scale:1.4f];
        id scale4Back = [CCScaleTo actionWithDuration:0.5f scale:0.7];
        id scale5To = [CCScaleTo actionWithDuration:0.5f scale:1.4f];
        id scale5Back = [CCScaleTo actionWithDuration:0.5f scale:0.7];
        id scale6To = [CCScaleTo actionWithDuration:0.5f scale:1.4f];
        id scale6Back = [CCScaleTo actionWithDuration:0.5f scale:0.7];
        
        id seq4 = [CCSequence actions: scale4To, scale4Back, scale4To, scale4Back, scale4To, scale4Back, scale4To, scale4Back, scale4To, scale4Back, scale4To, scale4Back, scale4To, scale4Back, scale4To, scale4Back, scale4To, scale4Back, scale4To, scale4Back, nil];
        id seq5 = [CCSequence actions: scale5To, scale5Back, scale5To, scale5Back, scale5To, scale5Back, scale5To, scale5Back, scale5To, scale5Back, scale5To, scale5Back, scale5To, scale5Back, scale5To, scale5Back, scale5To, scale5Back, scale5To, scale5Back, nil];
        id seq6 = [CCSequence actions: scale6To, scale6Back, scale6To, scale6Back, scale6To, scale6Back, scale6To, scale6Back, scale6To, scale6Back, scale6To, scale6Back, scale6To, scale6Back, scale6To, scale6Back, scale6To, scale6Back, scale6To, scale6Back, nil];
        
        [glow4Sprite runAction:seq4];
        glow4Sprite.blendFunc = (ccBlendFunc){GL_ONE, GL_ONE};
        [glow5Sprite runAction:seq5];
        glow5Sprite.blendFunc = (ccBlendFunc){GL_ONE, GL_ONE};
        [glow6Sprite runAction:seq6];
        glow6Sprite.blendFunc = (ccBlendFunc){GL_ONE, GL_ONE};
        
    }
    
    
}

- (void)removeSpriteGlowP1{
    NSLog(@"REMOVE GLOW 1");
    [p1Sprite removeChild:glow1Sprite cleanup:YES];
    [p2Sprite removeChild:glow2Sprite cleanup:YES];
    [p3Sprite removeChild:glow3Sprite cleanup:YES];
    glow1Sprite=nil;
    glow2Sprite=nil;
    glow3Sprite=nil;
    
}

- (void)removeSpriteGlowP2{
    NSLog(@"REMOVE GLOW 2");
    [p4Sprite removeChild:glow4Sprite cleanup:YES];
    [p5Sprite removeChild:glow5Sprite cleanup:YES];
    [p6Sprite removeChild:glow6Sprite cleanup:YES];
    glow4Sprite=nil;
    glow5Sprite=nil;
    glow6Sprite=nil;
    
}
- (void)removeSpriteGetReady:(NSTimer *)timer {
	[self removeChild:getReady cleanup:YES];	
}
- (void)removeSpriteWaitPlaying:(NSTimer *)timer {
	[self removeChild:waitPlaying cleanup:YES];	
}



//****************
// AUDIO
//****************
-(void)ballAudio{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		if([mySingleton ballAudio]==NO){
			[mySingleton setBallAudio:YES];
			int r = arc4random() % 2;
			if(r==1){
				[[SimpleAudioEngine sharedEngine] playEffect:@"kick1.mp3"];
			}else{
				[[SimpleAudioEngine sharedEngine] playEffect:@"kick.mp3"];
			}
            [self schedule:@selector(ballAudioReset:) interval:0 repeat:0 delay:0.5];
//			[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(ballAudioReset:) userInfo:nil repeats:NO];
		}
	}
}
-(void)gameCrowdAudio{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	
	if([mySingleton audioOn]==YES){
		
	int r = arc4random() % 2;
	if(r==1){
		[[SimpleAudioEngine sharedEngine] playEffect:@"ambience.mp3" pitch:1 pan:0.5 gain:0.15];
	}else{
		[[SimpleAudioEngine sharedEngine] playEffect:@"ambience2.mp3" pitch:1 pan:0.5 gain:0.15];
	}
	}
}
-(void)crowdGoalAudio{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
			[[SimpleAudioEngine sharedEngine] playEffect:@"crowd.mp3" pitch:1 pan:0.5 gain:0.3];
	}
}

-(void) ballAudioReset:(ccTime *)timer{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	[mySingleton setBallAudio:NO];
	
}
-(void)whistleAudio{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		[[SimpleAudioEngine sharedEngine] playEffect:@"whistle.mp3"];
	}
}


-(void)golScored:(int)team{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	[mySingleton setGolAnimationPlaying:YES];
	[mySingleton setMaxExecutionTime:0];
	
	if(team==1){
		[mySingleton setCurrentScoreP1:[mySingleton currentScoreP1]+1];
		[myCupSingleton setCurrentScoreP1:[mySingleton currentScoreP1]];
		[p1Score setString:[NSString stringWithFormat:@"%d", [mySingleton currentScoreP1]]];
	}else{
		[mySingleton setCurrentScoreP2:[mySingleton currentScoreP2]+1];
		[myCupSingleton setCurrentScoreP2:[mySingleton currentScoreP2]];
		[p2Score setString:[NSString stringWithFormat:@"%d", [mySingleton currentScoreP2]]];
	}
	[self whistleAudio];
	[self crowdGoalAudio];

	//NSLog(@"pos 3: %d",[mySingleton getFieldValueAtPos:3]);
	//NSLog(@"pos 2: %d",[mySingleton getFieldValueAtPos:2]);
	
	/// GOL ANIMATION
     
    CCSprite * golSprite = nil;
    CCAnimation * golAnim = nil;
    NSMutableArray *goalFrames = [NSMutableArray array];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            for(int j=1; j<=3; ++j){
                for(int i = 1; i <= 7; ++i) {
                    
                    CCSprite* mySprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"goal_anima%d-ipadhd.png", i]];
                    [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
                }
            }
            CCSprite* mySprite = [CCSprite spriteWithFile:@"goal_anima8-ipadhd.png"];
            [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
            golAnim = [CCAnimation animationWithSpriteFrames:goalFrames delay:1/8.0];
            golSprite = [CCSprite spriteWithFile:@"goal_anima8-ipadhd.png"];
        }else{
            //iPad screen
            for(int j=1; j<=3; ++j){
                for(int i = 1; i <= 7; ++i) {
                    
                    CCSprite* mySprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"goal_anima%d-ipad.png", i]];
                    [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
                }
            }
            CCSprite* mySprite = [CCSprite spriteWithFile:@"goal_anima8-ipad.png"];
            [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
            golAnim = [CCAnimation animationWithSpriteFrames:goalFrames delay:1/8.0];
            
            golSprite = [CCSprite spriteWithFile:@"goal_anima8-ipad.png"];
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                    //iphone 5
                    for(int j=1; j<=3; ++j){
                        for(int i = 1; i <= 7; ++i) {
                            
                            CCSprite* mySprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"goal_anima%d-hd.png", i]];
                            [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
                        }
                    }
                    CCSprite* mySprite = [CCSprite spriteWithFile:@"goal_anima8-hd.png"];
                    [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
                    golAnim = [CCAnimation animationWithSpriteFrames:goalFrames delay:1/8.0];
                    golSprite = [CCSprite spriteWithFile:@"goal_anima8-hd.png"];
               /* }else{
                    //iphone retina screen
                    bg = [CCSprite spriteWithFile:@"loading-hd.jpg"];
                    [self addChild:bg z:-10];
                }*/
            }else{
                //iphone screen
                for(int j=1; j<=3; ++j){
                    for(int i = 1; i <= 7; ++i) {
                        
                        CCSprite* mySprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"goal_anima%d.png", i]];
                        [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
                    }
                }
                CCSprite* mySprite = [CCSprite spriteWithFile:@"goal_anima8.png"];
                [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
                golAnim = [CCAnimation animationWithSpriteFrames:goalFrames delay:1/8.0];
                golSprite = [CCSprite spriteWithFile:@"goal_anima8.png"];
            }
        }
    }
    
	// Make the animation sequence
    golAnim.restoreOriginalFrame = NO;
	CCAnimate* golAction = [CCAnimate actionWithAnimation:golAnim];
    
	[golSprite setPosition:ADJUST_CCP(ccp(240, 160))];
	[self addChild:golSprite z:100 tag:100];

	// Make a sprite (defined elsewhere) actually perform the animation
	[golSprite runAction:golAction];
	[goalFrames removeAllObjects];
    
	
	[self unschedule: @selector(startPlayerTurn:)];
	[mySingleton setTimeToDelayAction:4];
	[self schedule: @selector(startDelayRestart:) interval: 1];
}

-(float)pointDistance:(cpVect)p1 point2:(cpVect)p2{
	float b = (p1.y-p2.y);
	float c = (p1.x-p2.x);
	int a = sqrt(b*b+c*c);
	return a;
}


-(cpVect)pointDistanceVector:(cpVect)p1 point2:(cpVect)p2 forceFactor:(int)forceFactor{
	float b = (p1.y-p2.y);
	float c = (p1.x-p2.x);
    float x = c*forceFactor*kSlide*teamFactor2*1.5;
    float y = b*forceFactor*kSlide*teamFactor2*1.5;
    x = x>14500.00f?14500.00f:x<-14500.00f?-14500.00f:x;
	return cpv(x,y);
}

-(void)aiPlayer:(cpVect)ballp p1:(cpVect)p1p p2:(cpVect)p2p p3:(cpVect)p3p p4:(cpVect)p4p p5:(cpVect)p5p p6:(cpVect)p6p{
	
	cpVect golProPos = cpv(60,153); 
	cpVect golConPos = cpv(420,153); 
	cpVect backByLeftFromOppField = cpv(240,0);
	cpVect backByLeftFromOwenField = cpv(480,0);
	cpVect backByRightFromOppField = cpv(240,320);
	cpVect backByRightFromOwenField = cpv(480,320);
	
	//STATE MACHINE
	BOOL p4Attack = p4p.x > ballp.x?YES:NO;
	
	BOOL p5Attack = p5p.x > ballp.x?YES:NO;
	
	BOOL p6Attack = p6p.x > ballp.x?YES:NO;
	
	cpVect ballpHitAttack;
	cpVect ballpHitDefense;
	ballpHitAttack = cpv(ballp.x,ballp.y+(ballp.y-153)/5);
	ballpHitDefense = cpv(ballp.x+20,ballp.y+(ballp.y-153)/5);
	
	//TARGET 
	int hitRelevanceP1=0;
	int hitRelevanceP2=0;
	int hitRelevanceP3=0;

	float p1BallDistance = [self pointDistance:ballp point2:p1p];
	float p2BallDistance = [self pointDistance:ballp point2:p2p];
	float p3BallDistance = [self pointDistance:ballp point2:p3p];

	float p1ConGolDistance = [self pointDistance:golConPos point2:p1p];
	float p2ConGolDistance = [self pointDistance:golConPos point2:p2p];
	float p3ConGolDistance = [self pointDistance:golConPos point2:p3p];

	float p1ProGolDistance = [self pointDistance:golProPos point2:p1p];
	float p2ProGolDistance = [self pointDistance:golProPos point2:p2p];
	float p3ProGolDistance = [self pointDistance:golProPos point2:p3p];

	if( p1BallDistance < p2BallDistance && p1BallDistance < p3BallDistance ){
		if( p1p.x < ballp.x ){
			hitRelevanceP1=hitRelevanceP1+4;
		}else{
			hitRelevanceP1=hitRelevanceP1+2;
		}	
	}
    
	if( p2BallDistance < p1BallDistance && p2BallDistance < p3BallDistance ){
		if(p2p.x<ballp.x){
			hitRelevanceP2=hitRelevanceP2+4;
		}else{
			hitRelevanceP2=hitRelevanceP2+2;
		}
	}
    
	if(p3BallDistance<p1BallDistance && p3BallDistance<p2BallDistance){
		if(p3p.x<ballp.x){
			hitRelevanceP3=hitRelevanceP3+4;
		}else{
			hitRelevanceP3=hitRelevanceP3+2;
		}
	}
	
	if(p1ProGolDistance<p2ProGolDistance && p1ProGolDistance<p3ProGolDistance && ballp.x<=240)hitRelevanceP1++;
	if(p2ProGolDistance<p1ProGolDistance && p2ProGolDistance<p3ProGolDistance && ballp.x<=240)hitRelevanceP2++;
	if(p3ProGolDistance<p1ProGolDistance && p3ProGolDistance<p2ProGolDistance && ballp.x<=240)hitRelevanceP3++;
	
	if(p1ConGolDistance<p2ConGolDistance && p1ConGolDistance<p3ConGolDistance  && ballp.x>240){
		hitRelevanceP1++;
		if(p1p.x<ballp.x)hitRelevanceP1++;
	}
	if(p2ConGolDistance<p1ConGolDistance && p2ConGolDistance<p3ConGolDistance  && ballp.x>240){
		hitRelevanceP2++;
		if(p2p.x<ballp.x)hitRelevanceP2++;
	}
	if(p3ConGolDistance<p1ConGolDistance && p3ConGolDistance<p2ConGolDistance  && ballp.x>240){
		hitRelevanceP3++;
		if(p3p.x<ballp.x)hitRelevanceP3++;
	}
	cpVect oppHitTarget; 
	if(hitRelevanceP1<=hitRelevanceP2 && hitRelevanceP1<=hitRelevanceP3)oppHitTarget = p1p;
	if(hitRelevanceP2<=hitRelevanceP1 && hitRelevanceP2<=hitRelevanceP3)oppHitTarget = p2p;
	if(hitRelevanceP3<=hitRelevanceP1 && hitRelevanceP3<=hitRelevanceP2)oppHitTarget = p3p;
	
	//ATTACK
	int attackRelevanceP4=0;
	int attackRelevanceP5=0;
	int attackRelevanceP6=0;
	
	float p4BallDistance = [self pointDistance:ballp point2:p4p];
	float p5BallDistance = [self pointDistance:ballp point2:p5p];
	float p6BallDistance = [self pointDistance:ballp point2:p6p];
	
	float p4ProGolDistance = [self pointDistance:golProPos point2:p4p];
	float p5ProGolDistance = [self pointDistance:golProPos point2:p5p];
	float p6ProGolDistance = [self pointDistance:golProPos point2:p6p];
	
	float p4ConGolDistance = [self pointDistance:golConPos point2:p4p];
	float p5ConGolDistance = [self pointDistance:golConPos point2:p5p];
	float p6ConGolDistance = [self pointDistance:golConPos point2:p6p];
	
	
	if(p4BallDistance<p5BallDistance && p4BallDistance<p6BallDistance){
		if(p4p.x>ballp.x){
			attackRelevanceP4=attackRelevanceP4+4;
		}else{
			attackRelevanceP4=attackRelevanceP4+2;
		}	
	}
	if(p5BallDistance<p4BallDistance && p5BallDistance<p6BallDistance){
		if(p5p.x<ballp.x){
			attackRelevanceP5=attackRelevanceP5+4;
		}else{
			attackRelevanceP5=attackRelevanceP5+2;
		}
	}
	if(p6BallDistance<p4BallDistance && p6BallDistance<p5BallDistance){
		if(p6p.x<ballp.x){
			attackRelevanceP6=attackRelevanceP6+4;
		}else{
			attackRelevanceP6=attackRelevanceP6+2;
		}
	}
	if(p4ProGolDistance<p5ProGolDistance && p4ProGolDistance<p6ProGolDistance && p4p.x>ballp.x)attackRelevanceP4++;
	if(p5ProGolDistance<p4ProGolDistance && p5ProGolDistance<p6ProGolDistance && p5p.x>ballp.x)attackRelevanceP5++;
	if(p6ProGolDistance<p4ProGolDistance && p6ProGolDistance<p5ProGolDistance && p6p.x>ballp.x)attackRelevanceP6++;
	
	if(p4ConGolDistance<p5ConGolDistance && p4ConGolDistance<p6ConGolDistance && p4p.x>ballp.x)attackRelevanceP4++;
	if(p5ConGolDistance<p4ConGolDistance && p5ConGolDistance<p6ConGolDistance && p4p.x>ballp.x)attackRelevanceP5++;
	if(p6ConGolDistance<p4ConGolDistance && p6ConGolDistance<p5ConGolDistance && p4p.x>ballp.x)attackRelevanceP6++;
	
	
	//all defending set closest to strike ball
 	if(p4Attack == NO && p5Attack == NO && p6Attack == NO ){
		if(p4BallDistance<p5BallDistance && p4BallDistance<p6BallDistance)attackRelevanceP4++;
		if(p5BallDistance<p4BallDistance && p5BallDistance<p6BallDistance)attackRelevanceP5++;
		if(p6BallDistance<p4BallDistance && p6BallDistance<p5BallDistance)attackRelevanceP6++;
	}
	
	cpVect aiP4ForceVector;
	cpVect aiP5ForceVector;
	cpVect aiP6ForceVector;
	
	if(p4Attack==YES){
		// attack 
		if(attackRelevanceP4>attackRelevanceP5 && attackRelevanceP4>attackRelevanceP6){
			aiP4ForceVector = [self pointDistanceVector:cpv(ballpHitAttack.x+1,ballpHitAttack.y+1) point2:p4p forceFactor:85];
		}else{
			int r = arc4random() % 4;
			if(r==0){
				aiP4ForceVector = [self pointDistanceVector:oppHitTarget point2:p4p forceFactor:55];
			}else{
				aiP4ForceVector = [self pointDistanceVector:ballpHitAttack point2:p4p forceFactor:55];
			}
		}
	}else{
		//on the gol line, back by the sides
		if(p4p.y>70 && p4p.y<227){
			if(p4p.y<217){
				int r = arc4random() % 3;
				if(r==0){
					aiP4ForceVector = [self pointDistanceVector:oppHitTarget point2:p4p forceFactor:65];
				}else{
					aiP4ForceVector = p4p.x<240?[self pointDistanceVector:backByLeftFromOppField point2:p4p forceFactor:65]:[self pointDistanceVector:backByLeftFromOwenField point2:p4p forceFactor:45];
				}	
			}else{
				aiP4ForceVector = p4p.x>=240?[self pointDistanceVector:backByRightFromOppField point2:p4p forceFactor:65]:[self pointDistanceVector:backByRightFromOwenField point2:p4p forceFactor:45];
			}
		}else{
			aiP4ForceVector = [self pointDistanceVector:golConPos point2:p4p forceFactor:45];
		}
	}
	
	if(p5Attack==YES){
		// attack 
		if(attackRelevanceP5>attackRelevanceP4 && attackRelevanceP5>attackRelevanceP6){
			aiP5ForceVector = [self pointDistanceVector:cpv(ballpHitAttack.x+1,ballpHitAttack.y+1) point2:p5p forceFactor:85];
		}else{
			int r = arc4random() % 4;
			if(r==0){
				aiP5ForceVector = [self pointDistanceVector:oppHitTarget point2:p5p forceFactor:55];
			}else{
				aiP5ForceVector = [self pointDistanceVector:ballpHitAttack point2:p5p forceFactor:55];
			}
		}
	}else{
		//on the gol line, back by the sides
		if(p5p.y>70 && p5p.y<227){
			if(p5p.y<217){
				aiP5ForceVector = p5p.x<240?[self pointDistanceVector:backByLeftFromOppField point2:p5p forceFactor:45]:[self pointDistanceVector:backByLeftFromOwenField point2:p5p forceFactor:45];
			}else{
				aiP5ForceVector = p5p.x>=240?[self pointDistanceVector:backByRightFromOppField point2:p5p forceFactor:45]:[self pointDistanceVector:backByRightFromOwenField point2:p5p forceFactor:45];
			}
		}else{
			aiP5ForceVector = [self pointDistanceVector:golConPos point2:p5p forceFactor:45];
		}
	}
	if(p6Attack==YES){
		// attack 
		if(attackRelevanceP6>attackRelevanceP4 && attackRelevanceP6>attackRelevanceP5){
			aiP6ForceVector = [self pointDistanceVector:cpv(ballpHitAttack.x+1,ballpHitAttack.y+1) point2:p6p forceFactor:85];
		}else{
			int r = arc4random() % 4;
			if(r==0){
				aiP6ForceVector = [self pointDistanceVector:oppHitTarget point2:p6p forceFactor:55];
			}else{
				aiP6ForceVector = [self pointDistanceVector:ballpHitAttack point2:p6p forceFactor:55];
				
			}
		}	
	}else{
		//on the gol line, back by the sides
		if(p6p.y>70 && p6p.y<227){
			if(p6p.y<217){
				aiP6ForceVector = p6p.x<240?[self pointDistanceVector:backByLeftFromOppField point2:p6p forceFactor:45]:[self pointDistanceVector:backByLeftFromOwenField point2:p6p forceFactor:45];
			}else{
				aiP6ForceVector = p6p.x>=240?[self pointDistanceVector:backByRightFromOppField point2:p6p forceFactor:45]:[self pointDistanceVector:backByRightFromOwenField point2:p6p forceFactor:45];
			}
		}else{
			aiP6ForceVector = [self pointDistanceVector:golConPos point2:p6p forceFactor:45];
		}
	}
	
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	[mySingleton setP4ForceVector:aiP4ForceVector];
	[mySingleton setP5ForceVector:aiP5ForceVector];
	[mySingleton setP6ForceVector:aiP6ForceVector];
	
}


-(void)clearBodyImpulseAndVectors{
	int steps = 10;
	cpFloat dt = 1.0/60.0; //(cpFloat)steps;
	
	for(int i=0; i<steps; i++){
		cpBodyUpdateVelocity(p1Body,space->gravity, space->damping, 0);
		cpBodyUpdateVelocity(p2Body,space->gravity, space->damping, 0);
		cpBodyUpdateVelocity(p3Body,space->gravity, space->damping, 0);
		cpBodyUpdateVelocity(p4Body,space->gravity, space->damping, 0);
		cpBodyUpdateVelocity(p5Body,space->gravity, space->damping, 0);
		cpBodyUpdateVelocity(p6Body,space->gravity, space->damping, 0);
		cpBodyUpdateVelocity(ballBody,space->gravity, space->damping, 0);		
		cpSpaceStep(space, dt);
	}
}

////////////////////////
//
//    MAIN LOOP
//
////////////////////////
int oldGameTurn = 10;
-(void)tick:(ccTime)dt{
    
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton  gameTurn]!=oldGameTurn)NSLog(@"GAME TURN : %i",[mySingleton  gameTurn]);
    oldGameTurn = [mySingleton gameTurn];
    //reset match
	if([mySingleton  gameTurn]==0){
        NSLog(@"GAME TURN 0");
		[self whistleAudio];

		p1Body->p = cpv(kP1PositionX, kP1PositionY);
		p2Body->p = cpv(kP2PositionX, kP2PositionY);
		p3Body->p = cpv(kP3PositionX, kP3PositionY);
		p4Body->p = cpv(kP4PositionX, kP4PositionY);
		p5Body->p = cpv(kP5PositionX, kP5PositionY);
		p6Body->p = cpv(kP6PositionX, kP6PositionY);
		ballBody->p = cpv(kBallPositionX, kBallPositionY);
        //reset position
        cpBodyUpdatePosition(p1Body, 0);
        cpBodyUpdatePosition(p2Body, 0);
        cpBodyUpdatePosition(p3Body, 0);
        cpBodyUpdatePosition(p4Body, 0);
        cpBodyUpdatePosition(p5Body, 0);
        cpBodyUpdatePosition(p6Body, 0);
        cpBodyUpdatePosition(ballBody, 0);

		//cpSpaceHashEach(space->staticShapes, &eachShape, nil);
		[self clearBodyImpulseAndVectors];
		[mySingleton setP1Position:cpv(kP1PositionX, kP1PositionY)];
		[mySingleton setP2Position:cpv(kP2PositionX, kP2PositionY)];
		[mySingleton setP3Position:cpv(kP3PositionX, kP3PositionY)];
		[mySingleton setP4Position:cpv(kP4PositionX, kP4PositionY)];
		[mySingleton setP5Position:cpv(kP5PositionX, kP5PositionY)];
		[mySingleton setP6Position:cpv(kP6PositionX, kP6PositionY)];
		[mySingleton setBallPosition:cpv(kBallPositionX, kBallPositionY)];
		[mySingleton setP1ForceVector:cpv(0, 0)];
		[mySingleton setP2ForceVector:cpv(0, 0)];
		[mySingleton setP3ForceVector:cpv(0, 0)];
		[mySingleton setP4ForceVector:cpv(0, 0)];
		[mySingleton setP5ForceVector:cpv(0, 0)];
		[mySingleton setP6ForceVector:cpv(0, 0)];
		
		
		[mySingleton  setGameTurn:1];
		cpSpaceStep(space, 1.0f/60.0f);
        cpSpaceEachShape(space, (cpSpaceShapeIteratorFunc)eachShape, nil);

	//start player arrows
	}else if([mySingleton  gameTurn]==1){
        NSLog(@"GAME TURN 1");
        
        
		if(itsYourTurn==nil){
            [self destroyChipmunk];
            [self setupChipmunk];
            
            [(GameScene*)[self parent] setFieldColor:ccc3(255, 255, 255)];
            
            [self addSpriteGlowP1];
            [self addSpriteGlowP2];
            
            
            
            itsYourTurn = [CCSprite spriteWithSpriteFrameName:@"its_your_turn.png"];
            [itsYourTurn setPosition:ADJUST_CCP(ccp(240, 40))];
            [self addChild:itsYourTurn z:310 tag:310];
            [self schedule:@selector(removeSpriteItsYourTurn:) interval:0 repeat:0 delay:5.0];
            //[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(removeSpriteItsYourTurn:) userInfo:nil repeats:NO];
		}
		//NSLog(@"GAME TURN :%i",[mySingleton  gameTurn]);
		if(	[mySingleton gameType]==1 ){
			[mySingleton setTimePlayTurn:12];
			[self unschedule:@selector(tick:)];
			[self schedule: @selector(startPlayerTurn:) interval:1];
	
		}else if([mySingleton gameType]==2 || [mySingleton gameType]==22){
				[mySingleton setTimePlayTurn:6];
				[self unschedule:@selector(tick:)];
				[self schedule: @selector(startPlayerTurn:) interval:1];

		}else{
			[self aiPlayer:ballBody->p p1:p1Body->p p2:p2Body->p p3:p3Body->p p4:p4Body->p p5:p5Body->p p6:p6Body->p];
			[mySingleton setTimePlayTurn:6];
			[self unschedule:@selector(tick:)];
			[self schedule: @selector(startPlayerTurn:) interval:1];
		}	
		

		
			
	// set forces to apply and hide arrows
	}else if([mySingleton  gameTurn]==2){
        NSLog(@"GAME TURN 2");
        [(GameScene*)[self parent] setFieldColor:ccc3(235, 235, 235)];
        

		
		//NSLog(@"GAME TURN :%i  SYNCING FORCEVECTORS!!",[mySingleton  gameTurn]);
		[self clearBodyImpulseAndVectors];
		/*
		p1Body->p = [mySingleton p1Position];
		p2Body->p = [mySingleton p2Position];
		p3Body->p = [mySingleton p3Position];
		p4Body->p = [mySingleton p4Position];
		p5Body->p = [mySingleton p5Position];
		p6Body->p = [mySingleton p6Position];
		ballBody->p = [mySingleton ballPosition];
        NSLog(@"POSITION p1 : (%f,%f )",[mySingleton p1Position].x,[mySingleton p1Position].y);
        NSLog(@"POSITION p2 : (%f,%f )",[mySingleton p2Position].x,[mySingleton p2Position].y);
        NSLog(@"POSITION p3 : (%f,%f )",[mySingleton p3Position].x,[mySingleton p3Position].y);
        NSLog(@"POSITION p4 : (%f,%f )",[mySingleton p4Position].x,[mySingleton p4Position].y);
        NSLog(@"POSITION p5 : (%f,%f )",[mySingleton p5Position].x,[mySingleton p5Position].y);
        NSLog(@"POSITION p6 : (%f,%f )",[mySingleton p6Position].x,[mySingleton p6Position].y);
        
        
        */
        //reset position
        cpBodyUpdatePosition(p1Body, 0);
        cpBodyUpdatePosition(p2Body, 0);
        cpBodyUpdatePosition(p3Body, 0);
        cpBodyUpdatePosition(p4Body, 0);
        cpBodyUpdatePosition(p5Body, 0);
        cpBodyUpdatePosition(p6Body, 0);
        cpBodyUpdatePosition(ballBody, 0);
		cpSpaceStep(space, 1.0f/60.0f);
        cpSpaceEachShape(space, (cpSpaceShapeIteratorFunc)eachShape, nil);
        

		
		waitPlaying = [CCSprite spriteWithSpriteFrameName:@"wait_playing.png"];
		[waitPlaying setPosition:ADJUST_CCP(ccp(240, 40))];
		[self addChild:waitPlaying z:310 tag:310];
		
        
        /*
        cpBodyApplyImpulse(p1Body,ccp(12396.384766,-1442.238647),cpv(0,0));
        cpBodyApplyImpulse(p2Body,ccp(12478.428711,198.070297 ),cpv(0,0));
        cpBodyApplyImpulse(p3Body,ccp(8896.144531,8752.658203 ),cpv(0,0));
        cpBodyApplyImpulse(p4Body,ccp(-6600.000000,-5781.600098 ),cpv(0,0));
        cpBodyApplyImpulse(p5Body,ccp(-9240.000000,-39.599998 ),cpv(0,0));
        cpBodyApplyImpulse(p6Body,ccp(-6600.000000,5702.400391 ),cpv(0,0));
        */

        if([mySingleton gameType]!= 2 && [mySingleton gameType]!= 22){
            if([mySingleton p1ForceVector].x!=0 || [mySingleton p1ForceVector].y!=0 ){
                cpBodyApplyImpulse(p1Body,[mySingleton p1ForceVector],cpv(0,0) );
            }
            if([mySingleton p2ForceVector].x!=0 || [mySingleton p2ForceVector].y!=0 ){
                cpBodyApplyImpulse(p2Body,[mySingleton p2ForceVector],cpv(0,0) );
            }
            if([mySingleton p3ForceVector].x!=0 || [mySingleton p3ForceVector].y!=0 ){
                cpBodyApplyImpulse(p3Body,[mySingleton p3ForceVector],cpv(0,0) );
            }
            if([mySingleton p4ForceVector].x!=0 || [mySingleton p4ForceVector].y!=0 ){
                cpBodyApplyImpulse(p4Body,[mySingleton p4ForceVector],cpv(0,0) );
            }
            if([mySingleton p5ForceVector].x!=0 || [mySingleton p5ForceVector].y!=0 ){
                cpBodyApplyImpulse(p5Body,[mySingleton p5ForceVector],cpv(0,0) );
            }
            if([mySingleton p6ForceVector].x!=0 || [mySingleton p6ForceVector].y!=0 ){
                cpBodyApplyImpulse(p6Body,[mySingleton p6ForceVector],cpv(0,0) );
            }
            [mySingleton  setGameTurn:3];
            
            [self schedule:@selector(removeSpriteWaitPlaying:) interval:0 repeat:0 delay:4.7];
            //[NSTimer scheduledTimerWithTimeInterval:4.7 target:self selector:@selector(removeSpriteWaitPlaying:) userInfo:nil repeats:NO];
        }else{
            NSLog(@"GAME TURN: %i", [mySingleton gameTurn]);
            if([mySingleton isP2] && [mySingleton hasSent2]==NO){
                NSLog(@"SENDING PLAYER 2");
                [(RootViewController*)[(AppDelegate*)[[UIApplication sharedApplication] delegate] navController] sendPlayer2GamePlay];
                
            }else{
                [(RootViewController*)[(AppDelegate*)[[UIApplication sharedApplication] delegate] navController] sendHeartBeat];
                
            }
            if([mySingleton isP1] && [mySingleton hasSent1]==NO && [mySingleton hasSynced2]==YES){
                NSLog(@"SENDING PLAYER 1 and 2");
                    [(RootViewController*)[(AppDelegate*)[[UIApplication sharedApplication] delegate] navController] sendServerFullGamePlay];
                
            }
            if([mySingleton hasSynced1]==YES && [mySingleton hasSynced2]==YES){
                NSLog(@"Everything is SYNC------------------------------------------------------");
                
                [mySingleton  setGameTurn:3];
                [mySingleton setHasSent1:NO];
                [mySingleton setHasSent2:NO];
                [mySingleton  setHasSynced1:NO];
                [mySingleton  setHasSynced2:NO];
                p1Body->p = [mySingleton p1Position];
                p2Body->p = [mySingleton p2Position];
                p3Body->p = [mySingleton p3Position];
                p4Body->p = [mySingleton p4Position];
                p5Body->p = [mySingleton p5Position];
                p6Body->p = [mySingleton p6Position];
                ballBody->p = [mySingleton ballPosition];
                //reset position
                cpBodyUpdatePosition(p1Body, 0);
                cpBodyUpdatePosition(p2Body, 0);
                cpBodyUpdatePosition(p3Body, 0);
                cpBodyUpdatePosition(p4Body, 0);
                cpBodyUpdatePosition(p5Body, 0);
                cpBodyUpdatePosition(p6Body, 0);
                cpBodyUpdatePosition(ballBody, 0);
                
                if([mySingleton p1ForceVector].x!=0 || [mySingleton p1ForceVector].y!=0 ){
                    cpBodyApplyImpulse(p1Body,[mySingleton p1ForceVector],cpv(0,0) );
                }
                if([mySingleton p2ForceVector].x!=0 || [mySingleton p2ForceVector].y!=0 ){
                    cpBodyApplyImpulse(p2Body,[mySingleton p2ForceVector],cpv(0,0) );
                }
                if([mySingleton p3ForceVector].x!=0 || [mySingleton p3ForceVector].y!=0 ){
                    cpBodyApplyImpulse(p3Body,[mySingleton p3ForceVector],cpv(0,0) );
                }
                if([mySingleton p4ForceVector].x!=0 || [mySingleton p4ForceVector].y!=0 ){
                    cpBodyApplyImpulse(p4Body,[mySingleton p4ForceVector],cpv(0,0) );
                }
                if([mySingleton p5ForceVector].x!=0 || [mySingleton p5ForceVector].y!=0 ){
                    cpBodyApplyImpulse(p5Body,[mySingleton p5ForceVector],cpv(0,0) );
                }
                if([mySingleton p6ForceVector].x!=0 || [mySingleton p6ForceVector].y!=0 ){
                    cpBodyApplyImpulse(p6Body,[mySingleton p6ForceVector],cpv(0,0) );
                }
                
                [self schedule:@selector(removeSpriteWaitPlaying:) interval:0 repeat:0 delay:4.7];
                //[NSTimer scheduledTimerWithTimeInterval:4.7 target:self selector:@selector(removeSpriteWaitPlaying:) userInfo:nil repeats:NO];
                
            }
            
        }
		
	// start turn execution
	}else if([mySingleton  gameTurn]==3){
        
        if([self cornerImpulse]!=0){
			if([self cornerImpulse]==1)cpBodyApplyImpulse(ballBody, cpv(1000,1000),cpv(0,0));  
			if([self cornerImpulse]==2)cpBodyApplyImpulse(ballBody, cpv(1000,-680),cpv(0,0));  
			if([self cornerImpulse]==3)cpBodyApplyImpulse(ballBody, cpv(-520,1000),cpv(0,0));  
			if([self cornerImpulse]==4)cpBodyApplyImpulse(ballBody, cpv(-520,-680),cpv(0,0));  
			[self setCornerImpulse:0];
		}
		if([self wallImpulse]!=0){
			//hit top
			if([self wallImpulse]==1)cpBodyApplyImpulse(ballBody, cpv(0,-1000),cpv(0,0));  
			//hit rightt
			if([self wallImpulse]==2)cpBodyApplyImpulse(ballBody, cpv(-1000,0),cpv(0,0));  
			//hit bottom
			if([self wallImpulse]==3)cpBodyApplyImpulse(ballBody, cpv(0,1000),cpv(0,0));  
			//hit left
			if([self wallImpulse]==4)cpBodyApplyImpulse(ballBody, cpv(1000,0),cpv(0,0));  
			[self setWallImpulse:0];
			
		}
		// get ready
		if([mySingleton maxExecutionTime]==160){
			getReady = [CCSprite spriteWithSpriteFrameName:@"get_ready.png"];
			[getReady setPosition:ADJUST_CCP(ccp(240, 40))];
			[self addChild:getReady z:310 tag:310];
            [self schedule:@selector(removeSpriteGetReady:) interval:0 repeat:0 delay:1];
            //[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(removeSpriteGetReady:) userInfo:nil repeats:NO];
		}

		if((ballSprite.position.x == [mySingleton ballPosition].x && ballSprite.position.y == [mySingleton ballPosition].y &&
		   p1Sprite.position.x == [mySingleton p1Position].x && p1Sprite.position.y == [mySingleton p1Position].y &&
		   p2Sprite.position.x == [mySingleton p2Position].x && p2Sprite.position.y == [mySingleton p2Position].y &&
		   p3Sprite.position.x == [mySingleton p3Position].x && p3Sprite.position.y == [mySingleton p3Position].y &&
		   p4Sprite.position.x == [mySingleton p4Position].x && p4Sprite.position.y == [mySingleton p4Position].y &&
		   p5Sprite.position.x == [mySingleton p5Position].x && p5Sprite.position.y == [mySingleton p5Position].y &&
		   p6Sprite.position.x == [mySingleton p6Position].x && p6Sprite.position.y == [mySingleton p6Position].y  && [mySingleton golAnimationPlaying]==NO) || (
		   [mySingleton maxExecutionTime]>=200 && [mySingleton golAnimationPlaying]==NO)){
			
			[mySingleton setCheckSleep:[mySingleton checkSleep]+1];
			
			if([mySingleton checkSleep]==4){
				[mySingleton setMaxExecutionTime:0];
				[mySingleton setCheckSleep:0];
				[mySingleton setP1Position:p1Body->p];
				[mySingleton setP2Position:p2Body->p];
				[mySingleton setP3Position:p3Body->p];
				[mySingleton setP4Position:p4Body->p];
				[mySingleton setP5Position:p5Body->p];
				[mySingleton setP6Position:p6Body->p];
				[mySingleton setBallPosition:ballBody->p];
				[mySingleton setP1ForceVector:cpv(0, 0)];
				[mySingleton setP2ForceVector:cpv(0, 0)];
				[mySingleton setP3ForceVector:cpv(0, 0)];
				[mySingleton setP4ForceVector:cpv(0, 0)];
				[mySingleton setP5ForceVector:cpv(0, 0)];
				[mySingleton setP6ForceVector:cpv(0, 0)];
				[mySingleton setGameTurn:1];
			}
			
		}
		[mySingleton setMaxExecutionTime:[mySingleton maxExecutionTime]+1];
		cpSpaceStep(space, 1.0f/60.0f);
        cpSpaceEachShape(space, (cpSpaceShapeIteratorFunc)eachShape, nil);

	}else if([mySingleton  gameTurn]==4){	
		cpSpaceStep(space, 1.0f/60.0f);
        cpSpaceEachShape(space, (cpSpaceShapeIteratorFunc)eachShape, nil);

	}
    [self ball3Danimation];
    
}

-(void)ball3Danimation{
    //BALL ROLLING
    float b = (ballBody->v.y);
    float c = (ballBody->v.x);
    int degreeAngle = atan2(b,c) * 180 / 3.14565656;
    //float degreesDiff =  ( degreeAngle+90 - ballSprite.rotation )*0.1+ballSprite.rotation;
    [ballSprite setRotation:-degreeAngle+90];
    
    //TODO implement 1 decimal to the animation
    // NSString* fnOldpy = [NSString stringWithFormat:@"%.1f", oldpy];
    //NSString* fnpy = [NSString stringWithFormat:@"%.1f", ballSprite.position.y];
    
    diffPosx = ((ballSprite.position.x - oldpx)*100 )+ diffPosx;
    diffPosy = ((ballSprite.position.y - oldpy)*100 )+ diffPosy;
    float diffMax = 60.0f;
   // NSLog(@"diff pos: %f", diffPosy);
    
    float diff = sqrtf(powf(diffPosx,2.0f)+powf(diffPosy,2.0f));
    
    //NSLog(@"FLOAT DIFF %f",diff);
    
    if( (diff > diffMax || diff < -diffMax) ){
        if (diff > fabs(diffMax)) {
      //      NSLog(@"DIFFERENCA: %i",(1*int(diff)));
            ballFrameNumber = ballFrameNumber+(1*int(diff)/60)<59?ballFrameNumber+(1*int(diff)/60):1+(1*int(diff)/60);
        }else{
            ballFrameNumber = ballFrameNumber+1<59?ballFrameNumber+1:0;
        }
        ballFrameNumber = ballFrameNumber<=59?ballFrameNumber:0;
        [ballSprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"ball_%04d.png", ballFrameNumber]]];
        diffPosy = 0.0f;
        diffPosx = 0.0f;
    }
    //NSLog(@"a CC_RADIANS:%f  body->a:%g",(float) CC_RADIANS_TO_DEGREES( -body->a ),(double)body->a);
    oldpy = ballSprite.position.y;
    oldpx = ballSprite.position.x;
    ballSpriteShadow.position = ballSprite.position;
    ballSpriteVolume.position = ballSprite.position;
}


-(void)startDelayRestart:(ccTime *)theTimer{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	//NSLog(@"startDelayRestart : %i",[mySingleton timeToDelayAction]);
	if([mySingleton timeToDelayAction]==4){
		[mySingleton  setGameTurn:4];
		[self unschedule: @selector(startPlayerTurn:)];
	}
	if([mySingleton timeToDelayAction]<=0){
		[mySingleton setMaxExecutionTime:0];
		[self removeChildByTag:100 cleanup:YES];
		[self unschedule: @selector(startDelayRestart:)];
		[mySingleton setTimeToDelayAction:4];
		[mySingleton setGolAnimationPlaying:NO];
		[mySingleton  setGameTurn:0];
	}
	[mySingleton setTimeToDelayAction:[mySingleton timeToDelayAction]-1];
}

-(void)startPlayerTurn:(ccTime *)theTimer{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    

    NSLog(@"startPlayerTurn : TIME PLAY TURN : %i",[mySingleton timePlayTurn]);
	NSLog(@"gameTurn 1 : %i gameType: %i  isP1: %i  isP2: %i ",[mySingleton timePlayTurn],[mySingleton gameType],[mySingleton  isP1],[mySingleton  isP2] );
	if(([mySingleton timePlayTurn]==6 && [mySingleton gameType]==0 ) || ( [mySingleton timePlayTurn] == 12 && [mySingleton gameType]==1 ) || ( [mySingleton timePlayTurn]==6 && ([mySingleton gameType]==2 || [mySingleton gameType]==22 ) && [mySingleton isP1]==YES ) || ([mySingleton timePlayTurn]==6 && [mySingleton gameType]==0 ) || ([mySingleton timePlayTurn]==6 && [mySingleton gameType]==3 )){
		if([mySingleton gameType]==1){
			[mySingleton setIsP1:YES];
			[mySingleton setIsP2:NO];
            
            //GLOW for VERSUS MODE
            [self addSpriteGlowP1];
            
            
		}
		//NSString*p1Turn = @"p1Turn";
        NSMutableArray *clockFrames = [NSMutableArray array];
        
		id clockAnim = [[[CCAnimation alloc] initWithSpriteFrames:clockFrames delay:1/4.0] autorelease];
		//id clockAnim = [CCAnimation animationWithName:p1Turn delay:1/2.0];

        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock1.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock1.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock2.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock2.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock3.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock3.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock4.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock4.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock5.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock5.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock6.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock6.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock7.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock7.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock8.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock8.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock9.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock9.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock10.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock10.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock11.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock11.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock12.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock12.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock13.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock13.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock14.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock14.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock15.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock15.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock16.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock16.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock17.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock17.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock18.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock18.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock19.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock19.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock20.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock20.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock21.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock21.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock22.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock22.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock23.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock23.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock24.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock24.png"] textureRect]];
        
		[p1SpriteDisplay setVisible:NO];
		
		// Make the animation sequence repeat forever
		id clockAction = [CCAnimate actionWithAnimation: clockAnim];
		//id repeating = [RepeatForever actionWithAction:myAction];
		
		clockTurnSprite = [CCSprite spriteWithSpriteFrameName:@"clock1.png"];
		
		[clockTurnSprite setPosition:ADJUST_CCP(ccp(180, 298))];
		[self addChild:clockTurnSprite z:200 tag:210];
		
		// Make a sprite (defined elsewhere) actually perform the animation
		[clockTurnSprite runAction:clockAction];
		[clockFrames removeAllObjects];
	}else if(([mySingleton timePlayTurn]==6 && [mySingleton gameType]==1) || ([mySingleton timePlayTurn]==6 && ([mySingleton gameType]==2 || [mySingleton gameType]==22 ) && [mySingleton isP2]==YES)){
        
        [clockTurnSprite stopAllActions];
		[self removeChild:clockTurnSprite cleanup:YES];
        clockTurnSprite = nil;
		
		if([mySingleton gameType]==1){
			[mySingleton setIsP1:NO];
			[mySingleton setIsP2:YES];
            CCSprite*arrowSelected;
            arrowSelected = (CCSprite *)[p1Sprite getChildByTag:201];
            [arrowSelected setScaleX:0.1];
            arrowSelected = (CCSprite *)[p2Sprite getChildByTag:201];
            [arrowSelected setScaleX:0.1];
            arrowSelected = (CCSprite *)[p3Sprite getChildByTag:201];
            [arrowSelected setScaleX:0.1];
            
            //GLOW for VERSUS MODE
            [self addSpriteGlowP2];

            [self schedule:@selector(removeSpriteItsYourTurn:) interval:0 repeat:0 delay:5];
            //[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(removeSpriteItsYourTurn:) userInfo:nil repeats:NO];
		}
		//NSString*p2Turn = @"p2Turn";
        NSMutableArray *clockFrames = [NSMutableArray array];
        
		id clockAnim = [[[CCAnimation alloc] initWithSpriteFrames:clockFrames delay:1/4.0] autorelease];
		[clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock1.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock1.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock2.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock2.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock3.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock3.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock4.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock4.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock5.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock5.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock6.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock6.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock7.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock7.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock8.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock8.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock9.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock9.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock10.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock10.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock11.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock11.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock12.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock12.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock13.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock13.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock14.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock14.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock15.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock15.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock16.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock16.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock17.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock17.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock18.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock18.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock19.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock19.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock20.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock20.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock21.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock21.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock22.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock22.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock23.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock23.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:[(CCSprite*)[CCSprite spriteWithSpriteFrameName:@"clock24.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock24.png"] textureRect]];
        [p2SpriteDisplay setVisible:NO];
		
		// Make the animation sequence repeat forever
		id clockAction = [CCAnimate actionWithAnimation: clockAnim];
		//id repeating = [RepeatForever actionWithAction:myAction];
		
		clockTurnSprite = [CCSprite spriteWithSpriteFrameName:@"clock1.png"];
		
		[clockTurnSprite setPosition:ADJUST_CCP(ccp(301, 298))];
		[self addChild:clockTurnSprite z:200 tag:210];
		
		// Make a sprite (defined elsewhere) actually perform the animation
		[clockTurnSprite runAction:clockAction];
        [clockFrames removeAllObjects];
	}
	
	if([mySingleton timePlayTurn]>0){
		
		[mySingleton setTimePlayTurn:[mySingleton timePlayTurn] - 1];
		
	}else{
		
		if([mySingleton gameType]==1){
			[mySingleton setIsP1:NO];
			[mySingleton setIsP2:NO];
		}
		
		[clockTurnSprite stopAllActions];
		[self removeChild:clockTurnSprite cleanup:YES];
        clockTurnSprite = nil;
		//Remove arrows
        CCSprite*arrowSelected;
		arrowSelected = (CCSprite *)[p1Sprite getChildByTag:201];
		[arrowSelected setScaleX:0.1];
		arrowSelected = (CCSprite *)[p2Sprite getChildByTag:201];
		[arrowSelected setScaleX:0.1];
		arrowSelected = (CCSprite *)[p3Sprite getChildByTag:201];
		[arrowSelected setScaleX:0.1];
		arrowSelected = (CCSprite *)[p4Sprite getChildByTag:201];
		[arrowSelected setScaleX:0.1];
		arrowSelected = (CCSprite *)[p5Sprite getChildByTag:201];
		[arrowSelected setScaleX:0.1];
		arrowSelected = (CCSprite *)[p6Sprite getChildByTag:201];
		[arrowSelected setScaleX:0.1];
		
		
		if(([mySingleton gameType]==2 || [mySingleton gameType]==22 ) ){

				[mySingleton  setGameTurn:2];
				[self schedule: @selector(tick:) interval: 1.0f/60.0f];
				[p1SpriteDisplay setVisible:YES];
                [p2SpriteDisplay setVisible:YES];
                [self unschedule: @selector(startPlayerTurn:)];
		}else{
			[mySingleton  setGameTurn:2];
			[self schedule: @selector(tick:) interval: 1.0f/60.0f];
            [p1SpriteDisplay setVisible:YES];
            [p2SpriteDisplay setVisible:YES];
			[self unschedule: @selector(startPlayerTurn:)];
		}
		
	}
}


-(void)updateTimer:(ccTime *)theTimer{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
    //AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	if([mySingleton timeLeft]==30 || [mySingleton timeLeft]==50 || [mySingleton timeLeft]==200 || [mySingleton timeLeft]==300 || [mySingleton timeLeft]==400  || [mySingleton timeLeft]==500  || [mySingleton timeLeft]==600){
		[self gameCrowdAudio];
	
	}
	if([mySingleton timeLeft]==2){
		[self whistleAudio];
        [[GlobalSingleton sharedInstance] setGameEnd:YES];
	}
	
	if([mySingleton timeLeft]>0){
			[mySingleton setTimeLeft: [mySingleton timeLeft] - 1];
			int totalSeconds = [[NSNumber numberWithFloat:[mySingleton timeLeft]] intValue];
			//NSLog(@"%d",totalSeconds);
			int minutes = totalSeconds/60;
			int seconds = totalSeconds%60;
			NSString* minutesString;
			NSString* secondsString;
			if(minutes<=9){
				minutesString = [NSString stringWithFormat:@"0%d",minutes];
			}else{
				minutesString = [NSString stringWithFormat:@"%d",minutes];
			}
			if(seconds<=9){
				secondsString = [NSString stringWithFormat:@"0%d",seconds];
			}else{
				secondsString = [NSString stringWithFormat:@"%d",seconds];
			}
        
        if(oldMinutes != minutes){
			
            labelTimerMinutes.scale = 0.01;
            id rot1 = [CCScaleTo actionWithDuration:0.03f scale:1.2f]; 
            id rot2 = [CCScaleTo actionWithDuration:0.07f scale:1.0f]; 
            id seq = [CCSequence actions:rot1, rot2, nil];
            [labelTimerMinutes runAction:seq];
            [labelTimerMinutes setString:minutesString];
        }
		
        if(oldSeconds != seconds){
            labelTimerSeconds.scale = 0.01;
            id rot11 = [CCScaleTo actionWithDuration:0.03f scale:1.2f]; 
            id rot21 = [CCScaleTo actionWithDuration:0.07f scale:1.0f]; 
            id seq1 = [CCSequence actions:rot11, rot21, nil];
            [labelTimerSeconds runAction:seq1];

            [labelTimerSeconds setString:secondsString];
        }
        
        oldMinutes = minutes;
        oldSeconds = seconds;
        
    }else if([mySingleton timeLeft]==0 && [mySingleton golAnimationPlaying]==YES){
			
    }else if([mySingleton timeLeft]==0 && [mySingleton golAnimationPlaying]==NO){
			[self unschedule: @selector(updateTimer:)];
			[self unschedule: @selector(startPlayerTurn:)];
			[self unschedule: @selector(startDelayRestart:)];
			[self unschedule: @selector(tick:)];
			//NSLog(@"DEFAULT TIME LEFT : %i",(int)[myCupSingleton defaultTimeLeft]);
			if((int)[myCupSingleton defaultTimeLeft]==300){
				[mySingleton setLeaderboardPlayTime:1];
			}else if((int)[myCupSingleton defaultTimeLeft]==480){
				[mySingleton setLeaderboardPlayTime:2];
			}else if((int)[myCupSingleton defaultTimeLeft]==600){
				[mySingleton setLeaderboardPlayTime:3];
			}else{
				[mySingleton setLeaderboardPlayTime:1];
			}
        
        
        [mySingleton setLeaderboardAddicted:@"309353"];
       /* [OFHighScoreService getPageWithLoggedInUserForLeaderboard:@"309353"  onSuccessInvocation:[OFInvocation invocationForTarget:appDelegate selector:@selector(_leaderboardAddicted:)]  onFailureInvocation:[OFInvocation invocationForTarget:appDelegate selector:@selector(onFailure:)]];*/

			if([mySingleton gameType]==0){
				//GAME TYPE 0 worldcup
				//FASE 1
				[myCupSingleton setChampGoalsConceded:[myCupSingleton champGoalsConceded]+[mySingleton currentScoreP2]];
				if([myCupSingleton atualMatch]<=2){
					if([mySingleton currentScoreP1] > [mySingleton currentScoreP2]){
						
						//winner
					}else if([mySingleton currentScoreP1] < [mySingleton currentScoreP2]){
						//loser	
						[myCupSingleton setSevenInARow:1];
					}else{
						//draw
						[myCupSingleton setSevenInARow:1];
					}
					if([myCupSingleton atualMatch]==2 && [myCupSingleton sevenInARow]==0){
						//[OFAchievementService updateAchievement:@"243024" andPercentComplete:100.0 andShowNotification:YES];
					}
                    //Remove banner
                    AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                    [(RootViewController*)[app navController] removeBanner];
                    
                    
					[[CCDirector sharedDirector] replaceScene:[AfterGameMatchsScene node]];
					
				//FASE 2 - tournament
				}else if([myCupSingleton atualMatch]>2){
					if([mySingleton currentScoreP1] > [mySingleton currentScoreP2]){
						//winner
						if([myCupSingleton champGoalsConceded]){
							// zero goals conceded firstround
							//[OFAchievementService updateAchievement:@"243014" andPercentComplete:100.0 andShowNotification:YES];
                            if([myCupSingleton atualMatch]==6){
								// zero goals conceded
								//[OFAchievementService updateAchievement:@"243014" andPercentComplete:100.0 andShowNotification:YES];
							}
						}
						if([myCupSingleton atualMatch]==6 && [myCupSingleton sevenInARow]==0){
							//[OFAchievementService updateAchievement:@"243074" andPercentComplete:100.0f andShowNotification:YES];
                            
						}
                        //Remove banner
                        AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                        [(RootViewController*)[app navController] removeBanner];
                        
						
						[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[MatchWinnerScene node]]];
					}else if([mySingleton currentScoreP1] < [mySingleton currentScoreP2]){
						//loser
                        //Remove banner
                        AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                        [(RootViewController*)[app navController] removeBanner];
                        
						
						[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[MatchLoserScene node]]];
						[myCupSingleton setSevenInARow:1];
					}else{
						//draw
                        //Remove banner
                        AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                        [(RootViewController*)[app navController] removeBanner];
                        
						
						[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[MatchDrawScene node]]];
						[myCupSingleton setSevenInARow:1];
					}
				}
			}else{
				//GAME TYPE 3 single
				//GAME TYPE 1 versus
				//GAME TYPE 5 penalty - not here check PenaltyBallLayer 
				//GAME TYPE 2 Multiplayer
                
                //GAME SINGLE MATCH
				if([mySingleton gameType]==3){
                    [mySingleton setLeaderboardGoalScored:@"233903"];
                    [mySingleton setLeaderboardGoalConceded:@"233913"];
                    [mySingleton setLeaderboardGoalDifference:@"309283"];

				}
                
                //GAME MULTIPLAYER
				if(([mySingleton gameType]==2 || [mySingleton gameType]==22 )){
                    [mySingleton setLeaderboardGoalScored:@"233783"];
                    [mySingleton setLeaderboardGoalConceded:@"233793"];
                    [mySingleton setLeaderboardGoalDifference:@"309293"];

				}
				if([mySingleton currentScoreP1] > [mySingleton currentScoreP2]){
					//winner
                    //GAME SINGLE MATCH
                    
					if([mySingleton gameType]==3){
						[mySingleton setLeaderboardMatchWinner:1];
                        [mySingleton setLeaderboardPoints:@"233863"];
                        [mySingleton setLeaderboardWins:@"233873"];

                        
					}
                    //GAME MULTIPLAYER
					if(([mySingleton gameType]==2 || [mySingleton gameType]==22 ) && [mySingleton isP1]){
						[mySingleton setLeaderboardMatchWinner:1];
                        [mySingleton setLeaderboardPoints:@"233813"];
                        [mySingleton setLeaderboardWins:@"233833"];

                    }else if(([mySingleton gameType]==2 || [mySingleton gameType]==22 )  && [mySingleton isP2]){
                        [mySingleton setLeaderboardMatchWinner:3];
                        [mySingleton setLeaderboardPoints:@"233813"];
                        [mySingleton setLeaderboardLoss:@"233843"];
                    }
                    //GAME VERSUS
					if([mySingleton gameType]==1){
						[mySingleton setLeaderboardMatchWinner:1];
						[mySingleton setLeaderboardPoints:@"69153"];

					}
					//point
                    //Remove banner
                    AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                    [(RootViewController*)[app navController] removeBanner];
                    if([mySingleton gameType]==2){
                        [[[GameCenterManager sharedManager] multiplayerObject] disconnectLocalPlayerFromMatch];
                    }
                    if([mySingleton gameType]==22){
                        //TODO DISCONNECT BLUETOOTH
                        [[PeerServiceManager sharedInstance] disconnect];
                    }
                    
                    if([mySingleton gameType]!=2 || (([mySingleton gameType]==2 || [mySingleton gameType]==22 ) && [mySingleton isP1])){
                        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[MatchWinnerScene node]]];
                    }else if(([mySingleton gameType]==2 || [mySingleton gameType]==22 ) && [mySingleton isP2]){
                        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[MatchLoserScene node]]];
                    }
				}else if([mySingleton currentScoreP1]< [mySingleton currentScoreP2]){
					//loser	
					//single match leaderboard
					if([mySingleton gameType]==3){
						[mySingleton setLeaderboardMatchWinner:3];
                        [mySingleton setLeaderboardPoints:@"233863"];
                        [mySingleton setLeaderboardLoss:@"233883"];
						
					}
                    //GAME MULTIPLAYER
                    if(([mySingleton gameType]==2 || [mySingleton gameType]==22 ) && [mySingleton isP1]){
                        [mySingleton setLeaderboardMatchWinner:3];
                        [mySingleton setLeaderboardPoints:@"233813"];
                        [mySingleton setLeaderboardLoss:@"233843"];
                    }else if(([mySingleton gameType]==2 || [mySingleton gameType]==22 )  && [mySingleton isP2]){
                        [mySingleton setLeaderboardMatchWinner:1];
                        [mySingleton setLeaderboardPoints:@"233813"];
                        [mySingleton setLeaderboardWins:@"233833"];
                    }
                    //GAME VERSUS
                    if([mySingleton gameType]==1){
						[mySingleton setLeaderboardMatchWinner:3];
                        [mySingleton setLeaderboardPoints:@"69153"];
                        
					}
					//point
                    
                    //Remove banner
                    AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                    [(RootViewController*)[app navController] removeBanner];
                    if([[GlobalSingleton sharedInstance] gameType]==2){
                        [[[GameCenterManager sharedManager] multiplayerObject] disconnectLocalPlayerFromMatch];
                    }
                    if([mySingleton gameType]==22){
                        //TODO DISCONNECT BLUETOOTH
                        [[PeerServiceManager sharedInstance] disconnect];

                    }
                    if([mySingleton gameType]!=2 || (([mySingleton gameType]==2 || [mySingleton gameType]==22 ) && [mySingleton isP1])){
                        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[MatchLoserScene node]]];
                    }else if(([mySingleton gameType]==2 || [mySingleton gameType]==22 ) && [mySingleton isP2]){
                        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[MatchWinnerScene node]]];
                    }
				}else{
					//DRAW
					//single match leaderboard
					if([mySingleton gameType]==3){
						[mySingleton setLeaderboardMatchWinner:2];
                        [mySingleton setLeaderboardPoints:@"233863"];
                        [mySingleton setLeaderboardDraws:@"233893"];
						
					}
                    //GAME MULTIPLAYER
					if([mySingleton gameType]==2){
						[mySingleton setLeaderboardMatchWinner:2];
                        [mySingleton setLeaderboardPoints:@"233813"];
                        [mySingleton setLeaderboardDraws:@"233853"];
					}
                    //GAME VERSUS
					if([mySingleton gameType]==1){
						[mySingleton setLeaderboardMatchWinner:2];
                        [mySingleton setLeaderboardPoints:@"69153"];
					}
					//point
                    
                    //Remove banner
                    AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                    [(RootViewController*)[app navController] removeBanner];
                    if([[GlobalSingleton sharedInstance] gameType]==2){
                        [[[GameCenterManager sharedManager] multiplayerObject] disconnectLocalPlayerFromMatch];
                    }
                    if([mySingleton gameType]==22){
                        //TODO DISCONNECT BLUETOOTH
                        [[PeerServiceManager sharedInstance] disconnect];

                    }
					[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[MatchDrawSceneSingle node]]];
                    
				}
				
				//UNLOCK ACHIEVIMENTS
                //gkHelper = [GameKitHelper sharedGameKitHelper];
			/*
                switch ([mySingleton currentScoreP1]){
					case 1:
                   
						//[OFAchievementService updateAchievement:@"122974" andPercentComplete:100.0f andShowNotification:YES];
                        [gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:YES];
						break;
					case 2:
						[gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"2" percentComplete:100 showNotification:YES];
						break;
					case 3:
						[gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"2" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"3" percentComplete:100 showNotification:YES];
						break;
					case 4:
						[gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"2" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"3" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"4" percentComplete:100 showNotification:YES];
						break;
					case 5:
						[gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"2" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"3" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"4" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"5" percentComplete:100 showNotification:YES];
                        break;
					case 6:
						[gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"2" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"3" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"4" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"5" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"6" percentComplete:100 showNotification:YES];
						break;
					case 7:
						[gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"2" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"3" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"4" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"5" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"6" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"7" percentComplete:100 showNotification:YES];
						break;
					case 8:
						[gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"2" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"3" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"4" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"5" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"6" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"7" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"8" percentComplete:100 showNotification:YES];
						break;
					case 9:
						[gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"2" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"3" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"4" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"5" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"6" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"7" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"8" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"9" percentComplete:100 showNotification:YES];
						break;
					case 10:
						[gkHelper reportAchievementWithID:@"1" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"2" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"3" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"4" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"5" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"6" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"7" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"8" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"9" percentComplete:100 showNotification:NO];
                        [gkHelper reportAchievementWithID:@"10" percentComplete:100 showNotification:YES];
						break;						
				}
             */
         
			}
		}
}



- (BOOL)canReceiveCallbacksNow
{
    return YES;
} 




CCLabelAtlas *label;
#define kFilterFactor 0.9



///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Touch Events ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	float kPowFactor = 13000 * kSlide;
	if([mySingleton  gameTurn]==1){
		UITouch *touch = [touches anyObject];
		CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
		//NSLog(@"-------------------------------------------------------------- Clicked moved [%i]: %f , %f",[mySingleton currentSelectedP1],location.x,location.y);
		location = ccp(location.x, location.y);
		[touchSprite setPosition:location];
        
		CCSprite* arrowSelected;
		if ([mySingleton currentSelectedP1]==1 && [mySingleton  gameTurn]==1) {
			float b = (location.y-p1Sprite.position.y);
			float c = (location.x-p1Sprite.position.x);
			int degreeAngle = atan2(b,c)*180/ 3.14565656;
			int a = sqrt(b*b+c*c);
			CGFloat scaleSize = round(10/5.9*a)/100;
			arrowSelected = (CCSprite *)[p1Sprite getChildByTag:201];
			[arrowSelected setRotation:-degreeAngle  ];
			if(scaleSize>1.2){
				scaleSize = 1.2;
			}
			[arrowSelected setScaleX:scaleSize];
			float d = scaleSize*cos(-atan2(b,c));
			float e = -scaleSize*sin(-atan2(b,c));
			if(scaleSize>0.25)[mySingleton setP1ForceVector:cpv(d*kPowFactor*teamFactor1,e*kPowFactor*teamFactor1)];
		}else if([mySingleton currentSelectedP1]==2 && [mySingleton  gameTurn]==1) {
			int b = (location.y-p2Sprite.position.y);
			int c = (location.x-p2Sprite.position.x);
			int degreeAngle = atan2(b,c)*180/ 3.14565656;
			int a = sqrt(b*b+c*c);
			CGFloat scaleSize = round(10/5.9*a)/100;
			arrowSelected = (CCSprite *)[p2Sprite getChildByTag:201];
			[arrowSelected setRotation:-degreeAngle  ];
			if(scaleSize>1.2){
				scaleSize = 1.2;
			}
			[arrowSelected setScaleX:scaleSize];
			float d = scaleSize*cos(-atan2(b,c));
			float e = -scaleSize*sin(-atan2(b,c));
			if(scaleSize>0.25)[mySingleton setP2ForceVector:cpv(d*kPowFactor*teamFactor1,e*kPowFactor*teamFactor1)];
			
		}else if([mySingleton currentSelectedP1]==3 && [mySingleton  gameTurn]==1) {
			int b = (location.y-p3Sprite.position.y);
			int c = (location.x-p3Sprite.position.x);
			int degreeAngle = atan2(b,c)*180/ 3.14565656;
			int a = sqrt(b*b+c*c);
			CGFloat scaleSize = round(10/5.9*a)/100;
			arrowSelected = (CCSprite *)[p3Sprite getChildByTag:201];
			[arrowSelected setRotation:-degreeAngle  ];
			if(scaleSize>1.2){
				scaleSize = 1.2;
			}
			[arrowSelected setScaleX:scaleSize];
			float d = scaleSize*cos(-atan2(b,c));
			float e = -scaleSize*sin(-atan2(b,c));
			if(scaleSize>0.25)[mySingleton setP3ForceVector:cpv(d*kPowFactor*teamFactor1,e*kPowFactor*teamFactor1)];

		}else if([mySingleton currentSelectedP2]==4 && [mySingleton  gameTurn]==1) {
			int b = (location.y-p4Sprite.position.y);
			int c = (location.x-p4Sprite.position.x);
			int degreeAngle = atan2(b,c)*180/ 3.14565656;
			int a = sqrt(b*b+c*c);
			CGFloat scaleSize = round(10/5.9*a)/100;
			arrowSelected = (CCSprite *)[p4Sprite getChildByTag:201];
			[arrowSelected setRotation:-degreeAngle  ];
			if(scaleSize>1.2){
				scaleSize = 1.2;
			}
			[arrowSelected setScaleX:scaleSize];
			float d = scaleSize*cos(-atan2(b,c));
			float e = -scaleSize*sin(-atan2(b,c));
			if(scaleSize>0.25)[mySingleton setP4ForceVector:cpv(d*kPowFactor*teamFactor2,e*kPowFactor*teamFactor2)];

		}else if([mySingleton currentSelectedP2]==5 && [mySingleton  gameTurn]==1) {
			int b = (location.y-p5Sprite.position.y);
			int c = (location.x-p5Sprite.position.x);
			int degreeAngle = atan2(b,c)*180/ 3.14565656;
			int a = sqrt(b*b+c*c);
			CGFloat scaleSize = round(10/5.9*a)/100;
			arrowSelected = (CCSprite *)[p5Sprite getChildByTag:201];
			[arrowSelected setRotation:-degreeAngle  ];
			if(scaleSize>1.2){
				scaleSize = 1.2;
			}
			[arrowSelected setScaleX:scaleSize];
			float d = scaleSize*cos(-atan2(b,c));
			float e = -scaleSize*sin(-atan2(b,c));
			if(scaleSize>0.25)[mySingleton setP5ForceVector:cpv(d*kPowFactor*teamFactor2,e*kPowFactor*teamFactor2)];
			
		}else if([mySingleton currentSelectedP2]==6 && [mySingleton  gameTurn]==1) {
			int b = (location.y-p6Sprite.position.y);
			int c = (location.x-p6Sprite.position.x);
			int degreeAngle = atan2(b,c)*180/ 3.14565656;
			int a = sqrt(b*b+c*c);
			CGFloat scaleSize = round(10/5.9*a)/100;
			arrowSelected = (CCSprite *)[p6Sprite getChildByTag:201];
			[arrowSelected setRotation:-degreeAngle  ];
			if(scaleSize>1.2){
				scaleSize = 1.2;
			}
			[arrowSelected setScaleX:scaleSize];
			float d = scaleSize*cos(-atan2(b,c));
			float e = -scaleSize*sin(-atan2(b,c));
			if(scaleSize>0.25)[mySingleton setP6ForceVector:cpv(d*kPowFactor*teamFactor2,e*kPowFactor*teamFactor2)];
		}
	}
//	return kEventHandled;
}

 -(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	 GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	 [mySingleton setCurrentSelectedP1: 0];
	 [mySingleton setCurrentSelectedP2: 0];
	 UITouch *touch = [touches anyObject];
	 CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	 //NSLog(@"-------------------------------------------------------------- Clicked began : %i , %i",location.x,location.y);
	 
     location = CGPointMake(location.x, location.y);
	 [touchSprite setPosition:location];
	 
     if (CGRectContainsPoint(CGRectMake(p1Sprite.position.x- 0.5*p1Sprite.contentSize.width, p1Sprite.position.y- 0.5*p1Sprite.contentSize.height,
										p1Sprite.contentSize.width, p1Sprite.contentSize.height), location) && [mySingleton isP1]==YES) {
		[mySingleton setCurrentSelectedP1: 1];
	 }else if(CGRectContainsPoint(CGRectMake(p2Sprite.position.x- 0.5*p2Sprite.contentSize.width, p2Sprite.position.y- 0.5*p2Sprite.contentSize.height,
											 p2Sprite.contentSize.width, p2Sprite.contentSize.height), location) && [mySingleton isP1]==YES) {
		[mySingleton setCurrentSelectedP1: 2];
	 }else if(CGRectContainsPoint(CGRectMake(p3Sprite.position.x- 0.5*p3Sprite.contentSize.width, p3Sprite.position.y- 0.5*p3Sprite.contentSize.height,
											 p3Sprite.contentSize.width, p3Sprite.contentSize.height), location) && [mySingleton isP1]==YES) {
		 [mySingleton setCurrentSelectedP1: 3];
	 }else if(CGRectContainsPoint(CGRectMake(p4Sprite.position.x- 0.5*p4Sprite.contentSize.width, p4Sprite.position.y- 0.5*p4Sprite.contentSize.height,
											 p4Sprite.contentSize.width, p4Sprite.contentSize.height), location) && [mySingleton isP2]==YES) {
		 //[p4Sprite  runAction:[ScaleTo actionWithDuration:0.2 scale:1.1]];
		 [mySingleton setCurrentSelectedP2: 4];
	 }else if(CGRectContainsPoint(CGRectMake(p5Sprite.position.x- 0.5*p5Sprite.contentSize.width, p5Sprite.position.y- 0.5*p5Sprite.contentSize.height,
											 p5Sprite.contentSize.width, p5Sprite.contentSize.height), location) && [mySingleton isP2]==YES) {
		 [mySingleton setCurrentSelectedP2: 5];
	 }else if(CGRectContainsPoint(CGRectMake(p6Sprite.position.x- 0.5*p6Sprite.contentSize.width, p6Sprite.position.y- 0.5*p6Sprite.contentSize.height,
											 p6Sprite.contentSize.width, p6Sprite.contentSize.height), location) && [mySingleton isP2]==YES) {
		 [mySingleton setCurrentSelectedP2: 6];
	 }else if(CGRectContainsPoint(CGRectMake(spriteBackMenu.position.x- 0.5*spriteBackMenu.contentSize.width, spriteBackMenu.position.y- 0.5*spriteBackMenu.contentSize.height, spriteBackMenu.contentSize.width, spriteBackMenu.contentSize.height), location)) {
		 [self goBackMenu];
	 }else if(CGRectContainsPoint(CGRectMake(spriteSoundToogler.position.x- 0.5*spriteSoundToogler.contentSize.width, spriteSoundToogler.position.y- 0.5*spriteSoundToogler.contentSize.height, spriteSoundToogler.contentSize.width, spriteSoundToogler.contentSize.height), location)) {
		 [self audioToogler];
	 }
     
     if(clockTurnSprite!=nil){
         if (CGRectContainsPoint(clockTurnSprite.boundingBox, location)) {
             [self removeSpriteItsYourTurn:0];
             
             if([mySingleton gameType]==1){
                 if([mySingleton isP1]){
                     [mySingleton setTimePlayTurn:6];
                 }else{
                     [mySingleton setTimePlayTurn:0];
                 }
             
             }else{
                 [mySingleton setTimePlayTurn:0];
             }
         }
     }
     
 }
 
-(void)goBackMenu{
    //Remove banner
    AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [(RootViewController*)[app navController] removeBanner];
    
    if([[GlobalSingleton sharedInstance] gameType]==2){
        [[[GameCenterManager sharedManager] multiplayerObject] disconnectLocalPlayerFromMatch];
    }
    if([[GlobalSingleton sharedInstance] gameType]==22){
        //TODO disconect bluetooth
        [[PeerServiceManager sharedInstance] disconnect];

    }
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[MenuScene node]]];
    
	
}

 -(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	 GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	 UITouch *touch = [touches anyObject];
     
	 CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	 location = CGPointMake(location.x, location.y);
	 [touchSprite setPosition:ADJUST_CCP(ccp(500, 500))];
	 [mySingleton setCurrentSelectedP1: 0];
	 [mySingleton setCurrentSelectedP2: 0];
 }


///////////////////////////////////////////////////////
//    a       x           x  =   a    * c
//  ----- = -----              -----
//    b       c                  b
///////////////////////////////////////////////////////

-(int)ruleOfThree:(int)a secondParam:(int)b thirdParam:(int)c {
	int x=(a/b)*c;
	return x;
}


-(void)destroyChipmunk{
    //Destroy Chipmunk
    cpSpaceFree(space);
    space = nil;
}


-(void)setupChipmunk{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
	//cpInitChipmunk();
	space = cpSpaceNew();
	space->gravity = cpv(0,0);
	//space->iterations = elasticIterations;
    space->iterations = 10;
	space->damping = 0.27;
	
	[self schedule: @selector(tick:) interval: 1.0f/60.0f];
	[self schedule: @selector(updateTimer:) interval:1];

    cpResetShapeIdCounter();
    
	//BALL
	ballBody = cpBodyNew(ballMass, cpMomentForCircle(ballMass/4, 12.0, 12.0, cpvzero));
    ballBody->p = [mySingleton ballPosition]; //cpv(kBallPositionX, kBallPositionY);
	cpSpaceAddBody(space, ballBody);
	cpBodySetAngle(ballBody,0);
	ballShape = cpCircleShapeNew(ballBody, 12.0, cpvzero);
	ballShape->e = 1;
	ballShape->u = 0.1;
    ballShape->data = ballSprite;
	ballShape->collision_type = 0;
	cpSpaceAddShape(space, ballShape);
	cpSpaceAddCollisionHandler(space, 0, 1,  &ballCollisionNew,NULL, NULL, NULL, self);
	
	//PLAYER 1
	p1Body = cpBodyNew(playerMass,  cpMomentForCircle(playerMass, 17.0, 17.0, cpvzero));
	p1Body->p = [mySingleton p1Position];//cpv(kP1PositionX, kP1PositionY);
	//cpBodySetMass(p1Body,50);
	cpBodySetAngle(p1Body,0);
	cpSpaceAddBody(space, p1Body);
	p1Shape = cpCircleShapeNew(p1Body, 17.0, cpvzero);
	p1Shape->e = 0.9;
    p1Shape->u = 0.1;
	p1Shape->data = p1Sprite;
	p1Shape->collision_type = 1;
	cpSpaceAddShape(space, p1Shape);
	
	//PLAYER 2
	p2Body = cpBodyNew(playerMass,  cpMomentForCircle(playerMass, 17.0, 17.0, cpvzero));
	p2Body->p = [mySingleton p2Position];//cpv(kP2PositionX, kP2PositionY);
	//cpBodySetMass(p2Body,50);
	cpSpaceAddBody(space, p2Body);
	p2Shape = cpCircleShapeNew(p2Body, 17.0, cpvzero);
    p2Shape->e = 0.9;
    p2Shape->u = 0.1;
	p2Shape->data = p2Sprite;
	p2Shape->collision_type = 1;
	cpSpaceAddShape(space, p2Shape);

	//PLAYER 3
	p3Body = cpBodyNew(playerMass,  cpMomentForCircle(playerMass, 17.0, 17.0, cpvzero));
	p3Body->p = [mySingleton p3Position];//cpv(kP3PositionX, kP3PositionY);
	//cpBodySetMass(p3Body,50);
	cpSpaceAddBody(space, p3Body);
	p3Shape = cpCircleShapeNew(p3Body, 17.0, cpvzero);
	p3Shape->e = 0.9;
    p3Shape->u = 0.1;
	
	p3Shape->data = p3Sprite;
	p3Shape->collision_type = 1;
	cpSpaceAddShape(space, p3Shape);

	//PLAYER 4
	//p4Body = cpBodyNew(playerMass, INFINITY);
	p4Body = cpBodyNew(playerMass, cpMomentForCircle(playerMass, 17.0, 17.0, cpvzero));
	p4Body->p = [mySingleton p4Position];//cpv(kP4PositionX, kP4PositionY);
	//cpBodySetAngle(p4Body,-0.7853);
	//cpBodySetMass(p4Body,50);
	cpSpaceAddBody(space, p4Body);
	p4Shape = cpCircleShapeNew(p4Body, 17.0, cpvzero);
	p4Shape->e = 0.9;
    p4Shape->u = 0.1;
	
	p4Shape->data = p4Sprite;
	p4Shape->collision_type = 1;
	cpSpaceAddShape(space, p4Shape);

	//PLAYER 5
	p5Body = cpBodyNew(playerMass, cpMomentForCircle(playerMass, 17.0, 17.0, cpvzero));
	p5Body->p = [mySingleton p5Position];//cpv(kP5PositionX, kP5PositionY);
	//cpBodySetMass(p5Body,50);
	cpSpaceAddBody(space, p5Body);
	p5Shape = cpCircleShapeNew(p5Body, 17.0, cpvzero);
	p5Shape->e = 0.9;
    p5Shape->u = 0.1;
	
	p5Shape->data = p5Sprite;
	p5Shape->collision_type = 1;
	cpSpaceAddShape(space, p5Shape);

	//PLAYER 6
	p6Body = cpBodyNew(playerMass, cpMomentForCircle(playerMass, 17.0, 17.0, cpvzero));
	p6Body->p = [mySingleton p6Position];//cpv(kP6PositionX, kP6PositionY);
	//cpBodySetMass(p6Body,50);
	cpSpaceAddBody(space, p6Body);
	p6Shape = cpCircleShapeNew(p6Body, 17.0, cpvzero);
	
    p6Shape->e = 0.9;
    p6Shape->u = 0.1;
	
	p6Shape->data = p6Sprite;
	p6Shape->collision_type = 1;
	cpSpaceAddShape(space, p6Shape);

	//WALLS
	cpBody* wallBody = cpBodyNew(INFINITY, INFINITY);
		
	//BOTTOM
	wallBody->p = cpv(0, 0);
	cpShape* wallShape = cpSegmentShapeNew(wallBody, cpv(0,17), cpv(480,17), 10);
    wallShape->e = 0.5;
    wallShape->u = 0.1;
	wallShape->data = invWallBottomSprite;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//TOP
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(0,283), cpv(480,283), 10);
	wallShape->e = 0.5;
    wallShape->u = 0.1;
	wallShape->data = invWallTopSprite;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//LEFT 1
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(60,12), cpv(60,87), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->data = invWallBottomLeftSprite;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//LEFT 2
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(60,214), cpv(60,317), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->data = invWallTopLeftSprite;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//LEFT 3
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(0,214), cpv(60,214), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//LEFT 4
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(0,87), cpv(60,87), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//GOL LEFT
	cpBody* gol1Body = cpBodyNew(INFINITY, INFINITY);
	gol1Body->p = cpv(0, 0);
	cpSpaceAddBody(space, gol1Body);
	cpShape* gol1Shape = cpSegmentShapeNew(gol1Body, cpv(60,87), cpv(60,214), 10);

        gol1Shape->e = 0.5;
        gol1Shape->u = 0.1;
	
	gol1Shape->data = invGol1Sprite;
	gol1Shape->collision_type = 1;
	cpSpaceAddStaticShape(space, gol1Shape);
	
	//GOL LEFT
	cpBody* gol1HitBody = cpBodyNew(INFINITY, INFINITY);
	gol1HitBody->p = cpv(0, 0);
	cpSpaceAddBody(space, gol1HitBody);
	cpShape* gol1HitShape = cpSegmentShapeNew(gol1HitBody, cpv(40,8), cpv(40,214), 10);

        gol1HitShape->e = 0.5;
        gol1HitShape->u = 0.1;
	
	gol1HitShape->data = invGol1HitSprite;
	gol1HitShape->collision_type = 1;
	cpSpaceAddStaticShape(space, gol1HitShape);
	
	//WALL LEFT
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(0,12), cpv(0,317), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//RIGHT 1
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(420,12), cpv(420,87), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->data = invWallBottomRightSprite;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//RIGHT 2
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(420,214), cpv(420,317), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->data = invWallTopRightSprite;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//RIGHT 3
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(420,214), cpv(480,214), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//RIGHT 4
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(420,87), cpv(480,87), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	

	
	//GOL RIGHT
	cpBody* gol2Body = cpBodyNew(INFINITY, INFINITY);
	gol2Body->p = cpv(0, 0);
	cpSpaceAddBody(space, gol2Body);
	cpShape* gol2Shape = cpSegmentShapeNew(gol2Body, cpv(420,87), cpv(420,214), 10);

        gol2Shape->e = 0.5;
        gol2Shape->u = 0.1;
	
	gol2Shape->data = invGol2Sprite;
	gol2Shape->collision_type = 1;
	cpSpaceAddStaticShape(space, gol2Shape);
	
	//GOL RIGHT
	cpBody* gol2HitBody = cpBodyNew(INFINITY, INFINITY);
	gol2HitBody->p = cpv(0, 0);
	cpSpaceAddBody(space, gol2HitBody);
	cpShape* gol2HitShape = cpSegmentShapeNew(gol2HitBody, cpv(440,87), cpv(440,214), 10);

        gol2HitShape->e = 0.5;
        gol2HitShape->u = 0.1;
	
	gol2HitShape->data = invGol2HitSprite;
	gol2HitShape->collision_type = 1;
	cpSpaceAddStaticShape(space, gol2HitShape);
	
	//WALL RIGHT
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(480,12), cpv(480,317), 10);

        wallShape->e = 0.5;
        wallShape->u = 0.1;
	
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
		
	//CORNER BOTTOM LEFT
	cpBody* cornerBody1 = cpBodyNew(INFINITY, INFINITY);
	cornerBody1->p = cpv(70, 27);
	//cpSpaceAddBody(space, cornerBody1);
	cpShape* cornerShapeBottomLeft = cpCircleShapeNew(cornerBody1, 20, cpvzero);

        cornerShapeBottomLeft->e = 0.5;
        cornerShapeBottomLeft->u = 0.1;
	
	cornerShapeBottomLeft->data = cornerSprite1;
	cornerShapeBottomLeft->collision_type = 1;
	cpSpaceAddStaticShape(space, cornerShapeBottomLeft);
	
	
	//CORNER TOP LEFT
	cpBody* cornerBody2 = cpBodyNew(INFINITY, INFINITY);
	cornerBody2->p = cpv(70, 273);
//	cpSpaceAddBody(space, cornerBody2);
	cpShape* cornerShapeTopLeft = cpCircleShapeNew(cornerBody2, 20, cpvzero);

        cornerShapeTopLeft->e = 0.5;
        cornerShapeTopLeft->u = 0.1;
	
	cornerShapeTopLeft->data = cornerSprite2;
	cornerShapeTopLeft->collision_type = 1;
	cpSpaceAddStaticShape(space, cornerShapeTopLeft);
	
	
	//CORNER BOTTOM RIGHT
	cpBody* cornerBody3 = cpBodyNew(INFINITY, INFINITY);
	cornerBody3->p = cpv(410, 27);
//	cpSpaceAddBody(space, cornerBody3);
	cpShape* cornerShapeBottomRight = cpCircleShapeNew(cornerBody3, 20, cpvzero);
	cpBodySetAngle(cornerBody3,-0.7853);

        cornerShapeBottomRight->e = 0.5;
        cornerShapeBottomRight->u = 0.1;
	
	cornerShapeBottomRight->data = cornerSprite3;
	cornerShapeBottomRight->collision_type = 1;
	cpSpaceAddStaticShape(space, cornerShapeBottomRight);
	
	//CORNER TOP LEFT
	cpBody* cornerBody4 = cpBodyNew(INFINITY, INFINITY);
	cornerBody4->p = cpv(410, 273);
	cpBodySetAngle(cornerBody4,0.7853);
//	cpSpaceAddBody(space, cornerBody4);
	cpShape* cornerShapeTopRight = cpCircleShapeNew(cornerBody4, 20, cpvzero);

        cornerShapeTopRight->e = 0.5;
        cornerShapeTopRight->u = 0.1;
	
	cornerShapeTopRight->data = cornerSprite4;
	cornerShapeTopRight->collision_type = 1;
	cpSpaceAddStaticShape(space, cornerShapeTopRight);
	 
}

//
// Called by NSTimer fire, hides the game label
//
- (void)hideGameLabel:(ccTime *)timer {
	[self removeChild:playerSprite cleanup:YES];
    NSLog(@"REMOVE PLAYER SPRITE ....");
	
}

-(void)audioToogler{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		
		[mySingleton setAudioOn:NO];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
		sae.backgroundMusicVolume = 0.0f;
		sae.effectsVolume = 0.0f;
	}else{
		[mySingleton setAudioOn:YES];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];	
		sae.backgroundMusicVolume = 0.2f;	
		sae.effectsVolume = 0.2f;
	}
}

- (void)setupBatchNode {
    NSString *spritesheetPvrCcz = @"spritesheet.pvr.ccz";
    NSString *spritesheetPlist = @"spritesheet.plist";
    
    NSString *gametablePvrCcz = @"gametable.pvr.ccz";
    NSString *gametablePlist = @"gametable.plist";
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            spritesheetPvrCcz = @"spritesheet-ipadhd.pvr.ccz";
            spritesheetPlist = @"spritesheet-ipadhd.plist";
            gametablePvrCcz = @"gametable-ipadhd.pvr.ccz";
            gametablePlist = @"gametable-ipadhd.plist";
        }else{
            //iPad screen
            spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
            spritesheetPlist = @"spritesheet-hd.plist";
            gametablePvrCcz = @"gametable-hd.pvr.ccz";
            gametablePlist = @"gametable-hd.plist";
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
                spritesheetPlist = @"spritesheet-hd.plist";
                
                gametablePvrCcz = @"gametable-hd.pvr.ccz";
                gametablePlist = @"gametable-hd.plist";
            }else{
                //iphone screen
                spritesheetPvrCcz = @"spritesheet.pvr.ccz";
                spritesheetPlist = @"spritesheet.plist";
                
                gametablePvrCcz = @"gametable.pvr.ccz";
                gametablePlist = @"gametable.plist";
            }
        }
    }
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesheetPvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesheetPlist];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:gametablePvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:gametablePlist];
}

-(id)init{
	self = [super init];
	if(nil != self){
		[self setupBatchNode];
        
        
        //set ball frame number
        ballFrameNumber = 0;
        diffPosy = 0;
        diffPosx = 0;
        
        /*gkHelper = [GameKitHelper sharedGameKitHelper];
        
        [gkHelper submitScore:3 category:@"307753"];
        */
        
        
        /*
        [[GameCenterManager sharedManager] saveAndReportScore:3 leaderboard:@"307753" sortOrder:GameCenterSortOrderLowToHigh];
        */
        
       // AppDelegate*appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            goal1 = [CCSprite spriteWithFile: @"goal_anima1-hd.png"];
            goal2 = [CCSprite spriteWithFile: @"goal_anima2-hd.png"];
            goal3 = [CCSprite spriteWithFile: @"goal_anima3-hd.png"];
            goal4 = [CCSprite spriteWithFile: @"goal_anima4-hd.png"];
            goal5 = [CCSprite spriteWithFile: @"goal_anima5-hd.png"];
            goal6 = [CCSprite spriteWithFile: @"goal_anima6-hd.png"];
            goal7 = [CCSprite spriteWithFile: @"goal_anima7-hd.png"];
            goal8 = [CCSprite spriteWithFile: @"goal_anima8-hd.png"];
        }else{
            goal1 = [CCSprite spriteWithFile: @"goal_anima1.png"];
            goal2 = [CCSprite spriteWithFile: @"goal_anima2.png"];
            goal3 = [CCSprite spriteWithFile: @"goal_anima3.png"];
            goal4 = [CCSprite spriteWithFile: @"goal_anima4.png"];
            goal5 = [CCSprite spriteWithFile: @"goal_anima5.png"];
            goal6 = [CCSprite spriteWithFile: @"goal_anima6.png"];
            goal7 = [CCSprite spriteWithFile: @"goal_anima7.png"];
            goal8 = [CCSprite spriteWithFile: @"goal_anima8.png"];
        }

        [self addChild:goal1 z:1];
        [self removeChild:goal1 cleanup:YES];
        [self addChild:goal2 z:1];
        [self removeChild:goal2 cleanup:YES];
        [self addChild:goal3 z:1];
        [self removeChild:goal3 cleanup:YES];
        [self addChild:goal4 z:1];
        [self removeChild:goal4 cleanup:YES];
        [self addChild:goal5 z:1];
        [self removeChild:goal5 cleanup:YES];
        [self addChild:goal6 z:1];
        [self removeChild:goal6 cleanup:YES];
        [self addChild:goal7 z:1];
        [self removeChild:goal7 cleanup:YES];
        [self addChild:goal8 z:1];
        [self removeChild:goal8 cleanup:YES];
        

		self.isTouchEnabled = YES;
		GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
		GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
			if (sae != nil) {
				[sae preloadEffect:@"ambience.mp3"];
				[sae preloadEffect:@"ambience2.mp3"];
				[sae preloadEffect:@"kick.mp3"];
				[sae preloadEffect:@"kick1.mp3"];
				[sae preloadEffect:@"whistle.mp3"];
				[sae playBackgroundMusic:@"loop.mp3" loop:YES];
				if([mySingleton audioOn]==YES){
					
					sae.backgroundMusicVolume = 0.2f;
				}else{
					sae.backgroundMusicVolume = 0.0f;
				}
			}
        //Set time
        [mySingleton setTimeLeft:[myCupSingleton defaultTimeLeft]];
        if(([mySingleton gameType] == 2 || [mySingleton gameType] == 22) && [mySingleton isP1]){
            [mySingleton setTimeLeft: [mySingleton timeLeft] + 1];
        }
		spriteBackMenu = [CCSprite spriteWithSpriteFrameName:@"back_menu.png"];
		[spriteBackMenu setPosition:ADJUST_CCP(ccp(20,300))];
		[self addChild:spriteBackMenu z:101 tag:101];
		
		spriteSoundToogler = [CCSprite spriteWithSpriteFrameName:@"sound.png"];
		[spriteSoundToogler setPosition:ADJUST_CCP(ccp(460,300))];
		[self addChild:spriteSoundToogler z:101 tag:101];
		
		DataHolder *myDataHolder = [DataHolder sharedInstance];
		[myDataHolder clearData];
		
		[mySingleton setCurrentScoreP1:0];
		[mySingleton setCurrentScoreP2:0];
		
		[myCupSingleton setCurrentScoreP1:0];
		[myCupSingleton setCurrentScoreP2:0];
		
		/* comment next two lines if dont want accelerometer*/ 
		//isAccelerometerEnabled = YES;
		//[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 60)];
		 NSLog(@"debug5");
		gameLayer = self;
		//Sets slide factor based on the the weather
		if([myCupSingleton matchWeather]==0){
			elasticIterations = 3;
			kSlide =  0.8;
			playerMass = 20;
			ballMass = 15;
		}else if([myCupSingleton matchWeather]==1){
			elasticIterations = 3;
			kSlide =  0.9;
			playerMass = 20;
			ballMass = 12;
		}else if([myCupSingleton matchWeather]==2){
			elasticIterations = 3;
			kSlide =  1.1;
			playerMass = 26;
			ballMass = 35;
		}else{
			//bluetooth/multiplayer
			elasticIterations = 5;
			kSlide =  1.1;
			playerMass = 20;
			ballMass = 20;
		}

		ballSpriteShadow = [CCSprite spriteWithSpriteFrameName:@"ball_shadow.png"];
		[ballSpriteShadow setPosition:ADJUST_CCP(ccp(240, 138 + kBannerAdjustY ))];
		[self addChild:ballSpriteShadow z:-1 tag:71];
        
		ballSprite = [CCSprite spriteWithSpriteFrameName:@"ball_0000.png"];
		[ballSprite setPosition:ADJUST_CCP(ccp(240, 138+kBannerAdjustY))];
		[self addChild:ballSprite z:0 tag:7];
		[ballSprite stopAllActions];
        
        ballSpriteVolume = [CCSprite spriteWithSpriteFrameName:@"ball_volume.png"];
		[ballSpriteVolume setPosition:ADJUST_CCP(ccp(240, 138 + kBannerAdjustY ))];
		[self addChild:ballSpriteVolume z:1 tag:72];
		
		if( [mySingleton gameType]==0 ){
            [Flurry logEvent:@"PLAY_WORLDCUP" timed:YES];
			[mySingleton clearData];
			[mySingleton setIsP1:YES];
			[mySingleton setIsP2:NO];
		}else if([mySingleton gameType]==1){
            [Flurry logEvent:@"PLAY_VERSUS" timed:YES];
			
			[mySingleton clearData];
			[mySingleton setIsP1:YES];
			[mySingleton setIsP2:NO];
		}else if([mySingleton gameType]==2 || [mySingleton gameType]==22){
            if([mySingleton gameType]==2){
                [Flurry logEvent:@"PLAY_MULTIPLAYER" timed:YES];
            }
            if([mySingleton gameType]==22){
                [Flurry logEvent:@"PLAY_BLUETOOTH" timed:YES];
            }
            [mySingleton clearData];
			//multiplayer
			if([mySingleton isP1]==YES){
				playerSprite = [CCSprite spriteWithSpriteFrameName:@"player1.png"];
				[playerSprite setPosition:ADJUST_CCP(ccp(240, 160+kBannerAdjustY))];
				[self addChild:playerSprite z:401 tag:401 ];
			}else if([mySingleton isP2]==YES){
				playerSprite = [CCSprite spriteWithSpriteFrameName:@"player2.png"];
				[playerSprite setPosition:ADJUST_CCP(ccp(240, 160+kBannerAdjustY))];
				[self addChild:playerSprite z:401 tag:401 ];				
			}
			// after 1 second fire method to hide the label
            [self schedule:@selector(hideGameLabel:) interval:0 repeat:0 delay:1];
            //[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(hideGameLabel:) userInfo:nil repeats:NO];
            NSLog(@"MULTIPLAYER GAME STARTING ....");
		}else if([mySingleton gameType]==3){
			[Flurry logEvent:@"PLAY_SINGLE" timed:YES];
			[mySingleton clearData];
			[mySingleton setIsP1:YES];
			[mySingleton setIsP2:NO];
		}
		
		//PLAYERSBOARD

		NSString* teamName1 = [myDataHolder getTeamShortNameForKey:[mySingleton spritePlayer1]];
		NSString* teamName2 = [myDataHolder getTeamShortNameForKey:[mySingleton spritePlayer2]];

        //Set factor based on player
        teamFactor1 = float([myDataHolder getFactorByTeamKey:[mySingleton spritePlayer1]])/10.0f + 0.6f;
        teamFactor2 = float([myDataHolder getFactorByTeamKey:[mySingleton spritePlayer2]])/10.0f + 0.6f;

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                labelPlayer1 = [CCLabelAtlas labelWithString:teamName1 charMapFile:@"charmap1-ipadhd.png" itemWidth:36 itemHeight:44 startCharMap:'A'];
                labelPlayer2 = [CCLabelAtlas labelWithString:teamName2 charMapFile:@"charmap1-ipadhd.png" itemWidth:36 itemHeight:44 startCharMap:'A'];
                labelTimerMinutes = [CCLabelAtlas labelWithString:@"00" charMapFile:@"clockNumbers2-ipadhd.png" itemWidth:28 itemHeight:49 startCharMap:'.'];
                labelTimerSeconds = [CCLabelAtlas labelWithString:@"00" charMapFile:@"clockNumbers2-ipadhd.png" itemWidth:28 itemHeight:49 startCharMap:'.'];
                p1Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-ipadhd.png" itemWidth:56 itemHeight:84 startCharMap:'.'];
                p2Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-ipadhd.png" itemWidth:56 itemHeight:84 startCharMap:'.'];
                
            }else{
                //iPad screen
                labelPlayer1 = [CCLabelAtlas labelWithString:teamName1 charMapFile:@"charmap1-hd.png" itemWidth:36 itemHeight:44 startCharMap:'A'];
                labelPlayer2 = [CCLabelAtlas labelWithString:teamName2 charMapFile:@"charmap1-hd.png" itemWidth:36 itemHeight:44 startCharMap:'A'];
                labelTimerMinutes = [CCLabelAtlas labelWithString:@"00" charMapFile:@"clockNumbers2-hd.png" itemWidth:28 itemHeight:49 startCharMap:'.'];
                labelTimerSeconds = [CCLabelAtlas labelWithString:@"00" charMapFile:@"clockNumbers2-hd.png" itemWidth:28 itemHeight:49 startCharMap:'.'];
                p1Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-hd.png" itemWidth:56 itemHeight:84 startCharMap:'.'];
                p2Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-hd.png" itemWidth:56 itemHeight:84 startCharMap:'.'];
            }
            
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    labelPlayer1 = [CCLabelAtlas labelWithString:teamName1 charMapFile:@"charmap1-hd.png" itemWidth:18 itemHeight:22 startCharMap:'A'];
                    labelPlayer2 = [CCLabelAtlas labelWithString:teamName2 charMapFile:@"charmap1-hd.png" itemWidth:18 itemHeight:22 startCharMap:'A'];
                    labelTimerMinutes = [CCLabelAtlas labelWithString:@"00" charMapFile:@"clockNumbers2-hd.png" itemWidth:14 itemHeight:25 startCharMap:'.'];
                    labelTimerSeconds = [CCLabelAtlas labelWithString:@"00" charMapFile:@"clockNumbers2-hd.png" itemWidth:14 itemHeight:25 startCharMap:'.'];
                    p1Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-hd.png" itemWidth:28 itemHeight:42 startCharMap:'.'];
                    p2Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-hd.png" itemWidth:28 itemHeight:42 startCharMap:'.'];
                }else{
                    //iphone screen
                    labelPlayer1 = [CCLabelAtlas labelWithString:teamName1 charMapFile:@"charmap1.png" itemWidth:9 itemHeight:11 startCharMap:'A'];
                    labelPlayer2 = [CCLabelAtlas labelWithString:teamName2 charMapFile:@"charmap1.png" itemWidth:9 itemHeight:11 startCharMap:'A'];
                    labelTimerMinutes = [CCLabelAtlas labelWithString:@"00" charMapFile:@"clockNumbers2.png" itemWidth:9 itemHeight:11 startCharMap:'.'];
                    labelTimerSeconds = [CCLabelAtlas labelWithString:@"00" charMapFile:@"clockNumbers2.png" itemWidth:9 itemHeight:11 startCharMap:'.'];
                    p1Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers.png" itemWidth:56 itemHeight:40 startCharMap:'.'];
                    p2Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers.png" itemWidth:56 itemHeight:40 startCharMap:'.'];
                }
            }
            
        }
        
        [labelPlayer1 setPosition:ADJUST_CCP(ccp(64,286))];
        [labelPlayer2 setPosition:ADJUST_CCP(ccp(362,286))];
        [labelTimerMinutes setPosition:ADJUST_CCP(ccp(210,283))];
        [labelTimerSeconds setPosition:ADJUST_CCP(ccp(244,283))];
        [p1Score setPosition:ADJUST_CCP(ccp(126,276))];
        [p2Score setPosition:ADJUST_CCP(ccp(328,276))];
		
		
		[self addChild:labelPlayer1 z:306 tag:306];
		
		
		//PLAYERSBOARD

		[self addChild:labelPlayer2 z:306 tag:306];
		
		
		//NSLog(@"TEAM NAME 1 : %p",teamName1);
		//NSLog(@"TEAM NAME 2 : %d",(NSString*)teamName2);
		
		
		//TIMEBOARD
		[self addChild:labelTimerMinutes z:306 tag:306];
		
		
        clockSprite = [CCSprite spriteWithSpriteFrameName:@"clockSprite_blk.png"];
		[clockSprite setPosition:ADJUST_CCP(ccp(240, 296))];
		[self addChild:clockSprite z:300 tag:300 ];
		
		
		[self addChild:labelTimerSeconds z:301 tag:301];
		
		
		//SCOREBOARD
		[self addChild:p1Score z:302 tag:302];
        
		
		[self addChild:p2Score z:303 tag:303];
		
        
//		versusScoreSprite = [CCSprite spriteWithSpriteFrameName:@"versusScoreSprite.png"];
//		[versusScoreSprite setPosition:ADJUST_CCP(ccp(240, 300))];
//		[self addChild:versusScoreSprite z:304 tag:304 ];

		invWallTopSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invWallTopSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[self addChild:invWallTopSprite z:1001 tag:1001 ];
		
		invWallTopRightSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invWallTopRightSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[self addChild:invWallTopRightSprite z:1002 tag:1002 ];
		
		invWallBottomRightSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invWallBottomRightSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[self addChild:invWallBottomRightSprite z:1003 tag:1003 ];
		
		invWallBottomSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invWallBottomSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[self addChild:invWallBottomSprite z:1004 tag:1004 ];
		
		invWallBottomLeftSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invWallBottomLeftSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[self addChild:invWallBottomLeftSprite z:1005 tag:1005 ];
		
		invWallTopLeftSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invWallTopLeftSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[self addChild:invWallTopLeftSprite z:1006 tag:1006 ];
		
		cornerSprite1 = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[cornerSprite1 setPosition:ADJUST_CCP(ccp(70, 30))];
		[cornerSprite1 setScale:1.2];
		[self addChild:cornerSprite1 z:601 tag:601 ];
		
		cornerSprite2 = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[cornerSprite2 setPosition:ADJUST_CCP(ccp(70, 276))];
		[cornerSprite2 setScale:1.2];
		[self addChild:cornerSprite2 z:602 tag:602 ];
		
		cornerSprite3 = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[cornerSprite3 setPosition:ADJUST_CCP(ccp(410, 30))];
		[cornerSprite3 setScale:1.2];
		[self addChild:cornerSprite3 z:603 tag:603 ];
		
		cornerSprite4 = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[cornerSprite4 setPosition:ADJUST_CCP(ccp(410, 276))];
		[cornerSprite4 setScale:1.2];
		[self addChild:cornerSprite4 z:604 tag:604 ];

		touchSprite = [CCSprite spriteWithSpriteFrameName:@"touchSprite.png"];
		[touchSprite setPosition:ADJUST_CCP(ccp(500, 500))];
		[self addChild:touchSprite z:10 tag:8];

        NSString* imageName1 = [myDataHolder getImageP1ForKey:[mySingleton spritePlayer1]];
		//NSLog(@"IMGA NAME :%d",imageName1);
		p1SpriteDisplay = [CCSprite spriteWithSpriteFrameName:imageName1];
		[p1SpriteDisplay setPosition:ADJUST_CCP(ccp(180,298))];
		[self addChild:p1SpriteDisplay  z:10 tag:1];
		
		p1Sprite = [CCSprite spriteWithSpriteFrameName:imageName1];
		p2Sprite = [CCSprite spriteWithSpriteFrameName:imageName1];
		p3Sprite = [CCSprite spriteWithSpriteFrameName:imageName1];
        
		NSString* imageName2 = [myDataHolder getImageP2ForKey:[mySingleton spritePlayer2]];
		p2SpriteDisplay = [CCSprite spriteWithSpriteFrameName:imageName2];
		[p2SpriteDisplay setPosition:ADJUST_CCP(ccp(301,298))];
		[self addChild:p2SpriteDisplay  z:10 tag:1];
		
		p4Sprite = [CCSprite spriteWithSpriteFrameName:imageName2];
		p5Sprite = [CCSprite spriteWithSpriteFrameName:imageName2];
		p6Sprite = [CCSprite spriteWithSpriteFrameName:imageName2];
		
		[p1Sprite setPosition:ADJUST_CCP(ccp(140, 225+kBannerAdjustY))];
		[self addChild:p1Sprite  z:1 tag:1];
		
		CCSprite*arrowSprite = [CCSprite spriteWithSpriteFrameName:@"drag_arrow.png" ];
		[arrowSprite setPosition:ccp(p1Sprite.contentSize.width/2, p1Sprite.contentSize.height/2)];
		[arrowSprite setAnchorPoint:CGPointMake(0.1, 0.5)];	
		[p1Sprite addChild:arrowSprite z:-1 tag:201];
		[arrowSprite setScaleX:0.1];
		
		//PLAYER 2
		//p2Sprite = [Sprite spriteWithSpriteFrameName:[mySingleton teamP1Image]];
		[p2Sprite setPosition:ADJUST_CCP(ccp(100, 138+kBannerAdjustY))];
		[self addChild:p2Sprite z:2 tag:2];
		
		arrowSprite = [CCSprite spriteWithSpriteFrameName:@"drag_arrow.png" ];
		[arrowSprite setAnchorPoint:CGPointMake(0.1, 0.5)];	
		[arrowSprite setPosition:ccp(p2Sprite.contentSize.width/2, p2Sprite.contentSize.height/2)];
		[p2Sprite addChild:arrowSprite z:-1 tag:201];
		[arrowSprite setScaleX:0.1];
		
		//PLAYER 3
		//p3Sprite = [Sprite spriteWithSpriteFrameName:[mySingleton teamP1Image]];
		[p3Sprite setPosition:ADJUST_CCP(ccp(140, 51+kBannerAdjustY))];
		[self addChild:p3Sprite  z:3 tag:3];
		
		arrowSprite = [CCSprite spriteWithSpriteFrameName:@"drag_arrow.png" ];
		[arrowSprite setAnchorPoint:CGPointMake(0.1, 0.5)];	
		[arrowSprite setPosition:ccp(p3Sprite.contentSize.width/2, p3Sprite.contentSize.height/2)];
		[p3Sprite addChild:arrowSprite z:-1 tag:201];
		[arrowSprite setScaleX:0.1];
		
		//NSLog(@"TEAM P2 IMAGE : %d",[mySingleton teamP2Image]);
		//PLAYER 4
		//p4Sprite = [Sprite spriteWithSpriteFrameName:[mySingleton teamP2Image]];
		[p4Sprite setPosition:ADJUST_CCP(ccp(340, 225+kBannerAdjustY))];
		[self addChild:p4Sprite  z:4 tag:4];
		
		arrowSprite = [CCSprite spriteWithSpriteFrameName:@"drag_arrow.png" ];
		[arrowSprite setAnchorPoint:CGPointMake(0.1, 0.5)];	
		[arrowSprite setPosition:ccp(p4Sprite.contentSize.width/2, p4Sprite.contentSize.height/2)];
		[p4Sprite addChild:arrowSprite z:-1 tag:201];
		[arrowSprite setScaleX:0.1];
		
		//PLAYER 5
		//p5Sprite = [Sprite spriteWithSpriteFrameName:[mySingleton teamP2Image]];
		[p5Sprite setPosition:ADJUST_CCP(ccp(380, 138+kBannerAdjustY))];
		[self addChild:p5Sprite  z:5 tag:5];
		
		arrowSprite = [CCSprite spriteWithSpriteFrameName:@"drag_arrow.png" ];
		[arrowSprite setAnchorPoint:CGPointMake(0.1, 0.5)];	
		[arrowSprite setPosition:ccp(p5Sprite.contentSize.width/2, p5Sprite.contentSize.height/2)];
		[p5Sprite addChild:arrowSprite z:-1 tag:201];
		[arrowSprite setScaleX:0.1];
		
		//PLAYER 6
		//p6Sprite = [Sprite spriteWithSpriteFrameName:[mySingleton teamP2Image]];
		[p6Sprite setPosition:ADJUST_CCP(ccp(340, 51+kBannerAdjustY))];
		[self addChild:p6Sprite  z:6 tag:6];
		
		arrowSprite = [CCSprite spriteWithSpriteFrameName:@"drag_arrow.png" ];
		[arrowSprite setAnchorPoint:CGPointMake(0.1, 0.5)];	
		[arrowSprite setPosition:ccp(p6Sprite.contentSize.width/2, p6Sprite.contentSize.height/2)];
		[p6Sprite addChild:arrowSprite z:-1 tag:201];
		[arrowSprite setScaleX:0.1];
		
		gol1Sprite = [CCSprite spriteWithSpriteFrameName:@"gol1.png"];
		[gol1Sprite setPosition:ADJUST_CCP(ccp(35, 138+kBannerAdjustY))];
		[gol1Sprite setTag:9999];
		[self addChild:gol1Sprite];
		
		gol2Sprite = [CCSprite spriteWithSpriteFrameName:@"gol2.png"];
		[gol2Sprite setPosition:ADJUST_CCP(ccp(445, 138+kBannerAdjustY))];
		[gol2Sprite setTag:9998];
		[self addChild:gol2Sprite z:2];
		
		invGol1Sprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invGol1Sprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[invGol1Sprite setTag:101];
		[self addChild:invGol1Sprite z:2];
		
		invGol2Sprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invGol2Sprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[invGol2Sprite setTag:102];
		[self addChild:invGol2Sprite];

		invGol1HitSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invGol1HitSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[invGol1HitSprite setTag:103];
		[self addChild:invGol1HitSprite];
		
		invGol2HitSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invGol2HitSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[invGol2HitSprite setTag:104];
		[self addChild:invGol2HitSprite];
		
		[self setupChipmunk];

		[mySingleton setP1ForceVector : cpv(0,0)];
		[mySingleton setP2ForceVector : cpv(0,0)];
		[mySingleton setP3ForceVector : cpv(0,0)];
		[mySingleton setP4ForceVector : cpv(0,0)];
		[mySingleton setP5ForceVector : cpv(0,0)];
		[mySingleton setP6ForceVector : cpv(0,0)];
		
		NSString * leaderBoardSelected;
		if([mySingleton gameType]==0){
			leaderBoardSelected = [NSString stringWithFormat:@"311313"];
            [mySingleton setLeaderboardGamesPlayed:leaderBoardSelected];
		}else if([mySingleton gameType]==1){
			leaderBoardSelected = [NSString stringWithFormat:@"311403"];
            [mySingleton setLeaderboardGamesPlayed:leaderBoardSelected];
		}else if([mySingleton gameType]==2 || [mySingleton gameType]==22){
			leaderBoardSelected = [NSString stringWithFormat:@"311303"];
            [mySingleton setLeaderboardGamesPlayed:leaderBoardSelected];
		}else if([mySingleton gameType]==3){
			leaderBoardSelected = [NSString stringWithFormat:@"311343"];
            [mySingleton setLeaderboardGamesPlayed:leaderBoardSelected];
		}
	}
	return self;
}
@end
