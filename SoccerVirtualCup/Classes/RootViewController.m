/*
 * Copyright (c) 2009-2011 Ricardo Quesada
 * Copyright (c) 2011-2012 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


//
// RootViewController + iAd
// If you want to support iAd, use this class as the controller of your iAd
//

@import GoogleMobileAds;

#import "cocos2d.h"
#import "RootViewController.h"
#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "AppDelegate.h"
#import "MKStoreManager.h"
#import "DeviceSettings.h"
#import "GameScene.h"
#import "MenuScene.h"
#import "PortraitStoreProductViewController.h"


#define kAdMobBanner @"ca-app-pub-4523467891811991/7302404292"
#define kAdMobInterstitial  @"ca-app-pub-4523467891811991/8779137498"


#import <Chartboost/Chartboost.h>
#import <Chartboost/CBAnalytics.h>
#import <Chartboost/CBInPlay.h>


#import "FlurryAds.h"




@implementation RootViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    bannerViewAdded = NO;
    interstitialViewAdded = NO;
    
    //[self addBanner];
    // Set GameCenter Manager Delegate
    [[GameCenterManager sharedManager] setDelegate:self];
    [[GameCenterManager sharedManager] setMultiplayerObject:[GCMMultiplayer defaultMultiplayerManager]];
    [[[GameCenterManager sharedManager] multiplayerObject] setMultiplayerDelegate:self];
    
    //PEER 2 PEER
    //[[PeerServiceManager sharedInstance] setDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)addBanner{
    
    NSLog(@"CALL ADD BANNER ROOTVIEWCONTROLLER");
    if(!bannerViewAdded && ![MKStoreManager isFeaturePurchased:kFeatureAId]){
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        NSLog(@"ADD BANNER ROOTVIEWCONTROLLER");
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        
        // Create adMob ad View (note the use of various macros to detect device)
        if (IS_IPAD || IS_IPADHD) {
            bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLeaderboard];
            bannerView.center = CGPointMake(size.width/2, (size.height-CGRectGetHeight(bannerView.frame)/2));
        }
        else if (IS_IPHONE6) {
            bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
            
            bannerView.center = CGPointMake(size.width/2, (size.height-CGRectGetHeight(bannerView.frame)/2));
        }
        else if (IS_IPHONE6P) {
            bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
            bannerView.center = CGPointMake(size.width/2, (size.height-CGRectGetHeight(bannerView.frame)/2));
        }
        else {
            
            // boring old iPhones and iPod touches
            bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
            bannerView.center = CGPointMake(size.width/2, (size.height-CGRectGetHeight(bannerView.frame)/2));
        }
        
        // Replace this ad unit ID with your own ad unit ID.
        bannerView.adUnitID = kAdMobBanner;
        bannerView.rootViewController = self;
        bannerView.delegate = self;
        [self.view addSubview:bannerView];
        
        
        GADRequest *request = [GADRequest request];
        request.testDevices = @[ 
                                @"2077ef9a63d2b398840261c8221a0c9a" // Eric's iPod Touch
                                ,kGADSimulatorID ];
        //request.testDevices = @[  @"f2d9b38efed88e6ad06c611a44f6327a", @"9035b238964153d588d85fa109f842b1"  , kGADSimulatorID ];
        
        [bannerView loadRequest:request];
        bannerViewAdded = YES;
            }];
    }
}




- (void)removeBanner {
    if(bannerViewAdded){
        bannerViewAdded = NO;
        [bannerView removeFromSuperview];
        [bannerView release];
        bannerView = nil;
    }
    if(localBannerAdded){
        localBannerAdded = NO;
        [localBannerButton removeFromSuperview];
        [localBannerButton release];
        localBannerButton = nil;
    }
    if(flurryBannerAdded){
        [FlurryAds removeAdFromSpace:@"SoccerVirtualCup Banner" ];
        flurryBannerAdded = NO;
    }
}


- (void)addInterstitial{
    if(!interstitialViewAdded && ![MKStoreManager isFeaturePurchased:kFeatureAId]){
        NSLog(@"INIT INTERSTITIAL ROOTVIEWCONTROLLER");
        interstitialView =  [[GADInterstitial  alloc] initWithAdUnitID:kAdMobInterstitial];
        
        GADRequest *request = [GADRequest request];
        // Requests test ads on devices you specify. Your test device ID is printed to the console when
        // an ad request is made. GADBannerView automatically returns test ads when running on a
        // simulator.
        request.testDevices = @[
                                @"2077ef9a63d2b398840261c8221a0c9a" // Eric's iPod Touch
                                ,kGADSimulatorID ];
        //request.testDevices = @[ kGADSimulatorID, @"9035b238964153d588d85fa109f842b1", @"f2d9b38efed88e6ad06c611a44f6327a" ];
        [interstitialView loadRequest:request];
        [interstitialView setDelegate:self];
        
    }
}

- (void)adView:(GADBannerView *)gadBannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    NSLog(@"adView:didFailToReceiveAdWithError -------------------------------- : %@", error.localizedDescription);
    [self removeBanner];
    if(!localBannerAdded){
        
            CGSize size = [[CCDirector sharedDirector] winSize];
            localBannerButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
            localBannerButton.frame = CGRectMake(0.0, 0.0, 320.0, 50.0);
            [localBannerButton setTitle:@"DOWNLOAD MORE FREE GAMES" forState:UIControlStateNormal];
            localBannerButton.backgroundColor = [UIColor whiteColor];//[UIColor clearColor];
            [localBannerButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal ];
            
            [self.view addSubview:localBannerButton];
            [localBannerButton setCenter:CGPointMake(self.view.center.x,(size.height-CGRectGetHeight(localBannerButton.frame)/2)-2)];
            
            // Add Target-Action Pair
            [localBannerButton addTarget:self action:@selector(openAppStore:) forControlEvents:UIControlEventTouchUpInside];
            
            localBannerAdded = YES;
    }
    
}

- (void)openAppStore:(id)sender {
    NSLog(@"OPEN APP STORE");
    
    // Initialize Product View Controller
    //453546463
    //347634769
    
    
    
    if ([SKStoreProductViewController class] != nil) {
        NSLog(@"HAS CLASS APP STORE");
        PortraitStoreProductViewController *storeViewController = [[PortraitStoreProductViewController alloc] init];
        [storeViewController setDelegate:self];
        
        NSDictionary *parameters =
        @{SKStoreProductParameterITunesItemIdentifier:
              [NSNumber numberWithInteger:347634769]};
        
        
        [storeViewController loadProductWithParameters:parameters
                                       completionBlock:^(BOOL result, NSError *error) {
                                           NSLog(@"OPEN!!");
                                           if (result){
                                               [[(AppDelegate*)[[UIApplication sharedApplication] delegate] navController] presentViewController:storeViewController
                                                                                                                                        animated:YES
                                                                                                                                      completion:nil];
                                           }
                                       }];
        
    }else{
        NSLog(@"OPEN APP STORE");
    }
}

- (void) adInterstitialDidFetchAd:(FlurryAdInterstitial*)interstitialAd
{
    // you can choose to present the ad as soon as it is received
    NSLog(@"FLURRY adInterstitialDidFetchAd");
    [adInterstitial presentWithViewController:self];
}


//Informational callback invoked when there is an ad error
- (void) adInterstitial:(FlurryAdInterstitial*)interstitialAd
                adError:(FlurryAdError) adError
       errorDescription:(NSError*) errorDescription
{
    // @param interstitialAd The interstitial ad object associated with the error
    // @param adError an enum that gives the reason for the error.
    // @param errorDescription An error object that gives additional information on the cause of the ad error.
}





- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark GAD ADMOB
- (void)adViewDidReceiveAd:(GADBannerView *)gadBannerView {
    NSLog(@"AdViewDidReceiveAd ---------------------------");
    [gadBannerView setHidden:NO];
}


-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error{
    adInterstitial = [[FlurryAdInterstitial alloc] initWithSpace:@"MAIN_SCREEN_INTERSTITIAL"] ;
    adInterstitial.adDelegate = self;
    
    FlurryAdTargeting* adTargeting = [FlurryAdTargeting targeting];
    adTargeting.testAdsEnabled = YES;
    adInterstitial.targeting = adTargeting;
    
    [adInterstitial fetchAd];
    
    
    
    //[Chartboost showInterstitial:CBLocationHomeScreen];
    
    NSLog(@"interstitial didFailToReceiveAdWithError --------------------------- : %@",  error);
    
    
}
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    interstitialViewAdded = YES;
    [interstitialView presentFromRootViewController:self];
}



- (void)interstitialDidDismissScreen:sender{
    interstitialViewAdded = NO;
}









// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    //
    //
    // return YES for the supported orientations
    
    // Only landscape ?
    return ( UIInterfaceOrientationIsPortrait( interfaceOrientation ) );
    
    // Only portrait ?
    //	return ( ! UIInterfaceOrientationIsLandscape( interfaceOrientation ) );
    
    // All orientations ?
    //  return YES;
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
}



- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}



//GAME CENTER
/// Required Delegate Method called when the user needs to be authenticated using the GameCenter Login View Controller
- (void)gameCenterManager:(GameCenterManager *)manager authenticateUser:(UIViewController *)gameCenterLoginController{
    NSLog(@"GAME CENTER user needs to login");
}

/// Delegate Method called when the availability of GameCenter changes
- (void)gameCenterManager:(GameCenterManager *)manager availabilityChanged:(NSDictionary *)availabilityInformation
{
    NSLog(@"GAME CENTER availability of GameCenter changes");
}

/// Delegate Method called when the there is an error with GameCenter or GC Manager
- (void)gameCenterManager:(GameCenterManager *)manager error:(NSError *)error
{
    NSLog(@"GAME CENTER there is an error with GameCenter or GC Manager");
}
/// Sent to the delegate when a score is reported to GameCenter
- (void)gameCenterManager:(GameCenterManager *)manager reportedScore:(GKScore *)score withError:(NSError *)error
{
    NSLog(@"GAME CENTER when a score is reported to GameCenter");
}
/// Sent to the delegate when an achievement is reported to GameCenter
- (void)gameCenterManager:(GameCenterManager *)manager reportedAchievement:(GKAchievement *)achievement withError:(NSError *)error
{
    NSLog(@"GAME CENTER when an achievement is reported to GameCenter");
}
/// Sent to the delegate when an achievement is saved locally
- (void)gameCenterManager:(GameCenterManager *)manager didSaveAchievement:(GKAchievement *)achievement
{
    NSLog(@"GAME CENTER when an achievement is saved locally");
}
/// Sent to the delegate when a score is saved locally
- (void)gameCenterManager:(GameCenterManager *)manager didSaveScore:(GKScore *)score
{
    NSLog(@"GAME CENTER when a score is saved locally");
}
/// Sent to the delegate when the Game Center is synced
- (void)gameCenterManager:(GameCenterManager *)manager gameCenterSynced:(BOOL)synced
{
    NSLog(@"GAME CENTER when the Game Center is synced");
}

/// Sent to the delegate when the Game Center View Controller is On Screen
- (void)gameCenterManager:(GameCenterManager *)manager gameCenterViewControllerPresented:(BOOL)finished;
{
    NSLog(@"GAME CENTER when the Game Center View Controller is On Screen");
}
/// Sent to the delegate when the Game Center View Controller has been dismissed
- (void)gameCenterManager:(GameCenterManager *)manager gameCenterViewControllerDidFinish:(BOOL)finished;
{
    NSLog(@"GAME CENTER when the Game Center View Controller has been dismissed");
}


//Multiplayer

/// Sent to the delegate when a live multiplayer match begins
- (void)gameCenterManager:(GCMMultiplayer *)manager matchStarted:(GKMatch *)theMatch{
    NSLog(@"MULTIPLAYER match begin  %@", theMatch);
    match = theMatch;
    localManager = manager;
    [self sendCoinToss];
}


/// Sent to the delegate when a live multiplayer match ends
- (void)gameCenterManager:(GCMMultiplayer *)manager matchEnded:(GKMatch *)theMatch{
    //guarantee its the same match
    if(match!=theMatch) return;
    if(![[GlobalSingleton sharedInstance] gameEnd]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ops..."
                                                        message:@"The opponent has disconnected."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    match = nil;
    NSLog(@"MULTIPLAYER match end");
    
    
}

/// Sent to the delegate when data is recieved on the current device (sent from another player in the match)
- (void)gameCenterManager:(GCMMultiplayer *)manager match:(GKMatch *)theMatch didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID{
    if(match!=theMatch) return;
    NSLog(@"MULTIPLAYER GAME STATE %i",gameState );
    NSLog(@"MULTIPLAYER RECEIVE DATA: %@",data );
    
    InitInfoPacket rInitPacket;
    GameInfoPacket rGamePacket;
    GameInfoPacket rFullGamePacket;
    ConfirmPacket rConfirmPacket;
    HeartBeatPacket rHeartBeat;
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
    
    
    
    switch(gameState){
        case kStateCointoss:
            [data getBytes:&rInitPacket length:sizeof(rInitPacket)];
            
            int coinToss = (int)rInitPacket.gameUniqueID;
            int pTeamID = (int)rInitPacket.pTeamID;
            int matchWeather = (int)rInitPacket.matchWeather;
            int timeLeft = (int)rInitPacket.timeLeft;
            
            if(coinToss > uid){
                [mySingleton setIsP1:YES];
                [mySingleton setIsP2:NO];
                [mySingleton setSpritePlayer1:[mySingleton pTeamID]];
                [mySingleton setSpritePlayer2:pTeamID];
                [myCupSingleton setMatchWeather:matchWeather];
                [myCupSingleton setDefaultTimeLeft:timeLeft];
                NSLog(@"PLAYER 1 RECEIVED COIN TOSS PLAYER SELECTED - PLAYER 1 is SERVER");
            }else{
                [mySingleton setIsP1:NO];
                [mySingleton setIsP2:YES];
                [mySingleton setSpritePlayer1:pTeamID];
                [mySingleton setSpritePlayer2:[mySingleton pTeamID]];
                NSLog(@"PLAYER 2 RECEIVED COIN TOSS PLAYER SELECTED - PLAYER 2 is CLIENT :  team = %i",[mySingleton spritePlayer1]);
            }
            
            if([mySingleton isP1]){
                gameState = kStatePlayer2;
            }else{
                gameState = kStateFullPlay;
            }
            GameScene * gs = [GameScene node];
            [[CCDirector sharedDirector] replaceScene:gs];
            break;
            
        case kStatePlayer2:
            [data getBytes:&rHeartBeat length:sizeof(rHeartBeat)];
            if(rHeartBeat.beat == YES){
                break;
            }
            NSLog(@"PLAYER 1 RECEIVED PLAYER 2 PLAY");
            [mySingleton setHasSynced2:YES];
            
            [data getBytes:&rGamePacket length:sizeof(rGamePacket)];
            [mySingleton setP4ForceVector:CGPointMake(rGamePacket.p4ForceVectorX,rGamePacket.p4ForceVectorY)];
            [mySingleton setP5ForceVector:CGPointMake(rGamePacket.p5ForceVectorX,rGamePacket.p5ForceVectorY)];
            [mySingleton setP6ForceVector:CGPointMake(rGamePacket.p6ForceVectorX,rGamePacket.p6ForceVectorY)];
            
            
            
            break;
        case kStateFullPlay:
            [data getBytes:&rHeartBeat length:sizeof(rHeartBeat)];
            if(rHeartBeat.beat == YES){
                break;
            }
            
            
            [data getBytes:&rFullGamePacket length:sizeof(rFullGamePacket)];
            if([mySingleton isP1]){
                NSLog(@"PLAYER 1 RECEIVED FULL GAMEPLAY");
            }else{
                NSLog(@"PLAYER 2 RECEIVED FULL GAMEPLAY");
            }
            
            NSLog(@" VECTOR P1- received (%f, %f)" , rFullGamePacket.p1ForceVectorX, rFullGamePacket.p1ForceVectorX);
            NSLog(@" VECTOR P2- received (%f, %f)" , rFullGamePacket.p2ForceVectorX, rFullGamePacket.p2ForceVectorX);
            NSLog(@" VECTOR P3- received (%f, %f)" , rFullGamePacket.p3ForceVectorX, rFullGamePacket.p3ForceVectorX);
            NSLog(@" VECTOR P4- received (%f, %f)" , rFullGamePacket.p4ForceVectorX, rFullGamePacket.p4ForceVectorX);
            NSLog(@" VECTOR P5- received (%f, %f)" , rFullGamePacket.p5ForceVectorX, rFullGamePacket.p5ForceVectorX);
            NSLog(@" VECTOR P6- received (%f, %f)" , rFullGamePacket.p6ForceVectorX, rFullGamePacket.p6ForceVectorX);
            
            NSLog(@" POSITIONS P1- received (%f, %f)" , rFullGamePacket.p1PositionX, rFullGamePacket.p1PositionY);
            NSLog(@" POSITIONS P2- received (%f, %f)" , rFullGamePacket.p2PositionX, rFullGamePacket.p2PositionY);
            NSLog(@" POSITIONS P3- received (%f, %f)" , rFullGamePacket.p3PositionX, rFullGamePacket.p3PositionY);
            NSLog(@" POSITIONS P4- received (%f, %f)" , rFullGamePacket.p4PositionX, rFullGamePacket.p4PositionY);
            NSLog(@" POSITIONS P5- received (%f, %f)" , rFullGamePacket.p5PositionX, rFullGamePacket.p5PositionY);
            NSLog(@" POSITIONS P6- received (%f, %f)" , rFullGamePacket.p6PositionX, rFullGamePacket.p6PositionY);
            NSLog(@" POSITIONS BALL- received (%f, %f)" , rFullGamePacket.ballPositionX, rFullGamePacket.ballPositionY);
            
            NSLog(@" POSITIONS P1- local (%f , %f )" ,[mySingleton p1Position].x, [mySingleton p1Position].y);
            NSLog(@" POSITIONS P2- local (%f , %f )" ,[mySingleton p2Position].x, [mySingleton p2Position].y);
            NSLog(@" POSITIONS P3- local (%f , %f )" ,[mySingleton p3Position].x, [mySingleton p3Position].y);
            NSLog(@" POSITIONS P4- local (%f , %f )" ,[mySingleton p4Position].x, [mySingleton p4Position].y);
            NSLog(@" POSITIONS P5- local (%f , %f )" ,[mySingleton p5Position].x, [mySingleton p5Position].y);
            NSLog(@" POSITIONS P6- local (%f , %f )" ,[mySingleton p6Position].x, [mySingleton p6Position].y);
            NSLog(@" POSITIONS BALL- local (%f , %f )",[mySingleton ballPosition].x, [mySingleton ballPosition].y);
            
            [mySingleton setP1Position:CGPointMake(rFullGamePacket.p1PositionX, rFullGamePacket.p1PositionY)];
            [mySingleton setP2Position:CGPointMake(rFullGamePacket.p2PositionX, rFullGamePacket.p2PositionY)];
            [mySingleton setP3Position:CGPointMake(rFullGamePacket.p3PositionX, rFullGamePacket.p3PositionY)];
            [mySingleton setP4Position:CGPointMake(rFullGamePacket.p4PositionX, rFullGamePacket.p4PositionY)];
            [mySingleton setP5Position:CGPointMake(rFullGamePacket.p5PositionX, rFullGamePacket.p5PositionY)];
            [mySingleton setP6Position:CGPointMake(rFullGamePacket.p6PositionX, rFullGamePacket.p6PositionY)];
            [mySingleton setBallPosition:CGPointMake(rFullGamePacket.ballPositionX, rFullGamePacket.ballPositionY)];
            
            NSLog(@"HAS SYNCED PLAYER 2 : %f,%f",rFullGamePacket.p1ForceVectorX, rFullGamePacket.p1ForceVectorY );
            [mySingleton setP1ForceVector:CGPointMake(rFullGamePacket.p1ForceVectorX, rFullGamePacket.p1ForceVectorY)];
            [mySingleton setP2ForceVector:CGPointMake(rFullGamePacket.p2ForceVectorX, rFullGamePacket.p2ForceVectorY)];
            [mySingleton setP3ForceVector:CGPointMake(rFullGamePacket.p3ForceVectorX ,rFullGamePacket.p3ForceVectorY)];
            
            NSLog(@" HAS SYNCED PLAYER 1 : %f,%f",rFullGamePacket.p4ForceVectorX, rFullGamePacket.p4PositionY);
            [mySingleton setP4ForceVector:CGPointMake(rFullGamePacket.p4ForceVectorX,rFullGamePacket.p4ForceVectorY)];
            [mySingleton setP5ForceVector:CGPointMake(rFullGamePacket.p5ForceVectorX,rFullGamePacket.p5ForceVectorY)];
            [mySingleton setP6ForceVector:CGPointMake(rFullGamePacket.p6ForceVectorX,rFullGamePacket.p6ForceVectorY)];
            
            [mySingleton  setHasSynced1:YES];
            [mySingleton  setHasSynced2:YES];
            
            break;
        case kStateConfirm:
            
            if([mySingleton isP1]){
                NSLog(@"PLAYER 1 CONFIRM START PLAY: %i",rConfirmPacket.pId);
                
            }else{
                NSLog(@"PLAYER 2 CONFIRM START PLAY: %i",rConfirmPacket.pId);
                
            }
            [data getBytes:&rConfirmPacket length:sizeof(rConfirmPacket)];
            
            [mySingleton setHasSynced3:YES];
            break;
        case kStateReconnect:
            break;
        default:
            break;
            
    }
    
    // NSLog(@"match data received: %i,  from player: %@",rInitPacket.pTeamID, playerID);
}


- (CGPoint) roundPosition:(CGPoint)posi {
    CGPoint intPosi = CGPointMake(roundf( posi.x*100.0)/100.0, roundf( posi.y*100.0)/100.0 );
    NSLog(@" ROUND POSITIONS (%f , %f )" ,intPosi.x, intPosi.y);
    NSLog(@" POSITIONS (%f , %f )" ,posi.x, posi.y);
    return intPosi;
    //return posi;
}

- (float) roundNumber:(float)posi {
    float intPosi = roundf( posi*100.0)/100.0;
    NSLog(@" ROUND POSITIONS (%f  )" ,intPosi);
    NSLog(@" POSITIONS (%f  )" ,posi);
    return intPosi;
}

-(void)sendCoinToss{
    
    NSLog(@"sendCoinToss");
    playCounter = 0;
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
    
    
    gameState = kStateCointoss;
    
    uid = (int)[[NSString stringWithFormat:@"%@",[NSUUID UUID]] hash];
    initPacket.gameUniqueID = uid;
    initPacket.pTeamID = [mySingleton pTeamID];
    initPacket.matchWeather = [myCupSingleton matchWeather];
    initPacket.timeLeft = [myCupSingleton defaultTimeLeft];
    
    
    
    // make a NSData object
    NSData *initData = [NSData dataWithBytes:&initPacket length:sizeof(initPacket)];
    //send to other player
    NSLog(@"DATA1 LENGTH : %lu",(unsigned long)initData.length);
    if([mySingleton gameType]==2){
        for (GKPlayer *player in match.players) {
            NSLog(@"MULTIPLAYER GAME INIT PLAYER ID: %@ %@  == %@",player, player.playerID, [[GameCenterManager sharedManager] localPlayerId]);
            
            if(![player.playerID isEqualToString:[[GameCenterManager sharedManager] localPlayerId]]){
                [localManager sendMatchData:initData toPlayers:@[player] shouldSendQuickly:NO completion:^(BOOL success, NSError *error) {
                    if(success){
                        NSLog(@"MULTIPLAYER GAME INIT SUCCESS TRUE ");
                    }else{
                        NSLog(@"MULTIPLAYER GAME INIT SUCCESS FALSE %@",error);
                    }
                    if(error){
                        NSLog(@"MULTIPLAYER GAME INIT ERROR %@",error);
                    }
                }];
            }
            
        }
    }else if([mySingleton gameType]==22){
        //TODO BLUETOOTH HERE
        NSLog(@"SEND COINTOSS BLUETOOTH");
        [[PeerServiceManager sharedInstance] sendAllPlayersMatchData:initData shouldSendQuickly:NO completion:^(BOOL success, NSError *error) {
            if(success){
                NSLog(@"MULTIPEER GAME INIT SUCCESS TRUE ");
            }else{
                NSLog(@"MULTIPEER GAME INIT SUCCESS FALSE %@",error);
            }
            if(error){
                NSLog(@"MULTIPEER GAME INIT ERROR %@",error);
            }
        }];
    }
}



-(void)sendPlayer2GamePlay{
    
    NSLog(@"PLAYER 2 - sendPlayer2GamePlay");
    gameState = kStateFullPlay;
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    gamePacket.p1ForceVectorX = 0.0f;
    gamePacket.p1ForceVectorY = 0.0f;
    gamePacket.p2ForceVectorX = 0.0f;
    gamePacket.p2ForceVectorY = 0.0f;
    gamePacket.p3ForceVectorX = 0.0f;
    gamePacket.p3ForceVectorY = 0.0f;
    gamePacket.p4ForceVectorX = 0.0f;
    gamePacket.p4ForceVectorY = 0.0f;
    gamePacket.p5ForceVectorX = 0.0f;
    gamePacket.p5ForceVectorY = 0.0f;
    gamePacket.p6ForceVectorX = 0.0f;
    gamePacket.p6ForceVectorY = 0.0f;
    
    
    if([mySingleton p4ForceVector].x){
        gamePacket.p4ForceVectorX = [self roundNumber:[mySingleton p4ForceVector].x];
    }
    
    if([mySingleton p4ForceVector].y){
        gamePacket.p4ForceVectorY = [self roundNumber:[mySingleton p4ForceVector].y];
    }
    
    
    if([mySingleton p5ForceVector].x){
        gamePacket.p5ForceVectorX = [self roundNumber:[mySingleton p5ForceVector].x];
    }
    
    if([mySingleton p5ForceVector].y){
        gamePacket.p5ForceVectorY = [self roundNumber:[mySingleton p5ForceVector].y];
    }
    
    
    if([mySingleton p6ForceVector].x){
        gamePacket.p6ForceVectorX = [self roundNumber:[mySingleton p6ForceVector].x];
    }
    
    if([mySingleton p6ForceVector].y){
        gamePacket.p6ForceVectorY = [self roundNumber:[mySingleton p6ForceVector].y];
    }
    
    // make a NSData object
    NSData *gameData = [NSData dataWithBytes:&gamePacket length:sizeof(gamePacket)];
    //send to other player
    NSLog(@"DATA3 LENGTH : %lu",(unsigned long)gameData.length);
    if([mySingleton gameType]==2){
        [localManager sendMatchData:gameData toPlayers:match.players shouldSendQuickly:NO completion:^(BOOL success, NSError *error) {
            if(success){
                NSLog(@"MULTIPLAYER GAME PLAYER 2 SUCCESS TRUE ");
                //set has synced on p2
                [mySingleton setHasSent2:YES];
            }else{
                NSLog(@"MULTIPLAYER GAME PLAYER 2 SUCCESS FALSE %@",error);
            }
            if(error){
                NSLog(@"MULTIPLAYER GAME PLAYER 2 ERROR %@",error);
            }
        }];
        
    }else if([mySingleton gameType]==22){
        //TODO BLUETOOTH HERE
        [[PeerServiceManager sharedInstance] sendAllPlayersMatchData:gameData shouldSendQuickly:NO completion:^(BOOL success, NSError *error) {
            if(success){
                NSLog(@"MULTIPEER GAME PLAYER 2 SUCCESS TRUE ");
                //set has synced on p2
                [mySingleton setHasSent2:YES];
            }else{
                NSLog(@"MULTIPEER GAME PLAYER 2 SUCCESS FALSE %@",error);
            }
            if(error){
                NSLog(@"MULTIPEER GAME PLAYER 2 ERROR %@",error);
            }
        }];
    }
    
}

-(void)sendServerFullGamePlay{
    
    
    
    NSLog(@"PLAYER 1 - sendServerFullGamePlay");
    gameState = kStatePlayer2;
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    
    gamePacket.p1ForceVectorX = 0.0f;
    gamePacket.p1ForceVectorY = 0.0f;
    gamePacket.p2ForceVectorX = 0.0f;
    gamePacket.p2ForceVectorY = 0.0f;
    gamePacket.p3ForceVectorX = 0.0f;
    gamePacket.p3ForceVectorY = 0.0f;
    gamePacket.p4ForceVectorX = 0.0f;
    gamePacket.p4ForceVectorY = 0.0f;
    gamePacket.p5ForceVectorX = 0.0f;
    gamePacket.p5ForceVectorY = 0.0f;
    gamePacket.p6ForceVectorX = 0.0f;
    gamePacket.p6ForceVectorY = 0.0f;
    
    gamePacket.p1PositionX = [self roundNumber:[mySingleton p1Position].x];
    gamePacket.p1PositionY = [self roundNumber:[mySingleton p1Position].y];
    gamePacket.p2PositionX = [self roundNumber:[mySingleton p2Position].x];
    gamePacket.p2PositionY = [self roundNumber:[mySingleton p2Position].y];
    gamePacket.p3PositionX = [self roundNumber:[mySingleton p3Position].x];
    gamePacket.p3PositionY = [self roundNumber:[mySingleton p3Position].y];
    gamePacket.p4PositionX = [self roundNumber:[mySingleton p4Position].x];
    gamePacket.p4PositionY = [self roundNumber:[mySingleton p4Position].y];
    gamePacket.p5PositionX = [self roundNumber:[mySingleton p5Position].x];
    gamePacket.p5PositionY = [self roundNumber:[mySingleton p5Position].y];
    gamePacket.p6PositionX = [self roundNumber:[mySingleton p6Position].x];
    gamePacket.p6PositionY = [self roundNumber:[mySingleton p6Position].y];
    gamePacket.ballPositionX = [self roundNumber:[mySingleton ballPosition].x];
    gamePacket.ballPositionY = [self roundNumber:[mySingleton ballPosition].y];
    
    
    if([mySingleton p1ForceVector].x){
        gamePacket.p1ForceVectorX = [self roundNumber:[mySingleton p1ForceVector].x];
    }
    if([mySingleton p1ForceVector].y){
        gamePacket.p1ForceVectorY = [self roundNumber:[mySingleton p1ForceVector].y];
    }
    
    if([mySingleton p2ForceVector].x){
        gamePacket.p2ForceVectorX = [self roundNumber:[mySingleton p2ForceVector].x];
    }
    
    if([mySingleton p2ForceVector].y){
        gamePacket.p2ForceVectorY = [self roundNumber:[mySingleton p2ForceVector].y];
    }
    
    if([mySingleton p3ForceVector].x){
        gamePacket.p3ForceVectorX = [self roundNumber:[mySingleton p3ForceVector].x];
    }
    
    if([mySingleton p3ForceVector].y){
        gamePacket.p3ForceVectorY = [self roundNumber:[mySingleton p3ForceVector].y];
    }
    
    if([mySingleton p4ForceVector].x){
        gamePacket.p4ForceVectorX = [self roundNumber:[mySingleton p4ForceVector].x];
    }
    if([mySingleton p4ForceVector].y){
        gamePacket.p4ForceVectorY = [self roundNumber:[mySingleton p4ForceVector].y];
    }
    
    if([mySingleton p5ForceVector].x){
        gamePacket.p5ForceVectorX = [self roundNumber:[mySingleton p5ForceVector].x];
    }
    if([mySingleton p5ForceVector].y){
        gamePacket.p5ForceVectorY = [self roundNumber:[mySingleton p5ForceVector].y];
    }
    
    
    if([mySingleton p6ForceVector].x){
        gamePacket.p6ForceVectorX = [self roundNumber:[mySingleton p6ForceVector].x];
    }
    if([mySingleton p6ForceVector].y){
        gamePacket.p6ForceVectorY = [self roundNumber:[mySingleton p6ForceVector].y];
    }
    
    
    [mySingleton setP1Position:CGPointMake(gamePacket.p1PositionX, gamePacket.p1PositionY)];
    [mySingleton setP2Position:CGPointMake(gamePacket.p2PositionX, gamePacket.p2PositionY)];
    [mySingleton setP3Position:CGPointMake(gamePacket.p3PositionX, gamePacket.p3PositionY)];
    [mySingleton setP4Position:CGPointMake(gamePacket.p4PositionX, gamePacket.p4PositionY)];
    [mySingleton setP5Position:CGPointMake(gamePacket.p5PositionX, gamePacket.p5PositionY)];
    [mySingleton setP6Position:CGPointMake(gamePacket.p6PositionX, gamePacket.p6PositionY)];
    [mySingleton setBallPosition:CGPointMake(gamePacket.ballPositionX, gamePacket.ballPositionY)];
    
    NSLog(@"HAS SYNCED PLAYER 2 : %f,%f",gamePacket.p1ForceVectorX, gamePacket.p1ForceVectorY );
    [mySingleton setP1ForceVector:CGPointMake(gamePacket.p1ForceVectorX, gamePacket.p1ForceVectorY)];
    [mySingleton setP2ForceVector:CGPointMake(gamePacket.p2ForceVectorX, gamePacket.p2ForceVectorY)];
    [mySingleton setP3ForceVector:CGPointMake(gamePacket.p3ForceVectorX ,gamePacket.p3ForceVectorY)];
    
    NSLog(@" HAS SYNCED PLAYER 1 : %f,%f",gamePacket.p4ForceVectorX, gamePacket.p4PositionY);
    [mySingleton setP4ForceVector:CGPointMake(gamePacket.p4ForceVectorX,gamePacket.p4ForceVectorY)];
    [mySingleton setP5ForceVector:CGPointMake(gamePacket.p5ForceVectorX,gamePacket.p5ForceVectorY)];
    [mySingleton setP6ForceVector:CGPointMake(gamePacket.p6ForceVectorX,gamePacket.p6ForceVectorY)];
    
    
    
    NSLog(@" POSITIONS P1- sending (%f, %f)" ,gamePacket.p1PositionX, gamePacket.p1PositionY);
    NSLog(@" POSITIONS P2- sending (%f, %f)" ,gamePacket.p2PositionX, gamePacket.p2PositionY);
    NSLog(@" POSITIONS P3- sending (%f, %f)" ,gamePacket.p3PositionX, gamePacket.p3PositionY);
    NSLog(@" POSITIONS P4- sending (%f, %f)" ,gamePacket.p4PositionX, gamePacket.p4PositionY);
    NSLog(@" POSITIONS P5- sending (%f, %f)" ,gamePacket.p5PositionX, gamePacket.p5PositionY);
    NSLog(@" POSITIONS P6- sending (%f, %f)" ,gamePacket.p6PositionX, gamePacket.p6PositionY);
    NSLog(@" POSITIONS BALL- sending (%f, %f)",gamePacket.ballPositionX, gamePacket.ballPositionY);
    
    NSLog(@" VECTOR P1- sending (%f, %f)" ,gamePacket.p1ForceVectorX, gamePacket.p1ForceVectorY);
    NSLog(@" VECTOR P2- sending (%f, %f)" ,gamePacket.p2ForceVectorX, gamePacket.p2ForceVectorY);
    NSLog(@" VECTOR P3- sending (%f, %f)" ,gamePacket.p3ForceVectorX, gamePacket.p3ForceVectorY);
    NSLog(@" VECTOR P4- sending (%f, %f)" ,gamePacket.p4ForceVectorX, gamePacket.p4ForceVectorY);
    NSLog(@" VECTOR P5- sending (%f, %f)" ,gamePacket.p5ForceVectorX, gamePacket.p5ForceVectorY);
    NSLog(@" VECTOR P6- sending (%f, %f)" ,gamePacket.p6ForceVectorX, gamePacket.p6ForceVectorY);
    
    // make a NSData object
    NSData *gameData = [NSData dataWithBytes:&gamePacket length:sizeof(gamePacket)];
    //send to other player
    NSLog(@"DATA2 LENGTH : %lu",(unsigned long)gameData.length);
    if([mySingleton gameType]==2){
        [localManager sendMatchData:gameData toPlayers:match.players shouldSendQuickly:NO completion:^(BOOL success, NSError *error) {
            if(success){
                //set has synced on p1
                [mySingleton setHasSent1:YES];
                NSLog(@"MULTIPLAYER GAME FULL SUCCESS TRUE ");
                [mySingleton  setHasSynced1:YES];
                [mySingleton  setHasSynced2:YES];
            }else{
                NSLog(@"MULTIPLAYER GAME FULL SUCCESS FALSE %@",error);
            }
            if(error){
                NSLog(@"MULTIPLAYER GAME FULL ERROR %@",error);
            }
        }];
    }else if([mySingleton gameType]==22){
        //TODO BLUETOOTH HERE
        [[PeerServiceManager sharedInstance] sendAllPlayersMatchData:gameData shouldSendQuickly:NO completion:^(BOOL success, NSError *error) {
            if(success){
                //set has synced on p1
                [mySingleton setHasSent1:YES];
                NSLog(@"MULTIPEER GAME FULL SUCCESS TRUE ");
                [mySingleton  setHasSynced1:YES];
                [mySingleton  setHasSynced2:YES];
            }else{
                NSLog(@"MULTIPEER GAME FULL SUCCESS FALSE %@",error);
            }
            if(error){
                NSLog(@"MULTIPEER GAME FULL ERROR %@",error);
            }
        }];
    }
}

-(void)sendHeartBeat {
    heartBeatPacket.beat = YES;
    // make a NSData object
    NSData *gameData = [NSData dataWithBytes:&heartBeatPacket length:sizeof(heartBeatPacket)];
    //send to other player
    NSLog(@"HEARTBEAT LENGTH : %lu",(unsigned long)gameData.length);
    
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton gameType]==2){
        [localManager sendMatchData:gameData toPlayers:match.players shouldSendQuickly:YES completion:^(BOOL success, NSError *error) {
            if(success){
                NSLog(@"MULTIPLAYER GAME HEARTBEAT ");
                
            }else{
                NSLog(@"MULTIPLAYER GAME HEARTBEAT %@",error);
            }
            if(error){
                NSLog(@"MULTIPLAYER GAME HEARTBEAT ERROR %@",error);
            }
        }];
    }else if([mySingleton gameType]==22){
        //TODO BLUETOOTH HERE
        [[PeerServiceManager sharedInstance] sendAllPlayersMatchData:gameData shouldSendQuickly:YES completion:^(BOOL success, NSError *error) {
            if(success){
                NSLog(@"MULTIPEER GAME HEARTBEAT ");
                
            }else{
                NSLog(@"MULTIPEER GAME HEARTBEAT %@",error);
            }
            if(error){
                NSLog(@"MULTIPEER GAME HEARTBEAT ERROR %@",error);
            }
        }];

    }
}



#pragma mark PEER TO PEER
-(void)openBrowserViewController{
    NSLog(@"OPEN BROWSER VIEW CONTROLLER");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //[[CCDirector sharedDirector] stopAnimation];
        [[PeerServiceManager sharedInstance] startBrowsingAndAdvertising:self];
    });
}



#pragma mark PeerServiceManagerDelegate
-(void)connectedDevicesChanged:(PeerServiceManager*)peerServiceManager connectedDevices: (NSArray<NSString *>*)connectedDevices totalDevices:(int)totalDevices{
    
    if(totalDevices > 0){
        
            NSString * usernames = [connectedDevices componentsJoinedByString:@","];
            NSLog(@" CONNECTED DEVICES CHANGED :%@ ",usernames);
            //_connectionsLabel.text = [NSString stringWithFormat:@"Connections: %@",usernames];
        
    }else{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            //_connectionsLabel.text = [NSString stringWithFormat:@"Disconnected"];
        }];
    }
    
}

-(void)receiveData:(PeerServiceManager*)peerServiceManager didConnect:(BOOL)connected{
    if(connected){
        NSLog(@"SEND COIN TOSS");
        dispatch_async(dispatch_get_main_queue(), ^{
             [self sendCoinToss];
        });
    }
    
}

-(void)receiveData:(PeerServiceManager*)peerServiceManager didReceiveData: (NSData*) data{
   // [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
  //      NSLog(@"receiveData DATA : %@",data);
    InitInfoPacket rInitPacket;
    GameInfoPacket rGamePacket;
    GameInfoPacket rFullGamePacket;
    ConfirmPacket rConfirmPacket;
    HeartBeatPacket rHeartBeat;
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
    
    
    
    switch(gameState){
        case kStateCointoss:
            [data getBytes:&rInitPacket length:sizeof(rInitPacket)];
            NSLog(@" COIN TOSS RECEIVED : %@",data);
            NSLog(@" COIN TOSS RECEIVED gameUniqueID: %i",rInitPacket.gameUniqueID);
            NSLog(@" COIN TOSS RECEIVED pTeamID     : %i",rInitPacket.pTeamID);
            NSLog(@" COIN TOSS RECEIVED matchWeather: %i",rInitPacket.matchWeather);
            NSLog(@" COIN TOSS RECEIVED timeLeft    : %i",rInitPacket.timeLeft);
            int coinToss = rInitPacket.gameUniqueID;
            int pTeamID = rInitPacket.pTeamID;
            int matchWeather = rInitPacket.matchWeather;
            int timeLeft = rInitPacket.timeLeft;
            
            if(coinToss > uid){
                [mySingleton setIsP1:YES];
                [mySingleton setIsP2:NO];
                [mySingleton setSpritePlayer1:[mySingleton pTeamID]];
                [mySingleton setSpritePlayer2:pTeamID];
                [myCupSingleton setMatchWeather:matchWeather];
                [myCupSingleton setDefaultTimeLeft:timeLeft];
                NSLog(@"PLAYER 1 RECEIVED COIN TOSS PLAYER SELECTED - PLAYER 1 is SERVER");
            }else{
                [mySingleton setIsP1:NO];
                [mySingleton setIsP2:YES];
                [mySingleton setSpritePlayer1:pTeamID];
                [mySingleton setSpritePlayer2:[mySingleton pTeamID]];
                NSLog(@"PLAYER 2 RECEIVED COIN TOSS PLAYER SELECTED - PLAYER 2 is CLIENT :  team = %i",[mySingleton spritePlayer1]);
            }
            
            if([mySingleton isP1]){
                gameState = kStatePlayer2;
            }else{
                gameState = kStateFullPlay;
            }

            [[CCDirector sharedDirector] purgeCachedData];
            
            GameScene * gs = [GameScene node];
            [[CCDirector sharedDirector] replaceScene:gs];
            //[[CCDirector sharedDirector] popScene];
            
            
            break;
            
        case kStatePlayer2:
            [data getBytes:&rHeartBeat length:sizeof(rHeartBeat)];
            if(rHeartBeat.beat == YES){
                break;
            }
            NSLog(@"PLAYER 1 RECEIVED PLAYER 2 PLAY");
            [mySingleton setHasSynced2:YES];
            
            [data getBytes:&rGamePacket length:sizeof(rGamePacket)];
            [mySingleton setP4ForceVector:CGPointMake(rGamePacket.p4ForceVectorX,rGamePacket.p4ForceVectorY)];
            [mySingleton setP5ForceVector:CGPointMake(rGamePacket.p5ForceVectorX,rGamePacket.p5ForceVectorY)];
            [mySingleton setP6ForceVector:CGPointMake(rGamePacket.p6ForceVectorX,rGamePacket.p6ForceVectorY)];
            
            
            break;
        case kStateFullPlay:
            [data getBytes:&rHeartBeat length:sizeof(rHeartBeat)];
            if(rHeartBeat.beat == YES){
                break;
            }
            
            
            [data getBytes:&rFullGamePacket length:sizeof(rFullGamePacket)];
            if([mySingleton isP1]){
                NSLog(@"PLAYER 1 RECEIVED FULL GAMEPLAY");
            }else{
                NSLog(@"PLAYER 2 RECEIVED FULL GAMEPLAY");
            }
            
            NSLog(@" VECTOR P1- received (%f, %f)" , rFullGamePacket.p1ForceVectorX, rFullGamePacket.p1ForceVectorX);
            NSLog(@" VECTOR P2- received (%f, %f)" , rFullGamePacket.p2ForceVectorX, rFullGamePacket.p2ForceVectorX);
            NSLog(@" VECTOR P3- received (%f, %f)" , rFullGamePacket.p3ForceVectorX, rFullGamePacket.p3ForceVectorX);
            NSLog(@" VECTOR P4- received (%f, %f)" , rFullGamePacket.p4ForceVectorX, rFullGamePacket.p4ForceVectorX);
            NSLog(@" VECTOR P5- received (%f, %f)" , rFullGamePacket.p5ForceVectorX, rFullGamePacket.p5ForceVectorX);
            NSLog(@" VECTOR P6- received (%f, %f)" , rFullGamePacket.p6ForceVectorX, rFullGamePacket.p6ForceVectorX);
            
            NSLog(@" POSITIONS P1- received (%f, %f)" , rFullGamePacket.p1PositionX, rFullGamePacket.p1PositionY);
            NSLog(@" POSITIONS P2- received (%f, %f)" , rFullGamePacket.p2PositionX, rFullGamePacket.p2PositionY);
            NSLog(@" POSITIONS P3- received (%f, %f)" , rFullGamePacket.p3PositionX, rFullGamePacket.p3PositionY);
            NSLog(@" POSITIONS P4- received (%f, %f)" , rFullGamePacket.p4PositionX, rFullGamePacket.p4PositionY);
            NSLog(@" POSITIONS P5- received (%f, %f)" , rFullGamePacket.p5PositionX, rFullGamePacket.p5PositionY);
            NSLog(@" POSITIONS P6- received (%f, %f)" , rFullGamePacket.p6PositionX, rFullGamePacket.p6PositionY);
            NSLog(@" POSITIONS BALL- received (%f, %f)" , rFullGamePacket.ballPositionX, rFullGamePacket.ballPositionY);
            
            NSLog(@" POSITIONS P1- local (%f , %f )" ,[mySingleton p1Position].x, [mySingleton p1Position].y);
            NSLog(@" POSITIONS P2- local (%f , %f )" ,[mySingleton p2Position].x, [mySingleton p2Position].y);
            NSLog(@" POSITIONS P3- local (%f , %f )" ,[mySingleton p3Position].x, [mySingleton p3Position].y);
            NSLog(@" POSITIONS P4- local (%f , %f )" ,[mySingleton p4Position].x, [mySingleton p4Position].y);
            NSLog(@" POSITIONS P5- local (%f , %f )" ,[mySingleton p5Position].x, [mySingleton p5Position].y);
            NSLog(@" POSITIONS P6- local (%f , %f )" ,[mySingleton p6Position].x, [mySingleton p6Position].y);
            NSLog(@" POSITIONS BALL- local (%f , %f )",[mySingleton ballPosition].x, [mySingleton ballPosition].y);
            
            [mySingleton setP1Position:CGPointMake(rFullGamePacket.p1PositionX, rFullGamePacket.p1PositionY)];
            [mySingleton setP2Position:CGPointMake(rFullGamePacket.p2PositionX, rFullGamePacket.p2PositionY)];
            [mySingleton setP3Position:CGPointMake(rFullGamePacket.p3PositionX, rFullGamePacket.p3PositionY)];
            [mySingleton setP4Position:CGPointMake(rFullGamePacket.p4PositionX, rFullGamePacket.p4PositionY)];
            [mySingleton setP5Position:CGPointMake(rFullGamePacket.p5PositionX, rFullGamePacket.p5PositionY)];
            [mySingleton setP6Position:CGPointMake(rFullGamePacket.p6PositionX, rFullGamePacket.p6PositionY)];
            [mySingleton setBallPosition:CGPointMake(rFullGamePacket.ballPositionX, rFullGamePacket.ballPositionY)];
            
            NSLog(@"HAS SYNCED PLAYER 2 : %f,%f",rFullGamePacket.p1ForceVectorX, rFullGamePacket.p1ForceVectorY );
            [mySingleton setP1ForceVector:CGPointMake(rFullGamePacket.p1ForceVectorX, rFullGamePacket.p1ForceVectorY)];
            [mySingleton setP2ForceVector:CGPointMake(rFullGamePacket.p2ForceVectorX, rFullGamePacket.p2ForceVectorY)];
            [mySingleton setP3ForceVector:CGPointMake(rFullGamePacket.p3ForceVectorX ,rFullGamePacket.p3ForceVectorY)];
            
            NSLog(@" HAS SYNCED PLAYER 1 : %f,%f",rFullGamePacket.p4ForceVectorX, rFullGamePacket.p4PositionY);
            [mySingleton setP4ForceVector:CGPointMake(rFullGamePacket.p4ForceVectorX,rFullGamePacket.p4ForceVectorY)];
            [mySingleton setP5ForceVector:CGPointMake(rFullGamePacket.p5ForceVectorX,rFullGamePacket.p5ForceVectorY)];
            [mySingleton setP6ForceVector:CGPointMake(rFullGamePacket.p6ForceVectorX,rFullGamePacket.p6ForceVectorY)];
            
            [mySingleton  setHasSynced1:YES];
            [mySingleton  setHasSynced2:YES];
            
            break;
        case kStateConfirm:
            
            if([mySingleton isP1]){
                NSLog(@"PLAYER 1 CONFIRM START PLAY: %i",rConfirmPacket.pId);
                
            }else{
                NSLog(@"PLAYER 2 CONFIRM START PLAY: %i",rConfirmPacket.pId);
                
            }
            [data getBytes:&rConfirmPacket length:sizeof(rConfirmPacket)];
            
            [mySingleton setHasSynced3:YES];
            break;
        case kStateReconnect:
            break;
        default:
            break;
            
    }
   // }];

}




#pragma mark MCBrowserViewControllerDelegate
- (void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController{
   // [[CCDirector sharedDirector] startAnimation];
    [self popToRootViewControllerAnimated:NO];
    NSLog(@"Dismissed FINISH !!");
}

// Notifies delegate that the user taps the cancel button.
- (void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController{
    NSLog(@"Dismissed Cancelled !!");
    [[PeerServiceManager sharedInstance] disconnect];
    //[[CCDirector sharedDirector] startAnimation];
    [self popToRootViewControllerAnimated:NO];
    
}

@end
