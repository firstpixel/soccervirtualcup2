//
//  Team.h
//  SoccerVirtualCup2
//
//  Created by Gil Beyruth on 3/1/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Team : NSObject

@property (nonatomic) int index;
@property (nonatomic) int factor;
@property (nonatomic) int groupKey;
@property (nonatomic,retain) NSString *teamShortName;
@property (nonatomic,retain) NSString *teamName;
@property (nonatomic,retain) NSString *teamFlag;
@property (nonatomic,retain) NSString *teamPlayer1;
@property (nonatomic,retain) NSString *teamPlayer2;
@property (nonatomic,retain) NSString *teamButton;

+(id)setIndex:(int)_index setFactor:(int)_factor setGroupKey:(int)_groupKey setShortName:(NSString*)_teamShortName setName:(NSString*)_teamName setFlag:(NSString*)_teamFlag
        setP1:(NSString*)_teamPlayer1 setP2:(NSString*)_teamPlayer2 setButton:(NSString*)_teamButton;

-(id)setIndex:(int)_index setFactor:(int)_factor setGroupKey:(int)_groupKey setShortName:(NSString*)_teamShortName setName:(NSString*)_teamName setFlag:(NSString*)_teamFlag
        setP1:(NSString*)_teamPlayer1 setP2:(NSString*)_teamPlayer2 setButton:(NSString*)_teamButton;

@end
