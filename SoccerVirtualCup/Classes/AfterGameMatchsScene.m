//
//  PreGameMatchsScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/21/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "AfterGameMatchsScene.h"
#import "GlobalCupSingleton.h"
#import "GlobalSingleton.h"
#import "DataHolder.h"
#import "TeamGroupsScene.h"
#import "cocos2d.h"
#import "DeviceSettings.h"

@implementation AfterGameMatchsScene
- (id) init {
    self = [super init];
    if (self != nil) {
	
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"matchesresults_screen-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"matchesresults_screen-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"matchesresults_screen-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"matchesresults_screen-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"matchesresults_screen.jpg"];
                }
            }
        }

        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        
        [self addChild:bg z:0];
		[self addChild:[AfterGameMatchsSceneLayer node] z:1];
		
    }
    return self;
}
@end


@implementation AfterGameMatchsSceneLayer
@synthesize winner;

- (id) init {
    self = [super init];
	if (self != nil) {
        
		self.isTouchEnabled = YES;
		myDataHolder = [DataHolder sharedInstance];
		[myDataHolder clearData];
        [self setupBatchNode];
		
		GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
		
		//int groupId = [myDataHolder getGroupForTeamKey:[mySingleton spritePlayer1]];
		
		GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
		
		spriteGoPlay = [CCSprite spriteWithSpriteFrameName:@"go_bt.png"];
		[spriteGoPlay setPosition:ADJUST_CCP(ccp(240,46))];
		[self addChild:spriteGoPlay z:101 tag:101];
		
		//[self schedule: @selector(startDelayRestart:) interval: 20];
		NSArray* groupAKeys;
		NSNumber*g1;
		NSNumber*g2;
		NSNumber*g3;
		NSNumber*g4;
		//int winner;
		int p1Scores;
		int p2Scores;
		int tempMatchResult;
		if( [myCupSingleton atualMatch] < 3 ){
            
            
            
			groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:0]];
			g1=[groupAKeys objectAtIndex:0];
			g2=[groupAKeys objectAtIndex:1];
			g3=[groupAKeys objectAtIndex:2];
			g4=[groupAKeys objectAtIndex:3];
			
		/*	NSLog(@"G1: %@",[myDataHolder getTeamNameForKey:[g1 intValue]]);
			NSLog(@"G2: %@",[myDataHolder getTeamNameForKey:[g2 intValue]]);
			NSLog(@"G3: %@",[myDataHolder getTeamNameForKey:[g3 intValue]]);
			NSLog(@"G4: %@",[myDataHolder getTeamNameForKey:[g4 intValue]]);
			NSLog(@"TEAM NAME0 %@",[myDataHolder getTeamNameForKey:[g1 intValue]]);
            */
            
            
            
			if([myCupSingleton atualMatch]==0){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g2 intValue]];
					p1Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
					
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];				
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = (int)[myCupSingleton setMatchWinner:[g1 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P2Array:0 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P1Array:0 ToValue:tempMatchResult];

				[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:130 posiY:247 p1Score:p1Scores p2Score:p2Scores];
				if([g3 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = (int)[myCupSingleton setRandomMatchWinner:[g3 intValue] indexP2:[g4 intValue]];
					p1Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
					p2Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g3 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = (int)[myCupSingleton setMatchWinner:[g3 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP3P4Array:0 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P3Array:0 ToValue:tempMatchResult];
				[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:130 posiY:222 p1Score:p1Scores p2Score:p2Scores];
                
                
                
                
			}else if([myCupSingleton atualMatch]==1){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = (int)[myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g3 intValue]];
					p1Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = (int)[myCupSingleton setMatchWinner:[g1 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];
					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P3Array:0 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P1Array:0 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:130 posiY:247 p1Score:p1Scores p2Score:p2Scores];
				if([g4 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = (int)[myCupSingleton setRandomMatchWinner:[g4 intValue] indexP2:[g2 intValue]];
					p1Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
					p2Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g4 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = (int)[myCupSingleton setMatchWinner:[g4 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP4P2Array:0 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P4Array:0 ToValue:tempMatchResult];
				[self displayMatch:[g4 intValue] versus:[g2 intValue] posiX:130 posiY:222 p1Score:p1Scores p2Score:p2Scores];
                
                
                
                
                
			}else if([myCupSingleton atualMatch]==2){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = (int)[myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g4 intValue]];
					p1Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = (int)[myCupSingleton setMatchWinner:[g1 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P4Array:0 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P1Array:0 ToValue:tempMatchResult];
				
				[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:130 posiY:247 p1Score:p1Scores p2Score:p2Scores];
				if([g2 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = (int)[myCupSingleton setRandomMatchWinner:[g2 intValue] indexP2:[g3 intValue]];
					p1Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
					p2Scores = (int)[myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g2 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = (int)[myCupSingleton setMatchWinner:[g2 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP2P3Array:0 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P2Array:0 ToValue:tempMatchResult];
				[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:130 posiY:222 p1Score:p1Scores p2Score:p2Scores];
			}
            
            
			
			
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:1]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
			if([myCupSingleton atualMatch]==0){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];			
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P2Array:1 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P1Array:1 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:130 posiY:197 p1Score:p1Scores p2Score:p2Scores];
				if([g3 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g3 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g3 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g3 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP3P4Array:1 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P3Array:1 ToValue:tempMatchResult];
				
				[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:130 posiY:172 p1Score:p1Scores p2Score:p2Scores];
			}else if([myCupSingleton atualMatch]==1){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P3Array:1 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P1Array:1 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:130 posiY:197 p1Score:p1Scores p2Score:p2Scores];
				if([g4 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g4 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g4 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g4 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP4P2Array:1 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P4Array:1 ToValue:tempMatchResult];
				
				[self displayMatch:[g4 intValue] versus:[g2 intValue] posiX:130 posiY:172 p1Score:p1Scores p2Score:p2Scores];
			}else if([myCupSingleton atualMatch]==2){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P4Array:1 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P1Array:1 ToValue:tempMatchResult];
				
				[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:130 posiY:197 p1Score:p1Scores p2Score:p2Scores];
				if([g2 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g2 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g2 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g2 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP2P3Array:1 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P2Array:1 ToValue:tempMatchResult];
				[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:130 posiY:172 p1Score:p1Scores p2Score:p2Scores];
			}
			
			
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:2]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		
            NSLog(@"TEAM NAME2 %@",[myDataHolder getTeamNameForKey:[g1 intValue]]);
			if([myCupSingleton atualMatch]==0){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P2Array:2 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P1Array:2 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:130 posiY:147 p1Score:p1Scores p2Score:p2Scores];
				if([g3 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g3 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g3 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g3 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];			
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP3P4Array:2 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P3Array:2 ToValue:tempMatchResult];
				
				[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:130 posiY:122 p1Score:p1Scores p2Score:p2Scores];
			}else if([myCupSingleton atualMatch]==1){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P3Array:2 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P1Array:2 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:130 posiY:147 p1Score:p1Scores p2Score:p2Scores];
				if([g4 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g4 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g4 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g4 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP4P2Array:2 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P4Array:2 ToValue:tempMatchResult];
				
				[self displayMatch:[g4 intValue] versus:[g2 intValue] posiX:130 posiY:122 p1Score:p1Scores p2Score:p2Scores];
			}else if([myCupSingleton atualMatch]==2){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P4Array:2 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P1Array:2 ToValue:tempMatchResult];
				
				[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:130 posiY:147 p1Score:p1Scores p2Score:p2Scores];
				if([g2 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g2 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g2 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g2 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP2P3Array:2 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P2Array:2 ToValue:tempMatchResult];
				[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:130 posiY:122 p1Score:p1Scores p2Score:p2Scores];
			}
			
			
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:3]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		NSLog(@"TEAM NAME3 %@",[myDataHolder getTeamNameForKey:[g1 intValue]]);
			if([myCupSingleton atualMatch]==0){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}	
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P2Array:3 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P1Array:3 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:130 posiY:97 p1Score:p1Scores p2Score:p2Scores];

				
				if([g3 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g3 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g3 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g3 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP3P4Array:3 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P3Array:3 ToValue:tempMatchResult];
				[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:130 posiY:72 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==1){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];					
					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P3Array:3 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P1Array:3 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:130 posiY:97 p1Score:p1Scores p2Score:p2Scores];
				
				if([g4 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g4 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g4 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g4 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];				
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP4P2Array:3 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P4Array:3 ToValue:tempMatchResult];
				
				[self displayMatch:[g4 intValue] versus:[g2 intValue] posiX:130 posiY:72 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==2){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P4Array:3 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P1Array:3 ToValue:tempMatchResult];
				
				[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:130 posiY:97 p1Score:p1Scores p2Score:p2Scores];
				
				if([g2 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g2 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g2 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g2 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];					
					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP2P3Array:3 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P2Array:3 ToValue:tempMatchResult];
				[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:130 posiY:72 p1Score:p1Scores p2Score:p2Scores];
				
			}
			
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:4]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		NSLog(@"TEAM NAME4 %@",[myDataHolder getTeamNameForKey:[g1 intValue]]);
			if([myCupSingleton atualMatch]==0){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P2Array:4 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P1Array:4 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:350 posiY:247 p1Score:p1Scores p2Score:p2Scores];
				
				if([g3 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g3 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g3 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g3 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];					
						
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP3P4Array:4 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P3Array:4 ToValue:tempMatchResult];
				
				[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:350 posiY:222 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==1){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];					
					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P3Array:4 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P1Array:4 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:350 posiY:247 p1Score:p1Scores p2Score:p2Scores];
				
				if([g4 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g4 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g4 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g4 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP4P2Array:4 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P4Array:4 ToValue:tempMatchResult];
				
				[self displayMatch:[g4 intValue] versus:[g2 intValue] posiX:350 posiY:222 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==2){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];	
					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P4Array:4 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P1Array:4 ToValue:tempMatchResult];
				
				[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:350 posiY:247 p1Score:p1Scores p2Score:p2Scores];
				
				if([g2 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g2 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g2 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g2 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP2P3Array:4 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P2Array:4 ToValue:tempMatchResult];
				[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:350 posiY:222 p1Score:p1Scores p2Score:p2Scores];
				
			}
			
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:5]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		NSLog(@"TEAM NAME5 %@",[myDataHolder getTeamNameForKey:[g1 intValue]]);
			if([myCupSingleton atualMatch]==0){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];		
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P2Array:5 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P1Array:5 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:350 posiY:197 p1Score:p1Scores p2Score:p2Scores];
				
				if([g3 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g3 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g3 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g3 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP3P4Array:5 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P3Array:5 ToValue:tempMatchResult];
				
				[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:350 posiY:172 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==1){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P3Array:5 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P1Array:5 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:350 posiY:197 p1Score:p1Scores p2Score:p2Scores];
				
				if([g4 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g4 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g4 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g4 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP4P2Array:5 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P4Array:5 ToValue:tempMatchResult];
				
				[self displayMatch:[g4 intValue] versus:[g2 intValue] posiX:350 posiY:172 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==2){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P4Array:5 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P1Array:5 ToValue:tempMatchResult];
				
				[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:350 posiY:197 p1Score:p1Scores p2Score:p2Scores];
				
				if([g2 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g2 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g2 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g2 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP2P3Array:5 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P2Array:5 ToValue:tempMatchResult];
				[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:350 posiY:172 p1Score:p1Scores p2Score:p2Scores];
				
			}	
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:6]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		NSLog(@"TEAM NAME0 %@",[myDataHolder getTeamNameForKey:[g1 intValue]]);
			if([myCupSingleton atualMatch]==0){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];		
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P2Array:6 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P1Array:6 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:350 posiY:147 p1Score:p1Scores p2Score:p2Scores];
				
				
				if([g3 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g3 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g3 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g3 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];		
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP3P4Array:6 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P3Array:6 ToValue:tempMatchResult];
				
				[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:350 posiY:122 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==1){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P3Array:6 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P1Array:6 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:350 posiY:147 p1Score:p1Scores p2Score:p2Scores];
				if([g4 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g4 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g4 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g4 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP4P2Array:6 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P4Array:6 ToValue:tempMatchResult];
				
				[self displayMatch:[g4 intValue] versus:[g2 intValue] posiX:350 posiY:122 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==2){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];						
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P4Array:6 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P1Array:6 ToValue:tempMatchResult];
				
				[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:350 posiY:147 p1Score:p1Scores p2Score:p2Scores];
				if([g2 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g2 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g2 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g2 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];					
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP2P3Array:6 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P2Array:6 ToValue:tempMatchResult];
				[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:350 posiY:122 p1Score:p1Scores p2Score:p2Scores];
				
			}
			
			
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:7]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		NSLog(@"TEAM NAME7 %@",[myDataHolder getTeamNameForKey:[g1 intValue]]);
			if([myCupSingleton atualMatch]==0){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];		
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P2Array:7 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P1Array:7 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:350 posiY:97 p1Score:p1Scores p2Score:p2Scores];
				
				if([g3 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g3 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g3 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g3 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];		
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP3P4Array:7 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P3Array:7 ToValue:tempMatchResult];
				[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:350 posiY:72 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==1){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P3Array:7 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P1Array:7 ToValue:tempMatchResult];
				[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:350 posiY:97 p1Score:p1Scores p2Score:p2Scores];
				
				if([g4 intValue] != [mySingleton spritePlayer1] && [g2 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g4 intValue] indexP2:[g2 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
				}else{
					if([g4 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g4 intValue] player2:[g2 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP4P2Array:7 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP2P4Array:7 ToValue:tempMatchResult];
				
				[self displayMatch:[g4 intValue] versus:[g2 intValue] posiX:350 posiY:72 p1Score:p1Scores p2Score:p2Scores];
				
			}else if([myCupSingleton atualMatch]==2){
				if([g1 intValue] != [mySingleton spritePlayer1] && [g4 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g1 intValue] indexP2:[g4 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g1 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g4 intValue]];
				}else{
					if([g1 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g1 intValue] player2:[g4 intValue] goalp1:p1Scores goalp2:p2Scores];						
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP1P4Array:7 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP4P1Array:7 ToValue:tempMatchResult];
				
				[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:350 posiY:97 p1Score:p1Scores p2Score:p2Scores];
				
				if([g2 intValue] != [mySingleton spritePlayer1] && [g3 intValue] != [mySingleton spritePlayer1]){
					winner = [myCupSingleton setRandomMatchWinner:[g2 intValue] indexP2:[g3 intValue]];
					p1Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g2 intValue]];
					p2Scores = [myCupSingleton getLastMatchScoreByTeamKey:[g3 intValue]];
				}else{
					if([g2 intValue] == [mySingleton spritePlayer1]){
						p1Scores = [mySingleton currentScoreP1];
						p2Scores = [mySingleton currentScoreP2];
						
					}else{
						p1Scores = [mySingleton currentScoreP2];
						p2Scores = [mySingleton currentScoreP1];					
					}
					winner = [myCupSingleton setMatchWinner:[g2 intValue] player2:[g3 intValue] goalp1:p1Scores goalp2:p2Scores];	
				}
				tempMatchResult = [self setMatchResult:p1Scores score2:p2Scores];
				[myCupSingleton setP2P3Array:7 ToValue:tempMatchResult];
				tempMatchResult = [self setMatchResult:p2Scores score2:p1Scores];
				[myCupSingleton setP3P2Array:7 ToValue:tempMatchResult];
				[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:350 posiY:72 p1Score:p1Scores p2Score:p2Scores];
				
			}
	//matchs >3 (first rounds)
	}
		[myCupSingleton setAtualMatch:[myCupSingleton atualMatch]+1];
		[myCupSingleton saveGameData];
	}

	return self;
}

-(int)setMatchResult:(int)score1 score2:(int)score2{
	if(score1>score2){
		return 1;
	}else if(score1<score2){
		return 2;
	}else{
		return 3;
	}
	
}




-(void)displayMatch:(int)t1 versus:(int)t2 posiX:(float)x1 posiY:(float)y1 p1Score:(int)p1Score p2Score:(int)p2Score{
    x1 = ADJUST_X(x1);
	y1 = ADJUST_Y(y1);
    
	CCLabelTTF *labelTeam1 = [CCLabelTTF labelWithString:[myDataHolder getTeamShortNameForKey:(int)t1] fontName:@"Arial" fontSize:HD_PIXELS(16)];
	[labelTeam1 setColor:ccWHITE];
    [labelTeam1 setContentSize:CGSizeMake(20, 20)];
    [labelTeam1 setHorizontalAlignment:kCCTextAlignmentRight];
	[self addChild: labelTeam1 z:10];
    [labelTeam1 setPosition:ccp(x1-HD_PIXELS(65), y1)];
	
	CCSprite* spriteFlag1 = [CCSprite spriteWithSpriteFrameName:[myDataHolder getFlagNameForKey:(int)t1]];
	[spriteFlag1 setPosition: ccp(x1-HD_PIXELS(35) ,y1)];
	[self addChild: spriteFlag1 z:20];
	
    
    CCLabelTTF *p1AtlasScore = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i",p1Score] fontName:@"Arial" fontSize:HD_PIXELS(16)];
    [p1AtlasScore setColor:ccWHITE];
    [p1AtlasScore setContentSize:CGSizeMake(20, 20)];
    [p1AtlasScore setHorizontalAlignment:kCCTextAlignmentRight];
    [self addChild: p1AtlasScore z:10];
    [p1AtlasScore setPosition:ccp(x1-HD_PIXELS(15),y1)];
    
    
    
    
    CCLabelTTF *versusFont = [CCLabelTTF labelWithString:@"x" fontName:@"Arial" fontSize:HD_PIXELS(16)];
    [versusFont setPosition:ccp(x1, y1)];
	[versusFont setColor:ccWHITE];
	[self addChild:versusFont z:21 tag:21 ];
    
    
    
    
    CCLabelTTF *p2AtlasScore = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i",p2Score]  fontName:@"Arial" fontSize:HD_PIXELS(16)];
    [p2AtlasScore setColor:ccWHITE];
    [p2AtlasScore setContentSize:CGSizeMake(20, 20)];
    [p2AtlasScore setHorizontalAlignment:kCCTextAlignmentLeft];
    [self addChild: p2AtlasScore z:10];
    [p2AtlasScore setPosition:ccp(x1 + HD_PIXELS(20),y1)];
    
	CCSprite* spriteFlag2 = [CCSprite spriteWithSpriteFrameName:[myDataHolder getFlagNameForKey:(int)t2]];
	[spriteFlag2 setPosition:  ccp(x1+HD_PIXELS(35),y1)];
	[self addChild: spriteFlag2 z:40];
		
	
    CCLabelTTF *labelTeam2 = [CCLabelTTF labelWithString:[myDataHolder getTeamShortNameForKey:(int)t2] fontName:@"Arial" fontSize:HD_PIXELS(16)];
	[labelTeam2 setContentSize:CGSizeMake(20, 20)];
    [labelTeam2 setColor:ccWHITE];
    [labelTeam2 setHorizontalAlignment:kCCTextAlignmentLeft];
	[labelTeam2 setPosition: cpv(x1+HD_PIXELS(65),y1)];
	[self addChild: labelTeam2 z:30];
    
	
}
-(void)startDelayLoading:(ccTime)ts{
	[self goPlay];
}
-(void)loadingScreen{
    
    //CCSprite * loading = nil;
    
    
    CCSprite * bg = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            bg = [CCSprite spriteWithFile:@"loading-ipadhd.jpg"];
        }else{
            //iPad screen
            bg = [CCSprite spriteWithFile:@"loading-ipad.jpg"];
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                    //iphone 5
                    bg = [CCSprite spriteWithFile:@"loading-hd.jpg"];
                }else{
                    //iphone retina screen
                    bg = [CCSprite spriteWithFile:@"loading-hd.jpg"];
                }
            }else{
                //iphone screen
                bg = [CCSprite spriteWithFile:@"loading.jpg"];
            }
        }
    }
    [bg setPosition:ADJUST_CCP(ccp(240, 160))];
    
	[self addChild:bg z:5000];
}
-(void)goPlay{
	
	TeamGroupsScene * gs = [TeamGroupsScene node];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:gs]];

}


///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Touch Events ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	
	if(CGRectContainsPoint(CGRectMake(spriteGoPlay.position.x- 0.5*spriteGoPlay.contentSize.width, spriteGoPlay.position.y- 0.5*spriteGoPlay.contentSize.height, spriteGoPlay.contentSize.width, spriteGoPlay.contentSize.height), location)) {
		[self loadingScreen];
		[self schedule: @selector(startDelayLoading:) interval: 1];
	}
	//return kEventHandled;	
}


- (void)setupBatchNode {
    NSString *spritesheetPvrCcz = @"spritesheet.pvr.ccz";
    NSString *spritesheetPlist = @"spritesheet.plist";
    
    NSString *gametablePvrCcz = @"gametable.pvr.ccz";
    NSString *gametablePlist = @"gametable.plist";
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            spritesheetPvrCcz = @"spritesheet-ipadhd.pvr.ccz";
            spritesheetPlist = @"spritesheet-ipadhd.plist";
            gametablePvrCcz = @"gametable-ipadhd.pvr.ccz";
            gametablePlist = @"gametable-ipadhd.plist";
        }else{
            //iPad screen
            spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
            spritesheetPlist = @"spritesheet-hd.plist";
            gametablePvrCcz = @"gametable-hd.pvr.ccz";
            gametablePlist = @"gametable-hd.plist";
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
                spritesheetPlist = @"spritesheet-hd.plist";
                
                gametablePvrCcz = @"gametable-hd.pvr.ccz";
                gametablePlist = @"gametable-hd.plist";
            }else{
                //iphone screen
                spritesheetPvrCcz = @"spritesheet.pvr.ccz";
                spritesheetPlist = @"spritesheet.plist";
                
                gametablePvrCcz = @"gametable.pvr.ccz";
                gametablePlist = @"gametable.plist";
            }
        }
    }
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesheetPvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesheetPlist];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:gametablePvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:gametablePlist];
}

@end

