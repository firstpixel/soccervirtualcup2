//
//  DataHolder.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/12/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "chipmunk.h"
#import "Team.h"

@interface DataHolder : NSObject {

    
    
    Team *teamObject;
    
	//TEAMS
    NSArray *teamObjectArray;
	NSArray *groupsKeys;
	NSArray *teamGroupsObject;
	
	
	/*NSMutableArray *p1p2Array;
	NSMutableArray *p1p3Array;
	NSMutableArray *p1p4Array;
	
	NSMutableArray *p2p1Array;
	NSMutableArray *p2p3Array;
	NSMutableArray *p2p4Array;
	
	NSMutableArray *p3p1Array;
	NSMutableArray *p3p2Array;
	NSMutableArray *p3p4Array;
	
	NSMutableArray *p4p1Array;
	NSMutableArray *p4p2Array;
	NSMutableArray *p4p3Array; */
	
}
/*
@property (nonatomic,retain) NSMutableArray *p1p2Array;
@property (nonatomic,retain) NSMutableArray *p1p3Array;
@property (nonatomic,retain) NSMutableArray *p1p4Array;
@property (nonatomic,retain) NSMutableArray *p2p1Array;
@property (nonatomic,retain) NSMutableArray *p2p3Array;
@property (nonatomic,retain) NSMutableArray *p2p4Array;
@property (nonatomic,retain) NSMutableArray *p3p1Array;
@property (nonatomic,retain) NSMutableArray *p3p2Array;
@property (nonatomic,retain) NSMutableArray *p3p4Array;
@property (nonatomic,retain) NSMutableArray *p4p1Array;
@property (nonatomic,retain) NSMutableArray *p4p2Array;
@property (nonatomic,retain) NSMutableArray *p4p3Array;
*/

@property (nonatomic, retain) Team *teamObject;

@property (nonatomic,retain) NSArray *teamObjectArray;
@property (nonatomic,retain) NSArray *teamGroupsObject;
@property (nonatomic,retain) NSArray *groupsKeys;

-(void)clearData;

+ (DataHolder *) sharedInstance;

-(NSString*)getTeamNameForKey:(int)key;
-(NSString*)getTeamShortNameForKey:(int)key;
-(NSString*)getFlagNameForKey:(int)key;
-(NSString*)getImageP1ForKey:(int)key;
-(NSString*)getImageP2ForKey:(int)key;
-(NSString*)getImageBtForKey:(int)key;
-(NSArray*)getIdForGroupKey:(NSNumber*)key;
-(NSArray*)setMatchArray:(NSMutableArray*)array setObject:(NSNumber*)state atIndex:(NSUInteger)index;
-(int)getMatchArray:(NSMutableArray*)array atIndex:(NSUInteger)index;
//-(NSArray*)setPointsArrayValue:(NSNumber*)value atIndex:(NSUInteger)index;
-(int)getPointsArrayAtIndex:(NSUInteger)index;
-(int)getGroupForTeamKey:(int)key;
-(int)getFactorByTeamKey:(int)key;

@end
