/*
 * cocos2d for iPhone: http://www.cocos2d-iphone.org
 *
 * Copyright (c) 2008-2011 Ricardo Quesada
 * Copyright (c) 2011 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#import "CCNode.h"
#import "CCProtocols.h"

@class CCSprite;
@class CCSpriteFrame;
@class CCLabelTTF;

#define kCCItemSize 32

#pragma mark -
#pragma mark LoopingMenuItem
/** CCMenuItem base class
 *
 *  Subclass CCMenuItem (or any subclass) to create your custom CCMenuItem objects.
 */
@interface LoopingMenuItem : CCNode
{
	// used for menu items using a block
	void (^block_)(id sender);

	BOOL isEnabled_;
	BOOL isSelected_;
}

/** returns whether or not the item is selected
@since v0.8.2
*/
@property (nonatomic,readonly) BOOL isSelected;

/** Creates a CCMenuItem with a target/selector.
 target/selector will be implemented using blocks.
 "target" won't be retained.
 */
+(id) itemWithTarget:(id)target selector:(SEL)selector;

/** Creates a CCMenuItem with the specified block.
 The block will be "copied".
 */
+(id) itemWithBlock:(void(^)(id sender))block;

/** Initializes a CCMenuItem with a target/selector */
-(id) initWithTarget:(id)target selector:(SEL)selector;

/** Initializes a CCMenuItem with the specified block.
 The block will be "copied".
*/
-(id) initWithBlock:(void(^)(id sender))block;

/** Returns the outside box in points */
-(CGRect) rect;

/** Activate the item */
-(void) activate;

/** The item was selected (not activated), similar to "mouse-over" */
-(void) selected;

/** The item was unselected */
-(void) unselected;

/** Enable or disabled the CCMenuItem */
-(void) setIsEnabled:(BOOL)enabled;

/** Returns whether or not the CCMenuItem is enabled */
-(BOOL) isEnabled;

/** Sets the block that is called when the item is tapped.
 The block will be "copied".
 */
-(void) setBlock:(void(^)(id sender))block;

/** Sets the target and selector that is called when the item is tapped.
 target/selector will be implemented using blocks.
 "target" won't be retained.
 */
-(void) setTarget:(id)target selector:(SEL)selector;

/** cleanup event. It will release the block and call [super cleanup] */
-(void) cleanup;

@end

#pragma mark -
#pragma mark LoopingMenuItemSprite

/** CCMenuItemSprite accepts CCNode objects as items.
 The images has 3 different states:
 - unselected image
 - selected image
 - disabled image

 @since v0.8.0
 */
@interface LoopingMenuItemSprite : LoopingMenuItem
{
    NSString *title_;
	CCNode *normalImage_, *selectedImage_, *disabledImage_;
    CCLabelTTF* titleLabel;
}

// weak references

/** the image used when the item is not selected */
@property (nonatomic,readwrite,retain) NSString *title;
/** the image used when the item is not selected */
@property (nonatomic,readwrite,retain) CCNode *normalImage;
/** the image used when the item is selected */
@property (nonatomic,readwrite,retain) CCNode *selectedImage;
/** the image used when the item is disabled */
@property (nonatomic,readwrite,retain) CCNode *disabledImage;
/** the label used when the item is cclabel */
@property (nonatomic,readwrite,retain) CCLabelTTF *titleLabel;

/** creates a menu item with a normal and selected image*/
+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite;
/** creates a menu item with a normal and selected image with target/selector.
 The "target" won't be retained.
 */
+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite target:(id)target selector:(SEL)selector;

/** creates a menu item with a normal,selected  and disabled image with target/selector.
 The "target" won't be retained.
 */
+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite target:(id)target selector:(SEL)selector;

/** creates a menu item with a normal,selected  and disabled image with target/selector.
 The "target" won't be retained.
 */
+(id) itemWithTitle:(NSString*)title normalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite target:(id)target selector:(SEL)selector;

/** creates a menu item with a normal and selected image with a block.
 The block will be "copied".
 */
+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite block:(void(^)(id sender))block;

/** creates a menu item with a normal,selected  and disabled image with a block.
 The block will be "copied".
 */
+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite block:(void(^)(id sender))block;

/** creates a menu item with a normal,selected  and disabled image with a block.
 The block will be "copied".
 */
+(id) itemWithTitle:(NSString*)title normalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite block:(void(^)(id sender))block;

/** initializes a menu item with a normal, selected  and disabled image with target/selector.
 The "target" won't be retained.
 */
-(id) initWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite target:(id)target selector:(SEL)selector;


/** initializes a menu item with a normal, selected  and disabled image with target/selector.
 The "target" won't be retained.
 */
-(id) initWithTitle:(NSString*)title normalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite target:(id)target selector:(SEL)selector;

/** initializes a menu item with a normal, selected  and disabled image with a block.
 The block will be "copied".
 */
-(id) initWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite block:(void(^)(id sender))block;

/** initializes a menu item with a normal, selected  and disabled image with a block.
 The block will be "copied".
 */
-(id) initWithTitle:(NSString*)title normalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite block:(void(^)(id sender))block;


@end

