//
//  SelectionScreen1Player.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/8/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "GlobalSingleton.h"
#import "DataHolder.h"

@interface SelectionScreen1Player : CCScene {}
@end
@interface SelectionScreen1PlayerLayer : CCLayer {
	
     CCSpriteBatchNode *_batchNode;
    CCSprite *spriteMatchTime;
	
	CCSprite *matchTime5min;
	CCSprite *matchTime8min;
	CCSprite *matchTime10min;
	
	int selectedPlayer;
	CCSprite *spriteMatchWeather;
	
	CCSprite *matchWeatherRandom;
	CCSprite *matchWeatherFine;
	CCSprite *matchWeatherRainy;
	
	
	CCSprite *selectedTeam;
	CCSprite *myTeam;
	CCSprite *opponentTeam;
	CCSprite *spriteGoPlay;
	
	CCMenu *menu;
	BOOL menuIsOpen;
	CCSprite *spriteBackMenu;
	CCSprite *spriteSoundToogler;
	
	DataHolder *myDataHolder;
    
    CCSprite* touchSprite;
    
    CCLabelTTF * teamName1Label;
    CCLabelTTF * teamName2Label;
    
    CCSprite* starHolder1;
    CCSprite* star1_1;
    CCSprite* star1_2;
    CCSprite* star1_3;
    CCSprite* star1_4;
    CCSprite* starBg1_1;
    CCSprite* starBg1_2;
    CCSprite* starBg1_3;
    CCSprite* starBg1_4;

    CCSprite* starHolder2;
    CCSprite* star2_1;
    CCSprite* star2_2;
    CCSprite* star2_3;
    CCSprite* star2_4;
    CCSprite* starBg2_1;
    CCSprite* starBg2_2;
    CCSprite* starBg2_3;
    CCSprite* starBg2_4;

  

}
@property (nonatomic,readwrite) int selectedPlayer;
@property (nonatomic,readwrite) BOOL menuIsOpen;
	
	-(void)audioToogler;
	-(void)goBackMenu;
	-(void)setMatchTimeMethod: (id)sender;
	-(void)setWeatherMethod: (id)sender;
	-(void)selectTeam:(id)sender;
    -(void)goPlay;
    -(void)setupBatchNode;
@end
