//
//  PortaitStoreProductViewController.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 8/15/16.
//  Copyright © 2016 Gil Beyruth. All rights reserved.
//

#import "PortraitStoreProductViewController.h"

@implementation PortraitStoreProductViewController

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeLeft;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}
@end