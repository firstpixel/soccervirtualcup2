//
//  MatchDrawScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/12/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//


#import "MatchDrawSceneSingle.h"
#import "MenuScene.h"
#import "GlobalSingleton.h"
#import "MKStoreManager.h"
#import "GameBuyNowScene.h"
#import "DeviceSettings.h"
#import "MKStoreManager.h"
#import "AppDelegate.h"
#import "RootViewController.h"

@implementation MatchDrawSceneSingle
- (id) init {
    self = [super init];
    if (self != nil) {
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"match_draw_single-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"match_draw_single-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"match_draw_single-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"match_draw_single-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"match_draw_single.jpg"];
                }
            }
        }
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
        [self addChild:[MatchDrawLayerSingle node] z:1];
    }
    return self;
}
@end

@implementation MatchDrawLayerSingle
- (id) init {
    self = [super init];
    if (self != nil) {
		self.isTouchEnabled = YES;
        if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
            AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            [(RootViewController*)[app navController] addInterstitial];
        }
        
    }
    return self;
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
   GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton gameType]==3){
		 if([MKStoreManager isFeaturePurchased:kFeatureAId]){
			MenuScene * ms = [MenuScene node];
			[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ms]];
		}else{
			GameBuyNowScene * ms = [GameBuyNowScene node];
			[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ms]];
		}
        
		
	}else {
		MenuScene * ms = [MenuScene node];
		[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ms]];
	}
}

@end
