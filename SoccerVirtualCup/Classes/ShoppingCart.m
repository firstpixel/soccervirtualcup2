//
//  ShoppingCart.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/19/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "ShoppingCart.h"
#import "MenuScene.h"
#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "MKStoreManager.h" 
#import "DeviceSettings.h"

@implementation ShoppingCart
- (id) init {
    self = [super init];
    if (self != nil) {
        [self setupBatchNode];
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"shopping_cart_screen-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"shopping_cart_screen-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"shopping_cart_screen-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"shopping_cart_screen-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"shopping_cart_screen.jpg"];
                }
            }
        }
        
        
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
        [self addChild:[ShoppingCartLayer node] z:1];
        
    }
    return self;
}
- (void)setupBatchNode {
    NSString *spritesheetPvrCcz = @"spritesheet.pvr.ccz";
    NSString *spritesheetPlist = @"spritesheet.plist";
    
    NSString *gametablePvrCcz = @"gametable.pvr.ccz";
    NSString *gametablePlist = @"gametable.plist";
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            spritesheetPvrCcz = @"spritesheet-ipadhd.pvr.ccz";
            spritesheetPlist = @"spritesheet-ipadhd.plist";
            gametablePvrCcz = @"gametable-ipadhd.pvr.ccz";
            gametablePlist = @"gametable-ipadhd.plist";
        }else{
            //iPad screen
            spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
            spritesheetPlist = @"spritesheet-hd.plist";
            gametablePvrCcz = @"gametable-hd.pvr.ccz";
            gametablePlist = @"gametable-hd.plist";
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
                spritesheetPlist = @"spritesheet-hd.plist";
                
                gametablePvrCcz = @"gametable-hd.pvr.ccz";
                gametablePlist = @"gametable-hd.plist";
            }else{
                //iphone screen
                spritesheetPvrCcz = @"spritesheet.pvr.ccz";
                spritesheetPlist = @"spritesheet.plist";
                
                gametablePvrCcz = @"gametable.pvr.ccz";
                gametablePlist = @"gametable.plist";
            }
        }
    }
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesheetPvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesheetPlist];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:gametablePvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:gametablePlist];
}


@end

@implementation ShoppingCartLayer
- (id) init {
    self = [super init];
    if (self != nil) {
		self.isTouchEnabled = YES;
        
		spriteBackMenu = [CCSprite spriteWithSpriteFrameName:@"back_menu.png"];
		[spriteBackMenu setPosition:ADJUST_CCP(ccp(20,300))];
		[self addChild:spriteBackMenu z:201 tag:101];

        buyRestoreCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-restore-all.png"];
		[buyRestoreCCSprite setPosition:ADJUST_CCP(ccp(400,240))];
		[self addChild:buyRestoreCCSprite z:101 tag:101];
        
       // if()
        buyCupCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-worldcup.png"];
		[buyCupCCSprite setPosition:ADJUST_CCP(ccp(80,240))];
		[self addChild:buyCupCCSprite z:101 tag:101];
        /*********/
        buyAustraliaCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-australia.png"];
		[buyAustraliaCCSprite setPosition:ADJUST_CCP(ccp(80,200))];
		[self addChild:buyAustraliaCCSprite z:101 tag:101];
        
        buyBrazilCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-brazil.png"];
		[buyBrazilCCSprite setPosition:ADJUST_CCP(ccp(240,200))];
		[self addChild:buyBrazilCCSprite z:101 tag:101];
        
        buyCroatiaCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-croatia.png"];
		[buyCroatiaCCSprite setPosition:ADJUST_CCP(ccp(400,200))];
		[self addChild:buyCroatiaCCSprite z:101 tag:101];
        /*********/
        buyEnglandCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-england.png"];
		[buyEnglandCCSprite setPosition:ADJUST_CCP(ccp(80,160))];
		[self addChild:buyEnglandCCSprite z:101 tag:101];
        
        buyFranceCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-france.png"];
		[buyFranceCCSprite setPosition:ADJUST_CCP(ccp(240,160))];
		[self addChild:buyFranceCCSprite z:101 tag:101];
        
        buyItalyCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-italy.png"];
		[buyItalyCCSprite setPosition:ADJUST_CCP(ccp(400,160))];
		[self addChild:buyItalyCCSprite z:101 tag:101];
        /*********/
        buyMexicoCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-mexico.png"];
		[buyMexicoCCSprite setPosition:ADJUST_CCP(ccp(80,120))];
		[self addChild:buyMexicoCCSprite z:101 tag:101];
        
        buyJapanCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-japan.png"];
		[buyJapanCCSprite setPosition:ADJUST_CCP(ccp(240,120))];
		[self addChild:buyJapanCCSprite z:101 tag:101];
        
        buyNetherlandsCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-netherlands.png"];
		[buyNetherlandsCCSprite setPosition:ADJUST_CCP(ccp(400,120))];
		[self addChild:buyNetherlandsCCSprite z:101 tag:101];
        /*********/
        buyPortugalCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-portugal.png"];
		[buyPortugalCCSprite setPosition:ADJUST_CCP(ccp(80,80))];
		[self addChild:buyPortugalCCSprite z:101 tag:101];
        
        buyNigeriaCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-nigeria.png"];
		[buyNigeriaCCSprite setPosition:ADJUST_CCP(ccp(240,80))];
		[self addChild:buyNigeriaCCSprite z:101 tag:101];
        
        buyRussiaCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-russia.png"];
		[buyRussiaCCSprite setPosition:ADJUST_CCP(ccp(400,80))];
		[self addChild:buyRussiaCCSprite z:101 tag:101];
        /*********/
        buySwitzerlandCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-switzerland.png"];
		[buySwitzerlandCCSprite setPosition:ADJUST_CCP(ccp(80,40))];
		[self addChild:buySwitzerlandCCSprite z:101 tag:101];
        
        buyUruguayCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-uruguay.png"];
		[buyUruguayCCSprite setPosition:ADJUST_CCP(ccp(240,40))];
		[self addChild:buyUruguayCCSprite z:101 tag:101];
        
        buyUSACCSprite = [CCSprite spriteWithSpriteFrameName:@"bt-buy-USA.png"];
		[buyUSACCSprite setPosition:ADJUST_CCP(ccp(400,40))];
		[self addChild:buyUSACCSprite z:101 tag:101];

        
    }
    return self;
}




-(void)buyProductFullGameStore{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureAId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
    //[myMKStoreManager buyFeatureA];
}
-(void)buyProductRestore{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager restorePreviousTransactionsOnComplete:^()
     {
         NSLog(@"RESTORED!");
         [buyLabel setString:@"Content Restored!"];
     }
                                                    onError:^(NSError *error)
     {
         NSLog(@"ERROR : %@",[error localizedDescription]);
     }];
    
}
-(void)buyProductAustralia4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureAustraliaId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductBrazil4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureBrazilId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductCroatia4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureCroatiaId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductEngland4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureEnglandId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductFrance4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureFranceId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductItaly4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureItalyId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductMexico4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureMexicoId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductJapan4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureJapanId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductNetherland4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureNetherlandsId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductPortugal4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeaturePortugalId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductNigeria4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureNigeriaId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductRussia4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureRussiaId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductSwitzerland4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureSwitzerlandId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductUruguay4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureUruguayId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}
-(void)buyProductUSA4stars{
    MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
    [myMKStoreManager buyFeature:kFeatureUSAId
                      onComplete:^(NSString* purchasedFeature)
     {
         NSLog(@"Purchased: %@", purchasedFeature);
         [buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
     }
                     onCancelled:^
     {
         NSLog(@"User Cancelled Transaction");
         [buyLabel setString:@"User Cancelled Transaction"];
     }];
}










-(void)goNextScreen{
	MenuScene * ms = [MenuScene node];
	[[CCDirector sharedDirector] replaceScene:ms];
}




- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	
    if (CGRectContainsPoint(spriteBackMenu.boundingBox, location)) {
        [self goNextScreen];
    }else if (CGRectContainsPoint(buyCupCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductFullGameStore];
        
    }else if (CGRectContainsPoint(buyRestoreCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductRestore];
        
    }else if (CGRectContainsPoint(buyAustraliaCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductAustralia4stars];
        
    }else if (CGRectContainsPoint(buyBrazilCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductBrazil4stars];
        
    }else if (CGRectContainsPoint(buyCroatiaCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductCroatia4stars];
        
    }else if (CGRectContainsPoint(buyEnglandCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductEngland4stars];
        
    }else if (CGRectContainsPoint(buyFranceCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductFrance4stars];
        
    }else if (CGRectContainsPoint(buyItalyCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductItaly4stars];
        
    }else if (CGRectContainsPoint(buyMexicoCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductMexico4stars];
        
    }else if (CGRectContainsPoint(buyJapanCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductJapan4stars];
        
    }else if (CGRectContainsPoint(buyNetherlandsCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductNetherland4stars];
        
    }else if (CGRectContainsPoint(buyPortugalCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductPortugal4stars];
        
    }else if (CGRectContainsPoint(buyNigeriaCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductNigeria4stars];
        
    }else if (CGRectContainsPoint(buyRussiaCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductRussia4stars];
        
    }else if (CGRectContainsPoint(buySwitzerlandCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductSwitzerland4stars];
        
    }else if (CGRectContainsPoint(buyUruguayCCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductUruguay4stars];
        
    }else if (CGRectContainsPoint(buyUSACCSprite.boundingBox, location)) {
        [self displayLoading];
        [self buyProductUSA4stars];
        
    }
    
    
//	if(CGRectContainsPoint(CGRectMake(spriteBackMenu.position.x- 0.5*spriteBackMenu.contentSize.width, spriteBackMenu.position.y- 0.5*spriteBackMenu.contentSize.height, spriteBackMenu.contentSize.width, spriteBackMenu.contentSize.height), location)) {
//		[self goNextScreen];
//	}
	//return kEventHandled;
}

-(void)displayLoading{
    CCSprite * bg1 = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            bg1 = [CCSprite spriteWithFile:@"shopping_cart_screen-ipadhd.jpg"];
        }else{
            //iPad screen
            bg1 = [CCSprite spriteWithFile:@"shopping_cart_screen-ipad.jpg"];
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                    //iphone 5
                    bg1 = [CCSprite spriteWithFile:@"shopping_cart_screen-hd.jpg"];
                }else{
                    //iphone retina screen
                    bg1 = [CCSprite spriteWithFile:@"shopping_cart_screen-hd.jpg"];
                }
            }else{
                //iphone screen
                bg1 = [CCSprite spriteWithFile:@"shopping_cart_screen.jpg"];
            }
        }
    }
    
    
    [bg1 setPosition:ADJUST_CCP(ccp(240, 160))];
    [self addChild:bg1 z:120];
    
    
    buyLabel = [CCLabelTTF labelWithString:@"Loading Store...\n if it takes too long, \ncheck your internet connection." fontName:@"Arial" fontSize:HD_PIXELS(22)];
    [buyLabel setPosition: ADJUST_CCP(ccp(240,160))];
    [buyLabel setDimensions:ADJUST_CGSIZE(CGSizeMake(480.0,100.0))];
    [buyLabel setHorizontalAlignment:kCCTextAlignmentCenter];
    [buyLabel setColor:ccWHITE];
    [bg1 addChild: buyLabel z:0];

}



@end
