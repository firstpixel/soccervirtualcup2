//
//  GameScene.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

/*#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface GameScene : Scene {}
@end

@interface GameLayer : Layer {}
@end*/


#import "GameScene.h"
#import "BallLayer.h"
#import "CCParticleExamples.h"

@interface GameScene : CCScene {
	BallLayer* ballLayer;
	CCParticleRain* raining;
    CCSpriteBatchNode* _batchNodeFields;
    CCSprite* bg;
}
@property (nonatomic, retain) BallLayer* ballLayer;
- (void)setupBatchNode;
- (void)setFieldColor:(ccColor3B)color;
@end

@interface GameLayer : CCLayer {}
@end