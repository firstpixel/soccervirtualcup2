//
//  BallLayer.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "CCLayer.h"
#import "chipmunk.h"
#import "ccMacros.h"
//#import "GameKitHelper.h"

@class GameScene;

@interface BallLayer : CCLayer {
	CCSpriteBatchNode* _batchNode;
    CCSpriteBatchNode* _batchNodeFields;
	float kSlide;
	int elasticIterations;
	float playerMass;
	float ballMass;
	int cornerImpulse;
	int wallImpulse;
	BOOL hasSent;
	BOOL hasSentPosition;
	BallLayer *gameLayer;
    
	CCSprite* ballSprite;
	CCSprite* ballSpriteShadow;
	CCSprite* ballSpriteVolume;
	CCSprite* p1Sprite;
	CCSprite* p2Sprite;
	CCSprite* p3Sprite;
	CCSprite* p4Sprite;
	CCSprite* p5Sprite;
	CCSprite* p6Sprite;
    CCSprite* glow1Sprite;
    CCSprite* glow2Sprite;
    CCSprite* glow3Sprite;
    CCSprite* glow4Sprite;
    CCSprite* glow5Sprite;
    CCSprite* glow6Sprite;
//	CCSprite* arrowSprite;
	CCSprite* gol1Sprite;
	CCSprite* gol2Sprite;
	CCSprite* invGol1Sprite;
	CCSprite* invGol2Sprite;
	CCSprite* invGol1HitSprite;
	CCSprite* invGol2HitSprite;
	CCSprite* touchSprite;
	CCSprite* cornerSprite1;
	CCSprite* cornerSprite2;
	CCSprite* cornerSprite3;
	CCSprite* cornerSprite4;
    
   // NSMutableArray *ballAnimationFrames;
    
    CCSprite* p1SpriteDisplay;
    CCSprite* p2SpriteDisplay;
    
    int oldMinutes;
    int oldSeconds;
	
    
	CCSprite* invWallBottomLeftSprite;
	CCSprite* invWallTopLeftSprite;
	CCSprite* invWallBottomRightSprite;
	CCSprite* invWallTopRightSprite;
	CCSprite* invWallTopSprite;
	CCSprite* invWallBottomSprite;
	
	CCSprite * clockTurnSprite;
	
    
	cpBody* p1Body;
	cpBody* p2Body;
	cpBody* p3Body;
	cpBody* p4Body;
	cpBody* p5Body;
	cpBody* p6Body;
	cpBody* ballBody;
    
    cpShape* ballShape;
    cpShape* p1Shape;
    cpShape* p2Shape;
    cpShape* p3Shape;
    cpShape* p4Shape;
    cpShape* p5Shape;
    cpShape* p6Shape;
    
	cpVect* p1ForceVector;
	cpVect* p2ForceVector;
	cpVect* p3ForceVector;
	cpVect* p4ForceVector;
	cpVect* p5ForceVector;
	cpVect* p6ForceVector;
	
	CCLabelAtlas* labelTimerMinutes;
	CCLabelAtlas* labelTimerSeconds;
	
	CCLabelAtlas* p1Score;
	CCLabelAtlas* p2Score;
	
	CCLabelAtlas* labelPlayer1;
	CCLabelAtlas* labelPlayer2;
	
	CCSprite* getReady;
	CCSprite* itsYourTurn;
	CCSprite* waitPlaying;
	
	CCSprite* clockSprite;
	
    CCSprite* playerSprite;
	
    CCSprite* player1Sprite;
	CCSprite* player2Sprite;
	CCSprite* versusScoreSprite;
	
	CCSprite* spriteSoundToogler;
	CCSprite* spriteBackMenu;
	
	CCSprite *goal1;
	CCSprite *goal2;
	CCSprite *goal3;
	CCSprite *goal4;
	CCSprite *goal5;
	CCSprite *goal6;
	CCSprite *goal7;
	CCSprite *goal8;
	
	
	cpSpace* space;
	
	//LEADERBOARDS
	NSMutableArray* highscores;

	int scoredPoint;
	int leaderboardMatchWinner;
	int leaderboardPlayTime;
    
    float teamFactor1;
    float teamFactor2;

	
	NSString *leaderboardAddicted;
	NSString *leaderboardGoalScored;
	NSString *leaderboardGoalConceded;
	NSString *leaderboardGoalDifference;
	NSString *leaderboardWins;
	NSString *leaderboardLoss;
	NSString *leaderboardDraws;
	NSString *leaderboardPoints;
    int ballFrameNumber;
    float diffPosy;
    float diffPosx;
    float oldpy,oldpx;
    
    //GameKitHelper *gkHelper;
    
	
}
//LEADERBOARDS
//@property (nonatomic, retain) GameKitHelper *gkHelper;
@property (nonatomic,retain) NSString *leaderboardAddicted;
@property (nonatomic,retain) NSString *leaderboardGoalScored;
@property (nonatomic,retain) NSString *leaderboardGoalConceded;
@property (nonatomic,retain) NSString *leaderboardGoalDifference;
@property (nonatomic,retain) NSString *leaderboardWins;
@property (nonatomic,retain) NSString *leaderboardLoss;
@property (nonatomic,retain) NSString *leaderboardDraws;
@property (nonatomic,retain) NSString *leaderboardPoints;
//@property (nonatomic,retain) NSMutableArray *ballAnimationFrames;



@property (nonatomic,retain) NSMutableArray *highscores;
@property (nonatomic,readwrite) int scoredPoint;
@property (nonatomic,readwrite) int leaderboardMatchWinner;
@property (nonatomic,readwrite) int leaderboardPlayTime;

@property (nonatomic,readwrite) float teamFactor1;
@property (nonatomic,readwrite) float teamFactor2;

- (BOOL)canReceiveCallbacksNow;

@property (nonatomic, readwrite) int cornerImpulse;
@property (nonatomic, readwrite) int wallImpulse;
@property (nonatomic, readwrite) BOOL hasSent;
@property (nonatomic, readwrite) float kSlide;
@property (nonatomic, readwrite) float playerMass;
@property (nonatomic, readwrite) float ballMass;
@property (nonatomic, readwrite) int elasticIterations;
@property (nonatomic, readwrite) int ballFrameNumber;

@property (nonatomic,readwrite) float diffPosy;
@property (nonatomic,readwrite) float diffPosx;
@property (nonatomic,readwrite) float oldpy,oldpx;

-(void)goBackMenu;
-(void)audioToogler;
-(void)ballAudio;
-(int)ruleOfThree:(int)a secondParam:(int)b thirdParam:(int)c;

-(void)startPlayerTurn:(ccTime *)theTimer;
-(void)startDelayRestart:(ccTime *)theTimer;

-(void)updateTimer:(ccTime *)theTimer;
-(void)tick:(ccTime )dt;
-(void)golScored:(int)team;

- (void)setupBatchNode;
-(void)ball3Danimation;

//-(NSMutableArray*)getBallAnimationFrames;
//multiplayer stuff


@end