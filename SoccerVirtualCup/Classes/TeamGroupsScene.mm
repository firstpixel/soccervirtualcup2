//
//  TeamGroups.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/13/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "TeamGroupsScene.h"
#import "DataHolder.h"
#import "GlobalCupSingleton.h"
#import "PreGameMatchsScene.h"
#import "TournamentScene.h"
#import "SimpleAudioEngine.h"
#import "DeviceSettings.h"
//	
//	
//	
//	
@implementation TeamGroupsScene
- (id) init {
    self = [super init];
    if (self != nil) {
		CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"teamgroup-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"teamgroup-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"teamgroup-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"teamgroup-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"teamgroup.jpg"];
                }
            }
        }
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
		[self addChild:[TeamGroupsSceneLayer node] z:1];
    }
    return self;
}
@end


@implementation TeamGroupsSceneLayer
@synthesize yourTeam;
@synthesize selectedGroup;
@synthesize yourTeamGroup;


- (id) init {
    self = [super init];
    if (self != nil) {
		GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
		
		if([mySingleton audioOn]==YES){
			SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
			if (sae != nil) {
				[sae playBackgroundMusic:@"loop.mp3" loop:YES];
				sae.backgroundMusicVolume = 0.1f;
				
			}
		}
		
		
		self.isTouchEnabled = YES;
		spriteGroupsHolder = [CCSprite node];
		[spriteGroupsHolder setPosition:ADJUST_CCP(ccp(0,0))];
		[self addChild:spriteGroupsHolder z:0 tag:0];
		
		spriteMenu = [CCSprite spriteWithSpriteFrameName:@"groupMenu.png"];
        
        [spriteMenu setPosition:ADJUST_CCP(ccp(470,160))];
        
		[self addChild:spriteMenu z:9 tag:100];
		
		spriteSelected = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		[spriteSelected setPosition:ADJUST_CCP(ccp(354,282))];
		[self addChild:spriteSelected z:10 tag:105];
		
		spriteMyGroup = [CCSprite spriteWithSpriteFrameName:@"you.png"];
		[spriteMyGroup setPosition:ADJUST_CCP(ccp(460,283))];
		[self addChild:spriteMyGroup z:21 tag:102];
		
		spriteMenuGroupA = [CCSprite spriteWithSpriteFrameName:@"groupA_bt.png"];
		[spriteMenuGroupA setPosition:ADJUST_CCP(ccp(400,283))];
		[self addChild:spriteMenuGroupA z:11 tag:1];
		
		spriteMenuGroupB = [CCSprite spriteWithSpriteFrameName:@"groupB_bt.png"];
		[spriteMenuGroupB setPosition:ADJUST_CCP(ccp(400,256))];
		[self addChild:spriteMenuGroupB z:12 tag:2];
		
		spriteMenuGroupC = [CCSprite spriteWithSpriteFrameName:@"groupC_bt.png"];
		[spriteMenuGroupC setPosition:ADJUST_CCP(ccp(400,229))];
		[self addChild:spriteMenuGroupC z:13 tag:3];
		
		spriteMenuGroupD = [CCSprite spriteWithSpriteFrameName:@"groupD_bt.png"];
		[spriteMenuGroupD setPosition:ADJUST_CCP(ccp(400,202))];
		[self addChild:spriteMenuGroupD z:14 tag:4];
		
		spriteMenuGroupE = [CCSprite spriteWithSpriteFrameName:@"groupE_bt.png"];
		[spriteMenuGroupE setPosition:ADJUST_CCP(ccp(400,175))];
		[self addChild:spriteMenuGroupE z:15 tag:5];
		
		spriteMenuGroupF = [CCSprite spriteWithSpriteFrameName:@"groupF_bt.png"];
		[spriteMenuGroupF setPosition:ADJUST_CCP(ccp(400,148))];
		[self addChild:spriteMenuGroupF z:16 tag:6];
		
		spriteMenuGroupG = [CCSprite spriteWithSpriteFrameName:@"groupG_bt.png"];
		[spriteMenuGroupG setPosition:ADJUST_CCP(ccp(400,121))];
		[self addChild:spriteMenuGroupG z:17 tag:7];
		
		spriteMenuGroupH = [CCSprite spriteWithSpriteFrameName:@"groupH_bt.png"];
		[spriteMenuGroupH setPosition:ADJUST_CCP(ccp(400,94))];
		[self addChild:spriteMenuGroupH z:18 tag:8];
		
		spriteGoPlay = [CCSprite spriteWithSpriteFrameName:@"go_bt.png"];
		[spriteGoPlay setPosition:ADJUST_CCP(ccp(410,40))];
		[self addChild:spriteGoPlay z:101 tag:101];
		
		
		DataHolder *myDataHolder = [DataHolder sharedInstance];
		[myDataHolder clearData];
		int groupId = [myDataHolder getGroupForTeamKey:[mySingleton spritePlayer1]];
		
		GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
		
		if(groupId==0){
			[spriteMyGroup setPosition:ADJUST_CCP(ccp(460,292))];
		}else if(groupId==1){
			[spriteMyGroup setPosition:ADJUST_CCP(ccp(460,265))];
		}else if(groupId==2){
			[spriteMyGroup setPosition:ADJUST_CCP(ccp(460,238))];
		}else if(groupId==3){
			[spriteMyGroup setPosition:ADJUST_CCP(ccp(460,211))];
		}else if(groupId==4){
			[spriteMyGroup setPosition:ADJUST_CCP(ccp(460,184))];
		}else if(groupId==5){
			[spriteMyGroup setPosition:ADJUST_CCP(ccp(460,157))];
		}else if(groupId==6){
			[spriteMyGroup setPosition:ADJUST_CCP(ccp(460,130))];
		}else if(groupId==7){
			[spriteMyGroup setPosition:ADJUST_CCP(ccp(460,103))];
		}

		NSArray* groupAKeys;
		NSNumber*g1;
		NSNumber*g2;
		NSNumber*g3;
		NSNumber*g4;
		
		spriteBoardGroupA = [CCSprite spriteWithSpriteFrameName:@"board_group.png"];
		[spriteBoardGroupA setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(168,160))];
		[spriteGroupsHolder addChild:spriteBoardGroupA z:0 tag:0];
		spriteTitleGroupA = [CCSprite spriteWithSpriteFrameName:@"groupA_title.png"];
		[spriteTitleGroupA setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(80,280))];
		[spriteBoardGroupA addChild:spriteTitleGroupA z:0 tag:0];
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInt:0]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		
		[self setScoreBoard:spriteBoardGroupA  
						tid1:[g1 intValue] tid2:[g2 intValue] tid3:[g3 intValue] tid4:[g4 intValue]
						t1:[myDataHolder getTeamShortNameForKey:[g1 intValue]] t2:[myDataHolder getTeamShortNameForKey:[g2 intValue]] 
						t3:[myDataHolder getTeamShortNameForKey:[g3 intValue]] t4:[myDataHolder getTeamShortNameForKey:[g4 intValue]]
						flag1:[myDataHolder getFlagNameForKey:[g1 intValue]] flag2:[myDataHolder getFlagNameForKey:[g2 intValue]] 
						flag3:[myDataHolder getFlagNameForKey:[g3 intValue]] flag4:[myDataHolder getFlagNameForKey:[g4 intValue]]  
						p1:[myCupSingleton getPointsArrayAtPos:[g1 intValue]] p2:[myCupSingleton getPointsArrayAtPos:[g2 intValue]]
						p3:[myCupSingleton getPointsArrayAtPos:[g3 intValue]] p4:[myCupSingleton getPointsArrayAtPos:[g4 intValue]] 
						w1:[myCupSingleton getWinsArrayAtPos:[g1 intValue]] w2:[myCupSingleton getWinsArrayAtPos:[g2 intValue]] 
						w3:[myCupSingleton getWinsArrayAtPos:[g3 intValue]] w4:[myCupSingleton getWinsArrayAtPos:[g4 intValue]]  
						l1:[myCupSingleton getLossArrayAtPos:[g1 intValue]] l2:[myCupSingleton getLossArrayAtPos:[g2 intValue]] 
						l3:[myCupSingleton getLossArrayAtPos:[g3 intValue]] l4:[myCupSingleton getLossArrayAtPos:[g4 intValue]]
						d1:[myCupSingleton getDrawArrayAtPos:[g1 intValue]] d2:[myCupSingleton getDrawArrayAtPos:[g2 intValue]] 
						d3:[myCupSingleton getDrawArrayAtPos:[g3 intValue]] d4:[myCupSingleton getDrawArrayAtPos:[g4 intValue]] 
						gs1:[myCupSingleton getGoalScoredArrayAtPos:[g1 intValue]] gs2:[myCupSingleton getGoalScoredArrayAtPos:[g2 intValue]] 
						gs3:[myCupSingleton getGoalScoredArrayAtPos:[g3 intValue]] gs4:[myCupSingleton getGoalScoredArrayAtPos:[g4 intValue]] 
						gc1:[myCupSingleton getGoalConcededArrayAtPos:[g1 intValue]] gc2:[myCupSingleton getGoalConcededArrayAtPos:[g2 intValue]]
						gc3:[myCupSingleton getGoalConcededArrayAtPos:[g3 intValue]] gc4:[myCupSingleton getGoalConcededArrayAtPos:[g4 intValue]] 
						gd1:[myCupSingleton getGoalDifferenceArrayAtPos:[g1 intValue]] gd2:[myCupSingleton getGoalDifferenceArrayAtPos:[g2 intValue]]
						gd3:[myCupSingleton getGoalDifferenceArrayAtPos:[g3 intValue]] gd4:[myCupSingleton getGoalDifferenceArrayAtPos:[g4 intValue]]];
		
		[self setMatchBoard:spriteBoardGroupA 
					   p1p2:[myCupSingleton getP1P2Array:0] p1p3:[myCupSingleton getP1P3Array:0] p1p4:[myCupSingleton getP1P4Array:0] 
					   p2p1:[myCupSingleton getP2P1Array:0] p2p3:[myCupSingleton getP2P3Array:0] p2p4:[myCupSingleton getP2P4Array:0] 
					   p3p1:[myCupSingleton getP3P1Array:0] p3p2:[myCupSingleton getP3P2Array:0] p3p4:[myCupSingleton getP3P4Array:0] 
					   p4p1:[myCupSingleton getP4P1Array:0] p4p2:[myCupSingleton getP4P2Array:0] p4p3:[myCupSingleton getP4P3Array:0]];
		
		spriteBoardGroupB = [CCSprite spriteWithSpriteFrameName:@"board_group.png"];
		[spriteBoardGroupB setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(648,160))];
		[spriteGroupsHolder addChild:spriteBoardGroupB z:1 tag:1];
		spriteTitleGroupB = [CCSprite spriteWithSpriteFrameName:@"groupB_title.png"];
		[spriteTitleGroupB setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(80,280))];
		[spriteBoardGroupB addChild:spriteTitleGroupB z:0 tag:0];
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInt:1]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		
		[self setScoreBoard:spriteBoardGroupB  
					   tid1:[g1 intValue] tid2:[g2 intValue] tid3:[g3 intValue] tid4:[g4 intValue]
						 t1:[myDataHolder getTeamShortNameForKey:[g1 intValue]] t2:[myDataHolder getTeamShortNameForKey:[g2 intValue]] 
						 t3:[myDataHolder getTeamShortNameForKey:[g3 intValue]] t4:[myDataHolder getTeamShortNameForKey:[g4 intValue]]
					  flag1:[myDataHolder getFlagNameForKey:[g1 intValue]] flag2:[myDataHolder getFlagNameForKey:[g2 intValue]] 
					  flag3:[myDataHolder getFlagNameForKey:[g3 intValue]] flag4:[myDataHolder getFlagNameForKey:[g4 intValue]]  
						 p1:[myCupSingleton getPointsArrayAtPos:[g1 intValue]] p2:[myCupSingleton getPointsArrayAtPos:[g2 intValue]]
						 p3:[myCupSingleton getPointsArrayAtPos:[g3 intValue]] p4:[myCupSingleton getPointsArrayAtPos:[g4 intValue]] 
						 w1:[myCupSingleton getWinsArrayAtPos:[g1 intValue]] w2:[myCupSingleton getWinsArrayAtPos:[g2 intValue]] 
						 w3:[myCupSingleton getWinsArrayAtPos:[g3 intValue]] w4:[myCupSingleton getWinsArrayAtPos:[g4 intValue]]  
						 l1:[myCupSingleton getLossArrayAtPos:[g1 intValue]] l2:[myCupSingleton getLossArrayAtPos:[g2 intValue]] 
						 l3:[myCupSingleton getLossArrayAtPos:[g3 intValue]] l4:[myCupSingleton getLossArrayAtPos:[g4 intValue]]
						 d1:[myCupSingleton getDrawArrayAtPos:[g1 intValue]] d2:[myCupSingleton getDrawArrayAtPos:[g2 intValue]] 
						 d3:[myCupSingleton getDrawArrayAtPos:[g3 intValue]] d4:[myCupSingleton getDrawArrayAtPos:[g4 intValue]] 
						gs1:[myCupSingleton getGoalScoredArrayAtPos:[g1 intValue]] gs2:[myCupSingleton getGoalScoredArrayAtPos:[g2 intValue]] 
						gs3:[myCupSingleton getGoalScoredArrayAtPos:[g3 intValue]] gs4:[myCupSingleton getGoalScoredArrayAtPos:[g4 intValue]] 
						gc1:[myCupSingleton getGoalConcededArrayAtPos:[g1 intValue]] gc2:[myCupSingleton getGoalConcededArrayAtPos:[g2 intValue]]
						gc3:[myCupSingleton getGoalConcededArrayAtPos:[g3 intValue]] gc4:[myCupSingleton getGoalConcededArrayAtPos:[g4 intValue]] 
						gd1:[myCupSingleton getGoalDifferenceArrayAtPos:[g1 intValue]] gd2:[myCupSingleton getGoalDifferenceArrayAtPos:[g2 intValue]]
						gd3:[myCupSingleton getGoalDifferenceArrayAtPos:[g3 intValue]] gd4:[myCupSingleton getGoalDifferenceArrayAtPos:[g4 intValue]]];
		
		[self setMatchBoard:spriteBoardGroupB 
					   p1p2:[myCupSingleton getP1P2Array:1] p1p3:[myCupSingleton getP1P3Array:1] p1p4:[myCupSingleton getP1P4Array:1] 
					   p2p1:[myCupSingleton getP2P1Array:1] p2p3:[myCupSingleton getP2P3Array:1] p2p4:[myCupSingleton getP2P4Array:1] 
					   p3p1:[myCupSingleton getP3P1Array:1] p3p2:[myCupSingleton getP3P2Array:1] p3p4:[myCupSingleton getP3P4Array:1] 
					   p4p1:[myCupSingleton getP4P1Array:1] p4p2:[myCupSingleton getP4P2Array:1] p4p3:[myCupSingleton getP4P3Array:1]];
		
		spriteBoardGroupC = [CCSprite spriteWithSpriteFrameName:@"board_group.png"];
		[spriteBoardGroupC setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(1128,160))];
		[spriteGroupsHolder addChild:spriteBoardGroupC z:2 tag:2];
		spriteTitleGroupC = [CCSprite spriteWithSpriteFrameName:@"groupC_title.png"];
		[spriteTitleGroupC setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(80,280))];
		[spriteBoardGroupC addChild:spriteTitleGroupC z:0 tag:0];
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInt:2]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		
		[self setScoreBoard:spriteBoardGroupC  
					   tid1:[g1 intValue] tid2:[g2 intValue] tid3:[g3 intValue] tid4:[g4 intValue]
						 t1:[myDataHolder getTeamShortNameForKey:[g1 intValue]] t2:[myDataHolder getTeamShortNameForKey:[g2 intValue]] 
						 t3:[myDataHolder getTeamShortNameForKey:[g3 intValue]] t4:[myDataHolder getTeamShortNameForKey:[g4 intValue]]
					  flag1:[myDataHolder getFlagNameForKey:[g1 intValue]] flag2:[myDataHolder getFlagNameForKey:[g2 intValue]] 
					  flag3:[myDataHolder getFlagNameForKey:[g3 intValue]] flag4:[myDataHolder getFlagNameForKey:[g4 intValue]]  
						 p1:[myCupSingleton getPointsArrayAtPos:[g1 intValue]] p2:[myCupSingleton getPointsArrayAtPos:[g2 intValue]]
						 p3:[myCupSingleton getPointsArrayAtPos:[g3 intValue]] p4:[myCupSingleton getPointsArrayAtPos:[g4 intValue]] 
						 w1:[myCupSingleton getWinsArrayAtPos:[g1 intValue]] w2:[myCupSingleton getWinsArrayAtPos:[g2 intValue]] 
						 w3:[myCupSingleton getWinsArrayAtPos:[g3 intValue]] w4:[myCupSingleton getWinsArrayAtPos:[g4 intValue]]  
						 l1:[myCupSingleton getLossArrayAtPos:[g1 intValue]] l2:[myCupSingleton getLossArrayAtPos:[g2 intValue]] 
						 l3:[myCupSingleton getLossArrayAtPos:[g3 intValue]] l4:[myCupSingleton getLossArrayAtPos:[g4 intValue]]
						 d1:[myCupSingleton getDrawArrayAtPos:[g1 intValue]] d2:[myCupSingleton getDrawArrayAtPos:[g2 intValue]] 
						 d3:[myCupSingleton getDrawArrayAtPos:[g3 intValue]] d4:[myCupSingleton getDrawArrayAtPos:[g4 intValue]] 
						gs1:[myCupSingleton getGoalScoredArrayAtPos:[g1 intValue]] gs2:[myCupSingleton getGoalScoredArrayAtPos:[g2 intValue]] 
						gs3:[myCupSingleton getGoalScoredArrayAtPos:[g3 intValue]] gs4:[myCupSingleton getGoalScoredArrayAtPos:[g4 intValue]] 
						gc1:[myCupSingleton getGoalConcededArrayAtPos:[g1 intValue]] gc2:[myCupSingleton getGoalConcededArrayAtPos:[g2 intValue]]
						gc3:[myCupSingleton getGoalConcededArrayAtPos:[g3 intValue]] gc4:[myCupSingleton getGoalConcededArrayAtPos:[g4 intValue]] 
						gd1:[myCupSingleton getGoalDifferenceArrayAtPos:[g1 intValue]] gd2:[myCupSingleton getGoalDifferenceArrayAtPos:[g2 intValue]]
						gd3:[myCupSingleton getGoalDifferenceArrayAtPos:[g3 intValue]] gd4:[myCupSingleton getGoalDifferenceArrayAtPos:[g4 intValue]]];
		
		[self setMatchBoard:spriteBoardGroupC
					   p1p2:[myCupSingleton getP1P2Array:2] p1p3:[myCupSingleton getP1P3Array:2] p1p4:[myCupSingleton getP1P4Array:2] 
					   p2p1:[myCupSingleton getP2P1Array:2] p2p3:[myCupSingleton getP2P3Array:2] p2p4:[myCupSingleton getP2P4Array:2] 
					   p3p1:[myCupSingleton getP3P1Array:2] p3p2:[myCupSingleton getP3P2Array:2] p3p4:[myCupSingleton getP3P4Array:2] 
					   p4p1:[myCupSingleton getP4P1Array:2] p4p2:[myCupSingleton getP4P2Array:2] p4p3:[myCupSingleton getP4P3Array:2]];
		
		spriteBoardGroupD = [CCSprite spriteWithSpriteFrameName:@"board_group.png"];
		[spriteBoardGroupD setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(1608,160))];
		[spriteGroupsHolder addChild:spriteBoardGroupD z:3 tag:3];
		spriteTitleGroupD = [CCSprite spriteWithSpriteFrameName:@"groupD_title.png"];
		[spriteTitleGroupD setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(80,280))];
		[spriteBoardGroupD addChild:spriteTitleGroupD z:0 tag:0];
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInt:3]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		
		[self setScoreBoard:spriteBoardGroupD  
					   tid1:[g1 intValue] tid2:[g2 intValue] tid3:[g3 intValue] tid4:[g4 intValue]
						 t1:[myDataHolder getTeamShortNameForKey:[g1 intValue]] t2:[myDataHolder getTeamShortNameForKey:[g2 intValue]] 
						 t3:[myDataHolder getTeamShortNameForKey:[g3 intValue]] t4:[myDataHolder getTeamShortNameForKey:[g4 intValue]]
					  flag1:[myDataHolder getFlagNameForKey:[g1 intValue]] flag2:[myDataHolder getFlagNameForKey:[g2 intValue]] 
					  flag3:[myDataHolder getFlagNameForKey:[g3 intValue]] flag4:[myDataHolder getFlagNameForKey:[g4 intValue]]  
						 p1:[myCupSingleton getPointsArrayAtPos:[g1 intValue]] p2:[myCupSingleton getPointsArrayAtPos:[g2 intValue]]
						 p3:[myCupSingleton getPointsArrayAtPos:[g3 intValue]] p4:[myCupSingleton getPointsArrayAtPos:[g4 intValue]] 
						 w1:[myCupSingleton getWinsArrayAtPos:[g1 intValue]] w2:[myCupSingleton getWinsArrayAtPos:[g2 intValue]] 
						 w3:[myCupSingleton getWinsArrayAtPos:[g3 intValue]] w4:[myCupSingleton getWinsArrayAtPos:[g4 intValue]]  
						 l1:[myCupSingleton getLossArrayAtPos:[g1 intValue]] l2:[myCupSingleton getLossArrayAtPos:[g2 intValue]] 
						 l3:[myCupSingleton getLossArrayAtPos:[g3 intValue]] l4:[myCupSingleton getLossArrayAtPos:[g4 intValue]]
						 d1:[myCupSingleton getDrawArrayAtPos:[g1 intValue]] d2:[myCupSingleton getDrawArrayAtPos:[g2 intValue]] 
						 d3:[myCupSingleton getDrawArrayAtPos:[g3 intValue]] d4:[myCupSingleton getDrawArrayAtPos:[g4 intValue]] 
						gs1:[myCupSingleton getGoalScoredArrayAtPos:[g1 intValue]] gs2:[myCupSingleton getGoalScoredArrayAtPos:[g2 intValue]] 
						gs3:[myCupSingleton getGoalScoredArrayAtPos:[g3 intValue]] gs4:[myCupSingleton getGoalScoredArrayAtPos:[g4 intValue]] 
						gc1:[myCupSingleton getGoalConcededArrayAtPos:[g1 intValue]] gc2:[myCupSingleton getGoalConcededArrayAtPos:[g2 intValue]]
						gc3:[myCupSingleton getGoalConcededArrayAtPos:[g3 intValue]] gc4:[myCupSingleton getGoalConcededArrayAtPos:[g4 intValue]] 
						gd1:[myCupSingleton getGoalDifferenceArrayAtPos:[g1 intValue]] gd2:[myCupSingleton getGoalDifferenceArrayAtPos:[g2 intValue]]
						gd3:[myCupSingleton getGoalDifferenceArrayAtPos:[g3 intValue]] gd4:[myCupSingleton getGoalDifferenceArrayAtPos:[g4 intValue]]];
		
		[self setMatchBoard:spriteBoardGroupD
					   p1p2:[myCupSingleton getP1P2Array:3] p1p3:[myCupSingleton getP1P3Array:3] p1p4:[myCupSingleton getP1P4Array:3] 
					   p2p1:[myCupSingleton getP2P1Array:3] p2p3:[myCupSingleton getP2P3Array:3] p2p4:[myCupSingleton getP2P4Array:3] 
					   p3p1:[myCupSingleton getP3P1Array:3] p3p2:[myCupSingleton getP3P2Array:3] p3p4:[myCupSingleton getP3P4Array:3] 
					   p4p1:[myCupSingleton getP4P1Array:3] p4p2:[myCupSingleton getP4P2Array:3] p4p3:[myCupSingleton getP4P3Array:3]];
	
		spriteBoardGroupE = [CCSprite spriteWithSpriteFrameName:@"board_group.png"];
		[spriteBoardGroupE setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(2088,160))];
		[spriteGroupsHolder addChild:spriteBoardGroupE z:4 tag:4];
		spriteTitleGroupE = [CCSprite spriteWithSpriteFrameName:@"groupE_title.png"];
		[spriteTitleGroupE setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(80,280))];
		[spriteBoardGroupE addChild:spriteTitleGroupE z:0 tag:0];
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInt:4]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		
		[self setScoreBoard:spriteBoardGroupE  
					   tid1:[g1 intValue] tid2:[g2 intValue] tid3:[g3 intValue] tid4:[g4 intValue]
						 t1:[myDataHolder getTeamShortNameForKey:[g1 intValue]] t2:[myDataHolder getTeamShortNameForKey:[g2 intValue]] 
						 t3:[myDataHolder getTeamShortNameForKey:[g3 intValue]] t4:[myDataHolder getTeamShortNameForKey:[g4 intValue]]
					  flag1:[myDataHolder getFlagNameForKey:[g1 intValue]] flag2:[myDataHolder getFlagNameForKey:[g2 intValue]] 
					  flag3:[myDataHolder getFlagNameForKey:[g3 intValue]] flag4:[myDataHolder getFlagNameForKey:[g4 intValue]]  
						 p1:[myCupSingleton getPointsArrayAtPos:[g1 intValue]] p2:[myCupSingleton getPointsArrayAtPos:[g2 intValue]]
						 p3:[myCupSingleton getPointsArrayAtPos:[g3 intValue]] p4:[myCupSingleton getPointsArrayAtPos:[g4 intValue]] 
						 w1:[myCupSingleton getWinsArrayAtPos:[g1 intValue]] w2:[myCupSingleton getWinsArrayAtPos:[g2 intValue]] 
						 w3:[myCupSingleton getWinsArrayAtPos:[g3 intValue]] w4:[myCupSingleton getWinsArrayAtPos:[g4 intValue]]  
						 l1:[myCupSingleton getLossArrayAtPos:[g1 intValue]] l2:[myCupSingleton getLossArrayAtPos:[g2 intValue]] 
						 l3:[myCupSingleton getLossArrayAtPos:[g3 intValue]] l4:[myCupSingleton getLossArrayAtPos:[g4 intValue]]
						 d1:[myCupSingleton getDrawArrayAtPos:[g1 intValue]] d2:[myCupSingleton getDrawArrayAtPos:[g2 intValue]] 
						 d3:[myCupSingleton getDrawArrayAtPos:[g3 intValue]] d4:[myCupSingleton getDrawArrayAtPos:[g4 intValue]] 
						gs1:[myCupSingleton getGoalScoredArrayAtPos:[g1 intValue]] gs2:[myCupSingleton getGoalScoredArrayAtPos:[g2 intValue]] 
						gs3:[myCupSingleton getGoalScoredArrayAtPos:[g3 intValue]] gs4:[myCupSingleton getGoalScoredArrayAtPos:[g4 intValue]] 
						gc1:[myCupSingleton getGoalConcededArrayAtPos:[g1 intValue]] gc2:[myCupSingleton getGoalConcededArrayAtPos:[g2 intValue]]
						gc3:[myCupSingleton getGoalConcededArrayAtPos:[g3 intValue]] gc4:[myCupSingleton getGoalConcededArrayAtPos:[g4 intValue]] 
						gd1:[myCupSingleton getGoalDifferenceArrayAtPos:[g1 intValue]] gd2:[myCupSingleton getGoalDifferenceArrayAtPos:[g2 intValue]]
						gd3:[myCupSingleton getGoalDifferenceArrayAtPos:[g3 intValue]] gd4:[myCupSingleton getGoalDifferenceArrayAtPos:[g4 intValue]]];
		
		[self setMatchBoard:spriteBoardGroupE
					   p1p2:[myCupSingleton getP1P2Array:4] p1p3:[myCupSingleton getP1P3Array:4] p1p4:[myCupSingleton getP1P4Array:4] 
					   p2p1:[myCupSingleton getP2P1Array:4] p2p3:[myCupSingleton getP2P3Array:4] p2p4:[myCupSingleton getP2P4Array:4] 
					   p3p1:[myCupSingleton getP3P1Array:4] p3p2:[myCupSingleton getP3P2Array:4] p3p4:[myCupSingleton getP3P4Array:4] 
					   p4p1:[myCupSingleton getP4P1Array:4] p4p2:[myCupSingleton getP4P2Array:4] p4p3:[myCupSingleton getP4P3Array:4]];
		
		spriteBoardGroupF = [CCSprite spriteWithSpriteFrameName:@"board_group.png"];
		[spriteBoardGroupF setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(2568,160))];
		[spriteGroupsHolder addChild:spriteBoardGroupF z:5 tag:5];
		spriteTitleGroupF = [CCSprite spriteWithSpriteFrameName:@"groupF_title.png"];
		[spriteTitleGroupF setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(80,280))];
		[spriteBoardGroupF addChild:spriteTitleGroupF z:0 tag:0];
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInt:5]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		
		[self setScoreBoard:spriteBoardGroupF  
					   tid1:[g1 intValue] tid2:[g2 intValue] tid3:[g3 intValue] tid4:[g4 intValue]
						 t1:[myDataHolder getTeamShortNameForKey:[g1 intValue]] t2:[myDataHolder getTeamShortNameForKey:[g2 intValue]] 
						 t3:[myDataHolder getTeamShortNameForKey:[g3 intValue]] t4:[myDataHolder getTeamShortNameForKey:[g4 intValue]]
					  flag1:[myDataHolder getFlagNameForKey:[g1 intValue]] flag2:[myDataHolder getFlagNameForKey:[g2 intValue]] 
					  flag3:[myDataHolder getFlagNameForKey:[g3 intValue]] flag4:[myDataHolder getFlagNameForKey:[g4 intValue]]  
						 p1:[myCupSingleton getPointsArrayAtPos:[g1 intValue]] p2:[myCupSingleton getPointsArrayAtPos:[g2 intValue]]
						 p3:[myCupSingleton getPointsArrayAtPos:[g3 intValue]] p4:[myCupSingleton getPointsArrayAtPos:[g4 intValue]] 
						 w1:[myCupSingleton getWinsArrayAtPos:[g1 intValue]] w2:[myCupSingleton getWinsArrayAtPos:[g2 intValue]] 
						 w3:[myCupSingleton getWinsArrayAtPos:[g3 intValue]] w4:[myCupSingleton getWinsArrayAtPos:[g4 intValue]]  
						 l1:[myCupSingleton getLossArrayAtPos:[g1 intValue]] l2:[myCupSingleton getLossArrayAtPos:[g2 intValue]] 
						 l3:[myCupSingleton getLossArrayAtPos:[g3 intValue]] l4:[myCupSingleton getLossArrayAtPos:[g4 intValue]]
						 d1:[myCupSingleton getDrawArrayAtPos:[g1 intValue]] d2:[myCupSingleton getDrawArrayAtPos:[g2 intValue]] 
						 d3:[myCupSingleton getDrawArrayAtPos:[g3 intValue]] d4:[myCupSingleton getDrawArrayAtPos:[g4 intValue]] 
						gs1:[myCupSingleton getGoalScoredArrayAtPos:[g1 intValue]] gs2:[myCupSingleton getGoalScoredArrayAtPos:[g2 intValue]] 
						gs3:[myCupSingleton getGoalScoredArrayAtPos:[g3 intValue]] gs4:[myCupSingleton getGoalScoredArrayAtPos:[g4 intValue]] 
						gc1:[myCupSingleton getGoalConcededArrayAtPos:[g1 intValue]] gc2:[myCupSingleton getGoalConcededArrayAtPos:[g2 intValue]]
						gc3:[myCupSingleton getGoalConcededArrayAtPos:[g3 intValue]] gc4:[myCupSingleton getGoalConcededArrayAtPos:[g4 intValue]] 
						gd1:[myCupSingleton getGoalDifferenceArrayAtPos:[g1 intValue]] gd2:[myCupSingleton getGoalDifferenceArrayAtPos:[g2 intValue]]
						gd3:[myCupSingleton getGoalDifferenceArrayAtPos:[g3 intValue]] gd4:[myCupSingleton getGoalDifferenceArrayAtPos:[g4 intValue]]];
		
		[self setMatchBoard:spriteBoardGroupF
					   p1p2:[myCupSingleton getP1P2Array:5] p1p3:[myCupSingleton getP1P3Array:5] p1p4:[myCupSingleton getP1P4Array:5] 
					   p2p1:[myCupSingleton getP2P1Array:5] p2p3:[myCupSingleton getP2P3Array:5] p2p4:[myCupSingleton getP2P4Array:5] 
					   p3p1:[myCupSingleton getP3P1Array:5] p3p2:[myCupSingleton getP3P2Array:5] p3p4:[myCupSingleton getP3P4Array:5] 
					   p4p1:[myCupSingleton getP4P1Array:5] p4p2:[myCupSingleton getP4P2Array:5] p4p3:[myCupSingleton getP4P3Array:5]];
		
		spriteBoardGroupG = [CCSprite spriteWithSpriteFrameName:@"board_group.png"];
		[spriteBoardGroupG setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(3048,160))];
		[spriteGroupsHolder addChild:spriteBoardGroupG z:6 tag:6];
		spriteTitleGroupG = [CCSprite spriteWithSpriteFrameName:@"groupG_title.png"];
		[spriteTitleGroupG setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(80,280))];
		[spriteBoardGroupG addChild:spriteTitleGroupG z:0 tag:0];
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInt:6]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		
		[self setScoreBoard:spriteBoardGroupG  
					   tid1:[g1 intValue] tid2:[g2 intValue] tid3:[g3 intValue] tid4:[g4 intValue]
						 t1:[myDataHolder getTeamShortNameForKey:[g1 intValue]] t2:[myDataHolder getTeamShortNameForKey:[g2 intValue]] 
						 t3:[myDataHolder getTeamShortNameForKey:[g3 intValue]] t4:[myDataHolder getTeamShortNameForKey:[g4 intValue]]
					  flag1:[myDataHolder getFlagNameForKey:[g1 intValue]] flag2:[myDataHolder getFlagNameForKey:[g2 intValue]] 
					  flag3:[myDataHolder getFlagNameForKey:[g3 intValue]] flag4:[myDataHolder getFlagNameForKey:[g4 intValue]]  
						 p1:[myCupSingleton getPointsArrayAtPos:[g1 intValue]] p2:[myCupSingleton getPointsArrayAtPos:[g2 intValue]]
						 p3:[myCupSingleton getPointsArrayAtPos:[g3 intValue]] p4:[myCupSingleton getPointsArrayAtPos:[g4 intValue]] 
						 w1:[myCupSingleton getWinsArrayAtPos:[g1 intValue]] w2:[myCupSingleton getWinsArrayAtPos:[g2 intValue]] 
						 w3:[myCupSingleton getWinsArrayAtPos:[g3 intValue]] w4:[myCupSingleton getWinsArrayAtPos:[g4 intValue]]  
						 l1:[myCupSingleton getLossArrayAtPos:[g1 intValue]] l2:[myCupSingleton getLossArrayAtPos:[g2 intValue]] 
						 l3:[myCupSingleton getLossArrayAtPos:[g3 intValue]] l4:[myCupSingleton getLossArrayAtPos:[g4 intValue]]
						 d1:[myCupSingleton getDrawArrayAtPos:[g1 intValue]] d2:[myCupSingleton getDrawArrayAtPos:[g2 intValue]] 
						 d3:[myCupSingleton getDrawArrayAtPos:[g3 intValue]] d4:[myCupSingleton getDrawArrayAtPos:[g4 intValue]] 
						gs1:[myCupSingleton getGoalScoredArrayAtPos:[g1 intValue]] gs2:[myCupSingleton getGoalScoredArrayAtPos:[g2 intValue]] 
						gs3:[myCupSingleton getGoalScoredArrayAtPos:[g3 intValue]] gs4:[myCupSingleton getGoalScoredArrayAtPos:[g4 intValue]] 
						gc1:[myCupSingleton getGoalConcededArrayAtPos:[g1 intValue]] gc2:[myCupSingleton getGoalConcededArrayAtPos:[g2 intValue]]
						gc3:[myCupSingleton getGoalConcededArrayAtPos:[g3 intValue]] gc4:[myCupSingleton getGoalConcededArrayAtPos:[g4 intValue]] 
						gd1:[myCupSingleton getGoalDifferenceArrayAtPos:[g1 intValue]] gd2:[myCupSingleton getGoalDifferenceArrayAtPos:[g2 intValue]]
						gd3:[myCupSingleton getGoalDifferenceArrayAtPos:[g3 intValue]] gd4:[myCupSingleton getGoalDifferenceArrayAtPos:[g4 intValue]]];
		
		[self setMatchBoard:spriteBoardGroupG
					   p1p2:[myCupSingleton getP1P2Array:6] p1p3:[myCupSingleton getP1P3Array:6] p1p4:[myCupSingleton getP1P4Array:6] 
					   p2p1:[myCupSingleton getP2P1Array:6] p2p3:[myCupSingleton getP2P3Array:6] p2p4:[myCupSingleton getP2P4Array:6] 
					   p3p1:[myCupSingleton getP3P1Array:6] p3p2:[myCupSingleton getP3P2Array:6] p3p4:[myCupSingleton getP3P4Array:6] 
					   p4p1:[myCupSingleton getP4P1Array:6] p4p2:[myCupSingleton getP4P2Array:6] p4p3:[myCupSingleton getP4P3Array:6]];
		
		spriteBoardGroupH = [CCSprite spriteWithSpriteFrameName:@"board_group.png"];
		[spriteBoardGroupH setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(3528,160))];
		[spriteGroupsHolder addChild:spriteBoardGroupH z:7 tag:7];
		spriteTitleGroupH = [CCSprite spriteWithSpriteFrameName:@"groupH_title.png"];
		[spriteTitleGroupH setPosition:ADJUST_INTERNAL_SPRITE_CCP(ccp(80,280))];
		[spriteBoardGroupH addChild:spriteTitleGroupH z:0 tag:0];
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInt:7]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		
		[self setScoreBoard:spriteBoardGroupH  
					   tid1:[g1 intValue] tid2:[g2 intValue] tid3:[g3 intValue] tid4:[g4 intValue]
						 t1:[myDataHolder getTeamShortNameForKey:[g1 intValue]] t2:[myDataHolder getTeamShortNameForKey:[g2 intValue]] 
						 t3:[myDataHolder getTeamShortNameForKey:[g3 intValue]] t4:[myDataHolder getTeamShortNameForKey:[g4 intValue]]
					  flag1:[myDataHolder getFlagNameForKey:[g1 intValue]] flag2:[myDataHolder getFlagNameForKey:[g2 intValue]] 
					  flag3:[myDataHolder getFlagNameForKey:[g3 intValue]] flag4:[myDataHolder getFlagNameForKey:[g4 intValue]]  
						 p1:[myCupSingleton getPointsArrayAtPos:[g1 intValue]] p2:[myCupSingleton getPointsArrayAtPos:[g2 intValue]]
						 p3:[myCupSingleton getPointsArrayAtPos:[g3 intValue]] p4:[myCupSingleton getPointsArrayAtPos:[g4 intValue]] 
						 w1:[myCupSingleton getWinsArrayAtPos:[g1 intValue]] w2:[myCupSingleton getWinsArrayAtPos:[g2 intValue]] 
						 w3:[myCupSingleton getWinsArrayAtPos:[g3 intValue]] w4:[myCupSingleton getWinsArrayAtPos:[g4 intValue]]  
						 l1:[myCupSingleton getLossArrayAtPos:[g1 intValue]] l2:[myCupSingleton getLossArrayAtPos:[g2 intValue]] 
						 l3:[myCupSingleton getLossArrayAtPos:[g3 intValue]] l4:[myCupSingleton getLossArrayAtPos:[g4 intValue]]
						 d1:[myCupSingleton getDrawArrayAtPos:[g1 intValue]] d2:[myCupSingleton getDrawArrayAtPos:[g2 intValue]] 
						 d3:[myCupSingleton getDrawArrayAtPos:[g3 intValue]] d4:[myCupSingleton getDrawArrayAtPos:[g4 intValue]] 
						gs1:[myCupSingleton getGoalScoredArrayAtPos:[g1 intValue]] gs2:[myCupSingleton getGoalScoredArrayAtPos:[g2 intValue]] 
						gs3:[myCupSingleton getGoalScoredArrayAtPos:[g3 intValue]] gs4:[myCupSingleton getGoalScoredArrayAtPos:[g4 intValue]] 
						gc1:[myCupSingleton getGoalConcededArrayAtPos:[g1 intValue]] gc2:[myCupSingleton getGoalConcededArrayAtPos:[g2 intValue]]
						gc3:[myCupSingleton getGoalConcededArrayAtPos:[g3 intValue]] gc4:[myCupSingleton getGoalConcededArrayAtPos:[g4 intValue]] 
						gd1:[myCupSingleton getGoalDifferenceArrayAtPos:[g1 intValue]] gd2:[myCupSingleton getGoalDifferenceArrayAtPos:[g2 intValue]]
						gd3:[myCupSingleton getGoalDifferenceArrayAtPos:[g3 intValue]] gd4:[myCupSingleton getGoalDifferenceArrayAtPos:[g4 intValue]]];
		
		[self setMatchBoard:spriteBoardGroupH
					   p1p2:[myCupSingleton getP1P2Array:7] p1p3:[myCupSingleton getP1P3Array:7] p1p4:[myCupSingleton getP1P4Array:7] 
					   p2p1:[myCupSingleton getP2P1Array:7] p2p3:[myCupSingleton getP2P3Array:7] p2p4:[myCupSingleton getP2P4Array:7] 
					   p3p1:[myCupSingleton getP3P1Array:7] p3p2:[myCupSingleton getP3P2Array:7] p3p4:[myCupSingleton getP3P4Array:7] 
					   p4p1:[myCupSingleton getP4P1Array:7] p4p2:[myCupSingleton getP4P2Array:7] p4p3:[myCupSingleton getP4P3Array:7]];
		
	}
	return self;
}

-(int)setBestTeam:(int)tid1 with:(int)tid2 with:(int)p1 with:(int)p2 with:(int)gd1 with:(int)gd2 with:(int)gs1 with:(int)gs2{
	NSLog(@"T1: %i p1: %i gd1: %i", tid1,p1,gd1);
	
	if(p1>p2){
		return (int)tid1;
	}else if(p2>p1){
		return (int)tid2;
	}else{
		if(gd1>gd2){
			return (int)tid1;
		}else if(gd2>gd1){
			return (int)tid2;
		}else{
			if(gs1>gs2){
				return (int)tid1;
			}else if(gs2>gs1){
				return (int)tid2;
			}else{
				if(tid1<tid2){
					return (int)tid1;
				}else{
					return (int)tid2;
				}
			}
		}
	}
	return 500;
}

-(int)setWorstTeam:(int)tid1 with:(int)tid2 with:(int)p1 with:(int)p2 with:(int)gd1 with:(int)gd2 with:(int)gs1 with:(int)gs2{
	
	if(p1<p2){
		return (int)tid1;
	}else if(p2<p1){
		return (int)tid2;
	}else{
		if(gd1<gd2){
			return (int)tid1;
		}else if(gd2<gd1){
			return (int)tid2;
		}else{
			if(gs1<gs2){
				return (int)tid1;
			}else if(gs2<gs1){
				return (int)tid2;
			}else{
				if(tid1>tid2){
					return (int)tid1;
				}else{
					return (int)tid2;
				}
			}
		}
	}
	return 400;
}
-(void)startDelayLoading:(ccTime)ts {
	 [self unschedule:@selector(startDelayLoading:)];
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	
	NSLog( @"------------------------------------------------------");
	NSLog( @"THE ACTUAL MATCH on TEAMGROUPSCENE END IS : %i",[myCupSingleton atualMatch]);
	NSLog( @"------------------------------------------------------");
	
	if((int)[myCupSingleton atualMatch]<=2){
		NSLog( @"---------------------GO PREMATCH---------------------");
		[self goPlay];


	}else{
		NSLog( @"---------------------GO TOURNAMENT---------------------");
		[self goTournament];
	}
	
}

-(void)loadingScreen{
    CCSprite * bg = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            bg = [CCSprite spriteWithFile:@"loading-ipadhd.jpg"];
        }else{
            //iPad screen
            bg = [CCSprite spriteWithFile:@"loading-ipad.jpg"];
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                    //iphone 5
                    bg = [CCSprite spriteWithFile:@"loading-hd.jpg"];
                }else{
                    //iphone retina screen
                    bg = [CCSprite spriteWithFile:@"loading-hd.jpg"];
                }
            }else{
                //iphone screen
                bg = [CCSprite spriteWithFile:@"loading.jpg"];
            }
        }
    }
    [bg setPosition:ADJUST_CCP(ccp(240, 160))];
    
	[self addChild:bg z:5000];
}




-(void)setScoreBoard:(CCSprite*)sprite tid1:(int)tid1 tid2:(int)tid2 tid3:(int)tid3 tid4:(int)tid4 
				t1:(NSString*)t1 t2:(NSString*)t2 t3:(NSString*)t3 t4:(NSString*)t4 
				flag1:(NSString*)flag1 flag2:(NSString*)flag2 flag3:(NSString*)flag3 flag4:(NSString*)flag4
				p1:(int)p1 p2:(int)p2 p3:(int)p3 p4:(int)p4 
				w1:(int)w1 w2:(int)w2 w3:(int)w3 w4:(int)w4
				l1:(int)l1 l2:(int)l2 l3:(int)l3 l4:(int)l4 
				d1:(int)d1 d2:(int)d2 d3:(int)d3 d4:(int)d4 
				gs1:(int)gs1 gs2:(int)gs2 gs3:(int)gs3 gs4:(int)gs4 
				gc1:(int)gc1 gc2:(int)gc2 gc3:(int)gc3 gc4:(int)gc4 
				gd1:(int)gd1 gd2:(int)gd2 gd3:(int)gd3 gd4:(int)gd4 {


    //HORIZONTAL
	CCLabelTTF *labelMatchP1Top = [CCLabelTTF labelWithString:t1 fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelMatchP1Top setColor:ccBLACK];
	[labelMatchP1Top setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(105,130))];
	[sprite addChild: labelMatchP1Top];
	spriteFlag1 = [CCSprite spriteWithSpriteFrameName:flag1];
	[spriteFlag1 setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(130,132))];
	[sprite addChild: spriteFlag1];

	
	CCLabelTTF *labelMatchP2Top = [CCLabelTTF labelWithString:t2 fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelMatchP2Top setColor:ccBLACK];
	[sprite addChild: labelMatchP2Top];
	[labelMatchP2Top setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(158,130))];
	spriteFlag2 = [CCSprite spriteWithSpriteFrameName:flag2];
	[spriteFlag2 setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(183,132))];
	[sprite addChild: spriteFlag2];
	
	CCLabelTTF *labelMatchP3Top = [CCLabelTTF labelWithString:t3 fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelMatchP3Top setColor:ccBLACK];
	[sprite addChild: labelMatchP3Top];
	[labelMatchP3Top setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(209,130))];
	spriteFlag3 = [CCSprite spriteWithSpriteFrameName:flag3];
	[spriteFlag3 setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(234,132))];
	[sprite addChild: spriteFlag3];
	
	CCLabelTTF *labelMatchP4Top = [CCLabelTTF labelWithString:t4 fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelMatchP4Top setColor:ccBLACK];
	[sprite addChild: labelMatchP4Top];
	[labelMatchP4Top setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(260,130))];
	spriteFlag4 = [CCSprite spriteWithSpriteFrameName:flag4];
	[spriteFlag4 setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(285,132))];
	[sprite addChild: spriteFlag4];
	//VERTICAL
	CCLabelTTF *labelMatchP1Side = [CCLabelTTF labelWithString:t1 fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelMatchP1Side setColor:ccBLACK];
	[labelMatchP1Side setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(50,108))];
	[sprite addChild: labelMatchP1Side];
	spriteFlag1 = [CCSprite spriteWithSpriteFrameName:flag1];
	[spriteFlag1 setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(78,109))];
	[sprite addChild: spriteFlag1];
	
	CCLabelTTF *labelMatchP2Side = [CCLabelTTF labelWithString:t2 fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelMatchP2Side setColor:ccBLACK];
	[sprite addChild: labelMatchP2Side];
	[labelMatchP2Side setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(50,86))];
	spriteFlag2 = [CCSprite spriteWithSpriteFrameName:flag2];
	[spriteFlag2 setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(78,87))];
	[sprite addChild: spriteFlag2];
	
	CCLabelTTF *labelMatchP3Side = [CCLabelTTF labelWithString:t3 fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelMatchP3Side setColor:ccBLACK];
	[sprite addChild: labelMatchP3Side];
	[labelMatchP3Side setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(50,64))];
	spriteFlag3 = [CCSprite spriteWithSpriteFrameName:flag3];
	[spriteFlag3 setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(78,65))];
	[sprite addChild: spriteFlag3];
	
	CCLabelTTF *labelMatchP4Side = [CCLabelTTF labelWithString:t4 fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelMatchP4Side setColor:ccBLACK];
	[sprite addChild: labelMatchP4Side];
	[labelMatchP4Side setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(50,42))];
	spriteFlag4 = [CCSprite spriteWithSpriteFrameName:flag4];
	[spriteFlag4 setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(78,43))];
	[sprite addChild: spriteFlag4];

	//RANKING TABLE
	int t1ido =0;
	int t2ido =0;
	int t3ido =0;
	int t4ido =0;
	
	t1ido = 100;
	t2ido = 100;
	t3ido = 100;
	t4ido = 100;
	
	NSString* t1o =0;
	NSString* t2o =0;
	NSString* t3o =0;
	NSString* t4o =0;
	NSString* flag1o =0;
	NSString* flag2o =0;
	NSString* flag3o =0;
	NSString* flag4o =0;
	
	int p1o=0, p2o=0, p3o=0, p4o=0;
	int w1o=0, w2o=0, w3o=0, w4o=0;
	int d1o=0, d2o=0, d3o=0, d4o=0;
	int l1o=0, l2o=0, l3o=0, l4o=0;
	int gs1o=0, gs2o=0, gs3o=0, gs4o=0;
	int gc1o=0, gc2o=0, gc3o=0, gc4o=0;
	int gd1o=0, gd2o=0, gd3o=0, gd4o=0;
	
	//FIND t1o
	
	t3ido = [self setBestTeam:tid1 with:tid2 with:p1 with:p2 with:gd1 with:gd2 with:gs1 with:gs2];
	if(t3ido==tid1){
		t2ido = [self setBestTeam:tid1 with:tid3 with:p1 with:p3 with:gd1 with:gd3 with:gs1 with:gs3];
		if(t2ido==tid1){
			t1ido = [self setBestTeam:tid1 with:tid4 with:p1 with:p4 with:gd1 with:gd4 with:gs1 with:gs4];	
			if(t1ido == tid1){
				//Set 1
				t1o = t1;
				flag1o = flag1;
				p1o = p1;
				w1o = w1;
				d1o = d1;
				l1o = l1;
				gs1o = gs1;
				gc1o = gc1;
				gd1o = gd1;
			}else{
				//Set 1
				t1o = t4;
				flag1o = flag4;
				p1o = p4;
				w1o = w4;
				d1o = d4;
				l1o = l4;
				gs1o = gs4;
				gc1o = gc4;
				gd1o = gd4;
			}
		}else{
			t1ido = [self setBestTeam:tid3 with:tid4 with:p3 with:p4 with:gd3 with:gd4 with:gs3 with:gs4];
			if(t1ido == tid3){
				//Set 1
				t1o = t3;
				flag1o = flag3;
				p1o = p3;
				w1o = w3;
				d1o = d3;
				l1o = l3;
				gs1o = gs3;
				gc1o = gc3;
				gd1o = gd3;
			}else{
				//Set 1
				t1o = t4;
				flag1o = flag4;
				p1o = p4;
				w1o = w4;
				d1o = d4;
				l1o = l4;
				gs1o = gs4;
				gc1o = gc4;
				gd1o = gd4;
			}
		}
	}else{
		t2ido = [self setBestTeam:tid2 with:tid3 with:p2 with:p3 with:gd2 with:gd3 with:gs2 with:gs3];
		if(t2ido==tid2){
			t1ido = [self setBestTeam:tid2 with:tid4 with:p2 with:p4 with:gd2 with:gd4 with:gs2 with:gs4];
			if(t1ido == tid2){
				//Set 1
				t1o = t2;
				flag1o = flag2;
				p1o = p2;
				w1o = w2;
				d1o = d2;
				l1o = l2;
				gs1o = gs2;
				gc1o = gc2;
				gd1o = gd2;
			}else{
				//Set 1
				t1o = t4;
				flag1o = flag4;
				p1o = p4;
				w1o = w4;
				d1o = d4;
				l1o = l4;
				gs1o = gs4;
				gc1o = gc4;
				gd1o = gd4;
			}
		}else{
			t1ido = [self setBestTeam:tid3 with:tid4 with:p3 with:p4 with:gd3 with:gd4 with:gs3 with:gs4];
			if(t1ido == tid3){
				//Set 1
				t1o = t3;
				flag1o = flag3;
				p1o = p3;
				w1o = w3;
				d1o = d3;
				l1o = l3;
				gs1o = gs3;
				gc1o = gc3;
				gd1o = gd3;
			}else{
				//Set 1
				t1o = t4;
				flag1o = flag4;
				p1o = p4;
				w1o = w4;
				d1o = d4;
				l1o = l4;
				gs1o = gs4;
				gc1o = gc4;
				gd1o = gd4;
			}
			
		}
	}
	

	//FIND t1o
	
	t2ido = [self setWorstTeam:tid1 with:tid2 with:p1 with:p2 with:gd1 with:gd2 with:gs1 with:gs2];
	if(t2ido==tid1){
		t3ido = [self setWorstTeam:tid1 with:tid3 with:p1 with:p3 with:gd1 with:gd3 with:gs1 with:gs3];
		if(t3ido==tid1){
			t4ido = [self setWorstTeam:tid1 with:tid4 with:p1 with:p4 with:gd1 with:gd4 with:gs1 with:gs4];	
			if(t4ido == tid1){
				//Set 4
				t4o = t1;
				flag4o = flag1;
				p4o = p1;
				w4o = w1;
				d4o = d1;
				l4o = l1;
				gs4o = gs1;
				gc4o = gc1;
				gd4o = gd1;
			}else{
				//Set 4
				t4o = t4;
				flag4o = flag4;
				p4o = p4;
				w4o = w4;
				d4o = d4;
				l4o = l4;
				gs4o = gs4;
				gc4o = gc4;
				gd4o = gd4;
			}
		}else{
			t4ido = [self setWorstTeam:tid3 with:tid4 with:p3 with:p4 with:gd3 with:gd4 with:gs3 with:gs4];
			if(t4ido == tid3){
				//Set 4
				t4o = t3;
				flag4o = flag3;
				p4o = p3;
				w4o = w3;
				d4o = d3;
				l4o = l3;
				gs4o = gs3;
				gc4o = gc3;
				gd4o = gd3;
			}else{
				//Set 4
				t4o = t4;
				flag4o = flag4;
				p4o = p4;
				w4o = w4;
				d4o = d4;
				l4o = l4;
				gs4o = gs4;
				gc4o = gc4;
				gd4o = gd4;
			}
		}
	}else{
		t3ido = [self setWorstTeam:tid2 with:tid3 with:p2 with:p3 with:gd2 with:gd3 with:gs2 with:gs3];
		if(t3ido==tid2){
			t4ido = [self setWorstTeam:tid2 with:tid4 with:p2 with:p4 with:gd2 with:gd4 with:gs2 with:gs4];
			if(t4ido == tid2){
				//Set 4
				t4o = t2;
				flag4o = flag2;
				p4o = p2;
				w4o = w2;
				d4o = d2;
				l4o = l2;
				gs4o = gs2;
				gc4o = gc2;
				gd4o = gd2;
			}else{
				//Set 4
				t4o = t4;
				flag4o = flag4;
				p4o = p4;
				w4o = w4;
				d4o = d4;
				l4o = l4;
				gs4o = gs4;
				gc4o = gc4;
				gd4o = gd4;
			}
		}else{
			t4ido = [self setWorstTeam:tid3 with:tid4 with:p3 with:p4 with:gd3 with:gd4 with:gs3 with:gs4];
			if(t4ido == tid3){
				//Set 4
				t4o = t3;
				flag4o = flag3;
				p4o = p3;
				w4o = w3;
				d4o = d3;
				l4o = l3;
				gs4o = gs3;
				gc4o = gc3;
				gd4o = gd3;
			}else{
				//Set 4
				t4o = t4;
				flag4o = flag4;
				p4o = p4;
				w4o = w4;
				d4o = d4;
				l4o = l4;
				gs4o = gs4;
				gc4o = gc4;
				gd4o = gd4;
			}
		}
	}
	
	
	
	
	//FIND t2o and t3o
	NSLog(@"T1 ID :%i  T4 ID:%i",t1ido,t4ido);
	
	if((t1ido == tid1 && t4ido==tid2) || (t1ido == tid2 && t4ido==tid1)){
		t2ido = [self setBestTeam:tid3 with:tid4 with:p3 with:p4 with:gd3 with:gd4 with:gs3 with:gs4];
		t3ido = [self setWorstTeam:tid3 with:tid4 with:p3 with:p4 with:gd3 with:gd4 with:gs3 with:gs4];
		if(t3ido == tid3){
			//Set 2 and 3
			t3o = t3;
			flag3o = flag3;
			p3o = p3;
			w3o = w3;
			d3o = d3;
			l3o = l3;
			gs3o = gs3;
			gc3o = gc3;
			gd3o = gd3;
			
			t2o = t4;
			flag2o = flag4;
			p2o = p4;
			w2o = w4;
			d2o = d4;
			l2o = l4;
			gs2o = gs4;
			gc2o = gc4;
			gd2o = gd4;
		}else{
			//Set 2 and 3
			t3o = t4;
			flag3o = flag4;
			p3o = p4;
			w3o = w4;
			d3o = d4;
			l3o = l4;
			gs3o = gs4;
			gc3o = gc4;
			gd3o = gd4;
			
			t2o = t3;
			flag2o = flag3;
			p2o = p3;
			w2o = w3;
			d2o = d3;
			l2o = l3;
			gs2o = gs3;
			gc2o = gc3;
			gd2o = gd3;
		}
		
		
	}else if((t1ido == tid1 && t4ido==tid3) || (t1ido == tid3 && t4ido==tid1)){
		t2ido = [self setBestTeam:tid2 with:tid4 with:p2 with:p4 with:gd2 with:gd4 with:gs2 with:gs4];
		t3ido = [self setWorstTeam:tid2 with:tid4 with:p2 with:p4 with:gd2 with:gd4 with:gs2 with:gs4];
		if(t3ido == tid2){
			//Set 3 and 4
			t3o = t2;
			flag3o = flag2;
			p3o = p2;
			w3o = w2;
			d3o = d2;
			l3o = l2;
			gs3o = gs2;
			gc3o = gc2;
			gd3o = gd2;
			
			t2o = t4;
			flag2o = flag4;
			p2o = p4;
			w2o = w4;
			d2o = d4;
			l2o = l4;
			gs2o = gs4;
			gc2o = gc4;
			gd2o = gd4;
		}else{
			//Set 3 and 4
			t3o = t4;
			flag3o = flag4;
			p3o = p4;
			w3o = w4;
			d3o = d4;
			l3o = l4;
			gs3o = gs4;
			gc3o = gc4;
			gd3o = gd4;
			
			t2o = t2;
			flag2o = flag2;
			p2o = p2;
			w2o = w2;
			d2o = d2;
			l2o = l2;
			gs2o = gs2;
			gc2o = gc2;
			gd2o = gd2;
		}
		

	}else if((t1ido == tid1 && t4ido==tid4) || (t1ido == tid4 && t4ido==tid1)){
		t2ido = [self setBestTeam:tid2 with:tid3 with:p2 with:p3 with:gd2 with:gd3 with:gs2 with:gs3];
		t3ido = [self setWorstTeam:tid2 with:tid3 with:p2 with:p3 with:gd2 with:gd3 with:gs2 with:gs3];
		if(t3ido == tid2){
			//Set 3 and 4
			t3o = t2;
			flag3o = flag2;
			p3o = p2;
			w3o = w2;
			d3o = d2;
			l3o = l2;
			gs3o = gs2;
			gc3o = gc2;
			gd3o = gd2;
			
			t2o = t3;
			flag2o = flag3;
			p2o = p3;
			w2o = w3;
			d2o = d3;
			l2o = l3;
			gs2o = gs3;
			gc2o = gc3;
			gd2o = gd3;
		}else{
			//Set 3 and 4
			t3o = t3;
			flag3o = flag3;
			p3o = p3;
			w3o = w3;
			d3o = d3;
			l3o = l3;
			gs3o = gs3;
			gc3o = gc3;
			gd3o = gd3;
			
			t2o = t2;
			flag2o = flag2;
			p2o = p2;
			w2o = w2;
			d2o = d2;
			l2o = l2;
			gs2o = gs2;
			gc2o = gc2;
			gd2o = gd2;
		}
		
	}else if((t1ido == tid2 && t4ido==tid3) || (t1ido == tid3 && t4ido==tid2)){
		t2ido = [self setBestTeam:tid1 with:tid4 with:p1 with:p4 with:gd1 with:gd4 with:gs1 with:gs4];
		t3ido = [self setWorstTeam:tid1 with:tid4 with:p1 with:p4 with:gd1 with:gd4 with:gs1 with:gs4];
		if(t3ido == tid1){
			//Set 3 and 4
			t3o = t1;
			flag3o = flag1;
			p3o = p1;
			w3o = w1;
			d3o = d1;
			l3o = l1;
			gs3o = gs1;
			gc3o = gc1;
			gd3o = gd1;
			
			t2o = t4;
			flag2o = flag4;
			p2o = p4;
			w2o = w4;
			d2o = d4;
			l2o = l4;
			gs2o = gs4;
			gc2o = gc4;
			gd2o = gd4;
		}else{
			//Set 3 and 4
			t3o = t4;
			flag3o = flag4;
			p3o = p4;
			w3o = w4;
			d3o = d4;
			l3o = l4;
			gs3o = gs4;
			gc3o = gc4;
			gd3o = gd4;
			
			t2o = t1;
			flag2o = flag1;
			p2o = p1;
			w2o = w1;
			d2o = d1;
			l2o = l1;
			gs2o = gs1;
			gc2o = gc1;
			gd2o = gd1;
		}
	}else if((t1ido == tid2 && t4ido==tid4) || (t1ido == tid4 && t4ido==tid2)){
		t2ido = [self setBestTeam:tid1 with:tid3 with:p1 with:p3 with:gd1 with:gd3 with:gs1 with:gs3];
		t3ido = [self setWorstTeam:tid1 with:tid3 with:p1 with:p3 with:gd1 with:gd3 with:gs1 with:gs3];
		if(t3ido == tid1){
			//Set 3 and 4
			t3o = t1;
			flag3o = flag1;
			p3o = p1;
			w3o = w1;
			d3o = d1;
			l3o = l1;
			gs3o = gs1;
			gc3o = gc1;
			gd3o = gd1;
			
			t2o = t3;
			flag2o = flag3;
			p2o = p3;
			w2o = w3;
			d2o = d3;
			l2o = l3;
			gs2o = gs3;
			gc2o = gc3;
			gd2o = gd3;
		}else{
			//Set 3 and 4
			t3o = t3;
			flag3o = flag3;
			p3o = p3;
			w3o = w3;
			d3o = d3;
			l3o = l3;
			gs3o = gs3;
			gc3o = gc3;
			gd3o = gd3;
			
			t2o = t1;
			flag2o = flag1;
			p2o = p1;
			w2o = w1;
			d2o = d1;
			l2o = l1;
			gs2o = gs1;
			gc2o = gc1;
			gd2o = gd1;
		}
	}else if((t1ido==tid3 && t4ido==tid4) || (t1ido==tid4 && t4ido==tid3)){
		t2ido = [self setBestTeam:tid1 with:tid2 with:p1 with:p2 with:gd1 with:gd2 with:gs1 with:gs2];
		t3ido = [self setWorstTeam:tid1 with:tid2 with:p1 with:p2 with:gd1 with:gd2 with:gs1 with:gs2];
		if(t3ido == tid1){
			//Set 3 and 4
			t3o = t1;
			flag3o = flag1;
			p3o = p1;
			w3o = w1;
			d3o = d1;
			l3o = l1;
			gs3o = gs1;
			gc3o = gc1;
			gd3o = gd1;
			
			t2o = t2;
			flag2o = flag2;
			p2o = p2;
			w2o = w2;
			d2o = d2;
			l2o = l2;
			gs2o = gs2;
			gc2o = gc2;
			gd2o = gd2;
		}else{
			//Set 3 and 4
			t3o = t2;
			flag3o = flag2;
			p3o = p2;
			w3o = w2;
			d3o = d2;
			l3o = l2;
			gs3o = gs2;
			gc3o = gc2;
			gd3o = gd2;
			
			t2o = t1;
			flag2o = flag1;
			p2o = p1;
			w2o = w1;
			d2o = d1;
			l2o = l1;
			gs2o = gs1;
			gc2o = gc1;
			gd2o = gd1;
		}
		
		
	}
	
	NSLog(@"T1 ID :%i  T2 ID:%i  T3 ID:%i  T4 ID:%i",t1ido,t2ido,t3ido,t4ido);

	CGPoint posiLabel1;
	CGPoint posiLabel2;
	CGPoint posiLabel3;
	CGPoint posiLabel4;
	CGPoint posi1;
	CGPoint posi2;
	CGPoint posi3;
	CGPoint posi4;
	
	
	posiLabel1 = ADJUST_INTERNAL_SPRITE_CCP(ccp(46,237));
	posi1 = ADJUST_INTERNAL_SPRITE_CCP(ccp(72,238));

	posiLabel2 = ADJUST_INTERNAL_SPRITE_CCP(ccp(46,215));
	posi2 = ADJUST_INTERNAL_SPRITE_CCP(ccp(72,216));
		
	posiLabel3 = ADJUST_INTERNAL_SPRITE_CCP(ccp(46,193));
	posi3 = ADJUST_INTERNAL_SPRITE_CCP(ccp(72,194));
		
	posiLabel4 = ADJUST_INTERNAL_SPRITE_CCP(ccp(46,171));
	posi4 = ADJUST_INTERNAL_SPRITE_CCP(ccp(72,172));
	
	DataHolder *myDataHolder = [DataHolder sharedInstance];
	
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	NSLog(@"ATUAL MATCH : %i",[myCupSingleton atualMatch]);
	if([myCupSingleton atualMatch]==3){
		if([myDataHolder getGroupForTeamKey:t1ido]==0){
			[myCupSingleton setTournament1T1:t1ido];
		}
		if([myDataHolder getGroupForTeamKey:t2ido]==1){
			[myCupSingleton setTournament1T2:t2ido];
		}
		if([myDataHolder getGroupForTeamKey:t1ido]==2){
			[myCupSingleton setTournament2T1:t1ido];
		}
		if([myDataHolder getGroupForTeamKey:t2ido]==3){
			[myCupSingleton setTournament2T2:t2ido];
		}
		if([myDataHolder getGroupForTeamKey:t1ido]==3){
			[myCupSingleton setTournament3T1:t1ido];
		}
		if([myDataHolder getGroupForTeamKey:t2ido]==2){
			[myCupSingleton setTournament3T2:t2ido];
		}
		if([myDataHolder getGroupForTeamKey:t1ido]==1){
			[myCupSingleton setTournament4T1:t1ido];
		}
		if([myDataHolder getGroupForTeamKey:t2ido]==0){
			[myCupSingleton setTournament4T2:t2ido];
		}
		
		if([myDataHolder getGroupForTeamKey:t1ido]==4){
			[myCupSingleton setTournament5T1:t1ido];
		}
		if([myDataHolder getGroupForTeamKey:t2ido]==5){
			[myCupSingleton setTournament5T2:t2ido];
		}
		if([myDataHolder getGroupForTeamKey:t1ido]==6){
			[myCupSingleton setTournament6T1:t1ido];
		}
		if([myDataHolder getGroupForTeamKey:t2ido]==7){
			[myCupSingleton setTournament6T2:t2ido];
		}
		if([myDataHolder getGroupForTeamKey:t1ido]==5){
			[myCupSingleton setTournament7T1:t1ido];
		}
		if([myDataHolder getGroupForTeamKey:t2ido]==4){
			[myCupSingleton setTournament7T2:t2ido];
		}
		if([myDataHolder getGroupForTeamKey:t1ido]==7){
			[myCupSingleton setTournament8T1:t1ido];
		}
		if([myDataHolder getGroupForTeamKey:t2ido]==6){
			[myCupSingleton setTournament8T2:t2ido];
		}
	}
	
	CCLabelTTF *labelRankP1 = [CCLabelTTF labelWithString:t1o fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelRankP1 setColor:ccBLACK];
	[labelRankP1 setPosition: posiLabel1];
	[sprite addChild: labelRankP1];
	spriteFlag1 = [CCSprite spriteWithSpriteFrameName:flag1o];
	[spriteFlag1 setPosition: posi1];
	[sprite addChild: spriteFlag1];
	//P1
	NSString* p1Str= [NSString stringWithFormat:@"%i",p1o];
	CCLabelTTF *labelPointP1 = [CCLabelTTF labelWithString:p1Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelPointP1 setColor:ccBLACK];
	[labelPointP1 setDimensions:CGSizeMake(HD_PIXELS(6),HD_PIXELS(12))];
	[labelPointP1 setPosition: ccp(posiLabel1.x+HD_PIXELS(56),posiLabel1.y)];
    [sprite addChild: labelPointP1];
	//W1
	NSString* w1Str= [NSString stringWithFormat:@"%i",w1o];
	CCLabelTTF *labelWinP1 = [CCLabelTTF labelWithString:w1Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelWinP1 setColor:ccBLACK];
	[labelWinP1 setDimensions:CGSizeMake(HD_PIXELS(6),HD_PIXELS(12))];
	[labelWinP1 setPosition: ccp(posiLabel1.x+HD_PIXELS(84),posiLabel1.y)]; ;
	[sprite addChild: labelWinP1];
	//D1
	NSString* d1Str= [NSString stringWithFormat:@"%i",d1o];
	CCLabelTTF *labelDrawP1 = [CCLabelTTF labelWithString:d1Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelDrawP1 setColor:ccBLACK];
	[labelDrawP1 setPosition: ccp(posiLabel1.x+HD_PIXELS(114),posiLabel1.y)]; ;
	[sprite addChild: labelDrawP1];
	//L1
	NSString* l1Str= [NSString stringWithFormat:@"%i",l1o];
	CCLabelTTF *labelLooseP1 = [CCLabelTTF labelWithString:l1Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelLooseP1 setColor:ccBLACK];
	[labelLooseP1 setPosition: ccp(posiLabel1.x+HD_PIXELS(144),posiLabel1.y)]; ;
	[sprite addChild: labelLooseP1];
	//GS1
	NSString* gs1Str= [NSString stringWithFormat:@"%i",gs1o];
	CCLabelTTF *labelGSP1 = [CCLabelTTF labelWithString:gs1Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGSP1 setColor:ccBLACK];
	[labelGSP1 setPosition: ccp(posiLabel1.x+HD_PIXELS(174),posiLabel1.y)]; ;
	[sprite addChild: labelGSP1];
	//GC1
	NSString* gc1Str= [NSString stringWithFormat:@"%i",gc1o];
	CCLabelTTF *labelGCP1 = [CCLabelTTF labelWithString:gc1Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGCP1 setColor:ccBLACK];
	[labelGCP1 setPosition:ccp(posiLabel1.x+HD_PIXELS(206),posiLabel1.y)]; ;
	[sprite addChild: labelGCP1];
	//GD1
	NSString* gd1Str= [NSString stringWithFormat:@"%i",gd1o];
	CCLabelTTF *labelGDP1 = [CCLabelTTF labelWithString:gd1Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGDP1 setColor:ccBLACK];
	[labelGDP1 setPosition: ccp(posiLabel1.x+HD_PIXELS(238),posiLabel1.y)]; ;
	[sprite addChild: labelGDP1];
	
	
	
	CCLabelTTF *labelRankP2 = [CCLabelTTF labelWithString:t2o fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelRankP2 setColor:ccBLACK];
	[labelRankP2 setPosition: posiLabel2];
	[sprite addChild: labelRankP2];
	spriteFlag2 = [CCSprite spriteWithSpriteFrameName:flag2o];
	[spriteFlag2 setPosition: posi2];
	[sprite addChild: spriteFlag2];
	
	//P2
	NSString* p2Str= [NSString stringWithFormat:@"%i",p2o];
	CCLabelTTF *labelPointP2 = [CCLabelTTF labelWithString:p2Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelPointP2 setColor:ccBLACK];
	[labelPointP2 setPosition: ccp(posiLabel2.x+HD_PIXELS(56),posiLabel2.y)]; ;
	[sprite addChild: labelPointP2];
	//W2
	NSString* w2Str= [NSString stringWithFormat:@"%i",w2o];
	CCLabelTTF *labelWinP2 = [CCLabelTTF labelWithString:w2Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelWinP2 setColor:ccBLACK];
	[labelWinP2 setPosition: ccp(posiLabel2.x+HD_PIXELS(84),posiLabel2.y)]; ;
	[sprite addChild: labelWinP2];
	//D2
	NSString* d2Str= [NSString stringWithFormat:@"%i",d2o];
	CCLabelTTF *labelDrawP2 = [CCLabelTTF labelWithString:d2Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelDrawP2 setColor:ccBLACK];
	[labelDrawP2 setPosition: ccp(posiLabel2.x+HD_PIXELS(114),posiLabel2.y)]; ;
	[sprite addChild: labelDrawP2];
	//L2
	NSString* l2Str= [NSString stringWithFormat:@"%i",l2o];
	CCLabelTTF *labelLooseP2 = [CCLabelTTF labelWithString:l2Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelLooseP2 setColor:ccBLACK];
	[labelLooseP2 setPosition: ccp(posiLabel2.x+HD_PIXELS(144),posiLabel2.y)]; ;
	[sprite addChild: labelLooseP2];
	//GS2
	NSString* gs2Str= [NSString stringWithFormat:@"%i",gs2o];
	CCLabelTTF *labelGSP2 = [CCLabelTTF labelWithString:gs2Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGSP2 setColor:ccBLACK];
	[labelGSP2 setPosition: ccp(posiLabel2.x+HD_PIXELS(174),posiLabel2.y)]; ;
	[sprite addChild: labelGSP2];
	//GC2
	NSString* gc2Str= [NSString stringWithFormat:@"%i",gc2o];
	CCLabelTTF *labelGCP2 = [CCLabelTTF labelWithString:gc2Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGCP2 setColor:ccBLACK];
	[labelGCP2 setPosition: ccp(posiLabel2.x+HD_PIXELS(206),posiLabel2.y)]; ;
	[sprite addChild: labelGCP2];
	//GD2
	NSString* gd2Str= [NSString stringWithFormat:@"%i",gd2o];
	CCLabelTTF *labelGDP2 = [CCLabelTTF labelWithString:gd2Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGDP2 setColor:ccBLACK];
	[labelGDP2 setPosition: ccp(posiLabel2.x+HD_PIXELS(238),posiLabel2.y)]; ;
	[sprite addChild: labelGDP2];
	
	
	
	CCLabelTTF *labelRankP3 = [CCLabelTTF labelWithString:t3o fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelRankP3 setColor:ccBLACK];
	[labelRankP3 setPosition: posiLabel3];
	[sprite addChild: labelRankP3];
	spriteFlag3 = [CCSprite spriteWithSpriteFrameName:flag3o];
	[spriteFlag3 setPosition: posi3];
	[sprite addChild: spriteFlag3];
	
	//P3
	NSString* p3Str= [NSString stringWithFormat:@"%i",p3o];
	CCLabelTTF *labelPointP3 = [CCLabelTTF labelWithString:p3Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelPointP3 setColor:ccBLACK];
	[labelPointP3 setPosition: ccp(posiLabel3.x+HD_PIXELS(56),posiLabel3.y)]; ;
	[sprite addChild: labelPointP3];
	//W3
	NSString* w3Str= [NSString stringWithFormat:@"%i",w3o];
	CCLabelTTF *labelWinP3 = [CCLabelTTF labelWithString:w3Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelWinP3 setColor:ccBLACK];
	[labelWinP3 setPosition: ccp(posiLabel3.x+HD_PIXELS(84),posiLabel3.y)]; ;
	[sprite addChild: labelWinP3];
	//D3
	NSString* d3Str= [NSString stringWithFormat:@"%i",d3o];
	CCLabelTTF *labelDrawP3 = [CCLabelTTF labelWithString:d3Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelDrawP3 setColor:ccBLACK];
	[labelDrawP3 setPosition: ccp(posiLabel3.x+HD_PIXELS(114),posiLabel3.y)]; ;
	[sprite addChild: labelDrawP3];
	//L3
	NSString* l3Str= [NSString stringWithFormat:@"%i",l3o];
	CCLabelTTF *labelLooseP3 = [CCLabelTTF labelWithString:l3Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelLooseP3 setColor:ccBLACK];
	[labelLooseP3 setPosition: ccp(posiLabel3.x+HD_PIXELS(144),posiLabel3.y)]; ;
	[sprite addChild: labelLooseP3];
	//GS3
	NSString* gs3Str= [NSString stringWithFormat:@"%i",gs3o];
	CCLabelTTF *labelGSP3 = [CCLabelTTF labelWithString:gs3Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGSP3 setColor:ccBLACK];
	[labelGSP3 setPosition: ccp(posiLabel3.x+HD_PIXELS(174),posiLabel3.y)]; ;
	[sprite addChild: labelGSP3];
	//GC3
	NSString* gc3Str= [NSString stringWithFormat:@"%i",gc3o];
	CCLabelTTF *labelGCP3 = [CCLabelTTF labelWithString:gc3Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGCP3 setColor:ccBLACK];
	[labelGCP3 setPosition: ccp(posiLabel3.x+HD_PIXELS(206),posiLabel3.y)]; ;
	[sprite addChild: labelGCP3];
	//GD3
	NSString* gd3Str= [NSString stringWithFormat:@"%i",gd3o];
	CCLabelTTF *labelGDP3 = [CCLabelTTF  labelWithString:gd3Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGDP3 setColor:ccBLACK];
	[labelGDP3 setPosition: ccp(posiLabel3.x+HD_PIXELS(238),posiLabel3.y)]; ;
	[sprite addChild: labelGDP3];
	
	
	CCLabelTTF *labelRankP4 = [CCLabelTTF labelWithString:t4o fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelRankP4 setColor:ccBLACK];
	[labelRankP4 setPosition: posiLabel4];
	[sprite addChild: labelRankP4];
	spriteFlag4 = [CCSprite spriteWithSpriteFrameName:flag4o];
	[spriteFlag4 setPosition: posi4];
	[sprite addChild: spriteFlag4];
	
	//P4
	NSString* p4Str= [NSString stringWithFormat:@"%i",p4o];
	CCLabelTTF *labelPointP4 = [CCLabelTTF labelWithString:p4Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelPointP4 setColor:ccBLACK];
	[labelPointP4 setPosition: ccp(posiLabel4.x+HD_PIXELS(56),posiLabel4.y)]; ;
	[sprite addChild: labelPointP4];
	//W4
	NSString* w4Str= [NSString stringWithFormat:@"%i",w4o];
	CCLabelTTF *labelWinP4 = [CCLabelTTF labelWithString:w4Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelWinP4 setColor:ccBLACK];
	[labelWinP4 setPosition: ccp(posiLabel4.x+HD_PIXELS(84),posiLabel4.y)]; ;
	[sprite addChild: labelWinP4];
	//D4
	NSString* d4Str= [NSString stringWithFormat:@"%i",d4o];
	CCLabelTTF *labelDrawP4 = [CCLabelTTF labelWithString:d4Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelDrawP4 setColor:ccBLACK];
	[labelDrawP4 setPosition: ccp(posiLabel4.x+HD_PIXELS(114),posiLabel4.y)]; ;
	[sprite addChild: labelDrawP4];
	//L4
	NSString* l4Str= [NSString stringWithFormat:@"%i",l4o];
	CCLabelTTF *labelLooseP4 = [CCLabelTTF labelWithString:l4Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelLooseP4 setColor:ccBLACK];
	[labelLooseP4 setPosition: ccp(posiLabel4.x+HD_PIXELS(144),posiLabel4.y)]; ;
	[sprite addChild: labelLooseP4];
	//GS4
	NSString* gs4Str= [NSString stringWithFormat:@"%i",gs4o];
	CCLabelTTF *labelGSP4 = [CCLabelTTF labelWithString:gs4Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGSP4 setColor:ccBLACK];
	[labelGSP4 setPosition: ccp(posiLabel4.x+HD_PIXELS(174),posiLabel4.y)]; ;
	[sprite addChild: labelGSP4];
	//GC4
	NSString* gc4Str= [NSString stringWithFormat:@"%i",gc4o];
	CCLabelTTF *labelGCP4 = [CCLabelTTF labelWithString:gc4Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGCP4 setColor:ccBLACK];
	[labelGCP4 setPosition: ccp(posiLabel4.x+HD_PIXELS(206),posiLabel4.y)]; ;
	[sprite addChild: labelGCP4];
	//GD4
	NSString* gd4Str= [NSString stringWithFormat:@"%i",gd4o];
	CCLabelTTF *labelGDP4 = [CCLabelTTF labelWithString:gd4Str fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[labelGDP4 setColor:ccBLACK];
	[labelGDP4 setPosition: ccp(posiLabel4.x+HD_PIXELS(238),posiLabel4.y)]; ;
	[sprite addChild: labelGDP4];
}

-(void)setMatchBoard:(CCSprite*)sprite 
                p1p2:(int)p1p2 p1p3:(int)p1p3 p1p4:(int)p1p4 
                p2p1:(int)p2p1 p2p3:(int)p2p3 p2p4:(int)p2p4
                p3p1:(int)p3p1 p3p2:(int)p3p2 p3p4:(int)p3p4
                p4p1:(int)p4p1 p4p2:(int)p4p2 p4p3:(int)p4p3
{
    
	CCSprite *spriteResult = nil;
	int p;
	p=p1p2;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else {
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}	
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(170,109))];
		[sprite addChild: spriteResult];
	}
	
	p=p1p3;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(222,109))];
		[sprite addChild: spriteResult];
	}
	
	p=p1p4;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(273,109))];
		[sprite addChild: spriteResult];
	}

	p=p2p1;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}	
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(120,87))];
		[sprite addChild: spriteResult];
	}
	
	p=p2p3;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(222,87))];
		[sprite addChild: spriteResult];
	}
	
	p=p2p4;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(273,87))];
		[sprite addChild: spriteResult];
	}
	
	
	p=p3p1;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}	
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(120,65))];
		[sprite addChild: spriteResult];
	}
	
	p=p3p2;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(170,65))];
		[sprite addChild: spriteResult];
	}
	
	p=p3p4;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(273,65))];
		[sprite addChild: spriteResult];
	}
	
	p=p4p1;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}	
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(120,43))];
		[sprite addChild: spriteResult];
	}
	
	p=p4p2;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(170,43))];
		[sprite addChild: spriteResult];
	}
	
	p=p4p3;
	if(p>0){
		if(p==1){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"win.png"];
		}else if(p==2){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"lost.png"];		
		}else if(p==3){
			spriteResult = [CCSprite spriteWithSpriteFrameName:@"draw.png"];
		}
		[spriteResult setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(222,43))];
		[sprite addChild: spriteResult];
	}
 
}
	
- (void)setupBatchNode {
    NSString *spritesheetPvrCcz = @"spritesheet.pvr.ccz";
    NSString *spritesheetPlist = @"spritesheet.plist";
    
    NSString *gametablePvrCcz = @"gametable.pvr.ccz";
    NSString *gametablePlist = @"gametable.plist";
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            spritesheetPvrCcz = @"spritesheet-ipadhd.pvr.ccz";
            spritesheetPlist = @"spritesheet-ipadhd.plist";
            gametablePvrCcz = @"gametable-ipadhd.pvr.ccz";
            gametablePlist = @"gametable-ipadhd.plist";
        }else{
            //iPad screen
            spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
            spritesheetPlist = @"spritesheet-hd.plist";
            gametablePvrCcz = @"gametable-hd.pvr.ccz";
            gametablePlist = @"gametable-hd.plist";
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
                spritesheetPlist = @"spritesheet-hd.plist";
                
                gametablePvrCcz = @"gametable-hd.pvr.ccz";
                gametablePlist = @"gametable-hd.plist";
            }else{
                //iphone screen
                spritesheetPvrCcz = @"spritesheet.pvr.ccz";
                spritesheetPlist = @"spritesheet.plist";
                
                gametablePvrCcz = @"gametable.pvr.ccz";
                gametablePlist = @"gametable.plist";
            }
        }
    }
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesheetPvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesheetPlist];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:gametablePvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:gametablePlist];
}
	
-(void)selectGroup:(id)sender{

}

-(void)goPlay{
	NSLog(@"GOING PRE MATCH");
	PreGameMatchsScene * gs = [PreGameMatchsScene node];
	[[CCDirector sharedDirector] replaceScene:gs]; 
}

-(void)goTournament{
	TournamentScene * ts = [TournamentScene node];
	[[CCDirector sharedDirector] replaceScene:ts]; 
}
-(void)goNextScreen{
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	
	NSLog( @"------------------------------------------------------");
	NSLog( @"THE ACTUAL MATCH on TEAMGROUPSCENE END IS : %i",[myCupSingleton atualMatch]);
	NSLog( @"------------------------------------------------------");
	
	if((int)[myCupSingleton atualMatch]<=2){
		NSLog( @"---------------------GO PREMATCH---------------------");
		[self goPlay];
		
		
	}else{
		NSLog( @"---------------------GO TOURNAMENT---------------------");
		[self goTournament];
	}
	
}
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Touch Events ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////


-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	//[touchCCSprite setPosition:location];
	if (CGRectContainsPoint(CGRectMake(spriteMenuGroupA.position.x- 0.5*spriteMenuGroupA.contentSize.width, spriteMenuGroupA.position.y- 0.5*spriteMenuGroupA.contentSize.height,
									   spriteMenuGroupA.contentSize.width, spriteMenuGroupA.contentSize.height), location)) {
		[spriteGroupsHolder runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(0,0))]];
		[spriteSelected runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(354,282))]];
	}else if (CGRectContainsPoint(CGRectMake(spriteMenuGroupB.position.x- 0.5*spriteMenuGroupB.contentSize.width, spriteMenuGroupB.position.y- 0.5*spriteMenuGroupB.contentSize.height,
											 spriteMenuGroupB.contentSize.width, spriteMenuGroupB.contentSize.height), location)) {
		[spriteGroupsHolder runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(-480,0))]];
		[spriteSelected runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(354,255))]];
	}else if (CGRectContainsPoint(CGRectMake(spriteMenuGroupC.position.x- 0.5*spriteMenuGroupC.contentSize.width, spriteMenuGroupC.position.y- 0.5*spriteMenuGroupC.contentSize.height,
											 spriteMenuGroupC.contentSize.width, spriteMenuGroupC.contentSize.height), location)) {
		[spriteGroupsHolder runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(-960,0))]];
		[spriteSelected runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(354,228))]];
	}else if (CGRectContainsPoint(CGRectMake(spriteMenuGroupD.position.x- 0.5*spriteMenuGroupD.contentSize.width, spriteMenuGroupD.position.y- 0.5*spriteMenuGroupD.contentSize.height,
											 spriteMenuGroupD.contentSize.width, spriteMenuGroupD.contentSize.height), location)) {
		[spriteGroupsHolder runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(-1440,0))]];
		[spriteSelected runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(354,201))]];
	}else if (CGRectContainsPoint(CGRectMake(spriteMenuGroupE.position.x- 0.5*spriteMenuGroupE.contentSize.width, spriteMenuGroupE.position.y- 0.5*spriteMenuGroupE.contentSize.height,
											 spriteMenuGroupE.contentSize.width, spriteMenuGroupE.contentSize.height), location)) {
		[spriteGroupsHolder runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(-1920,0))]];
		[spriteSelected runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(354,174))]];
	}else if (CGRectContainsPoint(CGRectMake(spriteMenuGroupF.position.x- 0.5*spriteMenuGroupF.contentSize.width, spriteMenuGroupF.position.y- 0.5*spriteMenuGroupF.contentSize.height,
											 spriteMenuGroupF.contentSize.width, spriteMenuGroupF.contentSize.height), location)) {
		[spriteGroupsHolder runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(-2400,0))]];
		[spriteSelected runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(354,147))]];
	}else if (CGRectContainsPoint(CGRectMake(spriteMenuGroupG.position.x- 0.5*spriteMenuGroupG.contentSize.width, spriteMenuGroupG.position.y- 0.5*spriteMenuGroupG.contentSize.height,
											 spriteMenuGroupG.contentSize.width, spriteMenuGroupG.contentSize.height), location)) {
		[spriteGroupsHolder runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(-2880,0))]];
		[spriteSelected runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(354,120))]];
	}else if (CGRectContainsPoint(CGRectMake(spriteMenuGroupH.position.x- 0.5*spriteMenuGroupH.contentSize.width, spriteMenuGroupH.position.y- 0.5*spriteMenuGroupH.contentSize.height,
											 spriteMenuGroupH.contentSize.width, spriteMenuGroupH.contentSize.height), location)) {
		[spriteGroupsHolder runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(-3360,0))]];
		[spriteSelected runAction:[CCMoveTo actionWithDuration:1 position:ADJUST_CCP(ccp(354,93))]];
	}else if(CGRectContainsPoint(CGRectMake(spriteGoPlay.position.x- 0.5*spriteGoPlay.contentSize.width, spriteGoPlay.position.y- 0.5*spriteGoPlay.contentSize.height, spriteGoPlay.contentSize.width, spriteGoPlay.contentSize.height), location)) {
		[self loadingScreen];
		[self goNextScreen];
		//[self schedule: @selector(startDelayLoading:) interval: 1];

	}
	//return kEventHandled;	
}


@end
