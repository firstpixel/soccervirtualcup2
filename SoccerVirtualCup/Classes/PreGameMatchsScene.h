//
//  PreGameMatchsScene.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/21/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "GlobalSingleton.h"
#import "DataHolder.h"

@interface PreGameMatchsScene : CCScene {}
@end
@interface PreGameMatchsSceneLayer : CCLayer {
	CCSprite*spriteGoPlay;
	DataHolder *myDataHolder;
}
-(void)displayMatch:(int)t1 versus:(int)t2 posiX:(float)x1 posiY:(float)y1;
-(void)goPlay;
-(void)openTournament;
-(void)goTournament:(ccTime)ts;
-(void)goTeamGroupScene:(ccTime)ts;

@end