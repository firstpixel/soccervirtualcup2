//
//  Team.m
//  SoccerVirtualCup2
//
//  Created by Gil Beyruth on 3/1/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import "Team.h"

@implementation Team
@synthesize index;
@synthesize factor;
@synthesize groupKey;
@synthesize teamShortName;
@synthesize teamName;
@synthesize teamFlag;
@synthesize teamPlayer1;
@synthesize teamPlayer2;
@synthesize teamButton;


+(id)setIndex:(int)_index setFactor:(int)_factor setGroupKey:(int)_groupKey setShortName:(NSString*)_teamShortName setName:(NSString*)_teamName setFlag:(NSString*)_teamFlag
        setP1:(NSString*)_teamPlayer1 setP2:(NSString*)_teamPlayer2 setButton:(NSString*)_teamButton{
    [self setIndex:_index setFactor:_factor setGroupKey:_groupKey setShortName:_teamShortName setName:_teamName setFlag:_teamFlag
             setP1:_teamPlayer1 setP2:_teamPlayer2 setButton:_teamButton];
    return self;
}

-(id)setIndex:(int)_index setFactor:(int)_factor setGroupKey:(int)_groupKey setShortName:(NSString*)_teamShortName setName:(NSString*)_teamName setFlag:(NSString*)_teamFlag
      setP1:(NSString*)_teamPlayer1 setP2:(NSString*)_teamPlayer2 setButton:(NSString*)_teamButton{
	    index = _index;
        factor = _factor;
        groupKey = _groupKey;
        teamShortName = _teamShortName;
        teamName = _teamName;
        teamFlag = _teamFlag;
        teamPlayer1 = _teamPlayer1;
        teamPlayer2 = _teamPlayer2;
        teamButton = _teamButton;
    return self;
}


@end
