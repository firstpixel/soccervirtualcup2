//
//  GlobalSingleton.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/23/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "chipmunk.h"
@interface GlobalSingleton : NSObject {
	NSUInteger board[2];  // c-style array
	NSMutableString*teamP1Image;
	NSMutableString*teamP2Image;
	//Temporary values
	int currentScoreP1;
	int currentSelectedP1;

	//Temporary values
	int currentScoreP2;
	int currentSelectedP2;
	int timeToDelayAction;
	int timeLeft;

	int timePlayTurn;
	int checkSleep;
	int gameType;
	int gameKitState;
	int maxExecutionTime;
	
	int penaltyAttack; 
	
	//int matchWeather;
//	int defaultTimeLeft;
	
	cpVect p1ForceVector;
	cpVect p2ForceVector;
	cpVect p3ForceVector;
	cpVect p4ForceVector;
	cpVect p5ForceVector;
	cpVect p6ForceVector;

	cpVect p1StartVector;
	cpVect p2StartVector;
	cpVect p3StartVector;
	cpVect p4StartVector;
	cpVect p5StartVector;
	cpVect p6StartVector;
	cpVect ballStartVector;

	cpVect p1Position;
	cpVect p2Position;
	cpVect p3Position;
	cpVect p4Position;
	cpVect p5Position;
	cpVect p6Position;
	cpVect ballPosition;
	
	int spritePlayer1;
	int spritePlayer2;
	
	//Saved values
	int playerType;
	BOOL ballAudio;
	BOOL audioOn;
	BOOL isP1;
	BOOL isP2;
	BOOL gamePlayed;
    BOOL hasSynced1;
    BOOL hasSynced2;
    BOOL hasSynced3;
    BOOL hasSent1;
    BOOL hasSent2;
	BOOL musicOn, sfxOn;
	BOOL golAnimationPlaying;
    BOOL gameEnd;
	int gameTurn;
	int pTeamID;
	int sevenInARow;
	int champGoalsConceded;
    
    
    //LEADERBOARDS
	NSMutableArray* highscores;
    
	int scoredPoint;
	int leaderboardMatchWinner;
	int leaderboardPlayTime;
    
	
	NSString *leaderboardAddicted;
	NSString *leaderboardGoalScored;
	NSString *leaderboardGoalConceded;
	NSString *leaderboardGoalDifference;
	NSString *leaderboardWins;
	NSString *leaderboardLoss;
	NSString *leaderboardDraws;
	NSString *leaderboardPoints;
    NSString *leaderboardGamesPlayed;
}

//LEADERBOARDS
@property (nonatomic,retain) NSString *leaderboardAddicted;
@property (nonatomic,retain) NSString *leaderboardGoalScored;
@property (nonatomic,retain) NSString *leaderboardGoalConceded;
@property (nonatomic,retain) NSString *leaderboardGoalDifference;
@property (nonatomic,retain) NSString *leaderboardWins;
@property (nonatomic,retain) NSString *leaderboardLoss;
@property (nonatomic,retain) NSString *leaderboardDraws;
@property (nonatomic,retain) NSString *leaderboardPoints;
@property (nonatomic,retain) NSString *leaderboardGamesPlayed;



@property (nonatomic,retain) NSMutableArray *highscores;
@property (nonatomic,readwrite) int scoredPoint;
@property (nonatomic,readwrite) int leaderboardMatchWinner;
@property (nonatomic,readwrite) int leaderboardPlayTime;

@property (nonatomic, readwrite) int pTeamID;
@property (nonatomic, readwrite) int penaltyAttack;

@property (nonatomic, retain) NSMutableString *teamP1Image;
@property (nonatomic, retain) NSMutableString *teamP2Image;

@property (nonatomic, readwrite) cpVect p1ForceVector,p2ForceVector,p3ForceVector, p4ForceVector, p5ForceVector, p6ForceVector , 
p1StartVector, p2StartVector, p3StartVector, p4StartVector, p5StartVector, p6StartVector, ballStartVector, 
p1Position, p2Position, p3Position, p4Position, p5Position, p6Position, ballPosition;

@property (nonatomic, readwrite) int playerType; // 0 = not defined   1 = Server   2 = client
@property (nonatomic, readwrite) int gameTurn; // 0 = reset Game   1 = user playing   2 = setForcevectors 3 = computer Executing Turn/ check gol / check sleep
@property (nonatomic, readwrite) int gameType; // 0 = SinglePlayer   1 = multiPlayer same Iphone   2 = multiPlayer bluetooth
@property (nonatomic, readwrite) int gameKitState; // 0 = Start Game   1 = Picker   2 = Multiplayer   3 = Multiplayer Coin Toss   4 = Multiplayer reconnect
@property (nonatomic, readwrite) BOOL audioOn, ballAudio,hasSynced1,hasSynced2,hasSynced3,hasSent1,hasSent2;

@property (nonatomic, readwrite) int spritePlayer1;
@property (nonatomic, readwrite) int spritePlayer2;

@property (nonatomic, readwrite) int sevenInARow;
@property (nonatomic, readwrite) int champGoalsConceded;

@property (nonatomic, readwrite) int currentScoreP1;
@property (nonatomic, readwrite) int currentSelectedP1;
@property (nonatomic, readwrite) BOOL isP1;
@property (nonatomic, readwrite) int currentScoreP2;
@property (nonatomic, readwrite) int currentSelectedP2;
@property (nonatomic, readwrite) BOOL isP2;
@property (nonatomic, readwrite) BOOL gamePlayed;
@property (nonatomic, readwrite) BOOL musicOn, sfxOn, gameEnd;

@property (nonatomic, readwrite) BOOL golAnimationPlaying;
@property (nonatomic, readwrite) int timeLeft , timeToDelayAction, timePlayTurn, checkSleep, maxExecutionTime ;


+ (GlobalSingleton *) sharedInstance;


- (NSUInteger) getFieldValueAtPos:(NSUInteger)x;
- (void) setFieldValueAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal;

-(void) clearData;


@end