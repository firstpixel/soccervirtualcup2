/*
 * cocos2d for iPhone: http://www.cocos2d-iphone.org
 *
 * Copyright (c) 2008-2011 Ricardo Quesada
 * Copyright (c) 2011 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#import "LoopingMenuItem.h"
#import "CCLabelTTF.h"
#import "CCLabelBMFont.h"
#import "CCActionInterval.h"
#import "CCSprite.h"
//#import "Support/CGPointExtension.h"
#import "DeviceSettings.h"


@interface NSObject (NSPerformSelector)

+ (id)target:(id)target performSelector:(SEL)selector;
+ (id)target:(id)target performSelector:(SEL)selector withObject:(id)object;
+ (id)target:(id)target performSelector:(SEL)selector withObject:(id)object1 withObject2:(id)object2;

@end

@implementation NSObject (NSPerformSelector)

+ (id)target:(id)target performSelector:(SEL)selector {
    
    IMP imp = [target methodForSelector:selector];
    id (*func)(id, SEL) = (void *)imp;
    return func(target, selector);
}

+ (id)target:(id)target performSelector:(SEL)selector withObject:(id)object {
    
    IMP imp = [target methodForSelector:selector];
    id (*func)(id, SEL, id) = (void *)imp;
    return func(target, selector, object);
}

+ (id)target:(id)target performSelector:(SEL)selector withObject:(id)object1 withObject2:(id)object2 {
    
    IMP imp = [target methodForSelector:selector];
    id (*func)(id, SEL, id, id) = (void *)imp;
    return func(target, selector, object1, object2);
}

@end

//static NSUInteger _fontSize = kCCItemSize;
static NSString *_fontName = @"Marker Felt";
//static BOOL _fontNameRelease = NO;


#define	kCCCurrentItemTag  @"0xc0c05001"
#define	kCCZoomActionTag  0xc0c05002


#pragma mark -
#pragma mark CCMenuItem

@implementation LoopingMenuItem

@synthesize isSelected=isSelected_;
+(id) itemWithTarget:(id) r selector:(SEL) s
{
    return [[self alloc] initWithTarget:r selector:s];
}

+(id) itemWithBlock:(void(^)(id sender))block {
    return [[self alloc] initWithBlock:block];
}

-(id) init
{
	return [self initWithBlock:nil];
}

-(id) initWithTarget:(id)target selector:(SEL)selector
{
	// avoid retain cycle
	__block id t = target;
	return [self initWithBlock:^(id sender) {
            [NSObject target:t performSelector:selector];
	}];
}

// Designated initializer
-(id) initWithBlock:(void (^)(id))block
{
	if((self=[super init]) ) {

		if( block )
			block_ = [block copy];

		anchorPoint_ = CGPointMake(0.5f, 0.5f);
		isEnabled_ = YES;
		isSelected_ = NO;
	}
	return self;
}

-(void) cleanup
{

	[super cleanup];
}

-(void) selected
{
	isSelected_ = YES;
}

-(void) unselected
{
	isSelected_ = NO;
}

-(void) activate
{
	if(isEnabled_&& block_ )
		block_(self);
}

-(void) setIsEnabled: (BOOL)enabled
{
    isEnabled_ = enabled;
}

-(BOOL) isEnabled
{
    return isEnabled_;
}

-(CGRect) rect
{
	return CGRectMake( position_.x - contentSize_.width* anchorPoint_.x,
					  position_.y - contentSize_.height* anchorPoint_.y,
					  contentSize_.width, contentSize_.height);
}

-(void) setBlock:(void(^)(id sender))block
{
    block_ = [block copy];
}

-(void) setTarget:(id)target selector:(SEL)selector
{
    [self setBlock:^(id sender) {
        [NSObject target:target performSelector:selector withObject:sender];
	}];
}

@end


#pragma mark - LoopingMenuItemSprite

@interface LoopingMenuItemSprite()
-(void) updateImagesVisibility;
@end

@implementation LoopingMenuItemSprite

@synthesize titleLabel=titleLabel_,title=title_,normalImage=normalImage_, selectedImage=selectedImage_, disabledImage=disabledImage_;

+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite
{
	return [self itemWithNormalSprite:normalSprite selectedSprite:selectedSprite disabledSprite:nil target:nil selector:nil];
}

+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite target:(id)target selector:(SEL)selector
{
	return [self itemWithNormalSprite:normalSprite selectedSprite:selectedSprite disabledSprite:nil target:target selector:selector];
}

+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite target:(id)target selector:(SEL)selector
{
	return [[self alloc] initWithNormalSprite:normalSprite selectedSprite:selectedSprite disabledSprite:disabledSprite target:target selector:selector];
}

+(id) itemWithTitle:(NSString*)title normalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite target:(id)target selector:(SEL)selector;
{
    return [[self alloc] initWithTitle:title normalSprite:normalSprite selectedSprite:selectedSprite disabledSprite:disabledSprite target:target selector:selector];
}

+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite block:(void(^)(id sender))block
{
	return [self itemWithNormalSprite:normalSprite selectedSprite:selectedSprite disabledSprite:nil block:block];
}

+(id) itemWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite block:(void(^)(id sender))block
{
    return [[self alloc] initWithNormalSprite:normalSprite selectedSprite:selectedSprite disabledSprite:disabledSprite block:block];
}

+(id) itemWithTitle:(NSString*)title normalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite block:(void(^)(id sender))block
{
    return [[self alloc] initWithTitle:title normalSprite:normalSprite selectedSprite:selectedSprite disabledSprite:disabledSprite block:block];
}

-(id) initWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite target:(id)target selector:(SEL)selector
{
    // avoid retain cycle
    __block id t = target;
    
    return [self initWithNormalSprite:normalSprite selectedSprite:selectedSprite disabledSprite:disabledSprite block:^(id sender) {
        [NSObject target:t performSelector:selector withObject:sender];
    } ];
}

-(id) initWithTitle:(NSString*)title normalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite target:(id)target selector:(SEL)selector
{
    // avoid retain cycle
    __block id t = target;
    
    return [self initWithTitle:title normalSprite:normalSprite selectedSprite:selectedSprite disabledSprite:disabledSprite block:^(id sender) {
        [NSObject target:t performSelector:selector withObject:sender];
    } ];
}

//
// Designated initializer
//
-(id) initWithNormalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite block:(void(^)(id sender))block
{
    if ( (self = [super initWithBlock:block] ) ) {
        
        self.normalImage = normalSprite;
        self.selectedImage = selectedSprite;
        self.disabledImage = disabledSprite;
        
        [self setContentSize: [normalImage_ contentSize]];
    }
    return self;
}


//
// Designated initializer
//
-(id) initWithTitle:(NSString*)title normalSprite:(CCNode*)normalSprite selectedSprite:(CCNode*)selectedSprite disabledSprite:(CCNode*)disabledSprite block:(void(^)(id sender))block
{
    if ( (self = [super initWithBlock:block] ) ) {
        self.normalImage = normalSprite;
        self.selectedImage = selectedSprite;
        self.disabledImage = disabledSprite;
        
        [self setContentSize: [normalImage_ contentSize]];
        self.title = title;
        
    }
    return self;
}

- (void) setTitle:(NSString*)titleText
{
    if( titleLabel && titleText)
    {
        [self removeChild:titleLabel cleanup:YES];
        
    }
    if(titleText){
        
        titleLabel = [CCLabelTTF labelWithString:titleText fontName:@"ArialMT" fontSize:HD_PIXELS(15)];
        [titleLabel setHorizontalAlignment:kCCTextAlignmentCenter];
        [titleLabel setPosition:CGPointMake(self.contentSize.width/2, -10.0f)];
        [self addChild:titleLabel];
    }
}


-(void) setNormalImage:(CCNode*)image
{
	if( image != normalImage_ ) {
		image.anchorPoint = CGPointMake(0,0);

		[self removeChild:normalImage_ cleanup:YES];
		[self addChild:image];

		normalImage_ = image;
        
        [self setContentSize: [normalImage_ contentSize]];
		
		[self updateImagesVisibility];
	}
}

-(void) setSelectedImage:(CCNode*)image
{
	if( image != selectedImage_ ) {
		image.anchorPoint = CGPointMake(0,0);
        
		[self removeChild:selectedImage_ cleanup:YES];
		[self addChild:image];
        [self setColor:[UIColor whiteColor]];
        [self setScale:1.2f];

		selectedImage_ = image;
		
		[self updateImagesVisibility];
	}
}

-(void) setDisabledImage:(CCNode*)image
{
	if( image != disabledImage_ ) {
		image.anchorPoint = CGPointMake(0,0);
        
		[self removeChild:disabledImage_ cleanup:YES];
		[self addChild:image];
        [self setColor:[UIColor whiteColor]];
		disabledImage_ = image;
		
		[self updateImagesVisibility];
	}
}

#pragma mark LoopingMenuItemSprite - CCRGBAProtocol protocol

- (void) setOpacity: (CGFloat)opacity
{
	[normalImage_ setOpacity:opacity];
	[selectedImage_ setOpacity:opacity];
	[disabledImage_ setOpacity:opacity];
}

-(void) setColor:(UIColor*)color
{
	[normalImage_ setColor:color];
	[selectedImage_ setColor:color];
	[disabledImage_ setColor:color];
}

-(CGFloat) opacity
{
	return [normalImage_ opacity];
}

-(UIColor*) color
{
	return [normalImage_ color];
}

-(void) selected
{
	[super selected];

	if( selectedImage_ ) {
		[normalImage_ setVisible:NO];
		[selectedImage_ setVisible:YES];
		[disabledImage_ setVisible:NO];

	} else { // there is not selected image

		[normalImage_ setVisible:YES];
		[selectedImage_ setVisible:NO];
		[disabledImage_ setVisible:NO];
	}
}

-(void) unselected
{
	[super unselected];
	[normalImage_ setVisible:YES];
	[selectedImage_ setVisible:NO];
	[disabledImage_ setVisible:NO];
}

-(void) setIsEnabled:(BOOL)enabled
{
	if( isEnabled_ != enabled ) {
		[super setIsEnabled:enabled];

		[self updateImagesVisibility];
	}
}


// Helper 
-(void) updateImagesVisibility
{
	if( isEnabled_ ) {
		[normalImage_ setVisible:YES];
		[selectedImage_ setVisible:NO];
		[disabledImage_ setVisible:NO];
		
	} else {
		if( disabledImage_ ) {
			[normalImage_ setVisible:NO];
			[selectedImage_ setVisible:NO];
			[disabledImage_ setVisible:YES];
		} else {
			[normalImage_ setVisible:YES];
			[selectedImage_ setVisible:NO];
			[disabledImage_ setVisible:NO];
		}
	}
}

@end




