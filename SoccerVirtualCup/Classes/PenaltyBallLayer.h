//
//  PenaltyBallLayer.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "CCLayer.h"
#import "chipmunk.h"
#import "ccMacros.h"

@class PenaltyScene;

@interface PenaltyBallLayer : CCLayer {
	
	float kSlide;
	int elasticIterations;
	float playerMass;
	float ballMass;
	BOOL hasSent;
	BOOL hasSentPosition;
	PenaltyBallLayer *penaltyBallLayer;
	CCSprite* ballPenaltyCCSprite;
	CCSprite* penalty1CCSprite;
	CCSprite* penalty2CCSprite;
	
	CCSprite* scored1CCSprite;
	CCSprite* scored2CCSprite;
	CCSprite* scored3CCSprite;
	CCSprite* scored4CCSprite;
	CCSprite* scored5CCSprite;
	CCSprite* scored6CCSprite;
	CCSprite* scored7CCSprite;
	CCSprite* scored8CCSprite;
	CCSprite* scored9CCSprite;
	CCSprite* scored10CCSprite;

	BOOL scored1;
	BOOL scored2;
	BOOL scored3;
	BOOL scored4;
	BOOL scored5;
	BOOL scored6;
	BOOL scored7;
	BOOL scored8;
	BOOL scored9;
	BOOL scored10;
	
	CCSprite* gol1CCSprite;
	CCSprite* invGol1CCSprite;
	CCSprite* invGol1HitCCSprite;
	CCSprite* touchCCSprite;
	
	CCSprite * clockTurnCCSprite;
	
	//cpBody* ballCCSprite;
	cpBody* penalty1Body;
	cpBody* penalty2Body;
	cpBody* ballPenaltyBody;
	
	cpVect* penalty1ForceVector;
	cpVect* penalty2ForceVector;
		
	CCLabelAtlas* p1Score;
	CCLabelAtlas* p2Score;
	
	CCLabelAtlas* labelPlayer1;
	CCLabelAtlas* labelPlayer2;
	
	CCSprite* getReady;
	CCSprite* itsYourTurn;
	CCSprite* waitPlaying;
	
	CCSprite* clockCCSprite;
	CCSprite* playerCCSprite;
	CCSprite* player1CCSprite;
	CCSprite* player2CCSprite;
	CCSprite* versusScoreCCSprite;
	
	CCSprite* spriteSoundToogler;
	CCSprite* spriteBackMenu;
	
	cpSpace* space;
    CCSpriteBatchNode *_batchNode;
    
    CCSprite *goal1;
	CCSprite *goal2;
	CCSprite *goal3;
	CCSprite *goal4;
	CCSprite *goal5;
	CCSprite *goal6;
	CCSprite *goal7;
	CCSprite *goal8;
    int ballFrameNumber;
    float diffPosy;
    float diffPosx;
    
    CCSprite* ballSpriteShadow;
    CCSprite* ballSpriteVolume;
    
}
@property (nonatomic, readwrite) BOOL scored1,scored2,scored3,scored4,scored5,scored6,scored7,scored8,scored9,scored10;
@property (nonatomic, readwrite) BOOL hasSent;
@property (nonatomic, readwrite) BOOL hasSentPosition;
@property (nonatomic, readwrite) float kSlide;
@property (nonatomic, readwrite) float playerMass;
@property (nonatomic, readwrite) float ballMass;
@property (nonatomic, readwrite) int elasticIterations;

@property (nonatomic, readwrite) int ballFrameNumber;
@property (nonatomic,readwrite) float diffPosy, diffPosx, oldpy, oldpx;

-(void)goBackMenu;
-(void)audioToogler;
-(void)ballAudio;
-(int)ruleOfThree:(int)a secondParam:(int)b thirdParam:(int)c;

-(void)startPlayerTurnPenalty:(ccTime *)theTimer;
-(void)startDelayRestartPenalty:(ccTime *)theTimer;

-(void)tickPenalty:(ccTime )dt;
-(void)golScored:(int)team;

- (void)setupBatchNode;
@end