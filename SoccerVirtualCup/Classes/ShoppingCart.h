//
//  ShoppingCart.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/19/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface ShoppingCart : CCScene {
CCSpriteBatchNode* _batchNode;
}


@end

@interface ShoppingCartLayer : CCLayer {
	CCSprite*spriteBackMenu;
    CCSprite*buyCupCCSprite;
    CCSprite*buyRestoreCCSprite;
    
    CCSprite*buyAustraliaCCSprite;
    CCSprite*buyBrazilCCSprite;
    CCSprite*buyCroatiaCCSprite;
    CCSprite*buyEnglandCCSprite;
    CCSprite*buyFranceCCSprite;
    CCSprite*buyItalyCCSprite;
    CCSprite*buyMexicoCCSprite;
    CCSprite*buyJapanCCSprite;
    CCSprite*buyNetherlandsCCSprite;
    CCSprite*buyPortugalCCSprite;
    CCSprite*buyNigeriaCCSprite;
    CCSprite*buyRussiaCCSprite;
    CCSprite*buySwitzerlandCCSprite;
    CCSprite*buyUruguayCCSprite;
    CCSprite*buyUSACCSprite;
    
    CCLabelTTF*buyLabel;
    
}
-(void)buyProductFullGameStore;
-(void)buyProductRestore;
-(void)buyProductAustralia4stars;
-(void)buyProductBrazil4stars;
-(void)buyProductCroatia4stars;
-(void)buyProductEngland4stars;
-(void)buyProductFrance4stars;
-(void)buyProductItaly4stars;
-(void)buyProductMexico4stars;
-(void)buyProductJapan4stars;
-(void)buyProductNetherland4stars;
-(void)buyProductUSA4stars;
@end