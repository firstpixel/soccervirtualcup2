//
//  AppDelegate.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h" 
#import "MenuScene.h"
#import "GameScene.h"
#import "OpenGL_Internal.h"
#import <GameKit/GameKit.h>

#include <stdio.h>
//Admob and firebase
#import <GoogleMobileAds/GADMobileAds.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "Firebase.h"

#import <Chartboost/Chartboost.h>

@class RootViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, GKPeerPickerControllerDelegate, GKSessionDelegate, UIAlertViewDelegate , CCDirectorDelegate, ChartboostDelegate > {

	CCDirectorIOS	*director_;

    int myNumber;
    NSData *myData;
    UILabel *textView;
    

	CCSprite* playerSprite;

}

@property (nonatomic, retain) UIWindow *window;
@property (readonly) UINavigationController *navController;
@property (readonly) CCDirectorIOS *director;


@property (nonatomic,retain) NSMutableArray *highscores;
@property (nonatomic,readwrite) int scoredPoint;
@property (nonatomic,readwrite) int leaderboardMatchWinner;
@property (nonatomic,readwrite) int leaderboardPlayTime;


-(void)onFailure;

-(void)onSuccess;



- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window;
- (NSUInteger)supportedInterfaceOrientations;
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;

@end
