//
//  ConfigGameScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 2/7/10.
//  Copyright 2010 Firstpixel. All rights reserved.
//
#import "MenuScene.h"
#import "GameScene.h"
#import "SelectionScreen1Player.h"
#import "ConfigGameScene.h"
#import "GlobalCupSingleton.h"
#import "PreGameMatchsScene.h" 
#import "DeviceSettings.h"

@implementation ConfigGameScene
- (id) init {
    self = [super init];
    if (self != nil) {
        
        [self setupBatchNode];
        
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"game-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"game-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"game-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"game-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"game.jpg"];
                }
            }
        }
       
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
        [self addChild:[ConfigGameLayer node] z:1];
	}
    return self;
}

- (void)setupBatchNode {
    NSString *spritesheetPvrCcz = @"spritesheet.pvr.ccz";
    NSString *spritesheetPlist = @"spritesheet.plist";
    
    NSString *gametablePvrCcz = @"gametable.pvr.ccz";
    NSString *gametablePlist = @"gametable.plist";
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            spritesheetPvrCcz = @"spritesheet-ipadhd.pvr.ccz";
            spritesheetPlist = @"spritesheet-ipadhd.plist";
            gametablePvrCcz = @"gametable-ipadhd.pvr.ccz";
            gametablePlist = @"gametable-ipadhd.plist";
        }else{
            //iPad screen
            spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
            spritesheetPlist = @"spritesheet-hd.plist";
            gametablePvrCcz = @"gametable-hd.pvr.ccz";
            gametablePlist = @"gametable-hd.plist";
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
                spritesheetPlist = @"spritesheet-hd.plist";
                
                gametablePvrCcz = @"gametable-hd.pvr.ccz";
                gametablePlist = @"gametable-hd.plist";
            }else{
                //iphone screen
                spritesheetPvrCcz = @"spritesheet.pvr.ccz";
                spritesheetPlist = @"spritesheet.plist";
                
                gametablePvrCcz = @"gametable.pvr.ccz";
                gametablePlist = @"gametable.plist";
            }
        }
    }
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesheetPvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesheetPlist];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:gametablePvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:gametablePlist];
}
@end

@implementation ConfigGameLayer
- (id) init {
    self = [super init];
    if (self != nil) {
		spriteBackMenu = [CCSprite spriteWithSpriteFrameName:@"back_menu.png"];
        if(IS_IPAD || IS_IPADHD){
            [spriteBackMenu setPosition:ADJUST_CCP(ccp(26,328))];
        }else{
            [spriteBackMenu setPosition:ADJUST_CCP(ccp(26,302))];
        }
        [self addChild:spriteBackMenu z:101 tag:1010];
        
		newGameCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt_new_game.png"];
		[newGameCCSprite setPosition:ADJUST_CCP(ccp(240,186))];
		[self addChild:newGameCCSprite z:101 tag:101];
		
		self.isTouchEnabled = YES;
		
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSString *saved = [prefs stringForKey:@"saved"];
		
		if(saved == nil){
		}else{
			continueGameCCSprite = [CCSprite spriteWithSpriteFrameName:@"bt_continue.png"];
			[continueGameCCSprite setPosition:ADJUST_CCP(ccp(240,76))];
			[self addChild:continueGameCCSprite z:101 tag:101];
        }
    }
    return self;
}

-(void)goBackScreen{
	MenuScene * ms = [MenuScene node];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ms]];
}

-(void) updatePrefs{
	NSUserDefaults *prefs  = [NSUserDefaults standardUserDefaults];
	[prefs  setObject:@"MyLastCupGame" forKey:@"greeting"];
	GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	[mySingletons setGameType:0];
	[mySingletons setChampGoalsConceded:0];
	[mySingletons setSevenInARow:0];
	[mySingletons clearData];
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	[myCupSingleton clearArraysData];
    SelectionScreen1Player * gs = [SelectionScreen1Player node];
    [[CCDirector sharedDirector] replaceScene:gs];
}

-(void)goMatchScreen{
	GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	[mySingletons setGameType:0];
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	[myCupSingleton loadGameData];
    [mySingletons setSpritePlayer1:[myCupSingleton spritePlayer1]];	
	PreGameMatchsScene * gs = [PreGameMatchsScene node];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:gs]];
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];
	location = CGPointMake(location.x, location.y);
	if(CGRectContainsPoint(CGRectMake(spriteBackMenu.position.x -0.5 * spriteBackMenu.contentSize.width, spriteBackMenu.position.y -0.5 * spriteBackMenu.contentSize.height, spriteBackMenu.contentSize.width, spriteBackMenu.contentSize.height), location)) {
		[self goBackScreen];
	}else if(CGRectContainsPoint(CGRectMake(newGameCCSprite.position.x -0.5 * newGameCCSprite.contentSize.width, newGameCCSprite.position.y -0.5 * newGameCCSprite.contentSize.height, newGameCCSprite.contentSize.width, newGameCCSprite.contentSize.height), location)) {
		[self updatePrefs];
	}else if(CGRectContainsPoint(CGRectMake(continueGameCCSprite.position.x -0.5 * continueGameCCSprite.contentSize.width, continueGameCCSprite.position.y -0.5 * continueGameCCSprite.contentSize.height, continueGameCCSprite.contentSize.width, continueGameCCSprite.contentSize.height), location)) {
		[self goMatchScreen];
	}
}

@end
