//
//  PeerServiceManager.m
//  PeerConnection
//
//  Created by Gil Beyruth on 8/27/16.
//  Copyright © 2016 Gil Beyruth. All rights reserved.
//


#import "PeerServiceManager.h"
enum {
    /// An unknown error occurred
    PSMErrorUnknown = 1,
    /// GameCenterManager is unavailable, possibly for a variety of reasons
    PSMErrorNotAvailable = 2,
    /// The requested feature is unavailable on the current device or iOS version
    PSMErrorFeatureNotAvailable = 3,
    /// There is no active internet connection for the requested operation
    PSMErrorInternetNotAvailable = 4,
    /// The achievement data submitted was not valid because there were missing parameters
    PSMErrorAchievementDataMissing = 5,
    /// The multiplayer data could not be sent with the specified connection type because it was too large
    PSMErrorMultiplayerDataPacketTooLarge = 6
};


@interface PeerServiceManager()<UIActionSheetDelegate, MCBrowserViewControllerDelegate, MCNearbyServiceAdvertiserDelegate, MCNearbyServiceBrowserDelegate, MCSessionDelegate>

@end

@implementation PeerServiceManager {
    
    MCPeerID* myPeerId;
    MCNearbyServiceAdvertiser* serviceAdvertiser;
    MCNearbyServiceBrowser* serviceBrowser;
    MCSession* sessionLocal;
    UIViewController* matchPresentingController;

    
}

@synthesize PeerServiceType;

static PeerServiceManager *_sharedInstance;

// Make a singleton class
+ (PeerServiceManager *) sharedInstance
{
    if (!_sharedInstance)
    {
        _sharedInstance = [[PeerServiceManager alloc] init];
    }
    
    return _sharedInstance;
}

- (id) init {
    self = [super init];
    if (self != nil){
        NSLog(@"PeerServiceManager init");
        
        // custom initialization
        PeerServiceType = @"peerConnection";
        myPeerId = [[MCPeerID alloc] initWithDisplayName: [[UIDevice currentDevice] name]];
        
        serviceAdvertiser = [[MCNearbyServiceAdvertiser alloc] initWithPeer:myPeerId discoveryInfo:nil serviceType:PeerServiceType];
        
        serviceBrowser = [[MCNearbyServiceBrowser alloc] initWithPeer:myPeerId serviceType:PeerServiceType];
    }
    return self;
}

- (void)startBrowsingAndAdvertising:(UIViewController *)viewController
{
    matchPresentingController = viewController;
    if (!sessionLocal) {
        sessionLocal = [[MCSession alloc] initWithPeer:myPeerId securityIdentity:nil encryptionPreference:MCEncryptionNone];
        sessionLocal.delegate = self;
    } else {
        NSLog(@"Session init skipped -- already exists");
    }
    
    serviceAdvertiser.delegate = self;
    [serviceAdvertiser startAdvertisingPeer];
    
    serviceBrowser.delegate = self;
    [serviceBrowser startBrowsingForPeers];
    /*
    if (!browserVC){
        browserVC = [[MCBrowserViewController alloc] initWithServiceType:PeerServiceType
                                                                      session:sessionLocal];
        browserVC.delegate = self;
    } else {
        NSLog(@"Browser VC init skipped -- already exists");
    }
    //UIViewController *rootController =(UIViewController*)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [matchPresentingController presentViewController:browserVC animated:NO completion:nil];
     */
}


- (void) stopAdvertisingAndBrowsing {
    [serviceAdvertiser stopAdvertisingPeer];
    [serviceBrowser stopBrowsingForPeers];
}

-(void)disconnect {
    if(sessionLocal){
        [sessionLocal disconnect];
    }
}


-(void)sendData:(NSData*)data {
    if(sessionLocal.connectedPeers.count > 0){
        NSError*error;
        
        if(![sessionLocal sendData:data toPeers:sessionLocal.connectedPeers withMode:MCSessionSendDataReliable error:&error])
        {
            NSLog(@"[Error] %@", error);
        }
    }
}

- (BOOL)sendAllPlayersMatchData:(NSData *)data shouldSendQuickly:(BOOL)sendQuickly completion:(void (^)(BOOL success, NSError *error))handler {
    // Create the error object
    // Check if data should be sent reliably or unreliably
    // Reliable: ensures that the data is sent and arrives, can take a long time. Best used for critical game updates.
    // Unreliable: data is sent quickly, data can be lost or fragmented. Best used for frequent game updates.
    NSLog(@"MP sendMatchData");
    NSError *error;
    if (sendQuickly == YES) {
        // The data should be sent unreliably
        if (data.length > 1000) {
            // Limit the size of unreliable messages to 1000 bytes or smaller - as per Apple documentation guidelines
            if (handler != nil) handler(NO, [NSError errorWithDomain:@"The specified data exceeded the unreliable sending limit of 1000 bytes. Either send the data reliably (max. 87 kB) or decrease data packet size." code:PSMErrorMultiplayerDataPacketTooLarge userInfo:@{@"data": data, @"method": @"unreliable"}]);
            return NO;
        }
        
        // Send the data unreliably to the specified players
        BOOL success = NO;
        success = [sessionLocal sendData:data toPeers:sessionLocal.connectedPeers withMode:MCSessionSendDataUnreliable error:&error];
        
        if (!success) {
            // There was an error while sending the data
            if (handler != nil) handler(NO, error);
            return NO;
        } else {
            // There was no error while sending the data
            // No gauruntee is made as to whether or not it is recieved.
            if (handler != nil) handler(YES, nil);
            return YES;
        }
    } else {
        // Limit the size of reliable messages to 87 kilobytes (89,088 bytes) or smaller - as per Apple documentation guidelines
        //NSLog(@"MP sendMatchData.length %lu",(unsigned long)data.length);
        
        if (data.length > 89088) {
            NSLog(@"MP sendMatchData > 89088");
            
            if (handler != nil) handler(NO, [NSError errorWithDomain:@"The specified data exceeded the reliable sending limit of 87 kilobytes. You must decrease the data packet size." code:PSMErrorMultiplayerDataPacketTooLarge userInfo:@{@"data": data, @"method": @"reliable"}]);
            return NO;
        }
        // Send the data reliably to the specified players
        BOOL success = NO;
        
            success = [sessionLocal sendData:data toPeers:sessionLocal.connectedPeers withMode:MCSessionSendDataUnreliable error:&error];
        if (!success) {
            // There was an error while sending the data
            if (handler != nil) handler(NO, error);
            return NO;
        } else {
            // There was no error while sending the data
            // No gauruntee is made as to when it will be recieved.
            if (handler != nil) handler(YES, nil);
            return YES;
        }
    }

    return NO;

}




#pragma mark MCNearbyServiceAdvertiserDelegate <NSObject>
// Incoming invitation request.  Call the invitationHandler block with YES
// and a valid session to connect the inviting peer to the session.
- (void)            advertiser:(MCNearbyServiceAdvertiser *)advertiser
  didReceiveInvitationFromPeer:(MCPeerID *)peerID
                   withContext:(nullable NSData *)context
             invitationHandler:(void (^)(BOOL accept, MCSession *session))invitationHandler {
    
    NSLog(@"didReceiveInvitationFromPeer: %@ ", peerID);

    invitationHandler(YES, sessionLocal);
}

// Advertising did not start due to an error.
- (void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didNotStartAdvertisingPeer:(NSError *)error{
    NSLog(@"didNotStartAdvertisingPeer: %@ ", error);
}


#pragma mark MCNearbyServiceBrowserDelegate
// Found a nearby advertising peer.
- (void)        browser:(MCNearbyServiceBrowser *)browser
              foundPeer:(MCPeerID *)peerID
      withDiscoveryInfo:(nullable NSDictionary<NSString *, NSString *> *)info{
    
    NSLog(@"foundPeer:%@ ",peerID);
    NSLog(@"invitePeer:%@ ",peerID);
    [browser invitePeer:peerID toSession:sessionLocal withContext:nil timeout:10];
}

// A nearby peer has stopped advertising.
- (void)browser:(MCNearbyServiceBrowser *)browser lostPeer:(MCPeerID *)peerID{
    NSLog(@"lostPeer:%@ ",peerID);
    
}


// Browsing did not start due to an error.
- (void)browser:(MCNearbyServiceBrowser *)browser didNotStartBrowsingForPeers:(NSError *)error{
    NSLog(@"didNotStartBrowsingForPeers:%@ ",error);
}

#pragma mark MCSessionDelegate

// Remote peer changed state.
- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state{
    NSString* stateString;
    switch (state) {
        default:
        case 0:
            stateString = @"Disconnected";
            [_delegate receiveData:self didConnect:NO];
            break;
        case 1:
            stateString = @"Connecting";
            [_delegate receiveData:self didConnect:NO];
            break;
        case 2:
            stateString = @"Connected";
            [self stopAdvertisingAndBrowsing];
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [_delegate receiveData:self didConnect:YES];
/*                [matchPresentingController dismissViewControllerAnimated:NO completion:^(void){
                    NSLog(@"CONNECTED PEER");
                    [_delegate receiveData:self didConnect:YES];
                }];*/
            }];
            break;
    }
    NSLog(@"peer: %@  didChangeState: %@ ", peerID.displayName, stateString);
    
    NSArray *arrayOfNames = [NSArray array];
    for (int i = 0; i < session.connectedPeers.count; i++) {
        MCPeerID* peer = (MCPeerID*)session.connectedPeers[i];
        NSString* nameString = [NSString stringWithFormat:@"%@",peer.displayName];
        
        arrayOfNames = [arrayOfNames arrayByAddingObject:nameString];
        
    }
    
    if(_delegate){
        
        [_delegate connectedDevicesChanged:self connectedDevices:arrayOfNames totalDevices:(int)session.connectedPeers.count];
        arrayOfNames = nil;
    }else{
        NSLog(@"Missing delegate for PeerServiceManagerDelegate, please set a delete for PeerServiceManager");
    }
}

// Received data from remote peer.
- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID{
    //NSLog(@"didReceiveData: %lu bytes", (unsigned long)data.length);
    [_delegate receiveData:self didReceiveData:data];
}

// Received a byte stream from remote peer.
- (void)    session:(MCSession *)session
   didReceiveStream:(NSInputStream *)stream
           withName:(NSString *)streamName
           fromPeer:(MCPeerID *)peerID{
    NSLog(@"didReceiveStream");

}

// Start receiving a resource from remote peer.
- (void)                    session:(MCSession *)session
  didStartReceivingResourceWithName:(NSString *)resourceName
                           fromPeer:(MCPeerID *)peerID
                       withProgress:(NSProgress *)progress{
    NSLog(@"didStartReceivingResourceWithName");

}

// Finished receiving a resource from remote peer and saved the content
// in a temporary location - the app is responsible for moving the file
// to a permanent location within its sandbox.
- (void)                    session:(MCSession *)session
 didFinishReceivingResourceWithName:(NSString *)resourceName
                           fromPeer:(MCPeerID *)peerID
                              atURL:(NSURL *)localURL
                          withError:(nullable NSError *)error{
    NSLog(@"didFinishReceivingResourceWithName");

}


// Made first contact with peer and have identity information about the
// remote peer (certificate may be nil).
- (void)        session:(MCSession *)session
  didReceiveCertificate:(nullable NSArray *)certificate
               fromPeer:(MCPeerID *)peerID
     certificateHandler:(void (^)(BOOL accept))certificateHandler{
    NSLog(@"didReceiveCertificate");
    certificateHandler(YES);
}


#pragma mark MCBrowserViewControllerDelegate
// Notifies the delegate, when the user taps the done button.
- (void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController{
    [self stopAdvertisingAndBrowsing];
    [matchPresentingController dismissViewControllerAnimated:NO completion:^(void){
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Dismissed FINISH here");
                //call delegate method
                [_delegate browserViewControllerDidFinish:browserViewController];
            });
        }];
}

// Notifies delegate that the user taps the cancel button.
- (void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController{
    [self stopAdvertisingAndBrowsing];
     NSLog(@"TEST");
        [matchPresentingController dismissViewControllerAnimated:NO completion:^(void){
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Dismissed Cancelled here");
                //call delegate method
                [_delegate browserViewControllerWasCancelled:browserViewController];
            });
        }];
       
        
}


@end



