//
//  AfterGameMatchsScene.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/22/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "GlobalSingleton.h"
#import "DataHolder.h"

@interface AfterGameMatchsScene : CCScene {}
@end

@interface AfterGameMatchsSceneLayer : CCLayer {
	CCSprite*spriteGoPlay;
	DataHolder *myDataHolder;
	int winner;
    CCSpriteBatchNode* _batchNode;
}

@property int winner;

-(int)setMatchResult:(int)score1 score2:(int)score2;
-(void)displayMatch:(int)t1 versus:(int)t2 posiX:(float)x1 posiY:(float)y1 p1Score:(int)p1Score p2Score:(int)p2Score;
-(void)goPlay;
-(void)setupBatchNode;

@end