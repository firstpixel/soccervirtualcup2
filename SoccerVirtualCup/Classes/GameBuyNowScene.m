//
//  GameBuyNowScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

#import "GameBuyNowScene.h"
#import "ShoppingCart.h"
#import "DeviceSettings.h"

@implementation GameBuyNowScene
- (id) init {
    self = [super init];
    if (self != nil) {
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"buy_fullversion_screen-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"buy_fullversion_screen-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"buy_fullversion_screen-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"buy_fullversion_screen-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"buy_fullversion_screen.jpg"];
                }
            }
        }
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
        [self addChild:[GameBuyNowLayer node] z:1];
    }
    return self;
}
@end

@implementation GameBuyNowLayer
- (id) init {
    self = [super init];
    if (self != nil) {
        self.isTouchEnabled = YES;
    }
    return self;
}
- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    ShoppingCart * ms = [ShoppingCart node];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ms]];
	// return kEventHandled;
}
@end
