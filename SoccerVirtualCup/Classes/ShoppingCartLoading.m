//
//  ShoppingCartLoading.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 3/25/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import "ShoppingCartLoading.h"
#import "MenuScene.h"
#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "MKStoreManager.h"
#import "DeviceSettings.h"
@implementation ShoppingCartLoading
- (id) init {
    self = [super init];
    if (self != nil) {
        [self setupBatchNode];
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"shopping_cart_screen-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"shopping_cart_screen-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"shopping_cart_screen-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"shopping_cart_screen-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"shopping_cart_screen.jpg"];
                }
            }
        }
        
        
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
        [self addChild:[ShoppingCartLoadingLayer node] z:1];
        
        
        CCLabelTTF* buyLabel = [CCLabelTTF labelWithString:@"Loading Store...\n if it takes too long, \ncheck your internet connection." fontName:@"Arial" fontSize:HD_PIXELS(22)];
        [buyLabel setPosition: ADJUST_CCP(ccp(240,160))];
        [buyLabel setDimensions:CGSizeMake(480.0,100.0)];
        [buyLabel setHorizontalAlignment:kCCTextAlignmentCenter];
        [buyLabel setColor:ccWHITE];
        [self addChild: buyLabel z:2];
        
        
    }
    return self;
}
- (void)setupBatchNode {
    NSString *spritesheetPvrCcz = @"spritesheet.pvr.ccz";
    NSString *spritesheetPlist = @"spritesheet.plist";
    
    NSString *gametablePvrCcz = @"gametable.pvr.ccz";
    NSString *gametablePlist = @"gametable.plist";
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            spritesheetPvrCcz = @"spritesheet-ipadhd.pvr.ccz";
            spritesheetPlist = @"spritesheet-ipadhd.plist";
            gametablePvrCcz = @"gametable-ipadhd.pvr.ccz";
            gametablePlist = @"gametable-ipadhd.plist";
        }else{
            //iPad screen
            spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
            spritesheetPlist = @"spritesheet-hd.plist";
            gametablePvrCcz = @"gametable-hd.pvr.ccz";
            gametablePlist = @"gametable-hd.plist";
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
                spritesheetPlist = @"spritesheet-hd.plist";
                
                gametablePvrCcz = @"gametable-hd.pvr.ccz";
                gametablePlist = @"gametable-hd.plist";
            }else{
                //iphone screen
                spritesheetPvrCcz = @"spritesheet.pvr.ccz";
                spritesheetPlist = @"spritesheet.plist";
                
                gametablePvrCcz = @"gametable.pvr.ccz";
                gametablePlist = @"gametable.plist";
            }
        }
    }
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesheetPvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesheetPlist];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:gametablePvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:gametablePlist];
}


@end

@implementation ShoppingCartLoadingLayer
- (id) init {
    self = [super init];
    if (self != nil) {
		self.isTouchEnabled = YES;
        
		spriteBackMenu = [CCSprite spriteWithSpriteFrameName:@"back_menu.png"];
		[spriteBackMenu setPosition:ADJUST_CCP(ccp(20,300))];
		[self addChild:spriteBackMenu z:101 tag:101];

    }
    return self;
}

-(void)goNextScreen{
	MenuScene * ms = [MenuScene node];
	[[CCDirector sharedDirector] replaceScene:ms];
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	
	if(CGRectContainsPoint(CGRectMake(spriteBackMenu.position.x- 0.5*spriteBackMenu.contentSize.width, spriteBackMenu.position.y- 0.5*spriteBackMenu.contentSize.height, spriteBackMenu.contentSize.width, spriteBackMenu.contentSize.height), location)) {
		[self goNextScreen];
	}
	//return kEventHandled;
}




@end
