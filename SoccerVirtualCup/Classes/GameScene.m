//
//  GameScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

#import "GameScene.h"
#import "GlobalCupSingleton.h"
#import "DeviceSettings.h"
#import "MKStoreManager.h"
#import "AppDelegate.h"
#import "RootViewController.h"

@implementation GameScene

@synthesize ballLayer;
-(void)dealloc{
	[ballLayer release];
	[super dealloc];
}
-(id)init{
	self = [super init];
	if(nil != self){
		GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
        [self setupBatchNode];
		
		BallLayer* layer = [[BallLayer alloc]init];
		self.ballLayer = layer;
		[layer release];
		[self addChild:ballLayer z:1];
        
		bg = nil;
		if([myCupSingleton matchWeather]==0){
			int r = arc4random()%4;
			if(r==0){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_1.png"];
			}else if(r==1){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2.png"];
			}else if(r==2){
				CCParticleRain* rain = [CCParticleRain node];
                rain.totalParticles = 1000.0;
                if(IS_IPAD || IS_IPADHD){
                     [rain setPosition:ADJUST_CCP(ccp(240,360))];
                }else{
                     [rain setPosition:ADJUST_CCP(ccp(240,320))];
                }
                [self addChild:rain z:1001];
                
                raining = [[CCParticleRain alloc] init];
				raining.totalParticles = 1000.0;
				raining.startSize = 8;
                if(IS_IPAD || IS_IPADHD){
                    [raining setPosition:ADJUST_CCP(ccp(240,360))];
                }else{
                    [raining setPosition:ADJUST_CCP(ccp(240,320))];
                }
				[self addChild:raining z:1000];
				bg = [CCSprite spriteWithSpriteFrameName:@"field_3.png"];
			}else if(r==3){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2.png"];
                //[bg setColor:ccc3(200,200,255)];
			}else{
				bg = [CCSprite spriteWithSpriteFrameName:@"field_1.png"];
			}
            NSLog(@"game field  = %i",r);
		}else if([myCupSingleton matchWeather]==1){
			int r = arc4random()%3;
			if(r==0){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_1.png"];
			}else if(r==1){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2.png"];
			}else if(r==2){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_3.png"];
			}else{
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2.png"];
			}
		}else if([myCupSingleton matchWeather]==2){
			CCParticleRain* rain = [CCParticleRain node];
            rain.totalParticles = 1000.0;
            if(IS_IPAD || IS_IPADHD){
                [rain setPosition:ADJUST_CCP(ccp(240,360))];
            }else{
                [rain setPosition:ADJUST_CCP(ccp(240,320))];
            }
            [self addChild:rain z:1001];
            
            raining = [CCParticleRain node];
            raining.startSize = 8;
            raining.totalParticles = 1000.0;
            if(IS_IPAD || IS_IPADHD){
                [raining setPosition:ADJUST_CCP(ccp(240,360))];
            }else{
                [raining setPosition:ADJUST_CCP(ccp(240,320))];
            }
            [self addChild:raining z:1000];
            
            
			bg = [CCSprite spriteWithSpriteFrameName:@"field_3.png"];
            
		}else {
			int r = arc4random()%3;
			if(r==0){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_1.png"];
			}else if(r==1){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2.png"];
			}else if(r==2){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_3.png"];
			}else{
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2.png"];
			}
		}
        
      

        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
        [self addChild:[GameLayer node] z:0];

	}
	return self;
}
- (void)setFieldColor:(ccColor3B)color {
    if(bg){
        [bg setColor:color];
    }
    
}

- (void)setupBatchNode {
    
    NSString *fieldsPvrCcz = @"fields.pvr.ccz";
    NSString *fieldsPlist = @"fields.plist";
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            fieldsPvrCcz = @"fields-ipadhd.pvr.ccz";
            fieldsPlist = @"fields-ipadhd.plist";
        }else{
            //iPad screen
            fieldsPvrCcz = @"fields-ipad.pvr.ccz";
            fieldsPlist = @"fields-ipad.plist";         }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                fieldsPvrCcz = @"fields-hd.pvr.ccz";
                fieldsPlist = @"fields-hd.plist";
            }else{
                //iphone screen
                fieldsPvrCcz = @"fields.pvr.ccz";
                fieldsPlist = @"fields.plist";
            }
        }
    }

    
    
    _batchNodeFields = [CCSpriteBatchNode batchNodeWithFile:fieldsPvrCcz];
    [self addChild:_batchNodeFields z:-2];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:fieldsPlist];
    
}
@end

@implementation GameLayer
- (id) init {
    self = [super init];
    if (self != nil) {
       self.isTouchEnabled = YES;
    }
    return self;
}
@end
