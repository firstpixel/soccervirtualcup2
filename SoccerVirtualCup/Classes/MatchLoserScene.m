//
//  MatchLoserScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/12/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//


#import "MatchLoserScene.h"
#import "MenuScene.h"
#import "GlobalSingleton.h"
#import "MKStoreManager.h"
#import "GameBuyNowScene.h"
#import "DeviceSettings.h"
#import "MKStoreManager.h"
#import "AppDelegate.h"
#import "RootViewController.h"

@implementation MatchLoserScene
- (id) init {
    self = [super init];
    if (self != nil) {
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"match_lost-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"match_lost-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"match_lost-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"match_lost-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"match_lost.jpg"];
                }
            }
        }
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        
        [self addChild:bg z:0];
        [self addChild:[MatchLoserLayer node] z:1];
    }
    return self;
}
@end

@implementation MatchLoserLayer
- (id) init {
    self = [super init];
    if (self != nil) {
		self.isTouchEnabled = YES;
		
		spriteGoPlay = [CCSprite spriteWithSpriteFrameName:@"go_bt.png"];
		[spriteGoPlay setPosition:ADJUST_CCP(ccp(60,46))];
		[self addChild:spriteGoPlay z:101 tag:101];
        
        if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
            AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            [(RootViewController*)[app navController] addInterstitial];
        }
		
		
    }
    return self;
}

-(void)goNextScreen{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton gameType]==3){
		 if([MKStoreManager isFeaturePurchased:kFeatureAId]){
			MenuScene * ms = [MenuScene node];
			[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ms]];
		}else{
			GameBuyNowScene * ms = [GameBuyNowScene node];
			[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ms]];
		}
	
	}else {
		MenuScene * ms = [MenuScene node];
		[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ms]];
	}
	
}




- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	
	if(CGRectContainsPoint(CGRectMake(spriteGoPlay.position.x- 0.5*spriteGoPlay.contentSize.width, spriteGoPlay.position.y- 0.5*spriteGoPlay.contentSize.height, spriteGoPlay.contentSize.width, spriteGoPlay.contentSize.height), location)) {
		[self goNextScreen];
	}
	//return kEventHandled;	
}
@end
