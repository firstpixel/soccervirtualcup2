//
//  PenaltyScene.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

/*#import <UIKit/UIKit.h>
 #import "cocos2d.h"
 
 @interface GameScene : Scene {}
 @end
 
 @interface GameLayer : Layer {}
 @end*/


#import "GameScene.h"
#import "PenaltyBallLayer.h"
#import "CCParticleExamples.h"

@interface PenaltyScene : CCScene {
	PenaltyBallLayer* penaltyBallLayer;
	CCParticleRain* raining;
    CCSpriteBatchNode*_batchNodeFields;
}
@property (nonatomic, retain) PenaltyBallLayer* penaltyBallLayer;
-(void)setupBatchNode;
@end

@interface PenaltyLayer : CCLayer {}
@end