//
//  GlobalCupSingleton.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/16/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "GlobalCupSingleton.h"
#import "DataHolder.h"
#import "chipmunk.h"

@implementation GlobalCupSingleton

static GlobalCupSingleton *_sharedInstance;

@synthesize matchWeather;
@synthesize defaultTimeLeft;

@synthesize myTeamID;

@synthesize spritePlayer1;
@synthesize spritePlayer2;
@synthesize currentScoreP1;
@synthesize currentScoreP2;

@synthesize atualMatch;
@synthesize tournament1T1;
@synthesize tournament1T2;
@synthesize tournament1S1;
@synthesize tournament1S2;
@synthesize tournament2T1;
@synthesize tournament2T2;
@synthesize tournament2S1;
@synthesize tournament2S2;
@synthesize tournament3T1;
@synthesize tournament3T2;
@synthesize tournament3S1;
@synthesize tournament3S2;
@synthesize tournament4T1;
@synthesize tournament4T2;
@synthesize tournament4S1;
@synthesize tournament4S2;
@synthesize tournament5T1;
@synthesize tournament5T2;
@synthesize tournament5S1;
@synthesize tournament5S2;
@synthesize tournament6T1;
@synthesize tournament6T2;
@synthesize tournament6S1;
@synthesize tournament6S2;
@synthesize tournament7T1;
@synthesize tournament7T2;
@synthesize tournament7S1;
@synthesize tournament7S2;
@synthesize tournament8T1;
@synthesize tournament8T2;
@synthesize tournament8S1;
@synthesize tournament8S2;
@synthesize tournament9T1;
@synthesize tournament9T2;
@synthesize tournament9S1;
@synthesize tournament9S2;
@synthesize tournament10T1;
@synthesize tournament10T2;
@synthesize tournament10S1;
@synthesize tournament10S2;
@synthesize tournament11T1;
@synthesize tournament11T2;
@synthesize tournament11S1;
@synthesize tournament11S2;
@synthesize tournament12T1;
@synthesize tournament12T2;
@synthesize tournament12S1;
@synthesize tournament12S2;
@synthesize tournament13T1;
@synthesize tournament13T2;
@synthesize tournament13S1;
@synthesize tournament13S2;
@synthesize tournament14T1;
@synthesize tournament14T2;
@synthesize tournament14S1;
@synthesize tournament14S2;
@synthesize tournament15T1;
@synthesize tournament15T2;
@synthesize tournament15S1;
@synthesize tournament15S2;
@synthesize tournament16T1;
@synthesize tournament16T2;
@synthesize tournament16S1;
@synthesize tournament16S2;

@synthesize sevenInARow;
@synthesize champGoalsConceded;


- (id) init {
	self = [super init];
	if (self != nil){
		// custom initialization
		// start saved vars from here
		[self clearArraysData];
		atualMatch = 0;
		sevenInARow = 0;
		champGoalsConceded = 0;


	}
	return self;
}


-(void)saveGameData{
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		
		[prefs setObject:@"MyLastCupGame" forKey:@"greeting"];
		[prefs setObject:@"YES" forKey:@"saved"];
		[prefs setInteger:myTeamID forKey:@"myTeamID"];

	for(int i=0;i<31;i++){
		[prefs setInteger:lastMatchScoreArray[i] forKey:[NSString stringWithFormat:@"lastMatchScoreArray%i",i]];
		[prefs setInteger:pointsArray[i] forKey:[NSString stringWithFormat:@"pointsArray%i",i]];
		[prefs setInteger:winsArray[i] forKey:[NSString stringWithFormat:@"winsArray%i",i]];
		[prefs setInteger:lossArray[i] forKey:[NSString stringWithFormat:@"lossArray%i",i]];
		[prefs setInteger:drawArray[i] forKey:[NSString stringWithFormat:@"drawArray%i",i]];
		[prefs setInteger:goalScoredArray[i] forKey:[NSString stringWithFormat:@"goalScoredArray%i",i]];
		[prefs setInteger:goalConcededArray[i] forKey:[NSString stringWithFormat:@"goalConcededArray%i",i]];
		[prefs setInteger:goalDifferenceArray[i] forKey:[NSString stringWithFormat:@"goalDifferenceArray%i",i]];
	}
	
	
	for(int i=0;i<8;i++){
		[prefs setInteger:p1p2Array[i] forKey:[NSString stringWithFormat:@"p1p2Array%i",i]];
		[prefs setInteger:p1p3Array[i] forKey:[NSString stringWithFormat:@"p1p3Array%i",i]];
		[prefs setInteger:p1p4Array[i] forKey:[NSString stringWithFormat:@"p1p4Array%i",i]];
		
		[prefs setInteger:p2p1Array[i] forKey:[NSString stringWithFormat:@"p2p1Array%i",i]];
		[prefs setInteger:p2p3Array[i] forKey:[NSString stringWithFormat:@"p2p3Array%i",i]];
		[prefs setInteger:p2p4Array[i] forKey:[NSString stringWithFormat:@"p2p4Array%i",i]];
		
		[prefs setInteger:p3p1Array[i] forKey:[NSString stringWithFormat:@"p3p1Array%i",i]];
		[prefs setInteger:p3p2Array[i] forKey:[NSString stringWithFormat:@"p3p2Array%i",i]];
		[prefs setInteger:p3p4Array[i] forKey:[NSString stringWithFormat:@"p3p4Array%i",i]];
		
		[prefs setInteger:p4p1Array[i] forKey:[NSString stringWithFormat:@"p4p1Array%i",i]];
		[prefs setInteger:p4p2Array[i] forKey:[NSString stringWithFormat:@"p4p2Array%i",i]];
		[prefs setInteger:p4p3Array[i] forKey:[NSString stringWithFormat:@"p4p3Array%i",i]];

	}
	
	[prefs setInteger:spritePlayer1 forKey:@"spritePlayer1"];
	[prefs setInteger:spritePlayer2 forKey:@"spritePlayer2"];
	
	[prefs setInteger:currentScoreP1 forKey:@"currentScoreP1"];
	[prefs setInteger:currentScoreP2 forKey:@"currentScoreP2"];
	
	[prefs setInteger:matchWeather forKey:@"matchWeather"];
	[prefs setInteger:defaultTimeLeft forKey:@"defaultTimeLeft"];
	
	[prefs setInteger:atualMatch forKey:@"atualMatch"];
	
	[prefs setInteger:sevenInARow forKey:@"sevenInARow"];
	[prefs setInteger:champGoalsConceded forKey:@"champGoalsConceded"];
	
	
	[prefs setInteger:tournament1T1 forKey:@"tournament1T1"];
	[prefs setInteger:tournament1T2 forKey:@"tournament1T2"];
	[prefs setInteger:tournament1S1 forKey:@"tournament1S1"];
	[prefs setInteger:tournament1S2 forKey:@"tournament1S2"];
	
	[prefs setInteger:tournament2T1 forKey:@"tournament2T1"];
	[prefs setInteger:tournament2T2 forKey:@"tournament2T2"];
	[prefs setInteger:tournament2S1 forKey:@"tournament2S1"];
	[prefs setInteger:tournament2S2 forKey:@"tournament2S2"];
	
	[prefs setInteger:tournament3T1 forKey:@"tournament3T1"];
	[prefs setInteger:tournament3T2 forKey:@"tournament3T2"];
	[prefs setInteger:tournament3S1 forKey:@"tournament3S1"];
	[prefs setInteger:tournament3S2 forKey:@"tournament3S2"];
	
	[prefs setInteger:tournament4T1 forKey:@"tournament4T1"];
	[prefs setInteger:tournament4T2 forKey:@"tournament4T2"];
	[prefs setInteger:tournament4S1 forKey:@"tournament4S1"];
	[prefs setInteger:tournament4S2 forKey:@"tournament4S2"];
	
	[prefs setInteger:tournament5T1 forKey:@"tournament5T1"];
	[prefs setInteger:tournament5T2 forKey:@"tournament5T2"];
	[prefs setInteger:tournament5S1 forKey:@"tournament5S1"];
	[prefs setInteger:tournament5S2 forKey:@"tournament5S2"];
	
	[prefs setInteger:tournament6T1 forKey:@"tournament6T1"];
	[prefs setInteger:tournament6T2 forKey:@"tournament6T2"];
	[prefs setInteger:tournament6S1 forKey:@"tournament6S1"];
	[prefs setInteger:tournament6S2 forKey:@"tournament6S2"];
	
	[prefs setInteger:tournament7T1 forKey:@"tournament7T1"];
	[prefs setInteger:tournament7T2 forKey:@"tournament7T2"];
	[prefs setInteger:tournament7S1 forKey:@"tournament7S1"];
	[prefs setInteger:tournament7S2 forKey:@"tournament7S2"];
	
	[prefs setInteger:tournament8T1 forKey:@"tournament8T1"];
	[prefs setInteger:tournament8T2 forKey:@"tournament8T2"];
	[prefs setInteger:tournament8S1 forKey:@"tournament8S1"];
	[prefs setInteger:tournament8S2 forKey:@"tournament8S2"];
	
	[prefs setInteger:tournament9T1 forKey:@"tournament9T1"];
	[prefs setInteger:tournament9T2 forKey:@"tournament9T2"];
	[prefs setInteger:tournament9S1 forKey:@"tournament9S1"];
	[prefs setInteger:tournament9S2 forKey:@"tournament9S2"];
	
	[prefs setInteger:tournament10T1 forKey:@"tournament10T1"];
	[prefs setInteger:tournament10T2 forKey:@"tournament10T2"];
	[prefs setInteger:tournament10S1 forKey:@"tournament10S1"];
	[prefs setInteger:tournament10S2 forKey:@"tournament10S2"];
	
	[prefs setInteger:tournament11T1 forKey:@"tournament11T1"];
	[prefs setInteger:tournament11T2 forKey:@"tournament11T2"];
	[prefs setInteger:tournament11S1 forKey:@"tournament11S1"];
	[prefs setInteger:tournament11S2 forKey:@"tournament11S2"];
	
	[prefs setInteger:tournament12T1 forKey:@"tournament12T1"];
	[prefs setInteger:tournament12T2 forKey:@"tournament12T2"];
	[prefs setInteger:tournament12S1 forKey:@"tournament12S1"];
	[prefs setInteger:tournament12S2 forKey:@"tournament12S2"];
	
	[prefs setInteger:tournament13T1 forKey:@"tournament13T1"];
	[prefs setInteger:tournament13T2 forKey:@"tournament13T2"];
	[prefs setInteger:tournament13S1 forKey:@"tournament13S1"];
	[prefs setInteger:tournament13S2 forKey:@"tournament13S2"];
	
	[prefs setInteger:tournament14T1 forKey:@"tournament14T1"];
	[prefs setInteger:tournament14T2 forKey:@"tournament14T2"];
	[prefs setInteger:tournament14S1 forKey:@"tournament14S1"];
	[prefs setInteger:tournament14S2 forKey:@"tournament14S2"];
	
	[prefs setInteger:tournament15T1 forKey:@"tournament15T1"];
	[prefs setInteger:tournament15T2 forKey:@"tournament15T2"];
	[prefs setInteger:tournament15S1 forKey:@"tournament15S1"];
	[prefs setInteger:tournament15S2 forKey:@"tournament15S2"];
	
	[prefs setInteger:tournament16T1 forKey:@"tournament16T1"];
	[prefs setInteger:tournament16T2 forKey:@"tournament16T2"];
	[prefs setInteger:tournament16S1 forKey:@"tournament16S1"];
	[prefs setInteger:tournament16S2 forKey:@"tournament16S2"];
	
	NSLog(@"***********************************************");
	NSLog(@"GAME SAVED!");
	NSLog(@"***********************************************");
}

-(void)loadGameData{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *saved = [prefs stringForKey:@"saved"];
	if(saved != nil){
		for(int i=0;i<31;i++){
			lastMatchScoreArray[i]=[prefs integerForKey:[NSString stringWithFormat:@"lastMatchScoreArray%i",i]];
			pointsArray[i] = [prefs integerForKey:[NSString stringWithFormat:@"pointsArray%i",i]];
			winsArray[i] = [prefs integerForKey:[NSString stringWithFormat:@"winsArray%i",i]];
			lossArray[i] = [prefs integerForKey:[NSString stringWithFormat:@"lossArray%i",i]];
			drawArray[i] = [prefs integerForKey:[NSString stringWithFormat:@"drawArray%i",i]];
			goalScoredArray[i] = [prefs integerForKey:[NSString stringWithFormat:@"goalScoredArray%i",i]];
			goalConcededArray[i] = [prefs integerForKey:[NSString stringWithFormat:@"goalConcededArray%i",i]];
			goalDifferenceArray[i] = [prefs integerForKey:[NSString stringWithFormat:@"goalDifferenceArray%i",i]];
		}
		for(int i=0;i<8;i++){
			p1p2Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p1p2Array%i",i]];
			p1p3Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p1p3Array%i",i]];
			p1p4Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p1p4Array%i",i]];	
			p2p1Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p2p1Array%i",i]];
			p2p3Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p2p3Array%i",i]];
			p2p4Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p2p4Array%i",i]];
	
			p3p1Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p3p1Array%i",i]];
			p3p2Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p3p2Array%i",i]];
			p3p4Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p3p4Array%i",i]];
			p4p1Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p4p1Array%i",i]];
			p4p2Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p4p2Array%i",i]];
			p4p3Array[i] = [prefs integerForKey:[NSString stringWithFormat:@"p4p3Array%i",i]];
		}
	
		spritePlayer1 =  (int)[prefs integerForKey:@"spritePlayer1"];
		spritePlayer2 =  (int)[prefs integerForKey:@"spritePlayer2"];
		
		currentScoreP1 =  (int)[prefs integerForKey:@"currentScoreP1"];
		currentScoreP2 =  (int)[prefs integerForKey:@"currentScoreP2"];

		
		matchWeather =  (int)[prefs integerForKey:@"matchWeather"];
		defaultTimeLeft =  (int)[prefs integerForKey:@"defaultTimeLeft"];
		
		atualMatch =  (int)[prefs integerForKey:@"atualMatch"];
		
		sevenInARow =  (int)[prefs integerForKey:@"sevenInARow"];
		champGoalsConceded =  (int)[prefs integerForKey:@"champGoalsConceded"];

		
		tournament1T1 =  (int)[prefs integerForKey:@"tournament1T1"];
		tournament1T2 =  (int)[prefs integerForKey:@"tournament1T2"];
		tournament1S1 =  (int)[prefs integerForKey:@"tournament1S1"];
		tournament1S2 =  (int)[prefs integerForKey:@"tournament1S2"];
	
		tournament2T1 =  (int)[prefs integerForKey:@"tournament2T1"];
		tournament2T2 =  (int)[prefs integerForKey:@"tournament2T2"];
		tournament2S1 =  (int)[prefs integerForKey:@"tournament2S1"];
		tournament2S2 =  (int)[prefs integerForKey:@"tournament2S2"];
	
		tournament3T1 =  (int)[prefs integerForKey:@"tournament3T1"];
		tournament3T2 =  (int)[prefs integerForKey:@"tournament3T2"];
		tournament3S1 =  (int)[prefs integerForKey:@"tournament3S1"];
		tournament3S2 =  (int)[prefs integerForKey:@"tournament3S2"];
	
		tournament4T1 =  (int)[prefs integerForKey:@"tournament4T1"];
		tournament4T2 =  (int)[prefs integerForKey:@"tournament4T2"];
		tournament4S1 =  (int)[prefs integerForKey:@"tournament4S1"];
		tournament4S2 =  (int)[prefs integerForKey:@"tournament4S2"];
	
		tournament5T1 =  (int)[prefs integerForKey:@"tournament5T1"];
		tournament5T2 =  (int)[prefs integerForKey:@"tournament5T2"];
		tournament5S1 =  (int)[prefs integerForKey:@"tournament5S1"];
		tournament5S2 =  (int)[prefs integerForKey:@"tournament5S2"];
	
		tournament6T1 =  (int)[prefs integerForKey:@"tournament6T1"];
		tournament6T2 =  (int)[prefs integerForKey:@"tournament6T2"];
		tournament6S1 =  (int)[prefs integerForKey:@"tournament6S1"];
		tournament6S2 =  (int)[prefs integerForKey:@"tournament6S2"];
	
		tournament7T1 =  (int)[prefs integerForKey:@"tournament7T1"];
		tournament7T2 =  (int)[prefs integerForKey:@"tournament7T2"];
		tournament7S1 =  (int)[prefs integerForKey:@"tournament7S1"];
		tournament7S2 =  (int)[prefs integerForKey:@"tournament7S2"];
	
		tournament8T1 =  (int)[prefs integerForKey:@"tournament8T1"];
		tournament8T2 =  (int)[prefs integerForKey:@"tournament8T2"];
		tournament8S1 =  (int)[prefs integerForKey:@"tournament8S1"];
		tournament8S2 =  (int)[prefs integerForKey:@"tournament8S2"];
	
		tournament9T1 =  (int)[prefs integerForKey:@"tournament9T1"];
		tournament9T2 =  (int)[prefs integerForKey:@"tournament9T2"];
		tournament9S1 =  (int)[prefs integerForKey:@"tournament9S1"];
		tournament9S2 =  (int)[prefs integerForKey:@"tournament9S2"];
	
		tournament10T1 =  (int)[prefs integerForKey:@"tournament10T1"];
		tournament10T2 =  (int)[prefs integerForKey:@"tournament10T2"];
		tournament10S1 =  (int)[prefs integerForKey:@"tournament10S1"];
		tournament10S2 =  (int)[prefs integerForKey:@"tournament10S2"];
	
		tournament11T1 =  (int)[prefs integerForKey:@"tournament11T1"];
		tournament11T2 =  (int)[prefs integerForKey:@"tournament11T2"];
		tournament11S1 =  (int)[prefs integerForKey:@"tournament11S1"];
		tournament11S2 =  (int)[prefs integerForKey:@"tournament11S2"];
	
		tournament12T1 =  (int)[prefs integerForKey:@"tournament12T1"];
		tournament12T2 =  (int)[prefs integerForKey:@"tournament12T2"];
		tournament12S1 =  (int)[prefs integerForKey:@"tournament12S1"];
		tournament12S2 =  (int)[prefs integerForKey:@"tournament12S2"];
	
		tournament13T1 =  (int)[prefs integerForKey:@"tournament13T1"];
		tournament13T2 =  (int)[prefs integerForKey:@"tournament13T2"];
		tournament13S1 =  (int)[prefs integerForKey:@"tournament13S1"];
		tournament13S2 =  (int)[prefs integerForKey:@"tournament13S2"];
	
		tournament14T1 =  (int)[prefs integerForKey:@"tournament14T1"];
		tournament14T2 =  (int)[prefs integerForKey:@"tournament14T2"];
		tournament14S1 =  (int)[prefs integerForKey:@"tournament14S1"];
		tournament14S2 =  (int)[prefs integerForKey:@"tournament14S2"];
	
		tournament15T1 =  (int)[prefs integerForKey:@"tournament15T1"];
		tournament15T2 =  (int)[prefs integerForKey:@"tournament15T2"];
		tournament15S1 =  (int)[prefs integerForKey:@"tournament15S1"];
		tournament15S2 =  (int)[prefs integerForKey:@"tournament15S2"];
	
		tournament16T1 =  (int)[prefs integerForKey:@"tournament16T1"];
		tournament16T2 =  (int)[prefs integerForKey:@"tournament16T2"];
		tournament16S1 =  (int)[prefs integerForKey:@"tournament16S1"];
		tournament16S2 =  (int)[prefs integerForKey:@"tournament16S2"];
		
		NSLog(@"***********************************************");
		NSLog(@"GAME LOADED! %li",(long)lastMatchScoreArray[0]);
		NSLog(@"***********************************************");
	}else{
		[self clearArraysData];
	}
}


-(void)saveTokenID:(int)tokenID{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	 if (tokenID!=0){
         NSNumber* tokenID_ = [NSNumber numberWithInt:tokenID];
        [prefs setInteger:[tokenID_ integerValue] forKey:@"tokenID"];
    }else{
    
        NSLog(@"Must have a tokenID to save");
    }  
}
-(NSNumber*)returnTokenID{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSNumber* tokenID = [NSNumber numberWithInt:(int)[prefs integerForKey:@"tokenID"]];
    return tokenID;
}
-(BOOL)hasTokenID{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if([prefs integerForKey:@"tokenID"] == nil ){
        return NO;
    }else{
        return YES;
    }
}
-(void)clearArraysData{
	for(int i=0;i<31;i++){
		lastMatchScoreArray[i] = 0;
		pointsArray[i] = 0;
		winsArray[i] = 0;
		lossArray[i] = 0;
		drawArray[i] = 0;
		goalScoredArray[i] = 0;
		goalConcededArray[i] = 0;
		goalDifferenceArray[i] = 0;
	}
	for(int i=0;i<8;i++){
		p1p2Array[i] = 0;
		p1p3Array[i] = 0;
		p1p4Array[i] = 0;
		
		p2p1Array[i] = 0;
		p2p3Array[i] = 0;
		p2p4Array[i] = 0;

		p3p1Array[i] = 0;
		p3p2Array[i] = 0;
		p3p4Array[i] = 0;

		p4p1Array[i] = 0;
		p4p2Array[i] = 0;
		p4p3Array[i] = 0;
	}
	atualMatch = 0;
	sevenInARow = 0;
	champGoalsConceded = 0;
	
	tournament1T1 = 100;
	tournament1T2 = 100;
	tournament1S1 = 100;
	tournament1S2 = 100;
	tournament2T1 = 100;
	tournament2T2 = 100;
	tournament2S1 = 100;
	tournament2S2 = 100;
	tournament3T1 = 100;
	tournament3T2 = 100;
	tournament3S1 = 100;
	tournament3S2 = 100;
	tournament4T1 = 100;
	tournament4T2 = 100;
	tournament4S1 = 100;
	tournament4S2 = 100;
	tournament5T1 = 100;
	tournament5T2 = 100;
	tournament5S1 = 100;
	tournament5S2 = 100;
	tournament6T1 = 100;
	tournament6T2 = 100;
	tournament6S1 = 100;
	tournament6S2 = 100;
	tournament7T1 = 100;
	tournament7T2 = 100;
	tournament7S1 = 100;
	tournament7S2 = 100;
	tournament8T1 = 100;
	tournament8T2 = 100;
	tournament8S1 = 100;
	tournament8S2 = 100;
	tournament9T1 = 100;
	tournament9T2 = 100;
	tournament9S1 = 100;
	tournament9S2 = 100;
	tournament10T1 = 100;
	tournament10T2 = 100;
	tournament10S1 = 100;
	tournament10S2 = 100;
	tournament11T1 = 100;
	tournament11T2 = 100;
	tournament11S1 = 100;
	tournament11S2 = 100;
	tournament12T1 = 100;
	tournament12T2 = 100;
	tournament12S1 = 100;
	tournament12S2 = 100;
	tournament13T1 = 100;
	tournament13T2 = 100;
	tournament13S1 = 100;
	tournament13S2 = 100;
	tournament14T1 = 100;
	tournament14T2 = 100;
	tournament14S1 = 100;
	tournament14S2 = 100;
	tournament15T1 = 100;
	tournament15T2 = 100;
	tournament15S1 = 100;
	tournament15S2 = 100;
	tournament16T1 = 100;
	tournament16T2 = 100;
	tournament16S1 = 100;
	tournament16S2 = 100;

}

+ (GlobalCupSingleton *) sharedInstance
{
	if (!_sharedInstance)
	{
		_sharedInstance = [[GlobalCupSingleton alloc] init];
	}
	
	return _sharedInstance;
}

- (int) setMatchWinner:(NSUInteger)p1 player2:(NSUInteger)p2 goalp1:(NSUInteger)goalp1 goalp2:(NSUInteger)goalp2 {
	int winner = (int)p1;
	int temp1;
	int temp2;
	if(goalp1 > goalp2){
		//p1 WIN
		//points
		temp1 = (int)[self getPointsArrayAtPos:p1];
		[self setPointsArrayAtPos:p1 ToValue:temp1+3];
		[self setWinsArrayAtPos:p1 ToValue:[self getWinsArrayAtPos:p1]+1];
		[self setLossArrayAtPos:p2 ToValue:[self getLossArrayAtPos:p2]+1];		
	}else if(goalp1 < goalp2){
		//p2 WIN
		winner = (int)p2;
		//points
		temp2 = (int)[self getPointsArrayAtPos:p2];
		[self setPointsArrayAtPos:p2 ToValue:temp2+3];
		[self setWinsArrayAtPos:p2 ToValue:[self getWinsArrayAtPos:p2]+1];
		[self setLossArrayAtPos:p1 ToValue:[self getLossArrayAtPos:p1]+1];		
	}else{
		//DRAW
		winner = -1;
		temp1 = (int)[self getPointsArrayAtPos:p1];
		[self setPointsArrayAtPos:p1 ToValue:temp1+1];
		temp2 = (int)[self getPointsArrayAtPos:p2];
		[self setPointsArrayAtPos:p2 ToValue:temp2+1];
		[self setDrawArrayAtPos:p1 ToValue:[self getDrawArrayAtPos:p1]+1];
		[self setDrawArrayAtPos:p2 ToValue:[self getDrawArrayAtPos:p2]+1];
	}
	[self setGoalScoredArrayAtPos:p1 ToValue:[self getGoalScoredArrayAtPos:p1]+goalp1];
	[self setGoalConcededArrayAtPos:p1 ToValue:[self getGoalConcededArrayAtPos:p1]+goalp2];
	[self setGoalDifferenceArrayAtPos:p1 ToValue:[self getGoalDifferenceArrayAtPos:p1]+goalp1-goalp2];
	[self setGoalScoredArrayAtPos:p2 ToValue:[self getGoalScoredArrayAtPos:p2]+goalp2];
	[self setGoalConcededArrayAtPos:p2 ToValue:[self getGoalConcededArrayAtPos:p2]+goalp1];
	[self setGoalDifferenceArrayAtPos:p2 ToValue:[self getGoalDifferenceArrayAtPos:p2]+goalp2-goalp1];
	
	[self setLastMatchScoreByTeamKey:p1 ToValue:goalp1];
	[self setLastMatchScoreByTeamKey:p2 ToValue:goalp2];
	return winner;
}

- (int) setRandomMatchWinner:(NSUInteger)p1 indexP2:(NSUInteger)p2{
	DataHolder*myDataHolder = [DataHolder sharedInstance];
	[myDataHolder clearData];
	
	int factor1 = (int)[myDataHolder getFactorByTeamKey:(int)p1];
	int factor2 = (int)[myDataHolder getFactorByTeamKey:(int)p2];
	int winner1 = arc4random() % factor1;
	int winner2 = arc4random() % factor2;
	//find the winner based on best team 
	int winner = (int)p1;
	int goalp1;
	int goalp2;
	int temp1;
	int temp2;
	if(winner1 > winner2){
		//p1 WIN
		winner = (int)p1;
		goalp1 = (arc4random() % 5) + 1;
		goalp2 = goalp1-(arc4random() % 5);
		goalp2 = goalp2<=0?0:goalp2>=goalp1?goalp1-1:0;
		
		//points
		temp1 = (int)[self getPointsArrayAtPos:p1];
		[self setPointsArrayAtPos:p1 ToValue:temp1+3];
		[self setWinsArrayAtPos:p1 ToValue:[self getWinsArrayAtPos:p1]+1];
		[self setLossArrayAtPos:p2 ToValue:[self getLossArrayAtPos:p2]+1];		
	}else if(winner1 < winner2){
		//p2 WIN
		winner = (int)p2;
		goalp2 = (arc4random() % 5) + 1;
		goalp1 = goalp2-(arc4random() % 5);
		goalp1 = goalp1<=0?0:goalp1>=goalp2?goalp2-1:0;
		//points
		temp2 = (int)[self getPointsArrayAtPos:p2];
		[self setPointsArrayAtPos:p2 ToValue:temp2+3];
		[self setWinsArrayAtPos:p2 ToValue:[self getWinsArrayAtPos:p2]+1];
		[self setLossArrayAtPos:p1 ToValue:[self getLossArrayAtPos:p1]+1];		
	}else{
		//DRAW
		winner = -1;
		goalp1 = (arc4random() % 5) + 1;
		goalp2 = goalp1;
		
		temp1 = (int)[self getPointsArrayAtPos:p1];
		[self setPointsArrayAtPos:p1 ToValue:temp1+1];
		temp2 = (int)[self getPointsArrayAtPos:p2];
		[self setPointsArrayAtPos:p2 ToValue:temp2+1];
		[self setDrawArrayAtPos:p1 ToValue:[self getDrawArrayAtPos:p1]+1];
		[self setDrawArrayAtPos:p2 ToValue:[self getDrawArrayAtPos:p2]+1];
	}
	[self setGoalScoredArrayAtPos:p1 ToValue:[self getGoalScoredArrayAtPos:p1]+goalp1];
	[self setGoalConcededArrayAtPos:p1 ToValue:[self getGoalConcededArrayAtPos:p1]+goalp2];
	[self setGoalDifferenceArrayAtPos:p1 ToValue:[self getGoalDifferenceArrayAtPos:p1]+goalp1-goalp2];
	[self setGoalScoredArrayAtPos:p2 ToValue:[self getGoalScoredArrayAtPos:p2]+goalp2];
	[self setGoalConcededArrayAtPos:p2 ToValue:[self getGoalConcededArrayAtPos:p2]+goalp1];
	[self setGoalDifferenceArrayAtPos:p2 ToValue:[self getGoalDifferenceArrayAtPos:p2]+goalp2-goalp1];
	
	[self setLastMatchScoreByTeamKey:p1 ToValue:goalp1];
	[self setLastMatchScoreByTeamKey:p2 ToValue:goalp2];
	return winner;
	
	
}


- (int) setTournamentWinner:(int)p1 player2:(int)p2 goalp1:(int)goalp1 goalp2:(int)goalp2 {
	int winner =p1;
	if(goalp1 > goalp2){
		winner = p1;
	}else if(goalp1 < goalp2){
		//p2 WIN
		winner = p2;
	}
	[self setLastTournamentScoreByTeamKey:p1 ToValue:goalp1];
	[self setLastTournamentScoreByTeamKey:p2 ToValue:goalp2];
	return winner;
}


- (int) setRandomTournamentWinner:(int)p1 indexP2:(int)p2{
	DataHolder*myDataHolder = [DataHolder sharedInstance];
	[myDataHolder clearData];
	
	int factor1 = (int)[myDataHolder getFactorByTeamKey:p1];
	int factor2 = (int)[myDataHolder getFactorByTeamKey:p2];
	int winner1 = arc4random() % factor1;
	int winner2 = arc4random() % factor2;
	//find the winner based on best team 
	int winner;
	int goalp1;
	int goalp2;
	if(winner1 > winner2){
		//p1 WIN
		winner = p1;
		goalp1 = (arc4random() % 5) + 1;
		goalp2 = goalp1-(arc4random() % 5);
		goalp2 = goalp2<=0?0:goalp2>=goalp1?goalp1-1:0;
	}else if(winner1 < winner2){
		//p2 WIN
		winner = p2;
		goalp2 = (arc4random() % 5) + 1;
		goalp1 = goalp2-(arc4random() % 5);
		goalp1 = goalp1<=0?0:goalp1>=goalp2?goalp2-1:0;
	}else{
		//DRAW
		winner = p1;
		goalp1 = (arc4random() % 6) + 1;
		goalp2 = goalp1-1>=0?goalp1-1:0;
		
	}
	[self setLastTournamentScoreByTeamKey:p1 ToValue:goalp1];
	[self setLastTournamentScoreByTeamKey:p2 ToValue:goalp2];
	return winner;
}

- (int) getLastTournamentScoreByTeamKey:(NSUInteger)x{
	return (int)lastMatchScoreArray[x];
}

- (void) setLastTournamentScoreByTeamKey:(NSUInteger)x ToValue:(NSUInteger)newVal{
	lastMatchScoreArray[x] = newVal;
}



- (int) getP1P2Array:(NSUInteger)group{
	return (int)p1p2Array[group];
}
- (void) setP1P2Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p1p2Array[group] = newVal;
}
- (int) getP1P3Array:(NSUInteger)group{
	return (int)p1p3Array[group];
}

- (void) setP1P3Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p1p3Array[group] = newVal;
}

- (int) getP1P4Array:(NSUInteger)group{
	return (int)p1p4Array[group];
}

- (void) setP1P4Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p1p4Array[group] = newVal;
}


- (int) getP2P1Array:(NSUInteger)group{
	return (int)p2p1Array[group];
}

- (void) setP2P1Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p2p1Array[group] = newVal;
}

- (int) getP2P3Array:(NSUInteger)group{
	return (int)p2p3Array[group];
}

- (void) setP2P3Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p2p3Array[group] = newVal;
}
- (int) getP2P4Array:(NSUInteger)group{
	return (int)p2p4Array[group];
}

- (void) setP2P4Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p2p4Array[group] = newVal;
}

- (int) getP3P1Array:(NSUInteger)group{
	return (int)p3p1Array[group];
}

- (void) setP3P1Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p3p1Array[group] = newVal;
}
- (int) getP3P2Array:(NSUInteger)group{
	return (int)p3p2Array[group];
}
- (void) setP3P2Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p3p2Array[group] = newVal;
}

- (int) getP3P4Array:(NSUInteger)group{
	return (int)p3p4Array[group];
}
- (void) setP3P4Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p3p4Array[group] = newVal;
}


- (int) getP4P1Array:(NSUInteger)group{
	return (int)p4p1Array[group];
}
- (void) setP4P1Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p4p1Array[group] = newVal;
}

- (int) getP4P2Array:(NSUInteger)group{
	return (int)p4p2Array[group];
}
- (void) setP4P2Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p4p2Array[group] = newVal;
}

- (int) getP4P3Array:(NSUInteger)group{
	return (int)p4p3Array[group];
}
- (void) setP4P3Array:(NSUInteger)group ToValue:(NSUInteger)newVal{
	p4p3Array[group] = newVal;
}






- (int) getLastMatchScoreByTeamKey:(NSUInteger)x{
	return (int)lastMatchScoreArray[x];
}

- (void) setLastMatchScoreByTeamKey:(NSUInteger)x ToValue:(NSUInteger)newVal{
	lastMatchScoreArray[x] = newVal;
}

- (int) getPointsArrayAtPos:(NSUInteger)x
{
	return (int)pointsArray[x];
}

- (void) setPointsArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal
{
	pointsArray[x] = newVal;
}

- (int) getWinsArrayAtPos:(NSUInteger)x
{
	return (int)winsArray[x];
}

- (void) setWinsArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal
{
	winsArray[x] = newVal;
}

- (int) getLossArrayAtPos:(NSUInteger)x
{
	return (int)lossArray[x];
}

- (void) setLossArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal
{
	lossArray[x] = newVal;
}

- (int) getDrawArrayAtPos:(NSUInteger)x
{
	return (int)drawArray[x];
}

- (void) setDrawArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal
{
	drawArray[x] = newVal;
}

- (int) getGoalScoredArrayAtPos:(NSUInteger)x
{
	return (int)goalScoredArray[x];
}

- (void) setGoalScoredArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal
{
	goalScoredArray[x] = newVal;
}

- (int) getGoalConcededArrayAtPos:(NSUInteger)x
{
	return (int)goalConcededArray[x];
}

- (void) setGoalConcededArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal
{
	goalConcededArray[x] = newVal;
}

- (int) getGoalDifferenceArrayAtPos:(NSUInteger)x
{
	return (int)goalDifferenceArray[x];
}

- (void) setGoalDifferenceArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal
{
	goalDifferenceArray[x] = newVal;
}

- (id)retain

{
	
    return self;
	
}



- (unsigned long)retainCount

{
	
    return UINT_MAX;  //denotes an object that cannot be released
	
}







- (id)autorelease

{
	
    return self;
	
}



@end
