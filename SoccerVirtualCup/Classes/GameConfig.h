//
//  GameConfig.h
//  iAdsTest
//
//  Created by Jose Andrade on 2/1/11.
//  Copyright Jose Andrade 2011. All rights reserved.
//

#ifndef __GAME_CONFIG_H
#define __GAME_CONFIG_H

//
// Supported Autorotations:
//		None,
//		UIViewController,
//		CCDirector
//
#define kGameAutorotationNone 0
#define kGameAutorotationCCDirector 1
#define kGameAutorotationUIViewController 2

//
// Define here the type of autorotation that you want for your game
//
#define GAME_AUTOROTATION kGameAutorotationCCDirector
//#define GAME_AUTOROTATION kGameAutorotationUIViewController
//#define GAME_AUTOROTATION kGameAutorotationNone

#define kP1PositionX 140
#define kP1PositionY 240
#define kP2PositionX 100
#define kP2PositionY 153
#define kP3PositionX 140
#define kP3PositionY 66
#define kP4PositionX 340
#define kP4PositionY 240
#define kP5PositionX 380
#define kP5PositionY 153
#define kP6PositionX 340
#define kP6PositionY 66
#define kBallPositionX 240
#define kBallPositionY 153

#endif // __GAME_CONFIG_H
