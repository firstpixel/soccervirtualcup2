//
//  PreGameMatchsScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/21/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "PreGameMatchsScene.h"
#import "TournamentScene.h"
#import "TeamGroupsScene.h"
#import "GlobalCupSingleton.h"
#import "GlobalSingleton.h"
#import "DataHolder.h"
#import "GameScene.h"
#import "cocos2d.h"
#import "SimpleAudioEngine.h"
#import "DeviceSettings.h"


@implementation PreGameMatchsScene
- (id) init {
    self = [super init];
    if (self != nil) {
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"nextmatchs_screen-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"nextmatchs_screen-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"nextmatchs_screen-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"nextmatchs_screen-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"nextmatchs_screen.jpg"];
                }
            }
        }
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
		[self addChild:[PreGameMatchsSceneLayer node] z:1];
    }
    return self;
}
@end


@implementation PreGameMatchsSceneLayer


- (id) init {
    self = [super init];
	if (self != nil) {
		GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
		
		NSLog( @"------------------------------------------------------");
		NSLog( @"THE ACTUAL MATCH on PRE MATCH END IS : %i",[myCupSingleton atualMatch]);
		NSLog( @"------------------------------------------------------");
		
		
		if([myCupSingleton atualMatch]==0 || [myCupSingleton atualMatch]==1 || [myCupSingleton atualMatch]==2 ){
		self.isTouchEnabled = YES;
		myDataHolder = [DataHolder sharedInstance];
		[myDataHolder clearData];
		
		GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
		if([mySingleton audioOn]==YES){
			SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
			if (sae != nil) {
				[sae playBackgroundMusic:@"loop.mp3" loop:YES];
				sae.backgroundMusicVolume = 0.1f;
			}
		}
		
		//int groupId = [myDataHolder getGroupForTeamKey:[mySingleton spritePlayer1]];
		
		[mySingleton setSpritePlayer1:[myCupSingleton spritePlayer1]];
		NSLog(@"MY PLAYER IS:%i",[mySingleton spritePlayer1]);
			if([myCupSingleton atualMatch]<=2){
		
			spriteGoPlay = [CCSprite spriteWithSpriteFrameName:@"go_bt.png"];
			[spriteGoPlay setPosition:ADJUST_CCP(ccp(240,46))];
			[self addChild:spriteGoPlay z:101 tag:101];
		
			//[self schedule: @selector(startDelayRestart:) interval: 20];
			NSArray* groupAKeys;
			NSNumber*g1;
			NSNumber*g2;
			NSNumber*g3;
			NSNumber*g4;
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:0]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		if([myCupSingleton atualMatch]==0){
			[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:130 posiY:247];
			[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:130 posiY:222];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];	
		}else if([myCupSingleton atualMatch]==1){
			[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:130 posiY:247];
			[self displayMatch:[g2 intValue] versus:[g4 intValue] posiX:130 posiY:222];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}else if([myCupSingleton atualMatch]==2){
			[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:130 posiY:247];
			[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:130 posiY:222];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:1]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		if([myCupSingleton atualMatch]==0){
			[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:130 posiY:197];
			[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:130 posiY:172];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];	
		}else if([myCupSingleton atualMatch]==1){
			[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:130 posiY:197];
			[self displayMatch:[g2 intValue] versus:[g4 intValue] posiX:130 posiY:172];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}else if([myCupSingleton atualMatch]==2){
			[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:130 posiY:197];
			[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:130 posiY:172];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:2]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		if([myCupSingleton atualMatch]==0){
			[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:130 posiY:147];
			[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:130 posiY:122];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];	
		}else if([myCupSingleton atualMatch]==1){
			[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:130 posiY:147];
			[self displayMatch:[g2 intValue] versus:[g4 intValue] posiX:130 posiY:122];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}else if([myCupSingleton atualMatch]==2){
			[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:130 posiY:147];
			[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:130 posiY:122];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:3]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		if([myCupSingleton atualMatch]==0){
			[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:130 posiY:97];
			[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:130 posiY:72];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];	
		}else if([myCupSingleton atualMatch]==1){
			[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:130 posiY:97];
			[self displayMatch:[g2 intValue] versus:[g4 intValue] posiX:130 posiY:72];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}else if([myCupSingleton atualMatch]==2){
			[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:130 posiY:97];
			[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:130 posiY:72];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:4]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		if([myCupSingleton atualMatch]==0){
			[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:350 posiY:247];
			[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:350 posiY:222];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];	
		}else if([myCupSingleton atualMatch]==1){
			[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:350 posiY:247];
			[self displayMatch:[g2 intValue] versus:[g4 intValue] posiX:350 posiY:222];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}else if([myCupSingleton atualMatch]==2){
			[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:350 posiY:247];
			[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:350 posiY:222];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:5]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		if([myCupSingleton atualMatch]==0){
			[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:350 posiY:197];
			[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:350 posiY:172];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];	
		}else if([myCupSingleton atualMatch]==1){
			[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:350 posiY:197];
			[self displayMatch:[g2 intValue] versus:[g4 intValue] posiX:350 posiY:172];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}else if([myCupSingleton atualMatch]==2){
			[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:350 posiY:197];
			[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:350 posiY:172];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:6]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		if([myCupSingleton atualMatch]==0){
			[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:350 posiY:147];
			[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:350 posiY:122];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];	
		}else if([myCupSingleton atualMatch]==1){
			[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:350 posiY:147];
			[self displayMatch:[g2 intValue] versus:[g4 intValue] posiX:350 posiY:122];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}else if([myCupSingleton atualMatch]==2){
			[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:350 posiY:147];
			[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:350 posiY:122];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}
		
		groupAKeys = [myDataHolder getIdForGroupKey:[NSNumber numberWithInteger:7]];
		g1=[groupAKeys objectAtIndex:0];
		g2=[groupAKeys objectAtIndex:1];
		g3=[groupAKeys objectAtIndex:2];
		g4=[groupAKeys objectAtIndex:3];
		if([myCupSingleton atualMatch]==0){
			[self displayMatch:[g1 intValue] versus:[g2 intValue] posiX:350 posiY:97];
			[self displayMatch:[g3 intValue] versus:[g4 intValue] posiX:350 posiY:72];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];	
		}else if([myCupSingleton atualMatch]==1){
			[self displayMatch:[g1 intValue] versus:[g3 intValue] posiX:350 posiY:97];
			[self displayMatch:[g2 intValue] versus:[g4 intValue] posiX:350 posiY:72];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}else if([myCupSingleton atualMatch]==2){
			[self displayMatch:[g1 intValue] versus:[g4 intValue] posiX:350 posiY:97];
			[self displayMatch:[g2 intValue] versus:[g3 intValue] posiX:350 posiY:72];
			if([mySingleton spritePlayer1]==[g1 intValue])[mySingleton setSpritePlayer2:[g4 intValue]];
			if([mySingleton spritePlayer1]==[g4 intValue])[mySingleton setSpritePlayer2:[g1 intValue]];
			if([mySingleton spritePlayer1]==[g2 intValue])[mySingleton setSpritePlayer2:[g3 intValue]];
			if([mySingleton spritePlayer1]==[g3 intValue])[mySingleton setSpritePlayer2:[g2 intValue]];	
		}	
	}
	}else if([myCupSingleton atualMatch]==3){
		NSLog(@"GOTO TEAM GROUP SCENE");
		[self schedule: @selector(goTeamGroupScene:) interval: 0.1];
	}else{
		[self schedule: @selector(goTournament:) interval: 0.1];
	}
	}
	return self;
}

-(void)displayMatch:(int)t1 versus:(int)t2 posiX:(float)x1 posiY:(float)y1 {
	x1 = ADJUST_X(x1);
	y1 = ADJUST_Y(y1);

	CCLabelTTF *labelTeam1 = [CCLabelTTF labelWithString:[myDataHolder getTeamShortNameForKey:(int)t1] fontName:@"Arial" fontSize:HD_PIXELS(16)];
	[labelTeam1 setColor:ccWHITE];
    [labelTeam1 setContentSize:CGSizeMake(20, 20)];
    [labelTeam1 setHorizontalAlignment:kCCTextAlignmentRight];
	[self addChild: labelTeam1 z:10];

	CCSprite* spriteFlag1 = [CCSprite spriteWithSpriteFrameName:[myDataHolder getFlagNameForKey:(int)t1]];
	[self addChild: spriteFlag1 z:20];
	
    CCLabelTTF *versusFont = [CCLabelTTF labelWithString:@"x" fontName:@"Arial" fontSize:HD_PIXELS(16)];
    [versusFont setPosition:ccp(x1, y1)];
	[versusFont setColor:ccWHITE];
	[self addChild:versusFont z:21 tag:21 ];
  
	CGSize versusContentSize = [versusFont contentSize];
	CGSize label1contentSize = [labelTeam1 contentSize];
	CGSize flag1ContentSize = [spriteFlag1 contentSize];
	
	CCSprite* spriteFlag2 = [CCSprite spriteWithSpriteFrameName:[myDataHolder getFlagNameForKey:(int)t2]];
	[self addChild: spriteFlag2 z:40];
	
	CCLabelTTF *labelTeam2 = [CCLabelTTF labelWithString:[myDataHolder getTeamShortNameForKey:(int)t2] fontName:@"Arial" fontSize:HD_PIXELS(16)];
	[labelTeam2 setColor:ccWHITE];
	[labelTeam2 setHorizontalAlignment:kCCTextAlignmentLeft];
    [labelTeam2 setContentSize:CGSizeMake(60, 20)];
    
    
	[self addChild: labelTeam2 z:30];
    
    CGSize flag2ContentSize = [spriteFlag2 contentSize];
    CGSize label2contentSize = [labelTeam2 contentSize];
	
    
    [spriteFlag1 setPosition: ccp(x1 - 20 - versusContentSize.width/2 ,y1)];
	[spriteFlag2 setPosition: ccp(x1 + 20 + versusContentSize.width/2 ,y1)];
	
    [labelTeam2 setPosition: ccp(x1 + label2contentSize.width/2 + versusContentSize.width/2 + flag2ContentSize.width + 15,y1)];
    [labelTeam1 setPosition: ccp(x1 - label1contentSize.width/2 - versusContentSize.width/2 - flag1ContentSize.width - 15,y1)];
	
}
-(void)startDelayLoading:(ccTime)ts{
	[self goPlay];
}
-(void)loadingScreen{
    
    //CCSprite * loading = nil;
    
    
    CCSprite * bg = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            bg = [CCSprite spriteWithFile:@"loading-ipadhd.jpg"];
        }else{
            //iPad screen
            bg = [CCSprite spriteWithFile:@"loading-ipad.jpg"];
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                    //iphone 5
                    bg = [CCSprite spriteWithFile:@"loading-hd.jpg"];
                }else{
                    //iphone retina screen
                    bg = [CCSprite spriteWithFile:@"loading-hd.jpg"];
                }
            }else{
                //iphone screen
                bg = [CCSprite spriteWithFile:@"loading.jpg"];
            }
        }
    }
    [bg setPosition:ADJUST_CCP(ccp(240, 160))];
    
	[self addChild:bg z:5000];
}


-(void)goTournament:(ccTime)ts{
	TournamentScene * tsc = [TournamentScene node];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:tsc]];
}
-(void)openTournament{
	TournamentScene * tsc = [TournamentScene node];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:tsc]];
}
-(void)goTeamGroupScene:(ccTime)ts{
	TeamGroupsScene * tgsc = [TeamGroupsScene node];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:tgsc]];

}
-(void)goPlay{
    [self unschedule: @selector(startDelayLoading:)];
    
	GameScene * gs = [GameScene node];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:gs]];
}



///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Touch Events ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	
	if(CGRectContainsPoint(CGRectMake(spriteGoPlay.position.x- 0.5*spriteGoPlay.contentSize.width, spriteGoPlay.position.y- 0.5*spriteGoPlay.contentSize.height, spriteGoPlay.contentSize.width, spriteGoPlay.contentSize.height), location)) {
		[self loadingScreen];
		[self schedule: @selector(startDelayLoading:) interval: 1];
	}
	//return kEventHandled;	
}

@end

