//
//  GameScoreScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

#import "GameCredit.h"
#import "MenuScene.h"
#import "DeviceSettings.h"
#import "Flurry.h"

@implementation GameCredit
- (id) init {
    self = [super init];
    if (self != nil) {
		
		
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"credits4-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"credits4-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"credits4-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"credits4-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"credits4.jpg"];
                }
            }
        }
        
       
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
		
        [self addChild:bg z:0];
		[self addChild:[GameCreditLayer node] z:1];

    }
    return self;
}
@end

@implementation GameCreditLayer
- (id) init {
    self = [super init];
    if (self != nil) {
        [Flurry logEvent:@"GAMELIST" timed:YES];
        
        /*GamesListViewController *myCtrl = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            myCtrl = [[GamesListViewController alloc] initWithNibName:@"GamesListViewController˜ipad" bundle:nil];
        } else {
           myCtrl = [[GamesListViewController alloc] init];
        }
        [[[[CCDirector sharedDirector] openGLView] window] addSubview:myCtrl.view];
         */
        self.isTouchEnabled = YES;
        
    }
    return self;
}
- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [Flurry endTimedEvent:@"GAMELIST" withParameters:nil];
	
    MenuScene * ms = [MenuScene node];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ms]];

}
@end
