//
//  GlobalSingleton.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/23/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "chipmunk.h"
#import "GameConfig.h"


@implementation GlobalSingleton

static GlobalSingleton *_sharedInstance;

@synthesize teamP1Image;
@synthesize teamP2Image;
@synthesize spritePlayer1;
@synthesize spritePlayer2;
@synthesize pTeamID;
//@synthesize matchWeather;

@synthesize p1ForceVector;
@synthesize p2ForceVector;
@synthesize p3ForceVector;
@synthesize p4ForceVector;
@synthesize p5ForceVector;
@synthesize p6ForceVector;

@synthesize p1StartVector;
@synthesize p2StartVector;
@synthesize p3StartVector;
@synthesize p4StartVector;
@synthesize p5StartVector;
@synthesize p6StartVector;
@synthesize ballStartVector;

@synthesize p1Position;
@synthesize p2Position;
@synthesize p3Position;
@synthesize p4Position;
@synthesize p5Position;
@synthesize p6Position;
@synthesize ballPosition;

@synthesize playerType;
@synthesize hasSent1;
@synthesize hasSent2;
@synthesize hasSynced1;
@synthesize hasSynced2;
@synthesize hasSynced3;
@synthesize ballAudio;
@synthesize audioOn;

@synthesize champGoalsConceded;
@synthesize sevenInARow;

@synthesize currentScoreP1;
@synthesize currentSelectedP1;
@synthesize isP1;

@synthesize currentScoreP2;
@synthesize currentSelectedP2;
@synthesize isP2;

@synthesize gameEnd;

@synthesize gameTurn;
@synthesize gameType;
@synthesize gameKitState;

@synthesize timeLeft;
//@synthesize defaultTimeLeft;
@synthesize timePlayTurn;
@synthesize timeToDelayAction;
@synthesize checkSleep;
@synthesize maxExecutionTime;

@synthesize gamePlayed;
@synthesize musicOn, sfxOn;
@synthesize golAnimationPlaying;

@synthesize penaltyAttack;

@synthesize highscores;
@synthesize leaderboardMatchWinner;
@synthesize scoredPoint;
@synthesize leaderboardPlayTime;

@synthesize leaderboardAddicted;
@synthesize leaderboardPoints;
@synthesize leaderboardGoalScored;
@synthesize leaderboardGoalConceded;
@synthesize leaderboardGoalDifference;
@synthesize leaderboardWins;
@synthesize leaderboardLoss;
@synthesize leaderboardDraws;
@synthesize leaderboardGamesPlayed;

- (id) init {
	self = [super init];
	if (self != nil){
		audioOn = YES;
		
		// custom initialization
		memset(board, 0, sizeof(board));
	}
	return self;
}

+ (GlobalSingleton *) sharedInstance
{
	if (!_sharedInstance)
	{
		_sharedInstance = [[GlobalSingleton alloc] init];
	}
	
	return _sharedInstance;
}

- (NSUInteger) getFieldValueAtPos:(NSUInteger)x
{
	return board[x];
}

- (void) setFieldValueAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal
{
	board[x] = newVal;
}






-(void) clearData {
	
	
	ballAudio=NO;
	checkSleep = 0;
	p1ForceVector = cpv(0,0);
	p2ForceVector = cpv(0,0);
	p3ForceVector = cpv(0,0);
	p4ForceVector = cpv(0,0);
	p5ForceVector = cpv(0,0);
	p6ForceVector = cpv(0,0);
	
	p1StartVector = cpv(kP1PositionX, kP1PositionY);
	p2StartVector = cpv(kP2PositionX, kP2PositionY);
	p3StartVector = cpv(kP3PositionX, kP3PositionY);
	p4StartVector = cpv(kP4PositionX, kP4PositionY);
	p5StartVector = cpv(kP5PositionX, kP5PositionY);
	p6StartVector = cpv(kP6PositionX, kP6PositionY);
	ballStartVector = cpv(kBallPositionX, kBallPositionY);
	
	p1Position = cpv(kP1PositionX, kP1PositionY);
	p2Position = cpv(kP2PositionX, kP2PositionY);
	p3Position = cpv(kP3PositionX, kP3PositionY);
	p4Position = cpv(kP4PositionX, kP4PositionY);
	p5Position = cpv(kP5PositionX, kP5PositionY);
	p6Position = cpv(kP6PositionX, kP6PositionY);
	ballPosition = cpv(kBallPositionX, kBallPositionY);
	
	playerType = 0;
	GlobalCupSingleton *myCupSingletons = [GlobalCupSingleton sharedInstance]; //setup the global vars singleton instance

	if(timeLeft==0)timeLeft = [myCupSingletons defaultTimeLeft];
	
	penaltyAttack = 0;
	hasSynced1 = NO;
	hasSynced2 = NO;
	currentScoreP1 = 0;
	currentSelectedP1 = 0;
	currentScoreP2 = 0;
	currentSelectedP2 = 0;
	timeToDelayAction = 4;
	timePlayTurn = 20;
	maxExecutionTime = 0;
	golAnimationPlaying = NO;
	gamePlayed = NO;
	musicOn = YES;
	sfxOn = YES;
	//start
	gameTurn = 0;
	gameKitState = 0;
    
    gameEnd = NO;

}



- (id)retain

{
	
    return self;
	
}


- (id)autorelease

{
	
    return self;
	
}




@end
