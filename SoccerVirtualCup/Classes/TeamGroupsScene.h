//
//  TeamGroups.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/13/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "GlobalSingleton.h"

@interface TeamGroupsScene : CCScene {}
@end
@interface TeamGroupsSceneLayer : CCLayer {
	int selectedGroup;
	int yourTeam;
	int yourTeamGroup;
	
	CCSprite * loading;
	
	CCSprite *spriteSelected;
	CCSprite *spriteMyGroup;
	
	CCSprite *spriteGroupsHolder;
	CCSprite *spriteMenu;
	CCSprite *spriteGoPlay;
	
	CCSprite *spriteGroupA;
	CCSprite *spriteGroupB;
	CCSprite *spriteGroupC;
	CCSprite *spriteGroupD;
	CCSprite *spriteGroupE;
	CCSprite *spriteGroupF;
	CCSprite *spriteGroupG;
	CCSprite *spriteGroupH;

	CCSprite *spriteTitleGroupA;
	CCSprite *spriteTitleGroupB;
	CCSprite *spriteTitleGroupC;
	CCSprite *spriteTitleGroupD;
	CCSprite *spriteTitleGroupE;
	CCSprite *spriteTitleGroupF;
	CCSprite *spriteTitleGroupG;
	CCSprite *spriteTitleGroupH;

	CCSprite *spriteMenuGroupA;
	CCSprite *spriteMenuGroupB;
	CCSprite *spriteMenuGroupC;
	CCSprite *spriteMenuGroupD;
	CCSprite *spriteMenuGroupE;
	CCSprite *spriteMenuGroupF;
	CCSprite *spriteMenuGroupG;
	CCSprite *spriteMenuGroupH;
	
	CCSprite *spriteBoardGroupA;
	CCSprite *spriteBoardGroupB;
	CCSprite *spriteBoardGroupC;
	CCSprite *spriteBoardGroupD;
	CCSprite *spriteBoardGroupE;
	CCSprite *spriteBoardGroupF;
	CCSprite *spriteBoardGroupG;
	CCSprite *spriteBoardGroupH;
	
	CCSprite *spriteFlag1;
	CCSprite *spriteFlag2;
	CCSprite *spriteFlag3;
	CCSprite *spriteFlag4;
	CCSprite *spriteFlag5;
	CCSprite *spriteFlag6;
	CCSprite *spriteFlag7;
	CCSprite *spriteFlag8;
	CCSprite *spriteFlag9;
	CCSprite *spriteFlag10;
	CCSprite *spriteFlag11;
	CCSprite *spriteFlag12;
	CCSprite *spriteFlag13;
	CCSprite *spriteFlag14;
	CCSprite *spriteFlag15;
	CCSprite *spriteFlag16;
	CCSprite *spriteFlag17;
	CCSprite *spriteFlag18;
	CCSprite *spriteFlag19;
	CCSprite *spriteFlag20;
	CCSprite *spriteFlag21;
	CCSprite *spriteFlag22;
	CCSprite *spriteFlag23;
	CCSprite *spriteFlag24;
	CCSprite *spriteFlag25;
	CCSprite *spriteFlag26;
	CCSprite *spriteFlag27;
	CCSprite *spriteFlag28;
	CCSprite *spriteFlag29;
	CCSprite *spriteFlag30;
	CCSprite *spriteFlag31;
	CCSprite *spriteFlag32;

/*	NSArray *teamKeys;
	NSArray *groupsKeys;
	NSArray *teamNamesObject;
	NSArray *teamShortNamesObject;
	NSArray *teamImagesObject;
	NSArray *teamGroupsObject;
	
	NSDictionary *teamNamesDictionary;
	NSDictionary *teamShortNamesDictionary;
	NSDictionary *teamImagesDictionary;
	NSDictionary *teamGroupsDictionary;*/
	
	CCLabelAtlas *labelT1;
	CCLabelAtlas *labelT2;
	CCLabelAtlas *labelT3;
	CCLabelAtlas *labelT4;
	
	CCLabelAtlas *labelP1;
	CCLabelAtlas *labelP2;
	CCLabelAtlas *labelP3;
	CCLabelAtlas *labelP4;
    
    CCSpriteBatchNode* _batchNode;
	
		
}
@property (nonatomic,readwrite) int selectedGroup;
@property (nonatomic,readwrite) int yourTeam;
@property (nonatomic,readwrite) int yourTeamGroup;

-(void)selectGroup:(id)sender;
-(void)goPlay;
-(void)goTournament;
- (void)setupBatchNode;

//
// Build tables, team 1 , team 2 , team 3, team 4 for each group
// t1 = team1 from matchs table
// p1 = points for t1 /  w1 = wins for p1  / l1 = loss for t1  / d1
//
-(void)setScoreBoard:(CCSprite*)sprite 
				tid1:(int)tid1 tid2:(int)tid2 tid3:(int)tid3 tid4:(int)tid4
				  t1:(NSString*)t1 t2:(NSString*)t2 t3:(NSString*)t3 t4:(NSString*)t4 
			   flag1:(NSString*)flag1 flag2:(NSString*)flag2 flag3:(NSString*)flag3 flag4:(NSString*)flag4
				  p1:(int)p1 p2:(int)p2 p3:(int)p3 p4:(int)p4
				  w1:(int)w1 w2:(int)w2 w3:(int)w3 w4:(int)w4 
				  l1:(int)l1 l2:(int)l2 l3:(int)l3 l4:(int)l4 
				  d1:(int)d1 d2:(int)d2 d3:(int)d3 d4:(int)d4 
				 gs1:(int)gs1 gs2:(int)gs2 gs3:(int)gs3 gs4:(int)gs4 
				 gc1:(int)gc1 gc2:(int)gc2 gc3:(int)gc3 gc4:(int)gc4 
				 gd1:(int)gd1 gd2:(int)gd2 gd3:(int)gd3 gd4:(int)gd4;

-(void)setMatchBoard:(CCSprite*)sprite
				p1p2:(int)p1p2 p1p3:(int)p1p3 p1p4:(int)p1p4 
				p2p1:(int)p2p1 p2p3:(int)p2p3 p2p4:(int)p2p4
				p3p1:(int)p3p1 p3p2:(int)p3p2 p3p4:(int)p3p4
				p4p1:(int)p4p1 p4p2:(int)p4p2 p4p3:(int)p4p3;
 

@end
