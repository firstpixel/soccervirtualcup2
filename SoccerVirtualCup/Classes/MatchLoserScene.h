//
//  MatchLoserScene.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/12/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface MatchLoserScene : CCScene {}
@end

@interface MatchLoserLayer : CCLayer {
	CCSprite*spriteGoPlay;
}
@end