//
//  GlobalCupSingleton.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/16/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "chipmunk.h"

@interface GlobalCupSingleton : NSObject {
	int myTeamID;
	int atualMatch;
	
	int matchWeather;
	int defaultTimeLeft;
	int currentScoreP1;
	int currentScoreP2;
	
	NSInteger teamsArray[32];  // c-style array
	NSInteger lastMatchScoreArray[32];
	
	NSInteger lastTournamentScoreArray[32];
	
	NSInteger pointsArray[32];
	NSInteger winsArray[32];
	NSInteger lossArray[32];
	NSInteger drawArray[32];
	NSInteger goalScoredArray[32];
	NSInteger goalConcededArray[32];
	NSInteger goalDifferenceArray[32];
	
	NSInteger p1p2Array[8];
	NSInteger p1p3Array[8];
	NSInteger p1p4Array[8];
	
	NSInteger p2p1Array[8];
	NSInteger p2p3Array[8];
	NSInteger p2p4Array[8];
	
	NSInteger p3p1Array[8];
	NSInteger p3p2Array[8];
	NSInteger p3p4Array[8];
	
	NSInteger p4p1Array[8];
	NSInteger p4p2Array[8];
	NSInteger p4p3Array[8];
	
	int spritePlayer1;
	int spritePlayer2;
	
	int tournament1T1;
	int tournament1T2;
	int tournament1S1;
	int tournament1S2;
	int tournament2T1;
	int tournament2T2;
	int tournament2S1;
	int tournament2S2;
	int tournament3T1;
	int tournament3T2;
	int tournament3S1;
	int tournament3S2;
	int tournament4T1;
	int tournament4T2;
	int tournament4S1;
	int tournament4S2;
	int tournament5T1;
	int tournament5T2;
	int tournament5S1;
	int tournament5S2;
	int tournament6T1;
	int tournament6T2;
	int tournament6S1;
	int tournament6S2;
	int tournament7T1;
	int tournament7T2;
	int tournament7S1;
	int tournament7S2;
	int tournament8T1;
	int tournament8T2;
	int tournament8S1;
	int tournament8S2;
	int tournament9T1;
	int tournament9T2;
	int tournament9S1;
	int tournament9S2;
	int tournament10T1;
	int tournament10T2;
	int tournament10S1;
	int tournament10S2;
	int tournament11T1;
	int tournament11T2;
	int tournament11S1;
	int tournament11S2;
	int tournament12T1;
	int tournament12T2;
	int tournament12S1;
	int tournament12S2;
	int tournament13T1;
	int tournament13T2;
	int tournament13S1;
	int tournament13S2;
	int tournament14T1;
	int tournament14T2;
	int tournament14S1;
	int tournament14S2;
	int tournament15T1;
	int tournament15T2;
	int tournament15S1;
	int tournament15S2;
	int tournament16T1;
	int tournament16T2;
	int tournament16S1;
	int tournament16S2;
	
	int champGoalsConceded;
	int sevenInARow;
	
	
}
@property (nonatomic, readwrite) int matchWeather; // 0 = random   1 = fine   2 = rainy
@property (nonatomic,readwrite) int defaultTimeLeft;

@property (nonatomic,readwrite) int spritePlayer1;
@property (nonatomic,readwrite) int spritePlayer2;
@property (nonatomic,readwrite) int currentScoreP1;
@property (nonatomic,readwrite) int currentScoreP2;

@property (nonatomic, readwrite) int sevenInARow;
@property (nonatomic, readwrite) int champGoalsConceded;

@property (nonatomic, readwrite) int myTeamID;
@property (nonatomic, readwrite) int atualMatch;
@property (nonatomic, readwrite) int tournament1T1;
@property (nonatomic, readwrite) int tournament1T2;
@property (nonatomic, readwrite) int tournament1S1;
@property (nonatomic, readwrite) int tournament1S2;
@property (nonatomic, readwrite) int tournament2T1;
@property (nonatomic, readwrite) int tournament2T2;
@property (nonatomic, readwrite) int tournament2S1;
@property (nonatomic, readwrite) int tournament2S2;
@property (nonatomic, readwrite) int tournament3T1;
@property (nonatomic, readwrite) int tournament3T2;
@property (nonatomic, readwrite) int tournament3S1;
@property (nonatomic, readwrite) int tournament3S2;
@property (nonatomic, readwrite) int tournament4T1;
@property (nonatomic, readwrite) int tournament4T2;
@property (nonatomic, readwrite) int tournament4S1;
@property (nonatomic, readwrite) int tournament4S2;
@property (nonatomic, readwrite) int tournament5T1;
@property (nonatomic, readwrite) int tournament5T2;
@property (nonatomic, readwrite) int tournament5S1;
@property (nonatomic, readwrite) int tournament5S2;
@property (nonatomic, readwrite) int tournament6T1;
@property (nonatomic, readwrite) int tournament6T2;
@property (nonatomic, readwrite) int tournament6S1;
@property (nonatomic, readwrite) int tournament6S2;
@property (nonatomic, readwrite) int tournament7T1;
@property (nonatomic, readwrite) int tournament7T2;
@property (nonatomic, readwrite) int tournament7S1;
@property (nonatomic, readwrite) int tournament7S2;
@property (nonatomic, readwrite) int tournament8T1;
@property (nonatomic, readwrite) int tournament8T2;
@property (nonatomic, readwrite) int tournament8S1;
@property (nonatomic, readwrite) int tournament8S2;
@property (nonatomic, readwrite) int tournament9T1;
@property (nonatomic, readwrite) int tournament9T2;
@property (nonatomic, readwrite) int tournament9S1;
@property (nonatomic, readwrite) int tournament9S2;
@property (nonatomic, readwrite) int tournament10T1;
@property (nonatomic, readwrite) int tournament10T2;
@property (nonatomic, readwrite) int tournament10S1;
@property (nonatomic, readwrite) int tournament10S2;
@property (nonatomic, readwrite) int tournament11T1;
@property (nonatomic, readwrite) int tournament11T2;
@property (nonatomic, readwrite) int tournament11S1;
@property (nonatomic, readwrite) int tournament11S2;
@property (nonatomic, readwrite) int tournament12T1;
@property (nonatomic, readwrite) int tournament12T2;
@property (nonatomic, readwrite) int tournament12S1;
@property (nonatomic, readwrite) int tournament12S2;
@property (nonatomic, readwrite) int tournament13T1;
@property (nonatomic, readwrite) int tournament13T2;
@property (nonatomic, readwrite) int tournament13S1;
@property (nonatomic, readwrite) int tournament13S2;
@property (nonatomic, readwrite) int tournament14T1;
@property (nonatomic, readwrite) int tournament14T2;
@property (nonatomic, readwrite) int tournament14S1;
@property (nonatomic, readwrite) int tournament14S2;
@property (nonatomic, readwrite) int tournament15T1;
@property (nonatomic, readwrite) int tournament15T2;
@property (nonatomic, readwrite) int tournament15S1;
@property (nonatomic, readwrite) int tournament15S2;
@property (nonatomic, readwrite) int tournament16T1;
@property (nonatomic, readwrite) int tournament16T2;
@property (nonatomic, readwrite) int tournament16S1;
@property (nonatomic, readwrite) int tournament16S2;



+ (GlobalCupSingleton *) sharedInstance;

-(void)saveGameData;
-(void)loadGameData;

- (int) setTournamentWinner:(int)p1 player2:(int)p2 goalp1:(int)goalp1 goalp2:(int)goalp2;
- (int) setRandomTournamentWinner:(int)p1 indexP2:(int)p2;
- (int) getLastTournamentScoreByTeamKey:(NSUInteger)x;
- (void) setLastTournamentScoreByTeamKey:(NSUInteger)x ToValue:(NSUInteger)newVal;


- (int) setMatchWinner:(NSUInteger)p1 player2:(NSUInteger)p2 goalp1:(NSUInteger)goalp1 goalp2:(NSUInteger)goalp2;
- (int) setRandomMatchWinner:(NSUInteger)p1 indexP2:(NSUInteger)p2;
- (int) getLastMatchScoreByTeamKey:(NSUInteger)x;
- (void) setLastMatchScoreByTeamKey:(NSUInteger)x ToValue:(NSUInteger)newVal;

- (int) getP1P2Array:(NSUInteger)group;
- (void) setP1P2Array:(NSUInteger)group ToValue:(NSUInteger)newVal;
- (int) getP1P3Array:(NSUInteger)group;
- (void) setP1P3Array:(NSUInteger)group ToValue:(NSUInteger)newVal;
- (int) getP1P4Array:(NSUInteger)group;
- (void) setP1P4Array:(NSUInteger)group ToValue:(NSUInteger)newVal;

- (int) getP2P1Array:(NSUInteger)group;
- (void) setP2P1Array:(NSUInteger)group ToValue:(NSUInteger)newVal;
- (int) getP2P3Array:(NSUInteger)group;
- (void) setP2P3Array:(NSUInteger)group ToValue:(NSUInteger)newVal;
- (int) getP2P4Array:(NSUInteger)group;
- (void) setP2P4Array:(NSUInteger)group ToValue:(NSUInteger)newVal;

- (int) getP3P1Array:(NSUInteger)group;
- (void) setP3P1Array:(NSUInteger)group ToValue:(NSUInteger)newVal;
- (int) getP3P2Array:(NSUInteger)group;
- (void) setP3P2Array:(NSUInteger)group ToValue:(NSUInteger)newVal;
- (int) getP3P4Array:(NSUInteger)group;
- (void) setP3P4Array:(NSUInteger)group ToValue:(NSUInteger)newVal;

- (int) getP4P1Array:(NSUInteger)group;
- (void) setP4P1Array:(NSUInteger)group ToValue:(NSUInteger)newVal;
- (int) getP4P2Array:(NSUInteger)group;
- (void) setP4P2Array:(NSUInteger)group ToValue:(NSUInteger)newVal;
- (int) getP4P3Array:(NSUInteger)group;
- (void) setP4P3Array:(NSUInteger)group ToValue:(NSUInteger)newVal;


- (int) getPointsArrayAtPos:(NSUInteger)x;
- (void) setPointsArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal;
- (int) getWinsArrayAtPos:(NSUInteger)x;
- (void) setWinsArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal;
- (int) getLossArrayAtPos:(NSUInteger)x;
- (void) setLossArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal;
- (int) getDrawArrayAtPos:(NSUInteger)x;
- (void) setDrawArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal;
- (int) getGoalScoredArrayAtPos:(NSUInteger)x;
- (void) setGoalScoredArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal;
- (int) getGoalConcededArrayAtPos:(NSUInteger)x;
- (void) setGoalConcededArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal;
- (int) getGoalDifferenceArrayAtPos:(NSUInteger)x;
- (void) setGoalDifferenceArrayAtPos:(NSUInteger)x ToValue:(NSUInteger)newVal;
- (void) clearArraysData;


-(void)saveTokenID:(int)tokenID;
-(NSNumber*)returnTokenID;
-(BOOL)hasTokenID;
@end
