//
//  PeerServiceManager.h
//  PeerConnection
//
//  Created by Gil Beyruth on 8/27/16.
//  Copyright © 2016 Gil Beyruth. All rights reserved.
//

#ifndef PeerServiceManager_h
#define PeerServiceManager_h


#endif /* PeerServiceManager_h */

#import <Foundation/Foundation.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>



@protocol PeerServiceManagerDelegate;

@interface PeerServiceManager : NSObject <MCNearbyServiceAdvertiserDelegate, MCNearbyServiceBrowserDelegate, MCSessionDelegate>{
    MCBrowserViewController* browserVC;
    NSString* PeerServiceType;
    }

@property (weak) id<PeerServiceManagerDelegate,MCBrowserViewControllerDelegate> delegate;
@property (retain) NSString* PeerServiceType;

-(void)startBrowsingAndAdvertising:(UIViewController *) viewController;
-(void)stopAdvertisingAndBrowsing;
-(void)disconnect;

+(PeerServiceManager *) sharedInstance;
-(void)sendData:(NSData*)data;
/*
 @param data The data to be sent to all of the players. This should not exceed 1000 bytes for quick sending, and should not exceed 87 kilobytes when sending reliably.
 @param players An array of GKPlayer objects to which the data should be sent (can be more efficient if you are not implementing a peer-to-peer connection, or don't need to send to all players).
 @param sendQuickly Specify YES if the data should be sent without ensuring delivery (faster). NO if the data's delivery should be guaranteed (slower).
 @param handler Implement the completion handler to recieve information about the status of the data send. The error parameter may be nil if there was no error. 
 */
- (BOOL)sendAllPlayersMatchData:(NSData *)data shouldSendQuickly:(BOOL)sendQuickly completion:(void (^)(BOOL success, NSError *error))handler;


@end

@protocol PeerServiceManagerDelegate <MCBrowserViewControllerDelegate>
@required
-(void)connectedDevicesChanged:(PeerServiceManager*)peerServiceManager connectedDevices: (NSArray<NSString *>*)connectedDevices totalDevices:(int)totalDevices;
-(void)receiveData:(PeerServiceManager*)peerServiceManager didReceiveData: (NSData*) data;
- (void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController;
- (void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController;
-(void)receiveData:(PeerServiceManager*)peerServiceManager didConnect:(BOOL)connected;



@end