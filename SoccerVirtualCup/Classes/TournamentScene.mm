//
//  TournamentScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/6/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "TournamentScene.h"
#import "GlobalCupSingleton.h"
#import "GlobalSingleton.h"
#import "DataHolder.h"
#import "GameScene.h"
#import "MatchDrawScene.h"
#import "MatchWinnerScene.h"
#import "MatchLoserScene.h"
#import "cocos2d.h"
#import "SimpleAudioEngine.h"
#import "TournamentWinnerScene.h"
#import "TournamentSecondScene.h"
#import "TournamentThirdScene.h"
#import "DeviceSettings.h"

@implementation TournamentScene
- (id) init {
    self = [super init];
    if (self != nil) {	
        
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"tournament_screen-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"tournament_screen-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"tournament_screen-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"tournament_screen-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"tournament_screen.jpg"];
                }
            }
        }
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
		[self addChild:[TournamentSceneLayer node] z:1];
		
    }
    return self;
}
@end




@implementation TournamentSceneLayer
@synthesize myTeamStayOnTournament;
@synthesize winner;
@synthesize second;
@synthesize third;
@synthesize champ;

- (id) init {
    self = [super init];
	if (self != nil) {
		champ = 100;
		second = 100;
		third = 100;
		winner = 100;
		self.isTouchEnabled = YES;
		myDataHolder = [DataHolder sharedInstance];
		[myDataHolder clearData];
		
		GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
		if([mySingleton audioOn]==YES){
			SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
			if (sae != nil) {
				[sae playBackgroundMusic:@"loop.mp3" loop:YES];
				sae.backgroundMusicVolume = 0.1f;

			}
		}
		
		
		//int groupId = [myDataHolder getGroupForTeamKey:[mySingleton spritePlayer1]];
		
		GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
		[mySingleton setSpritePlayer1:[myCupSingleton spritePlayer1]];

		
		spriteGoPlay = [CCSprite spriteWithSpriteFrameName:@"go_bt.png"];
		[spriteGoPlay setPosition:ADJUST_CCP(ccp(240,46))];
		[self addChild:spriteGoPlay z:101 tag:101];
		
		int id1;
		int id2;
		
		int p1Scores;
		int p2Scores;
		//int tempMatchResult;
		myTeamStayOnTournament = NO;
		
		if([myCupSingleton atualMatch]==3){
			id1 = [myCupSingleton tournament1T1];
			id2 = [myCupSingleton tournament1T2];
			if([mySingleton spritePlayer1]==id1 || [mySingleton spritePlayer1]==id2 ){
				myTeamStayOnTournament = YES;
				if([mySingleton spritePlayer1]==id1){
					[mySingleton setSpritePlayer2:id2];
				}else{
					[mySingleton setSpritePlayer2:id1];
				}
			}
			[self displayTournament:1 team:id1 versus:id2 p1Score:100 p2Score:100];
			
			id1 = [myCupSingleton tournament2T1];
			id2 = [myCupSingleton tournament2T2];
			if([mySingleton spritePlayer1]==id1 || [mySingleton spritePlayer1]==id2 ){
				myTeamStayOnTournament = YES;
				if([mySingleton spritePlayer1]==id1){
					[mySingleton setSpritePlayer2:id2];
				}else{
					[mySingleton setSpritePlayer2:id1];
				}
			}
			[self displayTournament:2 team:id1 versus:id2 p1Score:100 p2Score:100];
			id1 = [myCupSingleton tournament3T1];
			id2 = [myCupSingleton tournament3T2];
			if([mySingleton spritePlayer1]==id1 || [mySingleton spritePlayer1]==id2 ){
				myTeamStayOnTournament = YES;
				if([mySingleton spritePlayer1]==id1){
					[mySingleton setSpritePlayer2:id2];
				}else{
					[mySingleton setSpritePlayer2:id1];
				}
			}
			
			[self displayTournament:3 team:id1 versus:id2 p1Score:100 p2Score:100];
			
			id1 = [myCupSingleton tournament4T1];
			id2 = [myCupSingleton tournament4T2];
			if([mySingleton spritePlayer1]==id1 || [mySingleton spritePlayer1]==id2 ){
				myTeamStayOnTournament = YES;
				if([mySingleton spritePlayer1]==id1){
					[mySingleton setSpritePlayer2:id2];
				}else{
					[mySingleton setSpritePlayer2:id1];
				}
			}
			
			[self displayTournament:4 team:id1 versus:id2 p1Score:100 p2Score:100];
			
			id1 = [myCupSingleton tournament5T1];
			id2 = [myCupSingleton tournament5T2];
			if([mySingleton spritePlayer1]==id1 || [mySingleton spritePlayer1]==id2 ){
				myTeamStayOnTournament = YES;
				if([mySingleton spritePlayer1]==id1){
					[mySingleton setSpritePlayer2:id2];
				}else{
					[mySingleton setSpritePlayer2:id1];
				}
			}
			
			[self displayTournament:5 team:id1 versus:id2 p1Score:100 p2Score:100];
			
			id1 = [myCupSingleton tournament6T1];
			id2 = [myCupSingleton tournament6T2];
			if([mySingleton spritePlayer1]==id1 || [mySingleton spritePlayer1]==id2 ){
				myTeamStayOnTournament = YES;
				if([mySingleton spritePlayer1]==id1){
					[mySingleton setSpritePlayer2:id2];
				}else{
					[mySingleton setSpritePlayer2:id1];
				}
			}
			
			[self displayTournament:6 team:id1 versus:id2 p1Score:100 p2Score:100];
			
			id1 = [myCupSingleton tournament7T1];
			id2 = [myCupSingleton tournament7T2];
			if([mySingleton spritePlayer1]==id1 || [mySingleton spritePlayer1]==id2 ){
				myTeamStayOnTournament = YES;
				if([mySingleton spritePlayer1]==id1){
					[mySingleton setSpritePlayer2:id2];
				}else{
					[mySingleton setSpritePlayer2:id1];
				}
			}
			[self displayTournament:7 team:id1 versus:id2 p1Score:100 p2Score:100];
			
			id1 = [myCupSingleton tournament8T1];
			id2 = [myCupSingleton tournament8T2];
			if([mySingleton spritePlayer1]==id1 || [mySingleton spritePlayer1]==id2 ){
				myTeamStayOnTournament = YES;
				if([mySingleton spritePlayer1]==id1){
					[mySingleton setSpritePlayer2:id2];
				}else{
					[mySingleton setSpritePlayer2:id1];
				}
			}
			[self displayTournament:8 team:id1 versus:id2 p1Score:100 p2Score:100];

			
		}else if([myCupSingleton atualMatch]==4){
			[mySingleton setSpritePlayer1:[myCupSingleton spritePlayer1]];
			
			//***************************
			//	1 e 2
			//***************************
			id1 = [myCupSingleton tournament1T1];
			id2 = [myCupSingleton tournament1T2];
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
				
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament9T1:winner];
			[myCupSingleton setTournament1S1:p1Scores];
			[myCupSingleton setTournament1S2:p2Scores];			
			[self displayTournament:1 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			//--------------------------
			//
			id1 = [myCupSingleton tournament2T1];
			id2 = [myCupSingleton tournament2T2];
			
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
				
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament9T2:winner];
			[myCupSingleton setTournament2S1:p1Scores];
			[myCupSingleton setTournament2S2:p2Scores];
			
			[self displayTournament:2 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			//***************************
			//	3 e 4
			//***************************
			
			id1 = [myCupSingleton tournament3T1];
			id2 = [myCupSingleton tournament3T2];
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
				
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament10T1:winner];
			[myCupSingleton setTournament3S1:p1Scores];
			[myCupSingleton setTournament3S2:p2Scores];
			
			[self displayTournament:3 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			//--------------------------
			//
			id1 = [myCupSingleton tournament4T1];
			id2 = [myCupSingleton tournament4T2];
			
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament10T2:winner];
			[myCupSingleton setTournament4S1:p1Scores];
			[myCupSingleton setTournament4S2:p2Scores];
			
			[self displayTournament:4 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			//***************************
			//	5 e 6
			//***************************
			
			id1 = [myCupSingleton tournament5T1];
			id2 = [myCupSingleton tournament5T2];
			
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
				
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament11T1:winner];
			[myCupSingleton setTournament5S1:p1Scores];
			[myCupSingleton setTournament5S2:p2Scores];
			
			[self displayTournament:5 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			
			//--------------------------
			//
			id1 = [myCupSingleton tournament6T1];
			id2 = [myCupSingleton tournament6T2];
			
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament11T2:winner];
			[myCupSingleton setTournament6S1:p1Scores];
			[myCupSingleton setTournament6S2:p2Scores];
			
			
			[self displayTournament:6 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			
			
			
			//***************************
			//	7 e 8
			//***************************
			
			id1 = [myCupSingleton tournament7T1];
			id2 = [myCupSingleton tournament7T2];
			
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
				
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament12T1:winner];
			[myCupSingleton setTournament7S1:p1Scores];
			[myCupSingleton setTournament7S2:p2Scores];
		
			[self displayTournament:7 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			
			
			
			//--------------------------
			//
			id1 = [myCupSingleton tournament8T1];
			id2 = [myCupSingleton tournament8T2];
			
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament12T2:winner];
			[myCupSingleton setTournament8S1:p1Scores];
			[myCupSingleton setTournament8S2:p2Scores];
		
			[self displayTournament:8 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			
			
			
				[self displayTournament:9 team:[myCupSingleton tournament9T1] versus:[myCupSingleton tournament9T2]  p1Score:100 p2Score:100];
				[self displayTournament:10 team:[myCupSingleton tournament10T1] versus:[myCupSingleton tournament10T2] p1Score:100 p2Score:100];
				[self displayTournament:11 team:[myCupSingleton tournament11T1] versus:[myCupSingleton tournament11T2]  p1Score:100 p2Score:100];
				[self displayTournament:12 team:[myCupSingleton tournament12T1] versus:[myCupSingleton tournament12T2]  p1Score:100 p2Score:100];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament9T1])[mySingleton setSpritePlayer2:[myCupSingleton tournament9T2]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament9T2])[mySingleton setSpritePlayer2:[myCupSingleton tournament9T1]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament10T1])[mySingleton setSpritePlayer2:[myCupSingleton tournament10T2]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament10T2])[mySingleton setSpritePlayer2:[myCupSingleton tournament10T1]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament11T1])[mySingleton setSpritePlayer2:[myCupSingleton tournament11T2]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament11T2])[mySingleton setSpritePlayer2:[myCupSingleton tournament11T1]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament12T1])[mySingleton setSpritePlayer2:[myCupSingleton tournament12T2]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament12T2])[mySingleton setSpritePlayer2:[myCupSingleton tournament12T1]];
			
			
			
			
		}else if([myCupSingleton atualMatch]==5){
			myTeamStayOnTournament = NO;

			///9 e 10
			id1 = [myCupSingleton tournament9T1];
			id2 = [myCupSingleton tournament9T2];
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament13T2:winner];
			[myCupSingleton setTournament9S1:p1Scores];
			[myCupSingleton setTournament9S2:p2Scores];
			[self displayTournament:9 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			
			id1 = [myCupSingleton tournament10T1];
			id2 = [myCupSingleton tournament10T2];
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament13T1:winner];
			[myCupSingleton setTournament10S1:p1Scores];
			[myCupSingleton setTournament10S2:p2Scores];
			[self displayTournament:10 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			///11 e 12
			id1 = [myCupSingleton tournament11T1];
			id2 = [myCupSingleton tournament11T2];
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament14T1:winner];
			[myCupSingleton setTournament11S1:p1Scores];
			[myCupSingleton setTournament11S2:p2Scores];
			[self displayTournament:11 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			
			id1 = [myCupSingleton tournament12T1];
			id2 = [myCupSingleton tournament12T2];
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament14T2:winner];
			[myCupSingleton setTournament12S1:p1Scores];
			[myCupSingleton setTournament12S2:p2Scores];
			[self displayTournament:12 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			
			
			[self displayTournament:13 team:[myCupSingleton tournament13T1] versus:[myCupSingleton tournament13T2]  p1Score:100 p2Score:100];
			[self displayTournament:14 team:[myCupSingleton tournament14T1] versus:[myCupSingleton tournament14T2] p1Score:100 p2Score:100];
			if([mySingleton spritePlayer1]==[myCupSingleton tournament13T1])[mySingleton setSpritePlayer2:[myCupSingleton tournament13T2]];
			if([mySingleton spritePlayer1]==[myCupSingleton tournament13T2])[mySingleton setSpritePlayer2:[myCupSingleton tournament13T1]];
			if([mySingleton spritePlayer1]==[myCupSingleton tournament14T1])[mySingleton setSpritePlayer2:[myCupSingleton tournament14T2]];
			if([mySingleton spritePlayer1]==[myCupSingleton tournament14T2])[mySingleton setSpritePlayer2:[myCupSingleton tournament14T1]];
			
			[self displayTournament:1 team:[myCupSingleton tournament1T1] versus:[myCupSingleton tournament1T2] p1Score:[myCupSingleton tournament1S1] p2Score:[myCupSingleton tournament1S2]];
			[self displayTournament:2 team:[myCupSingleton tournament2T1] versus:[myCupSingleton tournament2T2] p1Score:[myCupSingleton tournament2S1] p2Score:[myCupSingleton tournament2S2]];
			[self displayTournament:3 team:[myCupSingleton tournament3T1] versus:[myCupSingleton tournament3T2] p1Score:[myCupSingleton tournament3S1] p2Score:[myCupSingleton tournament3S2]];
			[self displayTournament:4 team:[myCupSingleton tournament4T1] versus:[myCupSingleton tournament4T2] p1Score:[myCupSingleton tournament4S1] p2Score:[myCupSingleton tournament4S2]];
			[self displayTournament:5 team:[myCupSingleton tournament5T1] versus:[myCupSingleton tournament5T2] p1Score:[myCupSingleton tournament5S1] p2Score:[myCupSingleton tournament5S2]];
			[self displayTournament:6 team:[myCupSingleton tournament6T1] versus:[myCupSingleton tournament6T2] p1Score:[myCupSingleton tournament6S1] p2Score:[myCupSingleton tournament6S2]];
			[self displayTournament:7 team:[myCupSingleton tournament7T1] versus:[myCupSingleton tournament7T2] p1Score:[myCupSingleton tournament7S1] p2Score:[myCupSingleton tournament7S2]];
			[self displayTournament:8 team:[myCupSingleton tournament8T1] versus:[myCupSingleton tournament8T2] p1Score:[myCupSingleton tournament8S1] p2Score:[myCupSingleton tournament8S2]];
			
			
		}else if([myCupSingleton atualMatch]==6){
			myTeamStayOnTournament = NO;
				
			///13 e 14
			id1 = [myCupSingleton tournament13T1];
			id2 = [myCupSingleton tournament13T2];
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament16T1:winner];
			if(id1==winner){
				[myCupSingleton setTournament15T1:id2];
			}else{
				[myCupSingleton setTournament15T1:id1];			
			}
			[myCupSingleton setTournament13S1:p1Scores];
			[myCupSingleton setTournament13S2:p2Scores];
			[self displayTournament:13 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			
			id1 = [myCupSingleton tournament14T1];
			id2 = [myCupSingleton tournament14T2];
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				winner = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(winner==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;				
			}
			[myCupSingleton setTournament16T2:winner];
			if(id1==winner){
				[myCupSingleton setTournament15T2:id2];
			}else{
				[myCupSingleton setTournament15T2:id1];			
			}
			[myCupSingleton setTournament14S1:p1Scores];
			[myCupSingleton setTournament14S2:p2Scores];
			[self displayTournament:14 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			if([myCupSingleton atualMatch]==6){
				[self displayTournament:15 team:[myCupSingleton tournament15T1] versus:[myCupSingleton tournament15T2]  p1Score:100 p2Score:100];
				[self displayTournament:16 team:[myCupSingleton tournament16T1] versus:[myCupSingleton tournament16T2] p1Score:100 p2Score:100];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament15T1])[mySingleton setSpritePlayer2:[myCupSingleton tournament15T2]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament15T2])[mySingleton setSpritePlayer2:[myCupSingleton tournament15T1]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament16T1])[mySingleton setSpritePlayer2:[myCupSingleton tournament16T2]];
				if([mySingleton spritePlayer1]==[myCupSingleton tournament16T2])[mySingleton setSpritePlayer2:[myCupSingleton tournament16T1]];
				
			}
			[self displayTournament:1 team:[myCupSingleton tournament1T1] versus:[myCupSingleton tournament1T2] p1Score:[myCupSingleton tournament1S1] p2Score:[myCupSingleton tournament1S2]];
			[self displayTournament:2 team:[myCupSingleton tournament2T1] versus:[myCupSingleton tournament2T2] p1Score:[myCupSingleton tournament2S1] p2Score:[myCupSingleton tournament2S2]];
			[self displayTournament:3 team:[myCupSingleton tournament3T1] versus:[myCupSingleton tournament3T2] p1Score:[myCupSingleton tournament3S1] p2Score:[myCupSingleton tournament3S2]];
			[self displayTournament:4 team:[myCupSingleton tournament4T1] versus:[myCupSingleton tournament4T2] p1Score:[myCupSingleton tournament4S1] p2Score:[myCupSingleton tournament4S2]];
			[self displayTournament:5 team:[myCupSingleton tournament5T1] versus:[myCupSingleton tournament5T2] p1Score:[myCupSingleton tournament5S1] p2Score:[myCupSingleton tournament5S2]];
			[self displayTournament:6 team:[myCupSingleton tournament6T1] versus:[myCupSingleton tournament6T2] p1Score:[myCupSingleton tournament6S1] p2Score:[myCupSingleton tournament6S2]];
			[self displayTournament:7 team:[myCupSingleton tournament7T1] versus:[myCupSingleton tournament7T2] p1Score:[myCupSingleton tournament7S1] p2Score:[myCupSingleton tournament7S2]];
			[self displayTournament:8 team:[myCupSingleton tournament8T1] versus:[myCupSingleton tournament8T2] p1Score:[myCupSingleton tournament8S1] p2Score:[myCupSingleton tournament8S2]];
			[self displayTournament:9 team:[myCupSingleton tournament9T1] versus:[myCupSingleton tournament9T2] p1Score:[myCupSingleton tournament9S1] p2Score:[myCupSingleton tournament9S2]];
			[self displayTournament:10 team:[myCupSingleton tournament10T1] versus:[myCupSingleton tournament10T2] p1Score:[myCupSingleton tournament10S1] p2Score:[myCupSingleton tournament10S2]];
			[self displayTournament:11 team:[myCupSingleton tournament11T1] versus:[myCupSingleton tournament11T2] p1Score:[myCupSingleton tournament11S1] p2Score:[myCupSingleton tournament11S2]];
			[self displayTournament:12 team:[myCupSingleton tournament12T1] versus:[myCupSingleton tournament12T2] p1Score:[myCupSingleton tournament12S1] p2Score:[myCupSingleton tournament12S2]];
			
		}else if([myCupSingleton atualMatch]>6){
			myTeamStayOnTournament = NO;

			///15 e 16
			id1 = [myCupSingleton tournament15T1];
			id2 = [myCupSingleton tournament15T2];
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
					
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				third = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(third==[mySingleton spritePlayer1])myTeamStayOnTournament = YES;
			}
			[myCupSingleton setTournament15S1:p1Scores];
			[myCupSingleton setTournament15S2:p2Scores];
			[self displayTournament:15 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			
			id1 = [myCupSingleton tournament16T1];
			id2 = [myCupSingleton tournament16T2];
			NSLog(@"add my team: %i , t1 %i, t2 %i",[mySingleton spritePlayer1],id1, id2);
			if([mySingleton spritePlayer1]!=id1 && [mySingleton spritePlayer1]!=id2){
				winner = [myCupSingleton setRandomTournamentWinner:id1 indexP2:id2];
				p1Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id1];
				p2Scores = [myCupSingleton getLastTournamentScoreByTeamKey:id2];
			}else{
				if([mySingleton spritePlayer1]==id1){
					p1Scores = [myCupSingleton currentScoreP1];
					p2Scores = [myCupSingleton currentScoreP2];
				}else{
					p2Scores = [myCupSingleton currentScoreP1];
					p1Scores = [myCupSingleton currentScoreP2];
				}
				champ = [myCupSingleton setTournamentWinner:id1 player2:id2 goalp1:p1Scores goalp2:p2Scores];
				if(champ==[mySingleton spritePlayer1]){
					myTeamStayOnTournament = YES;
				}else{
					myTeamStayOnTournament = YES;
					second = id2;
				}
			}
			[myCupSingleton setTournament16S1:p1Scores];
			[myCupSingleton setTournament16S2:p2Scores];
			[self displayTournament:16 team:id1 versus:id2 p1Score:p1Scores p2Score:p2Scores];
			
			[self displayTournament:1 team:[myCupSingleton tournament1T1] versus:[myCupSingleton tournament1T2] p1Score:[myCupSingleton tournament1S1] p2Score:[myCupSingleton tournament1S2]];
			[self displayTournament:2 team:[myCupSingleton tournament2T1] versus:[myCupSingleton tournament2T2] p1Score:[myCupSingleton tournament2S1] p2Score:[myCupSingleton tournament2S2]];
			[self displayTournament:3 team:[myCupSingleton tournament3T1] versus:[myCupSingleton tournament3T2] p1Score:[myCupSingleton tournament3S1] p2Score:[myCupSingleton tournament3S2]];
			[self displayTournament:4 team:[myCupSingleton tournament4T1] versus:[myCupSingleton tournament4T2] p1Score:[myCupSingleton tournament4S1] p2Score:[myCupSingleton tournament4S2]];
			[self displayTournament:5 team:[myCupSingleton tournament5T1] versus:[myCupSingleton tournament5T2] p1Score:[myCupSingleton tournament5S1] p2Score:[myCupSingleton tournament5S2]];
			[self displayTournament:6 team:[myCupSingleton tournament6T1] versus:[myCupSingleton tournament6T2] p1Score:[myCupSingleton tournament6S1] p2Score:[myCupSingleton tournament6S2]];
			[self displayTournament:7 team:[myCupSingleton tournament7T1] versus:[myCupSingleton tournament7T2] p1Score:[myCupSingleton tournament7S1] p2Score:[myCupSingleton tournament7S2]];
			[self displayTournament:8 team:[myCupSingleton tournament8T1] versus:[myCupSingleton tournament8T2] p1Score:[myCupSingleton tournament8S1] p2Score:[myCupSingleton tournament8S2]];
			[self displayTournament:9 team:[myCupSingleton tournament9T1] versus:[myCupSingleton tournament9T2] p1Score:[myCupSingleton tournament9S1] p2Score:[myCupSingleton tournament9S2]];
			[self displayTournament:10 team:[myCupSingleton tournament10T1] versus:[myCupSingleton tournament10T2] p1Score:[myCupSingleton tournament10S1] p2Score:[myCupSingleton tournament10S2]];
			[self displayTournament:11 team:[myCupSingleton tournament11T1] versus:[myCupSingleton tournament11T2] p1Score:[myCupSingleton tournament11S1] p2Score:[myCupSingleton tournament11S2]];
			[self displayTournament:12 team:[myCupSingleton tournament12T1] versus:[myCupSingleton tournament12T2] p1Score:[myCupSingleton tournament12S1] p2Score:[myCupSingleton tournament12S2]];
			[self displayTournament:13 team:[myCupSingleton tournament13T1] versus:[myCupSingleton tournament13T2] p1Score:[myCupSingleton tournament13S1] p2Score:[myCupSingleton tournament13S2]];
			[self displayTournament:14 team:[myCupSingleton tournament14T1] versus:[myCupSingleton tournament14T2] p1Score:[myCupSingleton tournament14S1] p2Score:[myCupSingleton tournament14S2]];
			
			
			if(champ == [mySingleton spritePlayer1]){
				//you are the winner
				
			}
			
		
		}
		if(champ == [mySingleton spritePlayer1]){
			//you are the winner
			
		}

		
		[myCupSingleton saveGameData];
		
		
	}
	return self;
}

-(void)displayTournament:(int)match team:(int)t1 versus:(int)t2 p1Score:(int)p1Score p2Score:(int)p2Score{
	int px;
	int py;
	NSString* scoreString;
	
	switch (match) {
		case 1:
			px = 12;
			py = 210;
			break;
		case 2:
			px = 12;
			py = 153;
			break;
		case 3:
			px = 12;
			py = 97;
			break;	
		case 4:
			px = 12;
			py = 40;
			break;
		case 5:
			px = 426;
			py = 210;
			break;
		case 6:
			px = 426;
			py = 153;
			break;
		case 7:
			px = 426;
			py = 97;
			break;
		case 8:
			px = 426;
			py = 40;
			break;
		case 9:
			px = 79;
			py = 182;
			break;
		case 10:
			px = 79;
			py = 68;
			break;
		case 11:
			px = 358;
			py = 182;
			break;
		case 12:
			px = 358;
			py = 68;
			break;
		case 13:
			px = 146;
			py = 127;
			break;
		case 14:
			px = 292;
			py = 127;
			break;
		case 15:
			px = 220;
			py = 68;
			break;
		case 16:
			px = 220;
			py = 184;
			break;
		default:
			break;
	}
	
	infoHolderSprite = [CCSprite node];
    
	[self addChild:infoHolderSprite z:0 tag:0];
	
	teamName1Label = [CCLabelTTF labelWithString:[myDataHolder getTeamShortNameForKey:t1] fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[teamName1Label setColor:ccBLACK];
	[teamName1Label setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(7,35))];
	[infoHolderSprite addChild: teamName1Label];
	
	flag1Sprite = [CCSprite spriteWithSpriteFrameName:[myDataHolder getFlagNameForKey:t1]];
	[flag1Sprite setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(36,35))];
	[infoHolderSprite addChild: flag1Sprite];
	
	teamName2Label = [CCLabelTTF labelWithString:[myDataHolder getTeamShortNameForKey:t2] fontName:@"Arial" fontSize:HD_PIXELS(11)];
	[teamName2Label setColor:ccBLACK];
	[teamName2Label setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(35,4))];
	[infoHolderSprite addChild: teamName2Label];
	
	flag2Sprite = [CCSprite spriteWithSpriteFrameName:[myDataHolder getFlagNameForKey:t2]];
	[flag2Sprite setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(6,4))];
	[infoHolderSprite addChild: flag2Sprite];
	
	if(p1Score<100){
		scoreString = [NSString stringWithFormat:@"%d x %d",p1Score,p2Score];
		scoreBoardLabel = [CCLabelTTF labelWithString:scoreString fontName:@"Arial" fontSize:HD_PIXELS(12)];
		[scoreBoardLabel setColor:ccBLACK];
		[scoreBoardLabel setPosition: ADJUST_INTERNAL_SPRITE_CCP(ccp(20,22))];
		[infoHolderSprite addChild: scoreBoardLabel];
		
	}	
	[infoHolderSprite setPosition:ADJUST_CCP(ccp(px,py))];
	
	
}



-(void)goPlay{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	
	if(myTeamStayOnTournament==YES){
		if(champ==[mySingleton spritePlayer1]){
			TournamentWinnerScene * gs = [TournamentWinnerScene node];
			[[CCDirector sharedDirector] replaceScene:gs];
		}else if(second==[mySingleton spritePlayer1]){	
			TournamentSecondScene * gs = [TournamentSecondScene node];
			[[CCDirector sharedDirector] replaceScene:gs];

		}else if(third==[mySingleton spritePlayer1]){	
			TournamentThirdScene * gs = [TournamentThirdScene node];
			[[CCDirector sharedDirector] replaceScene:gs];

		}else{
			GameScene * gs = [GameScene node];
			[[CCDirector sharedDirector] replaceScene:gs];
		}
	}else{
		MatchLoserScene * mls = [MatchLoserScene node];
		[[CCDirector sharedDirector] replaceScene:mls];
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Touch Events ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	
	if(CGRectContainsPoint(CGRectMake(spriteGoPlay.position.x- 0.5*spriteGoPlay.contentSize.width, spriteGoPlay.position.y- 0.5*spriteGoPlay.contentSize.height, spriteGoPlay.contentSize.width, spriteGoPlay.contentSize.height), location)) {
		[self goPlay];
	}
	//return kEventHandled;	
}



@end