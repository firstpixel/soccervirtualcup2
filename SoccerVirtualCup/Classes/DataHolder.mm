//
//  DataHolder.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/12/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "DataHolder.h"
#import "Team.h"

@implementation DataHolder

static DataHolder *_sharedInstance;

@synthesize teamObjectArray;
@synthesize teamGroupsObject;
@synthesize groupsKeys;
@synthesize teamObject;


-(id)init{
	self = [super init];
	if (self != nil){
        [self clearData];
	}
	
	return self;
}

-(void)clearData{
	//TEAM
	//FIFA RANK
        //missing images
        //*DNK denmark
        //*EGY Egypt
        //*ISL - Iceland
        //*MAR -Marrocos
        //*PAN Panama
        //*PER Peru
        //*POL Poland
        //*PRT Portugal
        //*SAU Saudi Arabia
        //*SEN Senegal
        //*SRB Serbia
        //*SWE Sweden
        //TUN Tunisia
    NSLog(@"CLEAR DATA 1");
   // teamObjectArray = @[];
    teamObjectArray = [[NSArray alloc]  initWithObjects:
                       [[[Team alloc] init] setIndex:0 setFactor:4 setGroupKey:3 setShortName:@"ARG" setName:@"Argentina"                 setFlag:@"arg.png" setP1:@"arg_p1.png" setP2:@"arg_p2.png" setButton:@"arg_bt.png"],
                       [[[Team alloc] init] setIndex:1 setFactor:1 setGroupKey:2 setShortName:@"AUS" setName:@"Australia"                 setFlag:@"aus.png" setP1:@"aus_p1.png" setP2:@"aus_p2.png" setButton:@"aus_bt.png"],
                       [[[Team alloc] init] setIndex:2 setFactor:3 setGroupKey:6 setShortName:@"BEL" setName:@"Belgium"                   setFlag:@"bel.png" setP1:@"bel_p1.png" setP2:@"bel_p2.png" setButton:@"bel_bt.png"],
                       [[[Team alloc] init] setIndex:3 setFactor:3 setGroupKey:4 setShortName:@"BRA" setName:@"Brazil"                    setFlag:@"bra.png" setP1:@"bra_p1.png" setP2:@"bra_p2.png" setButton:@"bra_bt.png"],
                       [[[Team alloc] init] setIndex:4 setFactor:3 setGroupKey:7 setShortName:@"COL" setName:@"Colombia"                  setFlag:@"col.png" setP1:@"col_p1.png" setP2:@"col_p2.png" setButton:@"col_bt.png"],
                       [[[Team alloc] init] setIndex:5 setFactor:1 setGroupKey:4 setShortName:@"CRC" setName:@"Costa Rica"               setFlag:@"crc.png" setP1:@"crc_p1.png" setP2:@"crc_p2.png" setButton:@"crc_bt.png"],
                       [[[Team alloc] init] setIndex:6 setFactor:3 setGroupKey:3 setShortName:@"CRO" setName:@"Croatia"                  setFlag:@"cro.png" setP1:@"cro_p1.png" setP2:@"cro_p2.png" setButton:@"cro_bt.png"],
                       [[[Team alloc] init] setIndex:7 setFactor:3 setGroupKey:2 setShortName:@"DNK" setName:@"Denmark"                  setFlag:@"dnk.png" setP1:@"dnk_p1.png" setP2:@"dnk_p2.png" setButton:@"dnk_bt.png"],
                       [[[Team alloc] init] setIndex:8 setFactor:2 setGroupKey:0 setShortName:@"EGY" setName:@"Egypt"                  setFlag:@"egy.png" setP1:@"egy_p1.png" setP2:@"egy_p2.png" setButton:@"egy_bt.png"],
                       [[[Team alloc] init] setIndex:9 setFactor:3 setGroupKey:6 setShortName:@"ENG" setName:@"England"                  setFlag:@"eng.png" setP1:@"eng_p1.png" setP2:@"eng_p2.png" setButton:@"eng_bt.png"],
                       [[[Team alloc] init] setIndex:10 setFactor:3 setGroupKey:1 setShortName:@"ESP" setName:@"Spain"                    setFlag:@"esp.png" setP1:@"esp_p1.png" setP2:@"esp_p2.png" setButton:@"esp_bt.png"],
                       [[[Team alloc] init] setIndex:11 setFactor:3 setGroupKey:2 setShortName:@"FRA" setName:@"France"                   setFlag:@"fra.png" setP1:@"fra_p1.png" setP2:@"fra_p2.png" setButton:@"fra_bt.png"],
                       [[[Team alloc] init] setIndex:12 setFactor:4 setGroupKey:5 setShortName:@"GER" setName:@"Germany"                  setFlag:@"ger.png" setP1:@"ger_p1.png" setP2:@"ger_p2.png" setButton:@"ger_bt.png"],
                       [[[Team alloc] init] setIndex:13 setFactor:2 setGroupKey:3 setShortName:@"ISL" setName:@"Iceland"                     setFlag:@"isl.png" setP1:@"isl_p1.png" setP2:@"isl_p2.png" setButton:@"isl_bt.png"],
                       [[[Team alloc] init] setIndex:14 setFactor:1 setGroupKey:1 setShortName:@"IRN" setName:@"Iran"                     setFlag:@"irn.png" setP1:@"irn_p1.png" setP2:@"irn_p2.png" setButton:@"irn_bt.png"],
                       [[[Team alloc] init] setIndex:15 setFactor:2 setGroupKey:7 setShortName:@"JPN" setName:@"Japan"                    setFlag:@"jpn.png" setP1:@"jpn_p1.png" setP2:@"jpn_p2.png" setButton:@"jpn_bt.png"],
                       [[[Team alloc] init] setIndex:16 setFactor:1 setGroupKey:5 setShortName:@"KOR" setName:@"South Korea"                    setFlag:@"kor.png" setP1:@"kor_p1.png" setP2:@"kor_p2.png" setButton:@"kor_bt.png"],
                       [[[Team alloc] init] setIndex:17 setFactor:2 setGroupKey:5 setShortName:@"MEX" setName:@"Mexico"                   setFlag:@"mex.png" setP1:@"mex_p1.png" setP2:@"mex_p2.png" setButton:@"mex_bt.png"],
                       [[[Team alloc] init] setIndex:18 setFactor:1 setGroupKey:1 setShortName:@"MAR" setName:@"Marocco"                   setFlag:@"mar.png" setP1:@"mar_p1.png" setP2:@"mar_p2.png" setButton:@"mar_bt.png"],
                       [[[Team alloc] init] setIndex:19 setFactor:2 setGroupKey:3 setShortName:@"NGA" setName:@"Nigeria"                  setFlag:@"nga.png" setP1:@"nga_p1.png" setP2:@"nga_p2.png" setButton:@"nga_bt.png"],
                       [[[Team alloc] init] setIndex:20 setFactor:2 setGroupKey:6 setShortName:@"PAN" setName:@"Panama"                 setFlag:@"pan.png" setP1:@"pan_p1.png" setP2:@"pan_p2.png" setButton:@"pan_bt.png"],
                       [[[Team alloc] init] setIndex:21 setFactor:2 setGroupKey:2 setShortName:@"PER" setName:@"Peru"                 setFlag:@"per.png" setP1:@"per_p1.png" setP2:@"per_p2.png" setButton:@"per_bt.png"],
                       [[[Team alloc] init] setIndex:22 setFactor:2 setGroupKey:7 setShortName:@"POL" setName:@"Poland"                 setFlag:@"pol.png" setP1:@"pol_p1.png" setP2:@"pol_p2.png" setButton:@"pol_bt.png"],
                       [[[Team alloc] init] setIndex:23 setFactor:3 setGroupKey:1 setShortName:@"PRT" setName:@"Portugal"                 setFlag:@"prt.png" setP1:@"prt_p1.png" setP2:@"prt_p2.png" setButton:@"prt_bt.png"],
                       [[[Team alloc] init] setIndex:24 setFactor:3 setGroupKey:0 setShortName:@"RUS" setName:@"Russia"                   setFlag:@"rus.png" setP1:@"rus_p1.png" setP2:@"rus_p2.png" setButton:@"rus_bt.png"],
                       [[[Team alloc] init] setIndex:25 setFactor:2 setGroupKey:0 setShortName:@"SAU" setName:@"Saudi Arabia"                   setFlag:@"sau.png" setP1:@"sau_p1.png" setP2:@"sau_p2.png" setButton:@"sau_bt.png"],
                       [[[Team alloc] init] setIndex:26 setFactor:2 setGroupKey:7 setShortName:@"SEN" setName:@"Senegal"                   setFlag:@"sen.png" setP1:@"sen_p1.png" setP2:@"sen_p2.png" setButton:@"sen_bt.png"],
                       [[[Team alloc] init] setIndex:27 setFactor:2 setGroupKey:4 setShortName:@"SRB" setName:@"Serbia"                   setFlag:@"srb.png" setP1:@"srb_p1.png" setP2:@"srb_p2.png" setButton:@"srb_bt.png"],
                       [[[Team alloc] init] setIndex:28 setFactor:3 setGroupKey:5 setShortName:@"SWE" setName:@"Sweden"                    setFlag:@"swe.png" setP1:@"swe_p1.png" setP2:@"swe_p2.png" setButton:@"swe_bt.png"],
                       [[[Team alloc] init] setIndex:29 setFactor:3 setGroupKey:4 setShortName:@"CHE" setName:@"Switzerland"              setFlag:@"sui.png" setP1:@"sui_p1.png" setP2:@"sui_p2.png" setButton:@"sui_bt.png"],
                       [[[Team alloc] init] setIndex:30 setFactor:2 setGroupKey:6 setShortName:@"TUN" setName:@"Tunisia"              setFlag:@"tun.png" setP1:@"tun_p1.png" setP2:@"tun_p2.png" setButton:@"tun_bt.png"],
                       [[[Team alloc] init] setIndex:31 setFactor:3 setGroupKey:0 setShortName:@"URU" setName:@"Uruguay"                  setFlag:@"uru.png" setP1:@"uru_p1.png" setP2:@"uru_p2.png" setButton:@"uru_bt.png"],
                       
                       [[[Team alloc] init] setIndex:32 setFactor:2 setGroupKey:8 setShortName:@"ALG" setName:@"Algeria"                   setFlag:@"alg.png" setP1:@"alg_p1.png" setP2:@"alg_p2.png" setButton:@"alg_bt.png"],
                       [[[Team alloc] init] setIndex:33 setFactor:2 setGroupKey:8 setShortName:@"BIH" setName:@"Bosnia and Herzegovina"    setFlag:@"bih.png" setP1:@"bih_p1.png" setP2:@"bih_p2.png" setButton:@"bih_bt.png"],
                       [[[Team alloc] init] setIndex:34 setFactor:3 setGroupKey:8 setShortName:@"CHI" setName:@"Chile"                     setFlag:@"chi.png" setP1:@"chi_p1.png" setP2:@"chi_p2.png" setButton:@"chi_bt.png"],
                       [[[Team alloc] init] setIndex:35 setFactor:2 setGroupKey:8 setShortName:@"CIV" setName:@"Côte d'Ivoire"             setFlag:@"civ.png" setP1:@"civ_p1.png" setP2:@"civ_p2.png" setButton:@"civ_bt.png"],
                       [[[Team alloc] init] setIndex:36 setFactor:1 setGroupKey:9 setShortName:@"CMR" setName:@"Cameroon"                  setFlag:@"cmr.png" setP1:@"cmr_p1.png" setP2:@"cmr_p2.png" setButton:@"cmr_bt.png"],
                       [[[Team alloc] init] setIndex:37 setFactor:2 setGroupKey:9 setShortName:@"ECU" setName:@"Ecuador"                  setFlag:@"ecu.png" setP1:@"ecu_p1.png" setP2:@"ecu_p2.png" setButton:@"ecu_bt.png"],
                       [[[Team alloc] init] setIndex:38 setFactor:1 setGroupKey:9 setShortName:@"GHA" setName:@"Ghana"                    setFlag:@"gha.png" setP1:@"gha_p1.png" setP2:@"gha_p2.png" setButton:@"gha_bt.png"],
                       [[[Team alloc] init] setIndex:39 setFactor:3 setGroupKey:9 setShortName:@"GRE" setName:@"Greece"                   setFlag:@"gre.png" setP1:@"gre_p1.png" setP2:@"gre_p2.png" setButton:@"gre_bt.png"],
                       [[[Team alloc] init] setIndex:40 setFactor:1 setGroupKey:10 setShortName:@"HON" setName:@"Honduras"                 setFlag:@"hon.png" setP1:@"hon_p1.png" setP2:@"hon_p2.png" setButton:@"hon_bt.png"],
                       [[[Team alloc] init] setIndex:41 setFactor:3 setGroupKey:10 setShortName:@"ITA" setName:@"Italy"                    setFlag:@"ita.png" setP1:@"ita_p1.png" setP2:@"ita_p2.png" setButton:@"ita_bt.png"],
                       [[[Team alloc] init] setIndex:42 setFactor:3 setGroupKey:10 setShortName:@"NED" setName:@"Netherlands"              setFlag:@"ned.png" setP1:@"ned_p1.png" setP2:@"ned_p2.png" setButton:@"ned_bt.png"],
                       [[[Team alloc] init] setIndex:43 setFactor:3 setGroupKey:10 setShortName:@"USA" setName:@"USA"                      setFlag:@"usa.png" setP1:@"usa_p1.png" setP2:@"usa_p2.png" setButton:@"usa_bt.png"],
                       nil
                       ];
    
    
    /*
     [[[Team alloc] init] setIndex:0 setFactor:2 setGroupKey:7 setShortName:@"ALG" setName:@"Algeria"                   setFlag:@"alg.png" setP1:@"alg_p1.png" setP2:@"alg_p2.png" setButton:@"alg_bt.png"],
     [[[Team alloc] init] setIndex:4 setFactor:2 setGroupKey:5 setShortName:@"BIH" setName:@"Bosnia and Herzegovina"    setFlag:@"bih.png" setP1:@"bih_p1.png" setP2:@"bih_p2.png" setButton:@"bih_bt.png"],
     [[[Team alloc] init] setIndex:6 setFactor:3 setGroupKey:1 setShortName:@"CHI" setName:@"Chile"                     setFlag:@"chi.png" setP1:@"chi_p1.png" setP2:@"chi_p2.png" setButton:@"chi_bt.png"],
     [[[Team alloc] init] setIndex:7 setFactor:2 setGroupKey:2 setShortName:@"CIV" setName:@"Côte d'Ivoire"             setFlag:@"civ.png" setP1:@"civ_p1.png" setP2:@"civ_p2.png" setButton:@"civ_bt.png"],
     [[[Team alloc] init] setIndex:8 setFactor:1 setGroupKey:0 setShortName:@"CMR" setName:@"Cameroon"                  setFlag:@"cmr.png" setP1:@"cmr_p1.png" setP2:@"cmr_p2.png" setButton:@"cmr_bt.png"],
     [[[Team alloc] init] setIndex:12 setFactor:2 setGroupKey:4 setShortName:@"ECU" setName:@"Ecuador"                  setFlag:@"ecu.png" setP1:@"ecu_p1.png" setP2:@"ecu_p2.png" setButton:@"ecu_bt.png"],
     [[[Team alloc] init] setIndex:17 setFactor:1 setGroupKey:6 setShortName:@"GHA" setName:@"Ghana"                    setFlag:@"gha.png" setP1:@"gha_p1.png" setP2:@"gha_p2.png" setButton:@"gha_bt.png"],
     [[[Team alloc] init] setIndex:18 setFactor:3 setGroupKey:2 setShortName:@"GRE" setName:@"Greece"                   setFlag:@"gre.png" setP1:@"gre_p1.png" setP2:@"gre_p2.png" setButton:@"gre_bt.png"],
     [[[Team alloc] init] setIndex:19 setFactor:1 setGroupKey:4 setShortName:@"HON" setName:@"Honduras"                 setFlag:@"hon.png" setP1:@"hon_p1.png" setP2:@"hon_p2.png" setButton:@"hon_bt.png"],
     [[[Team alloc] init] setIndex:21 setFactor:3 setGroupKey:3 setShortName:@"ITA" setName:@"Italy"                    setFlag:@"ita.png" setP1:@"ita_p1.png" setP2:@"ita_p2.png" setButton:@"ita_bt.png"],
     [[[Team alloc] init] setIndex:25 setFactor:3 setGroupKey:1 setShortName:@"NED" setName:@"Netherlands"              setFlag:@"ned.png" setP1:@"ned_p1.png" setP2:@"ned_p2.png" setButton:@"ned_bt.png"],
     [[[Team alloc] init] setIndex:31 setFactor:3 setGroupKey:6 setShortName:@"USA" setName:@"USA"                      setFlag:@"usa.png" setP1:@"usa_p1.png" setP2:@"usa_p2.png" setButton:@"usa_bt.png"],
     */
    
    

    
    groupsKeys				= [NSArray arrayWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];
	teamGroupsObject		= [NSArray arrayWithObjects:@"Group A", @"Group B", @"Group C",@"Group D", @"Group E", @"Group F",@"Group G", @"Group H", nil];
	
   
}

-(NSString*)getImageP1ForKey:(int)key{
	return (NSString*)[(Team*)[teamObjectArray objectAtIndex:key] init].teamPlayer1;
}

-(NSString*)getImageP2ForKey:(int)key{
    return (NSString*)[(Team*)[teamObjectArray objectAtIndex:key] init].teamPlayer2;
}

-(NSString*)getImageBtForKey:(int)key{
    return (NSString*)[(Team*)[teamObjectArray objectAtIndex:key] init].teamButton;
}

-(NSString*)getTeamShortNameForKey:(int)key{
    NSLog(@"key %i", key);
    return (NSString*)[(Team*)[teamObjectArray objectAtIndex:key] init].teamShortName;
}

-(NSString*)getTeamNameForKey:(int)key{
    return (NSString*)[(Team*)[teamObjectArray objectAtIndex:key] init].teamName;
}

-(int)getFactorByTeamKey:(int)key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSInteger factorSaved = [prefs integerForKey:[NSString stringWithFormat:@"svc.teamFactor%i",key]];
    NSLog(@"VALUE OF FACTOR SAVED: %li",(long)factorSaved);
    if(factorSaved!=0){
        
        return 4;
    }else{
        return (int)[(Team*)[teamObjectArray objectAtIndex:key] init].factor;
    }
}

-(NSString*)getFlagNameForKey:(int)key{
    return (NSString*)[(Team*)[teamObjectArray objectAtIndex:key] init].teamFlag;
}

-(NSArray*)setMatchArray:(NSMutableArray*)array setObject:(NSNumber*)state atIndex:(NSUInteger)index{
	[array replaceObjectAtIndex:(NSUInteger)index withObject:state];
	return array;
}

-(int)getMatchArray:(NSMutableArray*)array atIndex:(NSUInteger)index{
	NSNumber*a = [array objectAtIndex:index];
	return [a intValue];
}

-(int)getPointsArrayAtIndex:(NSUInteger)index{
	return 1;
}

-(NSArray*)getIdForGroupKey:(NSNumber*)key{
	NSMutableArray* r = [NSMutableArray arrayWithCapacity:4];
	int n;
    for (int gKey = 0; gKey < [teamObjectArray count]; gKey++){
        n = [(Team*)[teamObjectArray objectAtIndex:gKey] init].groupKey;
		if(n == [key intValue]){
			[r addObject:[NSNumber numberWithInt:gKey]];
		}
	}
	return (NSArray*)r;
}

-(int)getGroupForTeamKey:(int)key{
	int n;
    for (int gKey = 0; gKey < [teamObjectArray count]; gKey++){
        n = [(Team*)[teamObjectArray objectAtIndex:gKey] init].groupKey;
		if(gKey==key){
			return (int)n;
		}
	}
	return -1;
}

+ (DataHolder *) sharedInstance
{
	if (!_sharedInstance)
	{
		_sharedInstance = [[DataHolder alloc] init];
	}
	return _sharedInstance;
}

- (id)retain
{
    return self;
}

//- (void)release
//{
//    //do nothing
//}

- (id)autorelease
{
    return self;
}
@end
