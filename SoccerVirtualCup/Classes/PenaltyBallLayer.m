//
//  PenaltyBallLayer.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import "PenaltyBallLayer.h"
#import "GameCredit.h"
#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "AppDelegate.h"
#import "AfterGameMatchsScene.h"
#import "TournamentScene.h"
#import "DataHolder.h"
#import "MatchDrawScene.h"
#import "MatchWinnerScene.h"
#import "MatchLoserScene.h"
#import "SimpleAudioEngine.h"
#import "DeviceSettings.h"
PenaltyBallLayer * penaltyBallLayer;


#define kBannerAdjustY 242

//main cocos2d rendering loop, this has to be streamlined
// e.g dont add logs here :/
static void eachShape(void *ptr, void* unused){
	//	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	
	cpShape *shape = (cpShape*) ptr;
	CCSprite *sprite = shape->data;
	if(sprite.tag== 1 || sprite.tag == 2 || sprite.tag == 3 ){
		cpBody *body = shape->body;
		[sprite setPosition: ADJUST_CCP(ccp( body->p.x, body->p.y))];
		[sprite setRotation: (float) CC_RADIANS_TO_DEGREES( -body->a )];
	}
}


static int ballCollisionNew(cpArbiter *arb,cpSpace *space, void *data)
{
	cpShape *a, *b; 
	cpArbiterGetShapes(arb, &a, &b);

	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	CCSprite* sprite1 = a->data;
	CCSprite* sprite2 = b->data;
	PenaltyBallLayer *penaltyBallLayer = (PenaltyBallLayer *)data;
	if(sprite1.tag==3 ){
		if((sprite1.tag==3 && sprite2.tag==101) || (sprite1.tag==3 && sprite2.tag==102) || (sprite1.tag==3 && sprite2.tag==103) || (sprite1.tag==3 && sprite2.tag==104)){
			if( [mySingleton  golAnimationPlaying]==NO && [mySingleton gameTurn]!=0 ){
				if((sprite1.tag==3 && sprite2.tag==103) && [mySingleton penaltyAttack]%2==0)[penaltyBallLayer golScored:2];
				if((sprite1.tag==3 && sprite2.tag==103) && [mySingleton penaltyAttack]%2!=0)[penaltyBallLayer golScored:1];
			}
			return 0;
		}else{
			[penaltyBallLayer ballAudio];
			return 1;
		}
	}else{
		return 1;
	}
}


@implementation PenaltyBallLayer

@synthesize elasticIterations;
@synthesize ballMass;
@synthesize playerMass;
@synthesize kSlide;

@synthesize hasSent;
@synthesize hasSentPosition;

@synthesize scored1;
@synthesize scored2;
@synthesize scored3;
@synthesize scored4;
@synthesize scored5;
@synthesize scored6;
@synthesize scored7;
@synthesize scored8;
@synthesize scored9;
@synthesize scored10;

@synthesize ballFrameNumber;
@synthesize diffPosy;
@synthesize diffPosx;
@synthesize oldpy,oldpx;


//
// Called by NSTimer fire, hides the game sprite
//
- (void)removeCCSpriteItsYourTurn:(ccTime *)timer {
	[self removeChild:itsYourTurn cleanup:YES];	
	itsYourTurn=nil;
}

- (void)removeCCSpriteGetReady:(ccTime *)timer {
	[self removeChild:getReady cleanup:YES];	
}
- (void)removeCCSpriteWaitPlaying:(ccTime *)timer {
	[self removeChild:waitPlaying cleanup:YES];	
}
//****************
// AUDIO
//****************
-(void)ballAudio{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		if([mySingleton ballAudio]==NO){
			[mySingleton setBallAudio:YES];
			int r = arc4random() % 2;
			if(r==1){
				[[SimpleAudioEngine sharedEngine] playEffect:@"kick1.mp3"];
			}else{
				[[SimpleAudioEngine sharedEngine] playEffect:@"kick.mp3"];
			}
            [self schedule:@selector(ballAudioReset:) interval:0.0 repeat:0 delay:0.5];
			//[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(ballAudioReset:) userInfo:nil repeats:NO];
		}
	}
}
-(void)gameCrowdAudio{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	
	if([mySingleton audioOn]==YES){
		
		int r = arc4random() % 2;
		if(r==1){
			[[SimpleAudioEngine sharedEngine] playEffect:@"ambience.mp3" pitch:1 pan:0.5 gain:0.15];
		}else{
			[[SimpleAudioEngine sharedEngine] playEffect:@"ambience2.mp3" pitch:1 pan:0.5 gain:0.15];
		}
	}
}
-(void)crowdGoalAudio{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		[[SimpleAudioEngine sharedEngine] playEffect:@"crowd.mp3" pitch:1 pan:0.5 gain:0.3];
	}
}

-(void) ballAudioReset:(ccTime *)timer{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	[mySingleton setBallAudio:NO];
	
}
-(void)whistleAudio{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		[[SimpleAudioEngine sharedEngine] playEffect:@"whistle.mp3"];
	}
}


-(void)golScored:(int)team{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	[mySingleton setGolAnimationPlaying:YES];
	[mySingleton setMaxExecutionTime:0];
	
	if(team==1){
		if([mySingleton penaltyAttack] == 1 || [mySingleton penaltyAttack] == 11 || [mySingleton penaltyAttack] == 21 || [mySingleton penaltyAttack] == 31)scored1 = YES;
		if([mySingleton penaltyAttack] == 3 || [mySingleton penaltyAttack] == 13 || [mySingleton penaltyAttack] == 23 || [mySingleton penaltyAttack] == 33)scored3 = YES;
		if([mySingleton penaltyAttack] == 5 || [mySingleton penaltyAttack] == 15 || [mySingleton penaltyAttack] == 25 || [mySingleton penaltyAttack] == 35)scored5 = YES;
		if([mySingleton penaltyAttack] == 7 || [mySingleton penaltyAttack] == 17 || [mySingleton penaltyAttack] == 27 || [mySingleton penaltyAttack] == 37)scored7 = YES;
		if([mySingleton penaltyAttack] == 9 || [mySingleton penaltyAttack] == 19 || [mySingleton penaltyAttack] == 29 || [mySingleton penaltyAttack] == 39)scored9 = YES;
		[mySingleton setCurrentScoreP1:[mySingleton currentScoreP1]+1];
		[myCupSingleton setCurrentScoreP1:[mySingleton currentScoreP1]];
		[p1Score setString:[NSString stringWithFormat:@"%d", [mySingleton currentScoreP1]]];
	}else{
		if([mySingleton penaltyAttack] == 2 || [mySingleton penaltyAttack] == 12 || [mySingleton penaltyAttack] == 22 || [mySingleton penaltyAttack] == 32)scored2 = YES;
		if([mySingleton penaltyAttack] == 4 || [mySingleton penaltyAttack] == 14 || [mySingleton penaltyAttack] == 24 || [mySingleton penaltyAttack] == 34)scored4 = YES;
		if([mySingleton penaltyAttack] == 6 || [mySingleton penaltyAttack] == 16 || [mySingleton penaltyAttack] == 26 || [mySingleton penaltyAttack] == 36)scored6 = YES;
		if([mySingleton penaltyAttack] == 8 || [mySingleton penaltyAttack] == 18 || [mySingleton penaltyAttack] == 28 || [mySingleton penaltyAttack] == 38)scored8 = YES;
		if([mySingleton penaltyAttack] == 10 || [mySingleton penaltyAttack] == 20 || [mySingleton penaltyAttack] == 30 || [mySingleton penaltyAttack] == 40)scored10 = YES;		
		[mySingleton setCurrentScoreP2:[mySingleton currentScoreP2]+1];
		[myCupSingleton setCurrentScoreP2:[mySingleton currentScoreP2]];
		[p2Score setString:[NSString stringWithFormat:@"%d", [mySingleton currentScoreP2]]];
	}
	[self whistleAudio];
	[self crowdGoalAudio];
	
	//NSLog(@"pos 3: %d",[mySingleton getFieldValueAtPos:3]);
	//NSLog(@"pos 2: %d",[mySingleton getFieldValueAtPos:2]);
	
	/// GOL ANIMATION
    
    CCSprite * golSprite = nil;
    CCAnimation* golAnim = nil;
    NSMutableArray *goalFrames = [NSMutableArray array];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        for(int j=1; j<=3; ++j){
            for(int i = 1; i <= 7; ++i) {
                
                CCSprite* mySprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"goal_anima%d-hd.png", i]];
                [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
            }
        }
        CCSprite* mySprite = [CCSprite spriteWithFile:@"goal_anima8-hd.png"];
        [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
        golAnim = [CCAnimation animationWithSpriteFrames:goalFrames delay:1/8.0];
        
        golSprite = [CCSprite spriteWithFile:@"goal_anima8-hd.png"];
    }else{
        for(int j=1; j<=3; ++j){
            for(int i = 1; i <= 7; ++i) {
                CCSprite* mySprite = [CCSprite spriteWithFile:[NSString stringWithFormat:@"goal_anima%d.png", i]];
                [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
            }
        }
        CCSprite* mySprite = [CCSprite spriteWithFile:@"goal_anima8.png"];
        [goalFrames addObject:[CCSpriteFrame frameWithTexture:[mySprite texture] rect:[mySprite textureRect]]];
        golAnim = [CCAnimation animationWithSpriteFrames:goalFrames delay:1/8.0];
        
        golSprite = [CCSprite spriteWithFile:@"goal_anima8.png"];
    }
    
    
	// Make the animation sequence
    CCAction* golAction = [CCAnimate actionWithAnimation:golAnim restoreOriginalFrame:NO];

    
	
	[golSprite setPosition:ADJUST_CCP(ccp(240, 160))];
	[self addChild:golSprite z:100 tag:100];
	
	// Make a sprite (defined elsewhere) actually perform the animation
	[golSprite runAction:golAction];
	
	
	[self unschedule: @selector(startPlayerTurnPenalty:)];
	[mySingleton setTimeToDelayAction:4];
	[self schedule: @selector(startDelayRestartPenalty:) interval: 1];
}

-(float)pointDistance:(cpVect)p1 point2:(cpVect)p2{
	float b = (p1.y-p2.y);
	float c = (p1.x-p2.x);
	int a = sqrt(b*b+c*c);
	return a;
}
-(cpVect)pointDistanceVector:(cpVect)p1 point2:(cpVect)p2 forceFactor:(int)forceFactor{
	float b = (p1.y-p2.y);
	float c = (p1.x-p2.x);
	return cpv(c*forceFactor*kSlide,b*forceFactor*kSlide);
}

-(void)aiPlayerAttack{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	float kPowFactor = 30 * kSlide;
	int r = arc4random() % 3;	
	if(r==0){
		[mySingleton setP2ForceVector:CGPointMake(-300*kPowFactor,-34*kPowFactor)];
	}else if(r==1){
		[mySingleton setP2ForceVector:CGPointMake(-300*kPowFactor,34*kPowFactor)];
	}else{
		[mySingleton setP2ForceVector:CGPointMake(-300*kPowFactor,3*kPowFactor)];
	}
	
}
-(void)aiPlayerDefender{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	float kPowFactor = 30 * kSlide;
	int r = arc4random() % 3;
	if(r==0){
		[mySingleton setP2ForceVector:CGPointMake(0,-20*kPowFactor)];
	}else if(r==1){
		[mySingleton setP2ForceVector:CGPointMake(0,20*kPowFactor)];
	}else{
		[mySingleton setP2ForceVector:CGPointMake(0,2)];
	}
	
}





-(void)clearBodyImpulseAndVectors{
	int steps = 6;
	cpFloat dt = 1.0/60.0/(cpFloat)steps;
	
	for(int i=0; i<steps; i++){
		cpBodyUpdateVelocity(penalty1Body,space->gravity, space->damping, 0);
		cpBodyUpdateVelocity(penalty2Body,space->gravity, space->damping, 0);
		cpBodyUpdateVelocity(ballPenaltyBody,space->gravity, space->damping, 0);		
		cpSpaceStep(space, dt);
	}
}

////////////////////////
//
//    MAIN LOOP
//
////////////////////////
-(void)tickPenalty:(ccTime)dt{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	
	//reset match
	if([mySingleton  gameTurn]==0){
		//NSLog(@"GAME TURN :%i  SYNCING FORCEVECTORS!!",[mySingleton  gameTurn]);
		[self clearBodyImpulseAndVectors];
		[self whistleAudio];
		if([mySingleton penaltyAttack]%2==0){
			[mySingleton setP1Position:cpv(300, 153)];
			[mySingleton setP2Position:cpv(90, 153)];
			penalty1Body->p = cpv(300, 153);
			penalty2Body->p = cpv(90, 153);
			ballPenaltyBody->p = cpv(240, 153);
		}else{
			[mySingleton setP1Position:cpv(90, 153)];
			[mySingleton setP2Position:cpv(300, 153)];
			penalty1Body->p = cpv(90, 153);
			penalty2Body->p = cpv(300, 153);
			ballPenaltyBody->p = cpv(240, 153);
		}
		cpSpaceEachShape(space, (cpSpaceShapeIteratorFunc)eachShape, nil);
		[self clearBodyImpulseAndVectors];
		[mySingleton setBallPosition:cpv(240, 153)];
		[mySingleton setP1ForceVector:cpv(0, 0)];
		[mySingleton setP2ForceVector:cpv(0, 0)];
		
		
		if([mySingleton penaltyAttack] == 11 || [mySingleton penaltyAttack] == 21 || [mySingleton penaltyAttack] == 31){
			[self removeChild:scored1CCSprite cleanup:YES];	
			scored1CCSprite=nil;	
			[self removeChild:scored2CCSprite cleanup:YES];	
			scored2CCSprite=nil;	
			[self removeChild:scored3CCSprite cleanup:YES];	
			scored3CCSprite=nil;	
			[self removeChild:scored4CCSprite cleanup:YES];	
			scored4CCSprite=nil;	
			[self removeChild:scored5CCSprite cleanup:YES];	
			scored5CCSprite=nil;	
			[self removeChild:scored6CCSprite cleanup:YES];	
			scored6CCSprite=nil;	
			[self removeChild:scored7CCSprite cleanup:YES];	
			scored7CCSprite=nil;	
			[self removeChild:scored8CCSprite cleanup:YES];	
			scored8CCSprite=nil;	
			[self removeChild:scored9CCSprite cleanup:YES];	
			scored9CCSprite=nil;	
			[self removeChild:scored10CCSprite cleanup:YES];	
			scored10CCSprite=nil;	
		}
		
		if([mySingleton penaltyAttack] == 1 || [mySingleton penaltyAttack] == 11  || [mySingleton penaltyAttack] == 21 || [mySingleton penaltyAttack] == 31){
			if(scored1 == YES){
				scored1CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored1 = NO;
			}else{
				scored1CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored1CCSprite setPosition:ADJUST_CCP(ccp(77, 14+kBannerAdjustY))];
			[self addChild:scored1CCSprite z:801 tag:801];
		}
		if([mySingleton penaltyAttack] == 2 || [mySingleton penaltyAttack] == 12  || [mySingleton penaltyAttack] == 22 || [mySingleton penaltyAttack] == 32){
			if(scored2 == YES){
				scored2CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored2 = NO;
			}else{
				scored2CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored2CCSprite setPosition:ADJUST_CCP(ccp(303, 14+kBannerAdjustY))];
			[self addChild:scored2CCSprite z:802 tag:802];
		}
		if([mySingleton penaltyAttack] == 3 || [mySingleton penaltyAttack] == 13  || [mySingleton penaltyAttack] == 23 || [mySingleton penaltyAttack] == 33){
			if(scored3 == YES){
				scored3CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored3 = NO;
			}else{
				scored3CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored3CCSprite setPosition:ADJUST_CCP(ccp(102, 14+kBannerAdjustY))];
			[self addChild:scored3CCSprite z:803 tag:803];
		}
		if([mySingleton penaltyAttack] == 4 || [mySingleton penaltyAttack] == 14  || [mySingleton penaltyAttack] == 24 || [mySingleton penaltyAttack] == 34){
			if(scored4 == YES){
				scored4CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored4 = NO;
			}else{
				scored4CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored4CCSprite setPosition:ADJUST_CCP(ccp(328, 14+kBannerAdjustY))];
			[self addChild:scored4CCSprite z:804 tag:804];
		}
		if([mySingleton penaltyAttack] == 5 || [mySingleton penaltyAttack] == 15  || [mySingleton penaltyAttack] == 25 || [mySingleton penaltyAttack] == 35){
			if(scored5 == YES){
				scored5CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored5 = NO;
			}else{
				scored5CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored5CCSprite setPosition:ADJUST_CCP(ccp(127, 14+kBannerAdjustY))];
			[self addChild:scored5CCSprite z:805 tag:805];
		}
		if([mySingleton penaltyAttack] == 6 || [mySingleton penaltyAttack] == 16  || [mySingleton penaltyAttack] == 26 || [mySingleton penaltyAttack] == 36){
			if(scored6 == YES){
				scored6CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored6 = NO;
			}else{
				scored6CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored6CCSprite setPosition:ADJUST_CCP(ccp(353, 14+kBannerAdjustY))];
			[self addChild:scored6CCSprite z:806 tag:806];
		}
		if([mySingleton penaltyAttack] == 7 || [mySingleton penaltyAttack] == 17 || [mySingleton penaltyAttack] == 27 || [mySingleton penaltyAttack] == 37){
			if(scored7 == YES){
				scored7CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored7 = NO;
			}else{
				scored7CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored7CCSprite setPosition:ADJUST_CCP(ccp(152, 14+kBannerAdjustY))];
			[self addChild:scored7CCSprite z:807 tag:807];
		}
		if([mySingleton penaltyAttack] == 8 || [mySingleton penaltyAttack] == 18 || [mySingleton penaltyAttack] == 28 || [mySingleton penaltyAttack] == 38){
			if(scored8 == YES){
				scored8CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored8 = NO;
			}else{
				scored8CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored8CCSprite setPosition:ADJUST_CCP(ccp(378, 14+kBannerAdjustY))];
			[self addChild:scored8CCSprite z:808 tag:808];
		}
		if([mySingleton penaltyAttack] == 9 || [mySingleton penaltyAttack] == 19 || [mySingleton penaltyAttack] == 29 || [mySingleton penaltyAttack] == 39){
			if(scored9 == YES){
				scored9CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored9 = NO;
			}else{
				scored9CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored9CCSprite setPosition:ADJUST_CCP(ccp(177, 14+kBannerAdjustY))];
			[self addChild:scored9CCSprite z:809 tag:809];
		}
		if([mySingleton penaltyAttack] == 10 || [mySingleton penaltyAttack] == 20 || [mySingleton penaltyAttack] == 30 || [mySingleton penaltyAttack] == 40){
			if(scored10 == YES){
				scored10CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_scored.png"];
				scored10 = NO;
			}else{
				scored10CCSprite = [CCSprite spriteWithSpriteFrameName:@"penalty_lost.png"];
			}
			[scored10CCSprite setPosition:ADJUST_CCP(ccp(403, 14+kBannerAdjustY))];
			[self addChild:scored10CCSprite z:809 tag:809];
		}
		
		
		
		[mySingleton setPenaltyAttack:[mySingleton penaltyAttack]+1];
		//WIN
		if(([mySingleton currentScoreP1]-[mySingleton currentScoreP2]==3) || 
		   ([mySingleton currentScoreP1]-[mySingleton currentScoreP2]==2 && [mySingleton penaltyAttack]==9) || 
		   ([mySingleton currentScoreP1]-[mySingleton currentScoreP2]==1 && [mySingleton penaltyAttack]==11) || 
		   ([mySingleton currentScoreP1]-[mySingleton currentScoreP2]==1 && [mySingleton penaltyAttack]>11 && [mySingleton penaltyAttack]%2!=0)){
			
			MatchWinnerScene  * mw = [MatchWinnerScene node];
			[[CCDirector sharedDirector] replaceScene:mw];
			
		
		//LOSE
		}else if(([mySingleton currentScoreP2]-[mySingleton currentScoreP1]==3) || 
				 ([mySingleton currentScoreP2]-[mySingleton currentScoreP1]==2 && [mySingleton penaltyAttack]==9) || 
				 ([mySingleton currentScoreP2]-[mySingleton currentScoreP1]==1 && [mySingleton penaltyAttack]==11) || 
				 ([mySingleton currentScoreP2]-[mySingleton currentScoreP1]==1 && [mySingleton penaltyAttack]>11 && [mySingleton penaltyAttack]%2!=0)){
		
			MatchLoserScene  * ml = [MatchLoserScene node];
			[[CCDirector sharedDirector] replaceScene:ml];
		}
		[mySingleton  setGameTurn:1];
		cpSpaceStep(space, 1.0f/60.0f);
		cpSpaceEachShape(space, (cpSpaceShapeIteratorFunc)eachShape, nil);
		
		//start player arrows
	}else if([mySingleton  gameTurn]==1){
		//NSLog(@"GAME TURN :%i  SYNCING FORCEVECTORS!!",[mySingleton  gameTurn]);
		if(itsYourTurn==nil){
			itsYourTurn = [CCSprite spriteWithSpriteFrameName:@"its_your_turn.png"];
			[itsYourTurn setPosition:ADJUST_CCP(ccp(240, 70))];
			[self addChild:itsYourTurn z:310 tag:310];
            [self schedule:@selector(removeCCSpriteItsYourTurn:) interval:0.0 repeat:0 delay:5.0];
            //[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(removeCCSpriteItsYourTurn:) userInfo:nil repeats:NO];
		}
		
		
		[mySingleton setTimePlayTurn:6];
		[self unschedule:@selector(tickPenalty:)];
		[self schedule: @selector(startPlayerTurnPenalty:) interval:1];
		
		
		
		
		
		// set forces to apply and hide arrows
	}else if([mySingleton  gameTurn]==2){
		if([mySingleton penaltyAttack]%2==0){
			[self aiPlayerAttack];
		}else{
			[self aiPlayerDefender];
		}
		
		[self clearBodyImpulseAndVectors];
		
		penalty1Body->p = [mySingleton p1Position];
		penalty2Body->p = [mySingleton p2Position];
		ballPenaltyBody->p = [mySingleton ballPosition];
		cpSpaceStep(space, 1.0f/60.0f);
		cpSpaceEachShape(space, (cpSpaceShapeIteratorFunc)eachShape, nil);
		
		
		waitPlaying = [CCSprite spriteWithSpriteFrameName:@"wait_playing.png"];
		[waitPlaying setPosition:ADJUST_CCP(ccp(240, 70))];
		[self addChild:waitPlaying z:310 tag:310];
        [self schedule:@selector(removeCCSpriteWaitPlaying:) interval:0.0 repeat:0 delay:4.7];
        //[NSTimer scheduledTimerWithTimeInterval:4.7 target:self selector:@selector(removeCCSpriteWaitPlaying:) userInfo:nil repeats:NO];
		
		
		if([mySingleton p1ForceVector].x==0 && [mySingleton p1ForceVector].y==0 ){
		}else{
			cpBodyApplyImpulse(penalty1Body,[mySingleton p1ForceVector],cpv(0,0) );
		}
		if([mySingleton p2ForceVector].x==0 && [mySingleton p2ForceVector].y==0 ){
		}else{
			cpBodyApplyImpulse(penalty2Body,[mySingleton p2ForceVector],cpv(0,0) );
		}
		[mySingleton  setGameTurn:3];
		
		// start turn execution
	}else if([mySingleton  gameTurn]==3){
		//NSLog(@"GAME TURN :%i  SYNCING FORCEVECTORS!!",[mySingleton  gameTurn]);
		
		// get ready
		if([mySingleton maxExecutionTime]==180){
			getReady = [CCSprite spriteWithSpriteFrameName:@"get_ready.png"];
			[getReady setPosition:ADJUST_CCP(ccp(240, 70))];
			[self addChild:getReady z:310 tag:310];
            [self schedule:@selector(removeCCSpriteGetReady:) interval:0.0 repeat:0 delay:1];
            //[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(removeCCSpriteGetReady:) userInfo:nil repeats:NO];
		}
		
		if((ballPenaltyCCSprite.position.x == [mySingleton ballPosition].x && ballPenaltyCCSprite.position.y == [mySingleton ballPosition].y &&
			penalty1CCSprite.position.x == [mySingleton p1Position].x && penalty1CCSprite.position.y == [mySingleton p1Position].y &&
			penalty2CCSprite.position.x == [mySingleton p2Position].x && penalty2CCSprite.position.y == [mySingleton p2Position].y &&
			[mySingleton golAnimationPlaying]==NO) || ( [mySingleton maxExecutionTime]>=240 && [mySingleton golAnimationPlaying]==NO)){
			
			[mySingleton setCheckSleep:[mySingleton checkSleep]+1];
			
			if([mySingleton checkSleep]==4){
				//NSLog(@"TIME END 220 -  GO TO NEXT %b",[mySingleton golAnimationPlaying]);
				[mySingleton setMaxExecutionTime:0];
				[mySingleton setCheckSleep:0];
				[mySingleton setP1Position:penalty1Body->p];
				[mySingleton setP2Position:penalty2Body->p];
				[mySingleton setBallPosition:ballPenaltyBody->p];
				[mySingleton setP1ForceVector:cpv(0, 0)];
				[mySingleton setP2ForceVector:cpv(0, 0)];
				[mySingleton  setGameTurn:0];
			}
			
		}
		[mySingleton setMaxExecutionTime:[mySingleton maxExecutionTime]+1];
		cpSpaceStep(space, 1.0f/60.0f);
		cpSpaceEachShape(space, (cpSpaceShapeIteratorFunc)eachShape, nil);
		
	}else if([mySingleton  gameTurn]==4){	
		//NSLog(@"GAME TURN :%i  SYNCING FORCEVECTORS!!",[mySingleton  gameTurn]);
		cpSpaceStep(space, 1.0f/60.0f);
		cpSpaceEachShape(space, (cpSpaceShapeIteratorFunc)eachShape, nil);
		
	}else{
	//	NSLog(@"GAME TURN :%i  SYNCING FORCEVECTORS!!",[mySingleton  gameTurn]);
		//cpBody *body = shape->body;
		//body->p = cpv(sprite.position.x, sprite.position.y);
		//cpBodySetAngle(body, -CC_DEGREES_TO_RADIANS(0));
	}
	 [self ball3Danimation];
}

-(void)ball3Danimation{
    //BALL ROLLING
    float b = (ballPenaltyBody->v.y);
    float c = (ballPenaltyBody->v.x);
    int degreeAngle = atan2(b,c) * 180 / 3.14565656;
    //float degreesDiff =  ( degreeAngle+90 - ballSprite.rotation )*0.1+ballSprite.rotation;
    [ballPenaltyCCSprite setRotation:-degreeAngle+90];
    
    //TODO implement 1 decimal to the animation
    // NSString* fnOldpy = [NSString stringWithFormat:@"%.1f", oldpy];
    //NSString* fnpy = [NSString stringWithFormat:@"%.1f", ballSprite.position.y];
    
    diffPosx = ((ballPenaltyCCSprite.position.x - oldpx) * 100 ) + diffPosx;
    diffPosy = ((ballPenaltyCCSprite.position.y - oldpy) * 100 ) + diffPosy;
    float diffMax = 60.0f;
    // NSLog(@"diff pos: %f", diffPosy);
    
    float diff = sqrtf(powf(diffPosx,2.0f)+powf(diffPosy,2.0f));
    
    //NSLog(@"FLOAT DIFF %f",diff);
    
    if( (diff > diffMax || diff < -diffMax) ){
        if (diff > fabs(diffMax)) {
            // NSLog(@"DIFFERENCA: %i",(1*int(diff)));
            ballFrameNumber = (int) ballFrameNumber+(1*(int)diff/60) < 59 ? (int)(ballFrameNumber+(1*(int)diff/60)):(int)(1+(1*(int)diff/60));
        }else{
            ballFrameNumber = ballFrameNumber+1<59?ballFrameNumber+1:0;
        }
        ballFrameNumber = ballFrameNumber<=59?ballFrameNumber:0;
        [ballPenaltyCCSprite setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"ball_%04d.png", ballFrameNumber]]];
        diffPosy = 0.0f;
        diffPosx = 0.0f;
    }
    //NSLog(@"a CC_RADIANS:%f  body->a:%g",(float) CC_RADIANS_TO_DEGREES( -body->a ),(double)body->a);
    oldpy = ballPenaltyCCSprite.position.y;
    oldpx = ballPenaltyCCSprite.position.x;
    ballSpriteShadow.position = ballPenaltyCCSprite.position;
    ballSpriteVolume.position = ballPenaltyCCSprite.position;
}


-(void)startDelayRestartPenalty:(ccTime *)theTimer{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	//NSLog(@"startDelayRestartPenalty : %i",[mySingleton timeToDelayAction]);
	if([mySingleton timeToDelayAction]==4){
		[mySingleton  setGameTurn:4];
		[self unschedule: @selector(startPlayerTurnPenalty:)];
	}
	if([mySingleton timeToDelayAction]<=0){
		[mySingleton setMaxExecutionTime:0];
		[self removeChildByTag:100 cleanup:YES];
		[self unschedule: @selector(startDelayRestartPenalty:)];
		[mySingleton setTimeToDelayAction:4];
		[mySingleton setGolAnimationPlaying:NO];
		[mySingleton  setGameTurn:0];
	}
	[mySingleton setTimeToDelayAction:[mySingleton timeToDelayAction]-1];
}

-(void)startPlayerTurnPenalty:(ccTime *)theTimer{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton timePlayTurn]==6){
		//NSString*p1Turn = @"p1Turn";
		//id clockAnim = [[[CCAnimation alloc] initWithName:p1Turn delay:1/2.0] autorelease];
		
        NSMutableArray *clockFrames = [NSMutableArray array];
        
		id clockAnim = [[[CCAnimation alloc] initWithSpriteFrames:clockFrames delay:1/4.0] autorelease];
		[clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock1.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock1.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock2.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock2.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock3.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock3.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock4.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock4.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock5.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock5.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock6.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock6.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock7.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock7.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock8.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock8.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock9.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock9.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock10.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock10.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock11.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock11.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock12.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock12.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock13.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock13.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock14.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock14.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock15.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock15.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock16.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock16.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock17.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock17.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock18.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock18.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock19.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock19.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock20.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock20.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock21.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock21.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock22.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock22.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock23.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock23.png"] textureRect]];
        [clockAnim addSpriteFrameWithTexture:(CCTexture2D*)[[CCSprite spriteWithSpriteFrameName:@"clock24.png"] texture] rect:[[CCSprite spriteWithSpriteFrameName:@"clock24.png"] textureRect]];

		
		// Make the animation sequence repeat forever
		id clockAction = [CCAnimate actionWithAnimation: clockAnim];
		//id repeating = [RepeatForever actionWithAction:myAction];
		
		clockTurnCCSprite = [CCSprite spriteWithSpriteFrameName:@"clock1.png"];
		
		[clockTurnCCSprite setPosition:ADJUST_CCP(ccp(180, 298))];
		[self addChild:clockTurnCCSprite z:200 tag:210];
		
		// Make a sprite (defined elsewhere) actually perform the animation
		[clockTurnCCSprite runAction:clockAction];
		[clockFrames removeAllObjects];
	}	
	if([mySingleton timePlayTurn]>0){
		
		[mySingleton setTimePlayTurn:[mySingleton timePlayTurn] - 1];
		
	}else{
	
		[self removeChild:clockTurnCCSprite cleanup:YES];
		
		CCSprite*arrowSelected;
		arrowSelected = (CCSprite *)[penalty1CCSprite getChildByTag:201];
		[arrowSelected setScaleX:0.1];
		
		[mySingleton  setGameTurn:2];
		[self unschedule: @selector(startPlayerTurnPenalty:)];
		[self schedule: @selector(tickPenalty:) interval: 1.0f/60.0f];
		
	}
}


CCLabelAtlas *label;
#define kFilterFactor 0.9



///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Touch Events ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	float kPowFactor = 30 * kSlide;
	if([mySingleton  gameTurn]==1){
		UITouch *touch = [touches anyObject];
		CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
        location = CGPointMake(location.x, location.y);
		[touchCCSprite setPosition:location];
		CCSprite* arrowSelected;
		if ([mySingleton currentSelectedP1]==1 && [mySingleton  gameTurn]==1 && [mySingleton penaltyAttack]%2!=0) {
			float b = (location.y-penalty1CCSprite.position.y);
			float c = (location.x-penalty1CCSprite.position.x);
			int degreeAngle = atan2(b,c)*180/ 3.14565656;
			int a = sqrt(b*b+c*c);
			CGFloat scaleSize = round(10/5.9*a)/100;
			arrowSelected = (CCSprite *)[penalty1CCSprite getChildByTag:201];
			[arrowSelected setRotation:-degreeAngle  ];
			[arrowSelected setScaleX:scaleSize];
			if(scaleSize>0.25)[mySingleton setP1ForceVector:cpv(c*kPowFactor,b*kPowFactor)];
		}else if([mySingleton currentSelectedP1]==1 && [mySingleton  gameTurn]==1 && [mySingleton penaltyAttack]%2==0) {
			int b = (location.y-penalty1CCSprite.position.y);
			int c = 0;//(location.x-penalty1CCSprite.position.x);
			int degreeAngle = atan2(b,c)*180/ 3.14565656;
			int a = sqrt(b*b+c*c);
			CGFloat scaleSize = round(10/5.9*a)/100;
			arrowSelected = (CCSprite *)[penalty1CCSprite getChildByTag:201];
			[arrowSelected setRotation:-degreeAngle ];
			[arrowSelected setScaleX:scaleSize];
			if(scaleSize>0.25)[mySingleton setP1ForceVector:CGPointMake(c*kPowFactor,b*kPowFactor)];
		}
	}
//	return kEventHandled;
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	[mySingleton setCurrentSelectedP1: 0];
	[mySingleton setCurrentSelectedP2: 0];
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	[touchCCSprite setPosition:location];
	if (CGRectContainsPoint(CGRectMake(penalty1CCSprite.position.x- 0.5*penalty1CCSprite.contentSize.width, penalty1CCSprite.position.y- 0.5*penalty1CCSprite.contentSize.height,
									   penalty1CCSprite.contentSize.width, penalty1CCSprite.contentSize.height), location) && [mySingleton isP1]==YES) {
		[mySingleton setCurrentSelectedP1: 1];
	}else if(CGRectContainsPoint(CGRectMake(spriteBackMenu.position.x- 0.5*spriteBackMenu.contentSize.width, spriteBackMenu.position.y- 0.5*spriteBackMenu.contentSize.height, spriteBackMenu.contentSize.width, spriteBackMenu.contentSize.height), location)) {
		[self goBackMenu];
	}else if(CGRectContainsPoint(CGRectMake(spriteSoundToogler.position.x- 0.5*spriteSoundToogler.contentSize.width, spriteSoundToogler.position.y- 0.5*spriteSoundToogler.contentSize.height, spriteSoundToogler.contentSize.width, spriteSoundToogler.contentSize.height), location)) {
		[self audioToogler];
	}
	
	//return kEventHandled;	
}

-(void)goBackMenu{
	MenuScene * ms = [MenuScene node];
	[[CCDirector sharedDirector] replaceScene:ms];
	
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	//UITouch *touch = [touches anyObject];
	//CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	//NSLog(@"-------------------------------------------------------------- Clicked ended : %d , %d",location.x,location.y);
	//location = CGPointMake(location.x, location.y);
	[touchCCSprite setPosition:CGPointMake(1000, 1000)];
	[mySingleton setCurrentSelectedP1: 0];
	//[arrowCCSprite stopAllActions];
	//return kEventHandled;
}


///////////////////////////////////////////////////////
//    a       x           x  =   a    * c
//  ----- = -----              -----
//    b       c                  b
///////////////////////////////////////////////////////

-(int)ruleOfThree:(int)a secondParam:(int)b thirdParam:(int)c {
	int x=(a/b)*c;
	return x;
}

-(void)setupChipmunk{
	cpInitChipmunk();
	space = cpSpaceNew();
	space->gravity = cpv(0,0);
	space->iterations = elasticIterations;
	space->damping = 0.3;
	
	//BALL
	ballPenaltyBody = cpBodyNew(ballMass, INFINITY);
	ballPenaltyBody->p = cpv(240, 153);
	cpSpaceAddBody(space, ballPenaltyBody);
	cpShape* ballShape = cpCircleShapeNew(ballPenaltyBody, 12.0, cpvzero);
	ballShape->e = 1;
	ballShape->u = 0.1;
	ballShape->data = ballPenaltyCCSprite;
	ballShape->collision_type = 0;
	cpSpaceAddShape(space, ballShape);
	//	cpSpaceAddCollisionPairFunc(space, 0, 1, &ballCollision, self);
	cpSpaceAddCollisionHandler(space, 0,1, &ballCollisionNew, NULL, NULL, NULL, self);
	
	//PLAYER 1
	penalty1Body = cpBodyNew(playerMass, INFINITY);
	penalty1Body->p = cpv(300, 153);
	//cpBodySetMass(penalty1Body,50);
	cpBodySetAngle(penalty1Body,0);
	cpSpaceAddBody(space, penalty1Body);
	cpShape* p1Shape = cpCircleShapeNew(penalty1Body, 14.0, cpvzero);
	p1Shape->e = 0.9;
	p1Shape->u = 0.1;
	p1Shape->data = penalty1CCSprite;
	p1Shape->collision_type = 1;
	cpSpaceAddShape(space, p1Shape);
	
	//PLAYER 2
	penalty2Body = cpBodyNew(playerMass, INFINITY);
	penalty2Body->p = cpv(90, 153);
	//cpBodySetMass(penalty2Body,50);
	cpSpaceAddBody(space, penalty2Body);
	cpShape* p2Shape = cpCircleShapeNew(penalty2Body, 14.0, cpvzero);
	p2Shape->e = 0.9;
	p2Shape->u = 0.1;
	p2Shape->data = penalty2CCSprite;
	p2Shape->collision_type = 1;
	cpSpaceAddShape(space, p2Shape);
	
	//WALLS
	cpBody* wallBody = cpBodyNew(INFINITY, INFINITY);
	
	//BOTTOM
	wallBody->p = cpv(0, 0);
	cpShape* wallShape = cpSegmentShapeNew(wallBody, cpv(0,20), cpv(480,20), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//TOP
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(0,286), cpv(480,286), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//LEFT 1
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(60,0), cpv(60,90), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//LEFT 2
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(60,217), cpv(60,320), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//LEFT 3
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(0,217), cpv(60,217), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//LEFT 4
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(0,90), cpv(60,90), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//GOL LEFT
	cpBody* gol1Body = cpBodyNew(INFINITY, INFINITY);
	gol1Body->p = cpv(0, 0);
	cpSpaceAddBody(space, gol1Body);
	cpShape* gol1Shape = cpSegmentShapeNew(gol1Body, cpv(60,90), cpv(60,217), 10);
	gol1Shape->e = 0.5;
	gol1Shape->u = 0.1;
	gol1Shape->data = invGol1CCSprite;
	gol1Shape->collision_type = 1;
	cpSpaceAddStaticShape(space, gol1Shape);
	
	//GOL LEFT
	cpBody* gol1HitBody = cpBodyNew(INFINITY, INFINITY);
	gol1HitBody->p = cpv(0, 0);
	cpSpaceAddBody(space, gol1HitBody);
	cpShape* gol1HitShape = cpSegmentShapeNew(gol1HitBody, cpv(40,90), cpv(40,217), 10);
	gol1HitShape->e = 0.5;
	gol1HitShape->u = 0.1;
	gol1HitShape->data = invGol1HitCCSprite;
	gol1HitShape->collision_type = 1;
	cpSpaceAddStaticShape(space, gol1HitShape);
	
	//WALL LEFT
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(0,0), cpv(0,320), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//RIGHT 1
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(420,0), cpv(420,90), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//RIGHT 2
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(420,217), cpv(420,320), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//RIGHT 3
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(420,217), cpv(480,217), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	//RIGHT 4
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(420,90), cpv(480,90), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	
	//WALL RIGHT
	wallBody->p = cpv(0, 0);
	wallShape = cpSegmentShapeNew(wallBody, cpv(480,0), cpv(480,320), 10);
	wallShape->e = 0.5;
	wallShape->u = 0.1;
	wallShape->collision_type = 1;
	cpSpaceAddStaticShape(space, wallShape);
	
	
	[self schedule: @selector(tickPenalty:) interval: 1.0f/60.0f];
	
}

//
// Called by NSTimer fire, hides the game label
//
- (void)hideGameLabel:(ccTime *)timer {
	[self removeChild:playerCCSprite cleanup:YES];
	
}

-(void)audioToogler{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		
		[mySingleton setAudioOn:NO];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
		sae.backgroundMusicVolume = 0.0f;
		sae.effectsVolume = 0.0f;
	}else{
		[mySingleton setAudioOn:YES];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];	
		sae.backgroundMusicVolume = 0.2f;	
		sae.effectsVolume = 0.2f;
	}
}


- (void)setupBatchNode {
    NSString *spritesheetPvrCcz = @"spritesheet.pvr.ccz";
    NSString *spritesheetPlist = @"spritesheet.plist";
    
    NSString *gametablePvrCcz = @"gametable.pvr.ccz";
    NSString *gametablePlist = @"gametable.plist";
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            spritesheetPvrCcz = @"spritesheet-ipadhd.pvr.ccz";
            spritesheetPlist = @"spritesheet-ipadhd.plist";
            gametablePvrCcz = @"gametable-ipadhd.pvr.ccz";
            gametablePlist = @"gametable-ipadhd.plist";
        }else{
            //iPad screen
            spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
            spritesheetPlist = @"spritesheet-hd.plist";
            gametablePvrCcz = @"gametable-hd.pvr.ccz";
            gametablePlist = @"gametable-hd.plist";
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
                spritesheetPlist = @"spritesheet-hd.plist";
                
                gametablePvrCcz = @"gametable-hd.pvr.ccz";
                gametablePlist = @"gametable-hd.plist";
            }else{
                //iphone screen
                spritesheetPvrCcz = @"spritesheet.pvr.ccz";
                spritesheetPlist = @"spritesheet.plist";
                
                gametablePvrCcz = @"gametable.pvr.ccz";
                gametablePlist = @"gametable.plist";
            }
        }
    }
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesheetPvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesheetPlist];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:gametablePvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:gametablePlist];
}

-(id)init{
	self = [super init];
	if(nil != self){
        [self setupBatchNode];
		GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
		GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
		if (sae != nil) {
			[sae preloadEffect:@"ambience.mp3"];
			[sae preloadEffect:@"ambience2.mp3"];
			[sae preloadEffect:@"kick.mp3"];
			[sae preloadEffect:@"kick1.mp3"];
			[sae preloadEffect:@"whistle.mp3"];
			[sae playBackgroundMusic:@"loop.mp3" loop:YES];
			if([mySingleton audioOn]==YES){
				
				sae.backgroundMusicVolume = 0.2f;
			}else{
				sae.backgroundMusicVolume = 0.0f;
			}
			
		}
		
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            goal1 = [CCSprite spriteWithFile: @"goal_anima1-hd.png"];
            goal2 = [CCSprite spriteWithFile: @"goal_anima2-hd.png"];
            goal3 = [CCSprite spriteWithFile: @"goal_anima3-hd.png"];
            goal4 = [CCSprite spriteWithFile: @"goal_anima4-hd.png"];
            goal5 = [CCSprite spriteWithFile: @"goal_anima5-hd.png"];
            goal6 = [CCSprite spriteWithFile: @"goal_anima6-hd.png"];
            goal7 = [CCSprite spriteWithFile: @"goal_anima7-hd.png"];
            goal8 = [CCSprite spriteWithFile: @"goal_anima8-hd.png"];
        }else{
            goal1 = [CCSprite spriteWithFile: @"goal_anima1.png"];
            goal2 = [CCSprite spriteWithFile: @"goal_anima2.png"];
            goal3 = [CCSprite spriteWithFile: @"goal_anima3.png"];
            goal4 = [CCSprite spriteWithFile: @"goal_anima4.png"];
            goal5 = [CCSprite spriteWithFile: @"goal_anima5.png"];
            goal6 = [CCSprite spriteWithFile: @"goal_anima6.png"];
            goal7 = [CCSprite spriteWithFile: @"goal_anima7.png"];
            goal8 = [CCSprite spriteWithFile: @"goal_anima8.png"];
        }
        
        [self addChild:goal1 z:1];
        [self removeChild:goal1 cleanup:YES];
        [self addChild:goal2 z:1];
        [self removeChild:goal2 cleanup:YES];
        [self addChild:goal3 z:1];
        [self removeChild:goal3 cleanup:YES];
        [self addChild:goal4 z:1];
        [self removeChild:goal4 cleanup:YES];
        [self addChild:goal5 z:1];
        [self removeChild:goal5 cleanup:YES];
        [self addChild:goal6 z:1];
        [self removeChild:goal6 cleanup:YES];
        [self addChild:goal7 z:1];
        [self removeChild:goal7 cleanup:YES];
        [self addChild:goal8 z:1];
        [self removeChild:goal8 cleanup:YES];
        
        
		[mySingleton  setGameTurn:0];
		spriteBackMenu = [CCSprite spriteWithSpriteFrameName:@"back_menu.png"];
		[spriteBackMenu setPosition:ADJUST_CCP(ccp(20,300))];
		[self addChild:spriteBackMenu z:101 tag:101];
		
		spriteSoundToogler = [CCSprite spriteWithSpriteFrameName:@"sound.png"];
		[spriteSoundToogler setPosition:ADJUST_CCP(ccp(460,300))];
		[self addChild:spriteSoundToogler z:101 tag:101];
		
		
		DataHolder *myDataHolder = [DataHolder sharedInstance];
		[myDataHolder clearData];
		
		self.isTouchEnabled = YES;
		
		//isAccelerometerEnabled = YES;
		//[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 60)];
		hasSent = NO;
		hasSentPosition = NO;
		penaltyBallLayer = self;
		
		//Sets slide factor based on the the weather
		if([myCupSingleton matchWeather]==0  &&  [mySingleton gameType]!=2){
			elasticIterations = 3;
			kSlide =  0.8;
			playerMass = 18;
			ballMass = 15;
		}else if([myCupSingleton matchWeather]==1 &&  [mySingleton gameType]!=2){
			elasticIterations = 2;
			kSlide =  0.9;
			playerMass = 20;
			ballMass = 12;
		}else if([myCupSingleton matchWeather]==2 &&  [mySingleton gameType]!=2){
			elasticIterations = 1;
			kSlide =  1.1;
			playerMass = 24;
			ballMass = 40;
		}else{
			//bluetooth
			elasticIterations = 5;
			kSlide =  1.1;
			playerMass = 20;
			ballMass = 22;
		}
		
		ballPenaltyCCSprite = [CCSprite spriteWithSpriteFrameName:@"ball.png"];
		[ballPenaltyCCSprite setPosition:ADJUST_CCP(ccp(240, 153))];
		[self addChild:ballPenaltyCCSprite z:0 tag:3];
		
		[mySingleton setIsP1:YES];
		[mySingleton setIsP2:NO];

        
        NSString* teamName1 = [myDataHolder getTeamShortNameForKey:[mySingleton spritePlayer1]];
        NSString* teamName2 = [myDataHolder getTeamShortNameForKey:[mySingleton spritePlayer2]];
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                labelPlayer1 = [CCLabelAtlas labelWithString:teamName1 charMapFile:@"charmap1-ipadhd.png" itemWidth:36 itemHeight:44 startCharMap:'A'];
                labelPlayer2 = [CCLabelAtlas labelWithString:teamName2 charMapFile:@"charmap1-ipadhd.png" itemWidth:36 itemHeight:44 startCharMap:'A'];
                 p1Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-ipadhd.png" itemWidth:56 itemHeight:84 startCharMap:'.'];
                p2Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-ipadhd.png" itemWidth:56 itemHeight:84 startCharMap:'.'];
                
                
                
            }else{
                //iPad screen
                labelPlayer1 = [CCLabelAtlas labelWithString:teamName1 charMapFile:@"charmap1-hd.png" itemWidth:36 itemHeight:44 startCharMap:'A'];
                labelPlayer2 = [CCLabelAtlas labelWithString:teamName2 charMapFile:@"charmap1-hd.png" itemWidth:36 itemHeight:44 startCharMap:'A'];
                p1Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-hd.png" itemWidth:56 itemHeight:84 startCharMap:'.'];
                p2Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-hd.png" itemWidth:56 itemHeight:84 startCharMap:'.'];
            }
            
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    labelPlayer1 = [CCLabelAtlas labelWithString:teamName1 charMapFile:@"charmap1-hd.png" itemWidth:18 itemHeight:22 startCharMap:'A'];
                    labelPlayer2 = [CCLabelAtlas labelWithString:teamName2 charMapFile:@"charmap1-hd.png" itemWidth:18 itemHeight:22 startCharMap:'A'];
                    p1Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-hd.png" itemWidth:28 itemHeight:42 startCharMap:'.'];
                    p2Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers-hd.png" itemWidth:28 itemHeight:42 startCharMap:'.'];
                }else{
                    //iphone screen
                    labelPlayer1 = [CCLabelAtlas labelWithString:teamName1 charMapFile:@"charmap1.png" itemWidth:9 itemHeight:11 startCharMap:'A'];
                    labelPlayer2 = [CCLabelAtlas labelWithString:teamName2 charMapFile:@"charmap1.png" itemWidth:9 itemHeight:11 startCharMap:'A'];
                    p1Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers.png" itemWidth:56 itemHeight:40 startCharMap:'.'];
                    p2Score = [CCLabelAtlas labelWithString:@"0" charMapFile:@"clockNumbers.png" itemWidth:56 itemHeight:40 startCharMap:'.'];
                }
            }
            
        }
        
        
        [labelPlayer1 setPosition:ADJUST_CCP(ccp(64,286))];
        [labelPlayer2 setPosition:ADJUST_CCP(ccp(362,286))];
        [p1Score setPosition:ADJUST_CCP(ccp(126,276))];
        [p2Score setPosition:ADJUST_CCP(ccp(328,276))];
		
		
		[self addChild:labelPlayer1 z:306 tag:306];
		[self addChild:labelPlayer2 z:306 tag:306];
		
		
		//NSLog(@"TEAM NAME 1 : %p",teamName1);
		//NSLog(@"TEAM NAME 2 : %d",(NSString*)teamName2);
		
		

		
		//SCOREBOARD
		[self addChild:p1Score z:302 tag:302];
        
		
		[self addChild:p2Score z:303 tag:303];
        
		
		/*versusScoreCCSprite = [CCSprite spriteWithSpriteFrameName:@"versusScoreSprite.png"];
		[versusScoreCCSprite setPosition:ADJUST_CCP(ccp(240, 300))];
		[self addChild:versusScoreCCSprite z:304 tag:304 ];
         */
		
		touchCCSprite = [CCSprite spriteWithSpriteFrameName:@"touchSprite.png"];
		[touchCCSprite setPosition:ADJUST_CCP(ccp(1000, 1000))];
		[self addChild:touchCCSprite z:10 tag:8];
		NSString* imageName1 = [myDataHolder getImageP1ForKey:[mySingleton spritePlayer1]];
		
		CCSprite* p1CCSpriteDisplay = [CCSprite spriteWithSpriteFrameName:imageName1];
		[p1CCSpriteDisplay setPosition:ADJUST_CCP(ccp(180,298))];
		[self addChild:p1CCSpriteDisplay  z:10 tag:1];
		
						
		NSString* imageName2 = [myDataHolder getImageP2ForKey:[mySingleton spritePlayer2]];
		
		CCSprite* p2CCSpriteDisplay = [CCSprite spriteWithSpriteFrameName:imageName2];
		[p2CCSpriteDisplay setPosition:ADJUST_CCP(ccp(301,298))];
		[self addChild:p2CCSpriteDisplay  z:10 tag:1];
		
		
		penalty1CCSprite = [CCSprite spriteWithSpriteFrameName:imageName1];
		[penalty1CCSprite setPosition:ADJUST_CCP(ccp(300, 153))];
		[self addChild:penalty1CCSprite  z:1 tag:1];
		
		CCSprite*arrowCCSprite = [CCSprite spriteWithSpriteFrameName:@"drag_arrow.png" ];
		[arrowCCSprite setAnchorPoint:CGPointMake(0.1, 0.5)];	
		[arrowCCSprite setPosition:CGPointMake(penalty1CCSprite.contentSize.width/2, penalty1CCSprite.contentSize.height/2)];
		[penalty1CCSprite addChild:arrowCCSprite z:-1 tag:201];
		[arrowCCSprite setScaleX:0.1];
		
		
		penalty2CCSprite = [CCSprite spriteWithSpriteFrameName:imageName2];
		//PLAYER 2
		//penalty2CCSprite = [CCSprite spriteWithFile:[mySingleton teamP1Image]];
		[penalty2CCSprite setPosition:ADJUST_CCP(ccp(90, 153))];
		[self addChild:penalty2CCSprite z:2 tag:2];
		
		arrowCCSprite = [CCSprite spriteWithSpriteFrameName:@"drag_arrow.png" ];
		[arrowCCSprite setAnchorPoint:CGPointMake(0.1, 0.5)];	
		[arrowCCSprite setPosition:CGPointMake(penalty2CCSprite.contentSize.width/2, penalty2CCSprite.contentSize.height/2)];
		[penalty2CCSprite addChild:arrowCCSprite z:-1 tag:201];
		[arrowCCSprite setScaleX:0.1];
		
				
		gol1CCSprite = [CCSprite spriteWithSpriteFrameName:@"gol1.png"];
		[gol1CCSprite setPosition:ADJUST_CCP(ccp(35, 153))];
		[gol1CCSprite setTag:9999];
		[self addChild:gol1CCSprite];
		
				
		invGol1CCSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invGol1CCSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[invGol1CCSprite setTag:101];
		[self addChild:invGol1CCSprite];
		
		
		
		invGol1HitCCSprite = [CCSprite spriteWithSpriteFrameName:@"invisible.png"];
		[invGol1HitCCSprite setPosition:ADJUST_CCP(ccp(0, 0))];
		[invGol1HitCCSprite setTag:103];
		[self addChild:invGol1HitCCSprite];
		
				
		[self setupChipmunk];
		
		[mySingleton setP1ForceVector : cpv(0,0)];
		[mySingleton setP2ForceVector : cpv(0,0)];
		
	}
	return self;
}
@end
