//
//  MenuScene.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "GlobalSingleton.h"



@interface MenuScene : CCScene  {
	CCSprite *spriteSoundToogler;
	UIViewController *viewController;
    CCSpriteBatchNode *_batchNode;

    
}



+(id)scene;
-(void)audioToogler;
-(void)startGameSingle: (id)sender;
-(void)startGameMulti: (id)sender;
-(void)startGameMultiBlueTooth: (id)sender;
-(void)scoreBoard: (id)sender;
-(void)help: (id)sender;

- (void)setupBatchNode;


@end


