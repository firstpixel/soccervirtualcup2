/*
 * Copyright (c) 2009-2011 Ricardo Quesada
 * Copyright (c) 2011-2012 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


typedef enum {
    M_NETWORK_ACK,					// no packet
    M_NETWORK_COINTOSS,				// decide who is going to be the server
    M_NETWORK_RESYNC_EVENT,			// send position
    M_NETWORK_RECEIVE_EVENT,
    M_NETWORK_FIRE_EVENT,				// send fire
    M_NETWORK_HEARTBEAT				// send of entire state at regular intervals
}NetworkCodes;

typedef struct {
    int pId;
}ConfirmPacket;

typedef struct {
    int pTeamID;
    int gameUniqueID;
    int matchWeather;
    int timeLeft;
}InitInfoPacket;

typedef struct {
    BOOL isP1;
    BOOL isP2;
    
    float p1ForceVectorX;
    float p1ForceVectorY;
    float p2ForceVectorX;
    float p2ForceVectorY;
    float p3ForceVectorX;
    float p3ForceVectorY;
    float p4ForceVectorX;
    float p4ForceVectorY;
    float p5ForceVectorX;
    float p5ForceVectorY;
    float p6ForceVectorX;
    float p6ForceVectorY;
    
    float p1PositionX;
    float p1PositionY;
    float p2PositionX;
    float p2PositionY;
    float p3PositionX;
    float p3PositionY;
    float p4PositionX;
    float p4PositionY;
    float p5PositionX;
    float p5PositionY;
    float p6PositionX;
    float p6PositionY;
    float ballPositionX;
    float ballPositionY;

}GameInfoPacket;

typedef enum {
    kStateCointoss,
    kStatePlayer2,
    kStateFullPlay,
    kStateConfirm,
    kStateReconnect
}GameStates;

typedef struct {
    BOOL beat;
}HeartBeatPacket;


// for the sake of simplicity player1 is the server and player2 is the client
typedef enum {
    kPlayerServer,
    kPlayerClient
}PlayerNetwork;


#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GADMobileAds.h>
#import <GoogleMobileAds/GoogleMobileAds.h>


#import "GameCenterManager.h"

#import "Flurry.h"
#import "FlurryAds.h"
#import "FlurryAdDelegate.h"
#import "FlurryAdBannerDelegate.h"
#import "FlurryAdBanner.h"
#import "FlurryAdInterstitial.h"
#import "FlurryAdInterstitialDelegate.h"

#import <Chartboost/CBInPlay.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "PeerServiceManager.h"


@class GADBannerView;
@class GADInterstitial;
@class FlurryAdBanner;
@class FlurryAdInterstitial;

@interface RootViewController : UINavigationController <PeerServiceManagerDelegate, GADInterstitialDelegate, GADBannerViewDelegate, GADNativeExpressAdViewDelegate, FlurryAdBannerDelegate, FlurryAdDelegate, FlurryAdInterstitialDelegate, GameCenterManagerDelegate, GameCenterMultiplayerManagerDelegate, SKStoreProductViewControllerDelegate> {
    GADBannerView *bannerView;
    GADInterstitial *interstitialView;
    GADNativeExpressAdView *nativeExpressAdView;
    UIButton *localBannerButton;

    BOOL localBannerAdded;
    BOOL flurryBannerAdded;
    BOOL bannerViewAdded;
    BOOL interstitialViewAdded;
    
    //FLURRY

    FlurryAdBanner* adBanner;
    FlurryAdInterstitial* adInterstitial;
    
    //Multiplayer
    GKMatch* match;
    int uid;
    int playCounter;
    GCMMultiplayer* localManager;
    
    //multiplayer networking
    GameInfoPacket gamePacket;
    InitInfoPacket initPacket;
    ConfirmPacket confirmPacket;
    HeartBeatPacket heartBeatPacket;
    
    //multiplayer local
    GameStates gameState;
    



}


- (void)addBanner;
- (void)removeBanner;

- (void)addInterstitial;
//- (void)removeInterstitial;


/// Sent to the delegate when a live multiplayer match begins
- (void)gameCenterManager:(GCMMultiplayer *)manager matchStarted:(GKMatch *)match;

/// Sent to the delegate when a live multiplayer match ends
- (void)gameCenterManager:(GCMMultiplayer *)manager matchEnded:(GKMatch *)match;

/// Sent to the delegate when data is recieved on the current device (sent from another player in the match)
- (void)gameCenterManager:(GCMMultiplayer *)manager match:(GKMatch *)match didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID;

// Multiplayer internet/bluetooth
-(void)sendCoinToss;
-(void)sendPlayer2GamePlay;
-(void)sendServerFullGamePlay;
-(void)sendHeartBeat;

-(void)openBrowserViewController;


@end
