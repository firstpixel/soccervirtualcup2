//
//  SelectionScreen1Player.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/8/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "LoopingMenu.h"
#import "LoopingMenuItem.h"
#import "SelectionScreen1Player.h"
#import "GameScene.h"
#import "PenaltyScene.h"
#import "TeamGroupsScene.h"
#import "AppDelegate.h"
#import "SimpleAudioEngine.h"
#import "GlobalCupSingleton.h"
#import "DeviceSettings.h"
#import "Flurry.h"
#import "GameCenterManager.h"
#import "RootViewController.h"

@implementation SelectionScreen1Player




- (id) init {
    self = [super init];
    if (self != nil) {
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"option-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"option-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"option-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"option-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"option.jpg"];
                }
            }
        }

	
        
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
		[self addChild:[SelectionScreen1PlayerLayer node] z:1];
		
    }
    return self;
}


@end


@implementation SelectionScreen1PlayerLayer
//@synthesize teamKeys;
@synthesize menuIsOpen;
@synthesize selectedPlayer;

- (id) init {
    self = [super init];
    if (self != nil) {
        [self setupBatchNode];
		self.isTouchEnabled = YES;
		menuIsOpen = NO;
        myDataHolder = [DataHolder sharedInstance];
        [myDataHolder clearData];
        
		//SET GLOBAL DEFAULTS
		GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
		GlobalCupSingleton *myCupSingletons = [GlobalCupSingleton sharedInstance]; //setup the global vars singleton instance

		spriteBackMenu = [CCSprite spriteWithSpriteFrameName:@"back_menu.png"];
        spriteSoundToogler = [CCSprite spriteWithSpriteFrameName:@"sound.png"];
		
        if(IS_IPAD || IS_IPADHD){
            [spriteBackMenu setPosition:ADJUST_CCP(ccp(26,328))];
            [spriteSoundToogler setPosition:ADJUST_CCP(ccp(454,328))];
		}else{
            [spriteBackMenu setPosition:ADJUST_CCP(ccp(26,302))];
            [spriteSoundToogler setPosition:ADJUST_CCP(ccp(454,302))];

        }
        [self addChild:spriteBackMenu z:101 tag:1010];
		[self addChild:spriteSoundToogler z:101 tag:1011];
		
		//MATCH TIME
		spriteMatchTime = [CCSprite spriteWithSpriteFrameName:@"time_3min.png"];
		[spriteMatchTime setPosition:ADJUST_CCP(ccp(58,220))];
		[self addChild:spriteMatchTime z:210 tag:2100];
		
		matchTime5min = [CCSprite spriteWithSpriteFrameName:@"time_3min_bt.png"];
		matchTime8min = [CCSprite spriteWithSpriteFrameName:@"time_5min_bt.png"];
		matchTime10min = [CCSprite spriteWithSpriteFrameName:@"time_8min_bt.png"];
		
		[self addChild:matchTime5min  z:201 tag:2010 ];
		[matchTime5min setPosition:ADJUST_CCP(ccp(58,220))];
		[self addChild:matchTime8min z:202  tag:2020 ];
		[matchTime8min setPosition:ADJUST_CCP(ccp(128,220))];
		[self addChild:matchTime10min  z:203 tag:2030 ];
		[matchTime10min setPosition:ADJUST_CCP(ccp(198,220))];
		
		//MATCH WEATHER
		spriteMatchWeather = [CCSprite spriteWithSpriteFrameName:@"weather_random.png"];
		[spriteMatchWeather setPosition:ADJUST_CCP(ccp(300,220))];
		[self addChild:spriteMatchWeather z:110 tag:1000];
		
		matchWeatherRandom = [CCSprite spriteWithSpriteFrameName:@"weather_random_bt.png"];
		matchWeatherFine = [CCSprite spriteWithSpriteFrameName:@"weather_fine_bt.png"];
		matchWeatherRainy = [CCSprite spriteWithSpriteFrameName:@"weather_rainy_bt.png"];
		
		[self addChild:matchWeatherRandom z:101 tag:1010];
		[matchWeatherRandom setPosition:ADJUST_CCP(ccp(300,220))];
		[self addChild:matchWeatherFine z:102 tag:1020];
		[matchWeatherFine setPosition:ADJUST_CCP(ccp(369,220))];
		[self addChild:matchWeatherRainy z:103 tag:1030];
		[matchWeatherRainy setPosition:ADJUST_CCP(ccp(428,220))];
        NSArray * teamArray = myDataHolder.teamObjectArray;
        
        
        NSUserDefaults *prefs  = [NSUserDefaults standardUserDefaults];
        
        int team1 = [prefs objectForKey:@"teamPlayer1"]!=nil?(int)[prefs integerForKey:@"teamPlayer1"]:24;
        int team2 = [prefs objectForKey:@"teamPlayer2"]!=nil?(int)[prefs integerForKey:@"teamPlayer2"]:12;
        
        
		if([mySingletons gameType]!=1 && [mySingletons gameType]!=3 && [mySingletons gameType]!=5){
            if([mySingletons gameType] == 0 && team1 > 31) {
                team1 = 24;
            }
            //TEAM BUTTON
			myTeam = [CCSprite spriteWithSpriteFrameName:[(Team*)[teamArray objectAtIndex:team1] init].teamButton];
			[self addChild:myTeam z:500 tag:team1];
			[myTeam setPosition:ADJUST_CCP(ccp(240,135))];
            [self addStarPlayer1:[myDataHolder getFactorByTeamKey:team1] x:200 teamName:[myDataHolder getTeamNameForKey:team1]];
		}else{
			//TEAM BUTTON
			myTeam = [CCSprite spriteWithSpriteFrameName:[(Team*)[teamArray objectAtIndex:team1] init].teamButton];
			[self addChild:myTeam z:500 tag:team1];
			[myTeam setPosition:ADJUST_CCP(ccp(180,135))];
            
            [self addStarPlayer1:[myDataHolder getFactorByTeamKey:team1] x:140 teamName:[myDataHolder getTeamNameForKey:team1]];
            
            //TEAM BUTTON
			opponentTeam = [CCSprite spriteWithSpriteFrameName:[(Team*)[teamArray objectAtIndex:team2] init].teamButton];
			[self addChild:opponentTeam z:500 tag:team2];
			[opponentTeam setPosition:ADJUST_CCP(ccp(300,135))];
            [self addStarPlayer2:[myDataHolder getFactorByTeamKey:team2] teamName:[myDataHolder getTeamNameForKey:team2]];
		}
		
		spriteGoPlay = [CCSprite spriteWithSpriteFrameName:@"go_bt.png"];
		[self addChild:spriteGoPlay z:1 tag:1];
		[spriteGoPlay setPosition:ADJUST_CCP(ccp(240,44))];
		
		[myCupSingletons setDefaultTimeLeft:180];
		[myCupSingletons setMatchWeather:0];
		if([mySingletons gameType]==0 || [mySingletons gameType]==1 || [mySingletons gameType]==3 || [mySingletons gameType]==5){
			[mySingletons setSpritePlayer1:team1];
			[myCupSingletons setSpritePlayer1:[mySingletons spritePlayer1]];	
			[mySingletons setSpritePlayer2:team2];
		}else if([mySingletons gameType]==2 || [mySingletons gameType]==22){
			[mySingletons setSpritePlayer1:team1];
			[mySingletons setSpritePlayer2:team1];
			[mySingletons setPTeamID:team1];
		}
    }
    return self;
}

-(void) addStarPlayer1:(int)teamFactor x:(float) posx teamName:(NSString*)name{
    
    starHolder1 = [CCLayer node];
    [self addChild: starHolder1];
    //TEAM NAME
    teamName1Label = [CCLabelTTF labelWithString:name fontName:@"Arial" fontSize:HD_PIXELS(11)];
    [teamName1Label setHorizontalAlignment:kCCTextAlignmentCenter];
    [teamName1Label setColor:ccWHITE];
    [starHolder1 addChild: teamName1Label];
    
    starBg1_1 = [CCSprite spriteWithSpriteFrameName:@"star_holder.png"];
    [starBg1_1 setPosition: ADJUST_CCP(ccp(0,10))];
    [starHolder1 addChild: starBg1_1];
    
    star1_1 = [CCSprite spriteWithSpriteFrameName:@"star.png"];
    [star1_1 setPosition: ADJUST_CCP(ccp(0,10))];
    [starHolder1 addChild: star1_1];
    
    starBg1_2 = [CCSprite spriteWithSpriteFrameName:@"star_holder.png"];
    [starBg1_2 setPosition: ADJUST_CCP(ccp(20,10))];
    [starHolder1 addChild: starBg1_2];
    
    star1_2 = [CCSprite spriteWithSpriteFrameName:@"star.png"];
    [star1_2 setPosition: ADJUST_CCP(ccp(20,10))];
    [starHolder1 addChild: star1_2];
    
    starBg1_3 = [CCSprite spriteWithSpriteFrameName:@"star_holder.png"];
    [starBg1_3 setPosition: ADJUST_CCP(ccp(40,10))];
    [starHolder1 addChild: starBg1_3];
    
    star1_3 = [CCSprite spriteWithSpriteFrameName:@"star.png"];
    [star1_3 setPosition: ADJUST_CCP(ccp(40,10))];
    [starHolder1 addChild: star1_3];
    
    starBg1_4 = [CCSprite spriteWithSpriteFrameName:@"star_holder.png"];
    [starBg1_4 setPosition: ADJUST_CCP(ccp(60,10))];
    [starHolder1 addChild: starBg1_4];
    
    star1_4 = [CCSprite spriteWithSpriteFrameName:@"star.png"];
    [star1_4 setPosition: ADJUST_CCP(ccp(60,10))];
    [starHolder1 addChild: star1_4];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
       
        
        [starHolder1 setPosition: ADJUST_CCP(ccp(posx,40))];
        [teamName1Label setPosition: ADJUST_CCP(ccp(25,25))];
        
    
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                if([[ UIScreen mainScreen ] bounds ].size.width == 736.0){
                    //iphone 6p
                    [starHolder1 setPosition: ADJUST_CCP(ccp(posx-110,20))];
                    CGPoint size = ADJUST_CCP(ccp(140,20));
                    [teamName1Label setDimensions:CGSizeMake(size.x,size.y)];
                    [teamName1Label setPosition: ADJUST_CCP(ccp(25,0))];
                    
                }else if([[ UIScreen mainScreen ] bounds ].size.width == 667.0){
                    //iphone 6
                    [starHolder1 setPosition: ADJUST_CCP(ccp(posx-80,40))];
                    CGPoint size = ADJUST_CCP(ccp(140,20));
                    [teamName1Label setDimensions:CGSizeMake(size.x,size.y)];
                    [teamName1Label setPosition: ADJUST_CCP(ccp(25,12))];
                    
                }else if([[ UIScreen mainScreen ] bounds ].size.width == 568.0){
                    //iphone 5
                    [starHolder1 setPosition: ADJUST_CCP(ccp(posx-30,70))];
                    CGPoint size = ADJUST_CCP(ccp(140,20));
                    [teamName1Label setDimensions:CGSizeMake(size.x,size.y)];
                    [teamName1Label setPosition: ADJUST_CCP(ccp(25,25))];
                    
                }else{
                    //iphone retina screen
                    [starHolder1 setPosition: ADJUST_CCP(ccp(posx+12,70))];
                    CGPoint size = ADJUST_CCP(ccp(140,20));
                    [teamName1Label setDimensions:CGSizeMake(size.x,size.y)];
                    [teamName1Label setPosition: ADJUST_CCP(ccp(25,25))];
                    
                }
            }else{
                //iphone screen
                [starHolder1 setPosition: ADJUST_CCP(ccp(posx+12,70))];
                CGPoint size = ADJUST_CCP(ccp(140,20));
                [teamName1Label setDimensions:CGSizeMake(size.x,size.y)];
                [teamName1Label setPosition: ADJUST_CCP(ccp(25,25))];
            }
        }
    }
    
    if(teamFactor<2){
        [star1_2 setVisible:NO];
    }else{
        [star1_2 setVisible:YES];
    }
    
    if(teamFactor<3){
        [star1_3 setVisible:NO];
    }else{
        [star1_3 setVisible:YES];
    }
    
    if(teamFactor<4){
        [star1_4 setVisible:NO];
    }else{
        [star1_4 setVisible:YES];
    }
    
    
}


-(void) addStarPlayer2:(int)teamFactor teamName:(NSString*)name{
    //TEAM NAME
    teamName2Label = [CCLabelTTF labelWithString:name fontName:@"Arial" fontSize:HD_PIXELS(11)];
    [teamName2Label setHorizontalAlignment:kCCTextAlignmentCenter];
    [teamName2Label setColor:ccWHITE];
    starHolder2 = [CCLayer node];
    
    [starHolder2 addChild: teamName2Label];
    
    starBg2_1 = [CCSprite spriteWithSpriteFrameName:@"star_holder.png"];
    [starHolder2 addChild: starBg2_1];
    
    star2_1 = [CCSprite spriteWithSpriteFrameName:@"star.png"];
    [starHolder2 addChild: star2_1];
    
    starBg2_2 = [CCSprite spriteWithSpriteFrameName:@"star_holder.png"];
    [starHolder2 addChild: starBg2_2];
    
    star2_2 = [CCSprite spriteWithSpriteFrameName:@"star.png"];
    [starHolder2 addChild: star2_2];
    
    starBg2_3 = [CCSprite spriteWithSpriteFrameName:@"star_holder.png"];
    [starHolder2 addChild: starBg2_3];
    
    star2_3 = [CCSprite spriteWithSpriteFrameName:@"star.png"];
    [starHolder2 addChild: star2_3];
    
    
    starBg2_4 = [CCSprite spriteWithSpriteFrameName:@"star_holder.png"];
    [starHolder2 addChild: starBg2_4];
    
    star2_4 = [CCSprite spriteWithSpriteFrameName:@"star.png"];
    [starHolder2 addChild: star2_4];
    [self addChild: starHolder2];
    [teamName2Label setPosition: ADJUST_CCP(ccp(30,24))];
    [starBg2_1 setPosition: ADJUST_CCP(ccp(0,10))];
    [star2_1 setPosition: ADJUST_CCP(ccp(0,10))];
    [starBg2_2 setPosition: ADJUST_CCP(ccp(20,10))];
    [star2_2 setPosition: ADJUST_CCP(ccp(20,10))];
    [starBg2_3 setPosition: ADJUST_CCP(ccp(40,10))];
    [star2_3 setPosition: ADJUST_CCP(ccp(40,10))];
    [starBg2_4 setPosition: ADJUST_CCP(ccp(60,10))];
    [star2_4 setPosition: ADJUST_CCP(ccp(60,10))];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {

        [starHolder2 setPosition: ADJUST_CCP(ccp(260,40))];
        [teamName2Label setPosition: ADJUST_CCP(ccp(25,25))];
        
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //retina
                
                if([[ UIScreen mainScreen ] bounds ].size.width == 736.0){
                    //iphone 6p
                    [starHolder2 setPosition: ADJUST_CCP(ccp(150,20))];
                    CGPoint size = ADJUST_CCP(ccp(140,20));
                    [teamName2Label setDimensions:CGSizeMake(size.x,size.y)];
                    [teamName2Label setPosition: ADJUST_CCP(ccp(25,0))];
                }else if([[ UIScreen mainScreen ] bounds ].size.width == 667.0 ){
                    //iphone 6
                    [starHolder2 setPosition: ADJUST_CCP(ccp(180,40))];
                    CGPoint size = ADJUST_CCP(ccp(140,20));
                    [teamName2Label setDimensions:CGSizeMake(size.x,size.y)];
                    [teamName2Label setPosition: ADJUST_CCP(ccp(25,12))];
                }else if([[ UIScreen mainScreen ] bounds ].size.width == 568.0){
                    //iphone 5
                    [starHolder2 setPosition: ADJUST_CCP(ccp(230,70))];
                    CGPoint size = ADJUST_CCP(ccp(140,20));
                    [teamName2Label setDimensions:CGSizeMake(size.x,size.y)];
                    [teamName2Label setPosition: ADJUST_CCP(ccp(25,25))];
                    
                }else{
                    //iphone retina screen
                    [starHolder2 setPosition: ADJUST_CCP(ccp(275,70))];
                    CGPoint size = ADJUST_CCP(ccp(140,20));
                    [teamName2Label setDimensions:CGSizeMake(size.x,size.y)];
                    [teamName2Label setPosition: ADJUST_CCP(ccp(25,25))];
                }
            }else{
                //iphone screen
                //iphone retina screen
                [starHolder2 setPosition: ADJUST_CCP(ccp(275,70))];
                CGPoint size = ADJUST_CCP(ccp(140,20));
                [teamName2Label setDimensions:CGSizeMake(size.x,size.y)];
                [teamName2Label setPosition: ADJUST_CCP(ccp(25,25))];
            }
        }
    }
    
    
    
    if(teamFactor<2){
        [star2_2 setVisible:NO];
    }else{
        [star2_2 setVisible:YES];
    }
    
    if(teamFactor<3){
        [star2_3 setVisible:NO];
    }else{
        [star2_3 setVisible:YES];
    }
    
    if(teamFactor<4){
        [star2_4 setVisible:NO];
    }else{
        [star2_4 setVisible:YES];
    }
   

}


- (void)setupBatchNode {
    NSString *spritesheetPvrCcz = @"spritesheet.pvr.ccz";
    NSString *spritesheetPlist = @"spritesheet.plist";
    
    NSString *gametablePvrCcz = @"gametable.pvr.ccz";
    NSString *gametablePlist = @"gametable.plist";
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            spritesheetPvrCcz = @"spritesheet-ipadhd.pvr.ccz";
            spritesheetPlist = @"spritesheet-ipadhd.plist";
            gametablePvrCcz = @"gametable-ipadhd.pvr.ccz";
            gametablePlist = @"gametable-ipadhd.plist";
        }else{
            //iPad screen
            spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
            spritesheetPlist = @"spritesheet-hd.plist";
            gametablePvrCcz = @"gametable-hd.pvr.ccz";
            gametablePlist = @"gametable-hd.plist";
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
                spritesheetPlist = @"spritesheet-hd.plist";
                
                gametablePvrCcz = @"gametable-hd.pvr.ccz";
                gametablePlist = @"gametable-hd.plist";
            }else{
                //iphone screen
                spritesheetPvrCcz = @"spritesheet.pvr.ccz";
                spritesheetPlist = @"spritesheet.plist";
                
                gametablePvrCcz = @"gametable.pvr.ccz";
                gametablePlist = @"gametable.plist";
            }
        }
    }
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesheetPvrCcz];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:gametablePvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesheetPlist];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:gametablePlist];
}
-(void)dealloc{
   // [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}
-(void)buildTeamMenu{
    NSMutableArray* menuItemsArray = [[NSMutableArray alloc] init];
    int gKey;
   
    int maxTeam = (int)[myDataHolder.teamObjectArray count];
    
    if([[GlobalSingleton sharedInstance] gameType] == 0 ){
        maxTeam = 32;
    }
    for (gKey = 0;  gKey<maxTeam; gKey++){
        
        NSString * fileName = [(Team*)[myDataHolder.teamObjectArray objectAtIndex:gKey] init].teamButton;
        CCSprite *mySprite = [CCSprite spriteWithSpriteFrameName:fileName];
        CCSprite *mySpriteSelected = [CCSprite spriteWithSpriteFrameName:fileName];
		CCSprite *mySpriteDisabled = [CCSprite spriteWithSpriteFrameName:fileName];
		CCMenuItemSprite* menuItem1 = [CCMenuItemSprite itemWithNormalSprite:mySprite selectedSprite:mySpriteSelected  disabledSprite:mySpriteDisabled target:self selector:@selector(selectTeam:)];
        [menuItem1 setTag:gKey];
        [menuItemsArray addObject:menuItem1];
	}

    menu = [LoopingMenu menuWithArray:menuItemsArray];
    menuIsOpen = YES;

    [menu alignItemsHorizontallyWithPadding: 15];
	[menu setPosition:ADJUST_CCP(ccp(160,120))];
	[self addChild:menu z:501 tag:501];

}


-(void)selectTeam:(id)sender {
	GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	
	//	AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	int myTag = (int)[sender tag];
    
	selectedTeam = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@",[myDataHolder getImageBtForKey:myTag]]];
	
	if([mySingletons gameType]==0  ){
		[mySingletons setSpritePlayer1:myTag];
		GlobalCupSingleton *myCupSingletons = [GlobalCupSingleton sharedInstance]; //setup the global vars singleton instance
		NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[myDataHolder getTeamNameForKey:myTag], 
         @"Team", 
         nil];
        [Flurry logEvent:@"SELECT_PLAYER1"  withParameters:dictionary timed:NO];
		[myCupSingletons setSpritePlayer1:[mySingletons spritePlayer1]];
        
        NSUserDefaults *prefs  = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:myTag forKey:@"teamPlayer1"];
        [prefs synchronize];
	}
	if(([mySingletons gameType]==1 || [mySingletons gameType]==3 || [mySingletons gameType]==5 ) && selectedPlayer==1){
		[mySingletons setSpritePlayer1:myTag];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[myDataHolder getTeamNameForKey:myTag], 
                                    @"Team", 
                                    nil];
        [Flurry logEvent:@"SELECT_PLAYER1"  withParameters:dictionary timed:NO];

        NSUserDefaults *prefs  = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:myTag forKey:@"teamPlayer1"];
        [prefs synchronize];
	}
	if(([mySingletons gameType]==1 || [mySingletons gameType]==3 || [mySingletons gameType]==5 )  && selectedPlayer==2){
		[mySingletons setSpritePlayer2:myTag];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[myDataHolder getTeamNameForKey:myTag], 
                                    @"Team", 
                                    nil];
        [Flurry logEvent:@"SELECT_PLAYER2"  withParameters:dictionary timed:NO];
        
        NSUserDefaults *prefs  = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:myTag forKey:@"teamPlayer2"];
        [prefs synchronize];

	}
	
	if([mySingletons gameType]==2 || [mySingletons gameType]==22){
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[myDataHolder getTeamNameForKey:myTag], 
                                    @"Team", 
                                    nil];
        [Flurry logEvent:@"SELECT_PLAYER1"  withParameters:dictionary timed:NO];
       
		[mySingletons setSpritePlayer1:myTag];
		[mySingletons setSpritePlayer2:myTag];
		[mySingletons setPTeamID:myTag];

        NSUserDefaults *prefs  = [NSUserDefaults standardUserDefaults];
        [prefs setInteger:myTag forKey:@"teamPlayer1"];
        [prefs synchronize];
	}
	
	menuIsOpen = NO;
	[self removeChild:menu cleanup:YES];
	if(([mySingletons gameType]==1 || [mySingletons gameType]==3 || [mySingletons gameType]==5)  && selectedPlayer==1){
		myTeam = selectedTeam;
		[self addChild:myTeam z:500 tag:500];
		[myTeam setPosition:ADJUST_CCP(ccp(180,135))];
        
        [self addStarPlayer1:[myDataHolder getFactorByTeamKey:myTag] x:140 teamName:[myDataHolder getTeamNameForKey:myTag]];
        
        
	}else if(([mySingletons gameType]==1 || [mySingletons gameType]==3 || [mySingletons gameType]==5)  && selectedPlayer==2){
		opponentTeam = selectedTeam;
		[self addChild:opponentTeam z:501 tag:501];
		[opponentTeam setPosition:ADJUST_CCP(ccp(300,135))];
        
        [self addStarPlayer2:[myDataHolder getFactorByTeamKey:myTag] teamName:[myDataHolder getTeamNameForKey:myTag]];
        
        
	}else{	
		myTeam = selectedTeam;
		[self addChild:myTeam z:500 tag:500];
		[myTeam setPosition:ADJUST_CCP(ccp(240,135))];
        
        [self addStarPlayer1:[myDataHolder getFactorByTeamKey:myTag] x:200 teamName:[myDataHolder getTeamNameForKey:myTag]];

	}

	
	NSLog(@"SELECTED %i : %@", myTag, [myDataHolder getTeamNameForKey:myTag]);
}

-(void)startDelayLoading:(ccTime)ts{
	[self unschedule: @selector(startDelayLoading:)];
	[self goPlay];
	
}
-(void)loadingScreen{
   /* CCSprite * bg = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            bg = [CCSprite spriteWithFile:@"loading-ipadhd.jpg"];
        }else{
            //iPad screen
            bg = [CCSprite spriteWithFile:@"loading-ipad.jpg"];
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                    //iphone 5
                    bg = [CCSprite spriteWithFile:@"loading-hd.jpg"];
                }else{
                    //iphone retina screen
                    bg = [CCSprite spriteWithFile:@"loading-hd.jpg"];
                }
            }else{
                //iphone screen
                bg = [CCSprite spriteWithFile:@"loading.jpg"];
            }
        }
    }
    [bg setPosition:ADJUST_CCP(ccp(240, 160))];
    
	[self addChild:bg z:5000];
    */
}




-(void)goPlay{
	
	GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	GlobalCupSingleton *myCupSingletons = [GlobalCupSingleton sharedInstance]; //setup the global vars singleton instance
	if([mySingletons gameType]==0){
		TeamGroupsScene * gs = [TeamGroupsScene node];
		[[CCDirector sharedDirector] replaceScene:gs];
	}else if([mySingletons gameType]==1){
		GameScene * gs = [GameScene node];
		[[CCDirector sharedDirector] replaceScene:gs]; 
	}else if([mySingletons gameType]==2 || [mySingletons gameType]==22){
		
		int w = arc4random() % 2+1;
		if([myCupSingletons matchWeather]==0)[myCupSingletons setMatchWeather:w];
		
        
        
        [[[GameCenterManager sharedManager] multiplayerObject] beginListeningForMatches];
        RootViewController *rootController =(RootViewController*)[[(AppDelegate*)
                                                                   [[UIApplication sharedApplication]delegate] window] rootViewController];
        
        if([mySingletons gameType]==2){
            [[[GameCenterManager sharedManager] multiplayerObject] findMatchWithMinimumPlayers:2 maximumPlayers:2 onViewController:(UIViewController*)rootController];
        }else{
            
            [rootController openBrowserViewController];
        }
        
	}else if([mySingletons gameType]==3){
		GameScene * gs = [GameScene node];
		[[CCDirector sharedDirector] replaceScene:gs]; 
		
	}else if([mySingletons gameType]==5){
		PenaltyScene * ps = [PenaltyScene node];
		[[CCDirector sharedDirector] replaceScene:ps]; 
		
	}
	
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// Touch Events ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	//GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
}

-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
   // [touchSprite setPosition:location];
	if (menuIsOpen==NO && CGRectContainsPoint(CGRectMake(matchTime5min.position.x- 0.5*matchTime5min.contentSize.width, matchTime5min.position.y- 0.5*matchTime5min.contentSize.height,
									   matchTime5min.contentSize.width, matchTime5min.contentSize.height), location)) {
        [Flurry logEvent:@"SET_3_MIN" timed:NO];
		NSLog(@"3 MIN 180");
		[myCupSingleton setDefaultTimeLeft:180];
		//[myCupSingleton setDefaultTimeLeft:40];
		[self removeChild:spriteMatchTime cleanup:YES];
		spriteMatchTime = [CCSprite spriteWithSpriteFrameName:@"time_3min.png"];
		[spriteMatchTime setPosition:ADJUST_CCP(ccp(58,220))];
		[self addChild:spriteMatchTime z:210];
		
	}else if(menuIsOpen==NO && CGRectContainsPoint(CGRectMake(matchTime8min.position.x- 0.5*matchTime8min.contentSize.width, matchTime8min.position.y- 0.5*matchTime8min.contentSize.height,
											matchTime8min.contentSize.width, matchTime8min.contentSize.height), location)) {
        [Flurry logEvent:@"SET_5_MIN" timed:NO];
		NSLog(@"5 MIN 300");
		[myCupSingleton setDefaultTimeLeft:300];
		[self removeChild:spriteMatchTime cleanup:YES];
		spriteMatchTime = [CCSprite spriteWithSpriteFrameName:@"time_5min.png"];
		[spriteMatchTime setPosition:ADJUST_CCP(ccp(128,220))];
		[self addChild:spriteMatchTime z:210];
		
	}else if(menuIsOpen==NO && CGRectContainsPoint(CGRectMake(matchTime10min.position.x- 0.5*matchTime10min.contentSize.width, matchTime10min.position.y- 0.5*matchTime10min.contentSize.height,
											matchTime10min.contentSize.width, matchTime10min.contentSize.height), location)) {
        [Flurry logEvent:@"SET_8_MIN" timed:NO];
		NSLog(@"8 MIN 480");
		[myCupSingleton setDefaultTimeLeft: 480];
		[self removeChild:spriteMatchTime cleanup:YES];
		spriteMatchTime = [CCSprite spriteWithSpriteFrameName:@"time_8min.png"];
		[spriteMatchTime setPosition:ADJUST_CCP(ccp(198,220))];
		[self addChild:spriteMatchTime z:210];
		
	}else if(menuIsOpen==NO && CGRectContainsPoint(CGRectMake(matchWeatherRandom.position.x- 0.5*matchWeatherRandom.contentSize.width, matchWeatherRandom.position.y- 0.5*matchWeatherRandom.contentSize.height,
											matchWeatherRandom.contentSize.width, matchWeatherRandom.contentSize.height), location)) {
		[myCupSingleton setMatchWeather: 0];
        [Flurry logEvent:@"SET_RANDOM_WEATHER" timed:NO];
		[self removeChild:spriteMatchWeather cleanup:YES];
		spriteMatchWeather = [CCSprite spriteWithSpriteFrameName:@"weather_random.png"];
		[spriteMatchWeather setPosition:ADJUST_CCP(ccp(300,220))];
		[self addChild:spriteMatchWeather z:210];

	}else if(menuIsOpen==NO && CGRectContainsPoint(CGRectMake(matchWeatherFine.position.x- 0.5*matchWeatherFine.contentSize.width, matchWeatherFine.position.y- 0.5*matchWeatherFine.contentSize.height,
											matchWeatherFine.contentSize.width, matchWeatherFine.contentSize.height), location)) {
		[Flurry logEvent:@"SET_FINE_WEATHER" timed:NO];
		[myCupSingleton setMatchWeather: 1];
		[self removeChild:spriteMatchWeather cleanup:YES];
		spriteMatchWeather = [CCSprite spriteWithSpriteFrameName:@"weather_fine.png"];
		[spriteMatchWeather setPosition:ADJUST_CCP(ccp(369,220))];
		[self addChild:spriteMatchWeather z:110];
	}else if(menuIsOpen==NO && CGRectContainsPoint(CGRectMake(matchWeatherRainy.position.x- 0.5*matchWeatherRainy.contentSize.width, matchWeatherRainy.position.y- 0.5*matchWeatherRainy.contentSize.height,
											matchWeatherRainy.contentSize.width, matchWeatherRainy.contentSize.height), location)) {
		[Flurry logEvent:@"SET_RAINY_WEATHER" timed:NO];
		[myCupSingleton setMatchWeather: 2];
		[self removeChild:spriteMatchWeather cleanup:YES];
		spriteMatchWeather = [CCSprite spriteWithSpriteFrameName:@"weather_rainy.png"];
		[spriteMatchWeather setPosition:ADJUST_CCP(ccp(428,220))];
		[self addChild:spriteMatchWeather z:110];
	}else
		if(menuIsOpen==NO && CGRectContainsPoint(
												 CGRectMake(myTeam.position.x- 0.5*myTeam.contentSize.width,
															myTeam.position.y- 0.5*myTeam.contentSize.height,
															myTeam.contentSize.width, myTeam.contentSize.height), location)) {
			NSLog(@"INSIDE TOUCH");	
			if([mySingleton gameType]==1 || [mySingleton gameType]==3|| [mySingleton gameType]==5){
				selectedPlayer = 1;
                
			}
            [self removeChild:starHolder1 cleanup:YES];
            [self removeChild:myTeam cleanup:YES];
			[self buildTeamMenu];
		}else
			if(menuIsOpen==NO && CGRectContainsPoint(
													 CGRectMake(opponentTeam.position.x- 0.5*opponentTeam.contentSize.width,
																opponentTeam.position.y- 0.5*opponentTeam.contentSize.height,
																opponentTeam.contentSize.width, opponentTeam.contentSize.height), location)) {
				NSLog(@"INSIDE TOUCH");	
				if([mySingleton gameType]==1 || [mySingleton gameType]==3 || [mySingleton gameType]==5){
					selectedPlayer = 2;
                    
				}
                [self removeChild:starHolder2 cleanup:YES];
            	[self removeChild:opponentTeam cleanup:YES];
				[self buildTeamMenu];
				
		}else if(menuIsOpen==NO && CGRectContainsPoint(CGRectMake(spriteGoPlay.position.x- 0.5*spriteGoPlay.contentSize.width, spriteGoPlay.position.y- 0.5*spriteGoPlay.contentSize.height,
											spriteGoPlay.contentSize.width, spriteGoPlay.contentSize.height), location)) {
			[self loadingScreen];
			[self schedule: @selector(startDelayLoading:) interval: 1];

		}else if(CGRectContainsPoint(CGRectMake(spriteBackMenu.position.x- 0.5*spriteBackMenu.contentSize.width, spriteBackMenu.position.y- 0.5*spriteBackMenu.contentSize.height, spriteBackMenu.contentSize.width, spriteBackMenu.contentSize.height), location)) {
			[self goBackMenu];
		}else if(CGRectContainsPoint(CGRectMake(spriteSoundToogler.position.x- 0.5*spriteSoundToogler.contentSize.width, spriteSoundToogler.position.y- 0.5*spriteSoundToogler.contentSize.height, spriteSoundToogler.contentSize.width, spriteSoundToogler.contentSize.height), location)) {
			[self audioToogler];
		}
	//return kEventHandled;	
}


-(void)audioToogler{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		[mySingleton setAudioOn:NO];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
		sae.backgroundMusicVolume = 0.0f;
	}else{
		[mySingleton setAudioOn:YES];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];	
		sae.backgroundMusicVolume = 0.2f;	
	}
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
}

-(void)goBackMenu{
	MenuScene * ms = [MenuScene node];
	[[CCDirector sharedDirector] replaceScene:ms];
}


-(void)setMatchTimeMethod: (id)sender {
	GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	[mySingletons setGameType:0];
}

-(void)setWeatherMethod: (id)sender {
	GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	[mySingletons setGameType:1];
}

@end
