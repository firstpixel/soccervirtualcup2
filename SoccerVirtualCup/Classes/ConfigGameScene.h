//
//  ConfigGameScene.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 2/7/10.
//  Copyright 2010 Firstpixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface ConfigGameScene: CCScene {
CCSpriteBatchNode* _batchNode;
}
@end

@interface ConfigGameLayer : CCLayer {
	CCSprite*spriteBackMenu;
	CCSprite*continueGameCCSprite;
	CCSprite*newGameCCSprite;
    
}
@end
