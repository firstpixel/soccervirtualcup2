//
//  AppDelegate.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

#import "cocos2d.h"
#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "AppDelegate.h"
#import "OpenGL_Internal.h"
#import "MKStoreManager.h"
#import "SimpleAudioEngine.h"
#import "GameCredit.h"
#import "GameConfig.h"
#import "RootViewController.h"
#import "GameCenterManager.h"
#import "IntroScene.h"

#import "GameKit/GameKit.h"

//FLURRY ANALYTICS
#import "Flurry.h"
#pragma mark -
@implementation AppDelegate

#pragma mark View Controller Related Methods
@synthesize highscores;
@synthesize leaderboardMatchWinner;
@synthesize scoredPoint;
@synthesize leaderboardPlayTime;
@synthesize window=window_, navController=navController_, director=director_;


- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    
    
    // Use Firebase library to configure APIs
    [FIRApp configure];
    [[FIRAnalyticsConfiguration sharedInstance] setAnalyticsCollectionEnabled:NO];
    
    // Initialize Google Mobile Ads SDK
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-4523467891811991~1182179890"];
    
    
    // Begin a user session. Must not be dependent on user actions or any prior network requests.
    [Chartboost startWithAppId:@"57bce1f043150f7f6cbb3941" appSignature:@"b4484d6d7c3585ceca92f0b58ad7050f81c81653" delegate:self];
    
    
    
    [MKStoreManager sharedManager];
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    
    
    //[Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"8MRV9ZSNV5ND67DCQC7K"];
    [FlurryAds initialize:window_.rootViewController];
    [Flurry setBackgroundSessionEnabled:YES];
    [Flurry setEventLoggingEnabled:YES];
    [Flurry logEvent:@"OPEN_GAME" timed:YES];
    
    
    [FIRAnalytics logEventWithName:@"OPEN_GAME"
                        parameters:@{
                                     @"name": @"open"
                                     }];
    
    
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	[mySingleton clearData];
	[mySingleton setFieldValueAtPos:3 ToValue:1];
	
    
	    // Main Window
    window_ = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Director
    director_ = (CCDirectorIOS*)[CCDirector sharedDirector];
    [director_ setDisplayStats:NO];
    [director_ setAnimationInterval:1.0/60];
    
    // GL View
    CCGLView *__glView = [CCGLView viewWithFrame:[window_ bounds]
                                     pixelFormat:kEAGLColorFormatRGB565
                                     depthFormat:0 /* GL_DEPTH_COMPONENT24_OES */
                              preserveBackbuffer:NO
                                      sharegroup:nil
                                   multiSampling:NO
                                 numberOfSamples:0
                          ];
    
    __glView.multipleTouchEnabled = YES;
    [director_ setView:__glView];
    [director_ setDelegate:self];
    //director_.wantsFullScreenLayout = YES;
    
    // 2D projection
    [director_ setProjection:kCCDirectorProjection2D];
    //	[director setProjection:kCCDirectorProjection3D];
    
    // Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
    if( ! [director_ enableRetinaDisplay:YES] )
        CCLOG(@"Retina Display Not supported");
    
    // Default texture format for PNG/BMP/TIFF/JPEG/GIF images
    // It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
    // You can change anytime.
    [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
    
    // Assume that PVR images have premultiplied alpha
    [CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
    
    CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
    [sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
    [sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
    [sharedFileUtils setiPadSuffix:@"-ipad"];					// Default on iPad is "ipad"
    [sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];	// Default on iPad RetinaDisplay is "-ipadhd"
    
    
    
    // Navigation Controller
    navController_ = [[RootViewController alloc] initWithRootViewController:director_];
    navController_.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    // AddSubView doesn't work on iOS6
    //	[window_ addSubview:navController_.view];
    [window_ setRootViewController:navController_];
    
    [window_ makeKeyAndVisible];
    
    //[[MKStoreManager sharedManager] removeAllKeychainData];
	
    // Setup GameCenter
    [[GameCenterManager sharedManager] setupManager];
    
    // Run
    [director_ pushScene: [IntroScene scene]];
    [director_ startAnimation];
    
	return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    [[CCDirector sharedDirector] stopAnimation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[CCDirector sharedDirector] startAnimation];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    [[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
    // TIP:
    // Stop the director mainloop.
    // Callback only called in iOS >=4 and in multitasking devices
    [[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
    // TIP:
    // Resume the director mainloop.
    // Callback only called in iOS >=4 and in multitasking devices
    [[CCDirector sharedDirector] startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    CCDirector *director = [CCDirector sharedDirector];
    [director.view removeFromSuperview];
    [director end];
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
    [[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}


- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
{
    return UIInterfaceOrientationMaskLandscape;
}

-(NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscape;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeLeft;
}


- (void) dealloc
{
    
    [[CCDirector sharedDirector] end];
    [window_ release];
    [navController_ release];
    [super dealloc];
}


/*
 * didInitialize
 *
 * This is used to signal when Chartboost SDK has completed its initialization.
 *
 * status is YES if the server accepted the appID and appSignature as valid
 * status is NO if the network is unavailable or the appID/appSignature are invalid
 *
 * Is fired on:
 * -after startWithAppId has completed background initialization and is ready to display ads
 */
- (void)didInitialize:(BOOL)status {
    NSLog(@"didInitialize");
    // chartboost is ready
    [Chartboost cacheRewardedVideo:CBLocationMainMenu];
    [Chartboost cacheMoreApps:CBLocationHomeScreen];

}





+(AppDelegate *)get {
	return  (AppDelegate *) [[UIApplication sharedApplication] delegate];

}

static void uncaughtExceptionHandler(NSException *exception) { [Flurry logError:@"Uncaught" message:@"Crash!" exception:exception]; }




/* UPDATE GAME LEADERBOARDS */

-(void)onFailure{
    NSLog(@"ON FAILURE!!");

};

-(void)onSuccess{
    NSLog(@"ON  SUCCESS!!");
};




- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // You can send here, for example, an asynchronous HTTP request to your web-server to store this deviceToken remotely.
    NSLog(@"Did register for remote notifications: %@", deviceToken);
   
    
    if(![[GlobalCupSingleton sharedInstance] hasTokenID]){
        NSNumber* tokenID = [[GlobalCupSingleton sharedInstance] returnTokenID];
        NSLog(@"my token ID: %li", [tokenID longValue]);
        
        NSString *phoneName = [[[UIDevice currentDevice] name] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSString *phoneUniqueIdentifier = [[UIDevice currentDevice].identifierForVendor.UUIDString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSString *model = [[[UIDevice currentDevice] model] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSString *localizedModel = [[[UIDevice currentDevice] localizedModel] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSString *systemName = [[[UIDevice currentDevice] systemName] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSString *systemVersion = [[[UIDevice currentDevice] systemVersion] stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
        NSString * tokenAsString = [[[deviceToken description]  stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]  stringByReplacingOccurrencesOfString:@" " withString:@"-"];
        NSString *urlstr = [[NSString alloc] initWithFormat:@"http://www.firstpixel.com/soccervirtualcup/apns/user.php?token=%@&game=soccervirtualcup&phoneName=%@&phoneUniqueIdentifier=%@&model=%@&localizedModel=%@&systemName=%@&systemVersion=%@",tokenAsString,phoneName,phoneUniqueIdentifier,model,localizedModel,systemName,systemVersion];
        
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlstr] cachePolicy:0 timeoutInterval:5];
        NSURLResponse* response=nil;
        NSError* error=nil;
        NSData* data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString* responseString = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        
        NSLog(@"Server querystring: %@", urlstr);
        if( responseString )
        {
            NSLog(@"Server returned Text=%@", responseString);
            NSLog(@"%@",[responseString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"userid=&"]] );
            int tokenIDToSet = [[responseString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"userid=&"]] intValue];
            [[GlobalCupSingleton sharedInstance] saveTokenID:tokenIDToSet];
        }else{
            NSLog(@"Server returned Error = %@", error);
        }
        [urlstr release];
    }else{
        NSNumber* tokenID = [[GlobalCupSingleton sharedInstance] returnTokenID];
        NSLog(@"my token ID: %i", [tokenID intValue]);
    }
}


@end
