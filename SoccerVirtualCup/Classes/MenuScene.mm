//
//  MenuScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

#import "MenuScene.h"
#import "ConfigGameScene.h"
#import "GameScene.h"
#import "SelectionScreen1Player.h"
#import "GameCredit.h"
#import "GameHelp.h"
#import "TournamentWinnerScene.h"
#import "TournamentSecondScene.h"
#import "TournamentThirdScene.h"
#import "GlobalCupSingleton.h"
#import "SimpleAudioEngine.h"
#import "CDAudioManager.h"
#import "MKStoreManager.h"
#import "ShoppingCart.h"
#import "MatchDrawSceneSingle.h"
#import "DeviceSettings.h"

#import "AfterGameMatchsScene.h"
#import "AppDelegate.h"
#import "Flurry.h"
#import "RootViewController.h"


//#import "BallLayer.h"


@implementation MenuScene
- (id) init {
    self = [super init];
    if (self != nil) {
        NSLog(@"IS_IPHONE           : %s",  IS_IPHONE? "Yes" : "No");
        NSLog(@"IS_RETINA           : %s",  IS_RETINA? "Yes" : "No");
        NSLog(@"IS_RETINA3X         : %s",  IS_RETINA3X? "Yes" : "No");
        NSLog(@"IS_IPHONE4_OR_LESS  : %s",  IS_IPHONE4_OR_LESS? "Yes" : "No");
        NSLog(@"IS_IPHONE5          : %s",  IS_IPHONE5? "Yes" : "No");
        NSLog(@"IS_IPHONE6          : %s",  IS_IPHONE6? "Yes" : "No");
        NSLog(@"IS_IPHONE6P         : %s",  IS_IPHONE6P? "Yes" : "No");
        NSLog(@"IS_IPAD             : %s",  IS_IPAD? "Yes" : "No");
        NSLog(@"IS_IPADHD           : %s",  IS_IPADHD? "Yes" : "No");
        NSLog(@"SCREEN_MAX_LENGTH   : %f",  SCREEN_MAX_LENGTH );
        NSLog(@"DBL_EPSILON         : %f",  DBL_EPSILON );
        NSLog(@"SCALE               : %f", [[UIScreen mainScreen] scale]);
        
        [self setupBatchNode];
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        NSLog(@"HEIGHT              : %f", winSize.height);
        NSLog(@"WIDTH               : %f", winSize.width);
        NSLog(@"REAL HEIGHT         : %f", [ [ UIScreen mainScreen ] bounds ].size.height);
        NSLog(@"REAL WIDTH          : %f", [ [ UIScreen mainScreen ] bounds ].size.width);
   
        
        //Add Random Background
        CCSprite * bg;
        int i = arc4random() % 2 + 1;
        bg = [CCSprite spriteWithFile:[NSString stringWithFormat:@"gamestart%i.jpg",i]];
        [bg setPosition:ccp(winSize.width/2, winSize.height/2)];
        [self addChild:bg z:0];
        [MKStoreManager sharedManager];
        
        
        NSUserDefaults *prefs  = [NSUserDefaults standardUserDefaults];
        
        if([MKStoreManager isFeaturePurchased:kFeatureAustraliaId]){
            NSLog(@"AUSTRALIA");
            int index = 1;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureBrazilId]){
            NSLog(@"BRAZIL");
            int index = 3;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureCroatiaId]){
            NSLog(@"CROATIA");
            int index = 6;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureEnglandId]){
            NSLog(@"ENGLAND");
            int index = 9;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureFranceId]){
            NSLog(@"FRANCE");
            int index = 11;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureItalyId]){
            NSLog(@"ITALY");
            int index = 41;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureJapanId]){
            NSLog(@"JAPAN");
            int index = 15;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureMexicoId]){
            NSLog(@"MEXICO");
            int index = 17;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureNetherlandsId]){
            NSLog(@"NETHERLANDS");
            int index = 42;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureNigeriaId]){
            NSLog(@"NIGERIA");
            int index = 19;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeaturePortugalId]){
            NSLog(@"PORTUGAL");
            int index = 23;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureRussiaId]){
            NSLog(@"RUSSIA");
            int index = 24;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureSwitzerlandId]){
            NSLog(@"SWISS");
            int index = 29;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureUruguayId]){
            NSLog(@"URUGUAY");
            int index = 31;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        if([MKStoreManager isFeaturePurchased:kFeatureUSAId]){
            NSLog(@"USA");
            int index = 43;
            [prefs  setInteger:5 forKey:[NSString stringWithFormat:@"svc.teamFactor%i",index]];
        }
        
        
        
        
        
        
        //Check if has featureAPurchased
        if([MKStoreManager isFeaturePurchased:kFeatureAId]){
		    
		    [Flurry endTimedEvent:@"PLAY_WORLDCUP" withParameters:nil];
            [Flurry endTimedEvent:@"PLAY_VERSUS" withParameters:nil];
            [Flurry endTimedEvent:@"PLAY_BLUETOOTH" withParameters:nil];
            [Flurry endTimedEvent:@"PLAY_SINGLE" withParameters:nil];
            [Flurry endTimedEvent:@"PLAY_PENALTY" withParameters:nil];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                [Flurry logEvent:@"IPAD" timed:NO];
            }else{
                [Flurry logEvent:@"IPHONE" timed:NO];
            }
            [self removeBanner];
            
		}else{
            
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                [Flurry logEvent:@"IPAD" timed:NO];
            }else{
                [Flurry logEvent:@"IPHONE" timed:NO];
            }
            
		}
        
        [Flurry logEvent:@"MENU_SCREEN" timed:YES];
        NSLog(@"FULL VERSION:  %s", [MKStoreManager isFeaturePurchased:kFeatureAId]?"Yes":"No");
		//isTouchEnabled = YES;
		if([MKStoreManager isFeaturePurchased:kFeatureAId])
        //if(YES)
        {
            [CCMenuItemFont setFontSize:HD_PIXELS(16)];
			[CCMenuItemFont setFontName:@"HelveticaNeue"];
			CCMenuItemFont *startSingle = [CCMenuItemFont itemWithString:@"Single Match" target:self selector:@selector(startGameSingle:)];
			CCMenuItemFont *startWorldCup = [CCMenuItemFont itemWithString:@"World Cup" target:self selector:@selector(startGameWorldCup:)];
			CCMenuItemFont *startMulti = [CCMenuItemFont itemWithString:@"Versus Mode" target:self selector:@selector(startGameMulti:)];
			CCMenuItemFont *startPenalty = [CCMenuItemFont itemWithString:@"Penalty" target:self selector:@selector(startGamePenalty:)];
            CCMenuItemFont *startMultiplayer = [CCMenuItemFont itemWithString:@"Multiplayer" target:self selector:@selector(startGameMultiGameCenter:)];
            //CCMenuItemFont *startMultiBlueTooth = [CCMenuItemFont itemWithString:@"Multiplayer Bluetooth" target:self selector:@selector(startGameMultiBlueTooth:)];
            CCMenuItemFont *startStore = [CCMenuItemFont itemWithString:@"Store" target:self selector:@selector(buyFullGame:)];
			
			CCMenuItemFont *help = [CCMenuItemFont itemWithString:@"Help" target:self selector:@selector(help:)];
			CCMenuItemFont *score = [CCMenuItemFont itemWithString:@"Scoreboard" target:self selector:@selector(scoreBoard:)];
			CCMenu *menu = [CCMenu menuWithItems:startSingle, startWorldCup, startMultiplayer, startMulti, startPenalty, score, startStore, help, nil];
			[menu alignItemsVerticallyWithPadding: HD_PIXELS(9)];
			[menu setPosition:ADJUST_CCP(ccp(205,165))];
			[self addChild:menu];
            [Flurry logEvent:@"PAYED" timed:NO];

            NSLog(@"REMOVE BANNER");
            [self removeBanner];

            
        }else{
			
            [CCMenuItemFont setFontSize:HD_PIXELS(17)];
            [CCMenuItemFont setFontName:@"HelveticaNeue"];
			CCMenuItemFont *startSingle = [CCMenuItemFont itemWithString:@"Single Match" target:self selector:@selector(startGameSingle:)];
            CCMenuItemFont *startStore = [CCMenuItemFont itemWithString:@"Store" target:self selector:@selector(buyFullGame:)];
			CCMenuItemFont *startMulti = [CCMenuItemFont itemWithString:@"Versus Mode" target:self selector:@selector(startGameMulti:)];
            CCMenuItemFont *startPenalty = [CCMenuItemFont itemWithString:@"Penalty" target:self selector:@selector(startGamePenalty:)];
			
            CCMenuItemFont *startMultiplayer = [CCMenuItemFont itemWithString:@"Multiplayer" target:self selector:@selector(startGameMultiGameCenter:)];
            //CCMenuItemFont *startMultiBlueTooth = [CCMenuItemFont itemWithString:@"Multiplayer Bluetooth" target:self selector:@selector(startGameMultiBlueTooth:)];
            CCMenuItemFont *help = [CCMenuItemFont itemWithString:@"Help" target:self selector:@selector(help:)];
			//[help setAnchorPoint:ccp(0,0.5)];
			CCMenuItemFont *score = [CCMenuItemFont itemWithString:@"Scoreboard" target:self selector:@selector(scoreBoard:)];
			//CCMenuItemFont *score = [CCMenuItemFont itemWithString:@"WORLDCUP" target:self selector:@selector(startGameWorldCup:)];
			//[score setAnchorPoint:ccp(0,0.5)];
			//COMMENT HERE
			CCMenu *menu = [CCMenu menuWithItems:startSingle, startStore, startMultiplayer, startMulti, startPenalty, score, help,  nil];
			//UNCOMMENT HERE
			//CCMenu *menu = [CCMenu menuWithItems:startSingle, startWorldCup, startPenalty, startMulti, help, score,  nil];
			[menu alignItemsVerticallyWithPadding: HD_PIXELS(10)];
			[menu setPosition:ADJUST_CCP(ccp(205,170))];
			[self addChild:menu];
            [Flurry logEvent:@"FREE" timed:NO];
            
            NSLog(@"ADD BANNER");
            AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            [(RootViewController*)[app navController] addBanner];
            
		}
      
        spriteSoundToogler = [CCSprite spriteWithSpriteFrameName:@"sound.png"];
		[spriteSoundToogler setPosition:ADJUST_CCP(ccp(460,50))];
		[self addChild:spriteSoundToogler z:1001 tag:1001];
		
		GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
		if([mySingleton audioOn]==YES){
			SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
            [sae preloadBackgroundMusic:@"loop.mp3"];
			if (sae != nil && [sae isBackgroundMusicPlaying]==NO) {
				[sae playBackgroundMusic:@"loop.mp3" loop:NO];
				sae.backgroundMusicVolume = 0.2f;
			}
		}

    }
    return self;
}

-(void)removeBanner {
    NSLog(@"REMOVE BANNER");
    AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [(RootViewController*)[app navController] removeBanner];
}


- (void)setupBatchNode {
    NSString *spritesheetPvrCcz = @"spritesheet.pvr.ccz";
    NSString *spritesheetPlist = @"spritesheet.plist";
    
    NSString *gametablePvrCcz = @"gametable.pvr.ccz";
    NSString *gametablePlist = @"gametable.plist";
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            spritesheetPvrCcz = @"spritesheet-ipadhd.pvr.ccz";
            spritesheetPlist = @"spritesheet-ipadhd.plist";
            gametablePvrCcz = @"gametable-ipadhd.pvr.ccz";
            gametablePlist = @"gametable-ipadhd.plist";
        }else{
            //iPad screen
            spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
            spritesheetPlist = @"spritesheet-hd.plist";
            gametablePvrCcz = @"gametable-hd.pvr.ccz";
            gametablePlist = @"gametable-hd.plist";
        }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                spritesheetPvrCcz = @"spritesheet-hd.pvr.ccz";
                spritesheetPlist = @"spritesheet-hd.plist";
                
                gametablePvrCcz = @"gametable-hd.pvr.ccz";
                gametablePlist = @"gametable-hd.plist";
            }else{
                //iphone screen
                spritesheetPvrCcz = @"spritesheet.pvr.ccz";
                spritesheetPlist = @"spritesheet.plist";
                
                gametablePvrCcz = @"gametable.pvr.ccz";
                gametablePlist = @"gametable.plist";
            }
        }
    }
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesheetPvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesheetPlist];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:gametablePvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:gametablePlist];
}
-(void)dealloc{
    //[[CCTextureCache sharedTextureCache] removeUnusedTextures];
    
    [super dealloc];
}

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [MenuScene node];
	[scene addChild:child];
	return scene;
}

-(void)buyFullGame: (id)sender{
    [Flurry endTimedEvent:@"MENU_SCREEN" withParameters:nil];
    ShoppingCart  * sh = [ShoppingCart node];
    [self removeBanner];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:sh]];

}
-(void)audioToogler{
	GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
	if([mySingleton audioOn]==YES){
		[mySingleton setAudioOn:NO];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
        
		sae.backgroundMusicVolume = 0.0f;
	}else{
		[mySingleton setAudioOn:YES];
		SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];	
        sae.backgroundMusicVolume = 0.2f;	
	}
}
-(void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	if(CGRectContainsPoint(CGRectMake(spriteSoundToogler.position.x- 0.5*spriteSoundToogler.contentSize.width, spriteSoundToogler.position.y- 0.5*spriteSoundToogler.contentSize.height, spriteSoundToogler.contentSize.width, spriteSoundToogler.contentSize.height), location)) {
		[self audioToogler];
	}
}
-(void)startGameWorldCup: (id)sender {
    [Flurry endTimedEvent:@"MENU_SCREEN" withParameters:nil];
    [self removeBanner];
    ConfigGameScene  * gs = [ConfigGameScene node];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:gs]];
    
    /*GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	[mySingletons setGameType:0];
	[mySingletons clearData];
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	[myCupSingleton clearArraysData];
    SelectionScreen1Player * gs = [SelectionScreen1Player node];
    [[Director sharedDirector] replaceScene:gs];*/
}
-(void)startGamePenalty: (id)sender {
    [Flurry endTimedEvent:@"MENU_SCREEN" withParameters:nil];
    [self removeBanner];
    GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	[mySingletons setGameType:5];
	[mySingletons clearData];
	GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
	[myCupSingleton clearArraysData];
    SelectionScreen1Player * gs = [SelectionScreen1Player node];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:gs]];
}
-(void)startGameSingle: (id)sender {
    [Flurry endTimedEvent:@"MENU_SCREEN" withParameters:nil];
    [self removeBanner];
    GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	[mySingletons setGameType:3];
	SelectionScreen1Player * gs = [SelectionScreen1Player node];
    //AfterGameMatchsScene  * gs = [AfterGameMatchsScene node];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:gs]];
}
-(void)startGameMulti: (id)sender {
    [Flurry endTimedEvent:@"MENU_SCREEN" withParameters:nil];
    [self removeBanner];

    GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
	[mySingletons setGameType:1];
	SelectionScreen1Player * gs = [SelectionScreen1Player node];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:gs]];
}


-(void)startGameMultiGameCenter: (id)sender {
    [Flurry endTimedEvent:@"MENU_SCREEN" withParameters:nil];
    [self removeBanner];

    BOOL available = [[GameCenterManager sharedManager] checkGameCenterAvailability:YES];
    if (available) {
        //[self.navigationController.navigationBar setValue:@"GameCenter Available" forKeyPath:@"prompt"];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Multiplayer"
                                                        message:@"No GameCenter player found, to play multiplayer you need to log in to the Apple Game Center, go to settings on your device."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    GKLocalPlayer *player = [[GameCenterManager sharedManager] localPlayerData];
    if (player) {
        if ([player isUnderage] == NO) {
            
            //actionBarLabel.title = [NSString stringWithFormat:@"%@ signed in.", player.displayName];
            //playerName.text = player.displayName;
            //playerStatus.text = @"Player is not underage";
            [[GameCenterManager sharedManager] localPlayerPhoto:^(UIImage *playerPhoto) {
                //playerPicture.image = playerPhoto;
            }];
            
            [self removeBanner];
            GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
            [mySingletons setGameType:2];
            SelectionScreen1Player * gs = [SelectionScreen1Player node];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:gs]];
            
            
        } else {
            //playerName.text = player.displayName;
            //playerStatus.text = @"Player is underage";
            //actionBarLabel.title = [NSString stringWithFormat:@"Underage player, %@, signed in.", player.displayName];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Multiplayer"
                                                            message:[NSString stringWithFormat:@"Underage player, %@, signed in.", player.displayName]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    } else {
        //actionBarLabel.title = [NSString stringWithFormat:@"No GameCenter player found."];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Multiplayer"
                                                        message:@"No GameCenter player found, to play multiplayer you need to log in to the Apple Game Center, go to settings on your device."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    
    //[[[UIApplication sharedApplication] delegate] startMultiplayer];
}
-(void)startGameMultiBlueTooth: (id)sender {
    [Flurry endTimedEvent:@"MENU_SCREEN" withParameters:nil];
    [self removeBanner];
    GlobalSingleton *mySingletons = [GlobalSingleton sharedInstance]; //setup the global vars singleton instance
    [mySingletons setGameType:22];
    SelectionScreen1Player * gs = [SelectionScreen1Player node];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:gs]];
    
}


-(void)help: (id)sender {
    [Flurry endTimedEvent:@"MENU_SCREEN" withParameters:nil];
    [self removeBanner];

    if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
        AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
        [(RootViewController*)[app navController] addInterstitial];
    }
    
    GameHelpScene  * gh = [GameHelpScene node];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFlipAngular transitionWithDuration:0.5f scene:gh]];
}


-(void)scoreBoard:(id)sender{

/*    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
        if(!localPlayer.isAuthenticated){
            NSLog(@"TRY TO AUTHENTICATE AGAIN");
            GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
            
            [gkHelper authenticateLocalPlayer];
            NSLog(@"SHOW LEADERBOARD");
            [gkHelper showLeaderboard];
        }else{
            GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
            NSLog(@"SHOW LEADERBOARD2");
            
            [gkHelper showLeaderboard];

        }
 */
    /***
     OPEN GAME CENTER LEADERBOARD
     ***/
    RootViewController *rootController =(RootViewController*)[[(AppDelegate*)
                                                               [[UIApplication sharedApplication]delegate] window] rootViewController];
    
    [[GameCenterManager sharedManager] presentLeaderboardsOnViewController:rootController withLeaderboard:nil];
    
    
    }
@end
