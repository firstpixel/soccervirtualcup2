//
//  IntroNode.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 07/01/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "IntroScene.h"
#import "MenuScene.h"
#import "DeviceSettings.h"
//
// This is an small Scene that makes the trasition smoother from the Defaul.png image to the menu scene
//

@implementation IntroScene

+(id) scene {
	CCScene *s = [CCScene node];
	id node = [IntroScene node];
	[s addChild:node];
	return s;
}

-(id) init {
	if( (self=[super init])) {
		// Load all the sprites/platforms now
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *background;
        
        if(IS_IPAD) {
            background = [CCSprite spriteWithFile:@"Default-1024x768.png"];
        } else if(IS_IPADHD) {
            background = [CCSprite spriteWithFile:@"Default-2048x1536.png"];
        } else if(IS_IPHONE6P) {
            background = [CCSprite spriteWithFile:@"Default-2208x1242.png"];
        } else if(IS_IPHONE6) {
            background = [CCSprite spriteWithFile:@"Default-750x1334.png"];
            background.rotation = 90;
        } else if(IS_IPHONE5) {
            background = [CCSprite spriteWithFile:@"Default-640x1136.png"];
            background.rotation = 90;
        }else{
            background = [CCSprite spriteWithFile:@"Default-640x960.png"];
            background.rotation = 90;
        }
        background.position = ccp(size.width/2, size.height/2);
        
        // add the label as a child to this Layer
        [self addChild:background];
		[self schedule:@selector(wait1second:) interval:1];
	}
	return self;
}

-(void)dealloc{
    [super dealloc];
}



-(void) wait1second:(ccTime)dt
{
    [[CCDirector sharedDirector] replaceScene: [CCTransitionProgressInOut transitionWithDuration:1.0f scene:[MenuScene scene]]];
    [self unschedule: @selector(wait1second:)];
}
@end
