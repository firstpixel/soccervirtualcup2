//
//  TournamentScene.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/6/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "GlobalSingleton.h"
#import "DataHolder.h"


@interface TournamentScene : CCScene {}
@end
@interface TournamentSceneLayer : CCLayer {
	CCSprite*spriteGoPlay;
	DataHolder *myDataHolder;
	CCSprite* infoHolderSprite;
	CCSprite* flag1Sprite;
	CCSprite* flag2Sprite;
	CCLabelTTF* teamName1Label;
	CCLabelTTF* teamName2Label;
	CCLabelTTF* scoreBoardLabel;
	BOOL myTeamStayOnTournament;
	int winner;
	int second;
	int third;
	int champ;
}

@property (nonatomic, readwrite) BOOL myTeamStayOnTournament;
@property (nonatomic, readwrite) int winner, second, third, champ;
-(void)displayTournament:(int)match team:(int)t1 versus:(int)t2 p1Score:(int)p1Score p2Score:(int)p2Score;

-(void)goPlay;


@end