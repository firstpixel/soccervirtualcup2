//
//  LoopingMenu.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 11/8/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "LoopingMenu.h"
#import "InputController.h"
#import "DeviceSettings.h"

@interface CCMenu (Private)

// returns touched menu item, if any, implemented in Menu.m
-(CCMenuItem *) itemForTouch: (UITouch *) touch;
@end

@implementation LoopingMenu

#pragma mark -
#pragma mark Menu
-(void)updateAnimation{
	for(int i=0; i<(int)[children_ count]; i++){
		CCMenuItem* menuItem = [children_ objectAtIndex:i];
        float kFactor = 0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
             kFactor =  ((menuItem.position.x+self.position.x-480)/1000);
        }else{
             kFactor =  ((menuItem.position.x+self.position.x-240)/500);
            
        }
        
        
        if (kFactor < 0) {kFactor *= -1;};  
		[menuItem setScale:1-kFactor];
		
	}
	
}



-(void) alignItemsVerticallyWithPadding:(float)padding
{
    for(int i=0; i<(int)[children_ count]; i++){
    CCMenuItem* menuItem = [children_ objectAtIndex:i];
        [menuItem setPosition:ccp( menuItem.position.x, menuItem.position.y)];
    }
	[self alignItemsHorizontallyWithPadding:padding];
}

-(void) alignItemsHorizontallyWithPadding:(float)padding
{
	[super alignItemsHorizontallyWithPadding:padding];
}

-(void) registerWithTouchDispatcher
{
	[self updateAnimation];

	[[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:INT_MIN+1 swallowsTouches:false];
}


-(CGPoint) icDistance:(int)finger  t:(NSSet*)touches e:(UIEvent*)event
{
	NSSet *allTouches = [event allTouches];
	
	if(finger == 0 || finger > (int)[allTouches count])
		return CGPointZero;
	
	UITouch *touch = [[allTouches allObjects] objectAtIndex:finger - 1];
	
	CGPoint start1 = [touch previousLocationInView:[touch view]];
	CGPoint end1 = [touch locationInView:[touch view]];
	start1 = _icRotateRealWorld(start1);
	end1 = _icRotateRealWorld(end1);
	
	float xDistance = ( fabs(start1.x - end1.x));
	float yDistance =  ( fabs(start1.y - end1.y));
	return CGPointMake(xDistance, yDistance);
}

-(bool) icWasSwipeLeft:(NSSet *) touches  e:(UIEvent *)event
{
	NSSet *allTouches = [event allTouches];
	
	if(1 != [allTouches count])
		return false;
	
	UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
	
	CGPoint start = [touch previousLocationInView:[touch view]];
	CGPoint end = [touch locationInView:[touch view]];
	start = _icRotateRealWorld(start);
	end = _icRotateRealWorld(end);
	
	if(start.x > end.x)
	{
		return true;
	}
	
	return false;
}

-(bool) icWasSwipeRight:(NSSet *) touches e:(UIEvent *) event
{
	NSSet *allTouches = [event allTouches];
	
	if(1 != [allTouches count])
		return false;
	
	UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
	
	CGPoint start = [touch previousLocationInView:[touch view]];
	CGPoint end = [touch locationInView:[touch view]];
	start = _icRotateRealWorld(start);
	end = _icRotateRealWorld(end);
	
	if(start.x < end.x)
	{
		return true;
	}
	
	return false;
}


#pragma mark -
#pragma mark Touches
-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
	moving = false;
	if( state_ != kCCMenuStateWaiting || !visible_ )
		return NO;
    selectedItem_ = [super itemForTouch:touch];
    [selectedItem_ selected];
    
	if( selectedItem_ ) {
		state_ = kCCMenuStateTrackingTouch;
		return YES;
	}
	return NO;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
	if(!moving)
		[super ccTouchEnded:touch withEvent:event];
	else
		[super ccTouchCancelled:touch withEvent:event];
	moving = false;
}

-(void) ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
	[super ccTouchCancelled:touch withEvent:event];
	moving = false;
}



-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
	NSMutableSet* touches = [[[NSMutableSet alloc] initWithObjects:touch, nil] autorelease];
	
	CGPoint distance = [self icDistance:1 t:touches e:event];
	
	if([self icWasSwipeLeft:touches e:event] && distance.y < distance.x)
	{
		moving = true;
		[selectedItem unselected];
		[self setPosition:ccpAdd([self position], ccp(-distance.x, 0))];
		
		CCMenuItem* leftItem = [children_ objectAtIndex:0];
		if([leftItem position].x + [self position].x + [leftItem contentSize].width / 2.0  < 0)
		{
			[leftItem retain];
			[children_ removeObjectAtIndex:0];
			CCMenuItem* lastItem = [children_ objectAtIndex:[children_ count] - 1];
			[leftItem setPosition:ccpAdd([lastItem position], ccp([lastItem contentSize].width / 2.0 + [leftItem contentSize].width / 2.0, 0))];
			[children_ addObject:leftItem];
			[leftItem autorelease];
		}
	} 
	else if([self icWasSwipeRight:touches e:event] && distance.y < distance.x)
	{
		moving = true;
		[selectedItem unselected];
		[self setPosition:ccpAdd([self position], ccp(distance.x, 0))];
		
		CCMenuItem* lastItem = [children_ objectAtIndex:[children_ count] - 1];
		if([lastItem position].x + [self position].x - [lastItem contentSize].width / 2.0 > ADJUST_X(480))
		{
			[lastItem retain];
			[children_ removeObjectAtIndex:[children_ count] - 1];
			CCMenuItem* firstItem = [children_ objectAtIndex:0];
			[lastItem setPosition:ccpSub([firstItem position], ccp([firstItem contentSize].width / 2.0 + [lastItem contentSize].width / 2.0, 0))];
			[children_ insertObject:lastItem atIndex:0];
			[lastItem autorelease];
		}
	}
	else if(!moving)
	{
		[super ccTouchMoved:touch withEvent:event];
	}
	
	[self updateAnimation];
}







@end
