//
//  CacheController.m
//  eCast
//
//  Created by Ricardo Menezes on 01/01/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CacheController.h"

@implementation CacheController
bool filewasupdated;

- (id) init {
	self = [super init];
	if (self != nil) {
	}
	return self;
}

- (void)saveImage:(UIImage*)imagem image:(NSString*)imageName {
	NSData *imageData = UIImagePNGRepresentation(imagem); //convert image into .png format.
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
	[fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
	NSLog(@"Saving:%@", fullPath);
}

- (UIImage*)loadImage:(NSString*)imageName {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]];
	return [UIImage imageWithContentsOfFile:fullPath];	
}

- (NSData*)loadXML:(NSString*)fileName {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	return [NSData dataWithContentsOfFile:fullPath];	
}

- (BOOL)saveXML:(NSData*)data file: (NSString*)fileName {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	NSError* error;
    NSString *currentfile = [[NSString alloc] initWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
    
	NSString *newfile = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];  
	
	[self removeFile:@"news.xml"];
    [data writeToFile:fullPath atomically:TRUE];
    filewasupdated = true;
    NSLog(@"Saving:%@", fullPath);
    [currentfile release];
	[newfile release];
	currentfile = nil;
	newfile = nil;
	return filewasupdated;
}

- (void)saveHTML:(NSData*)data file:(NSString*)fileName {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	NSLog(@"Saving:%@", fullPath);
	[data writeToFile:fullPath atomically:TRUE];
	[NSData dataWithContentsOfFile:fullPath];
}

- (void)removeFile:(NSString*)fileName {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	[fileManager removeItemAtPath: fullPath error:NULL];
	NSLog(@"Removing:%@", fullPath);
}

-(void)deleteFiles{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSError* error=nil;
    //NSArray *dirContents = [[NSFileManager defaultManager] directoryContentsAtPath:documentsDirectory];
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
	int count = 0;
	while( count < [dirContents count]){
		//NSLog(@"Deleting: %@",[dirContents objectAtIndex:count]);
		NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[dirContents objectAtIndex:count]];
		NSLog(@"Deleting: %@",fullPath);
		[fileManager removeItemAtPath:fullPath error:NULL];
		count++;
	}
}

-(void)listFiles{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSError *error;
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
	//NSArray *onlyJPGs = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.jpg'"]];
	NSLog(@"Files in Cache ==========================================");
	int count = 0;
	while( count < [dirContents count]){
		NSLog(@"- %@",[dirContents objectAtIndex:count]);
		count++;
	}
	NSLog(@"==========================================================");
}

- (void)dealloc {
	[super dealloc];
}


@end

