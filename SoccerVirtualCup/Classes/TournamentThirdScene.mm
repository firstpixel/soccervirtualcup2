//
//  TournamentThirdScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/14/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "TournamentThirdScene.h"
#import "MenuScene.h"
#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "TournamentScene.h"
#import "SimpleAudioEngine.h"
#import "DeviceSettings.h"
/*
 #import "OFAchievementService.h"
#import "OFAchievement.h"
#import "OFSocialNotificationService.h" 
*/
@implementation TournamentThirdScene
- (id) init {
    self = [super init];
    if (self != nil) {
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                //iPad retina screen
                bg = [CCSprite spriteWithFile:@"tournament_third_screen-ipadhd.jpg"];
            }else{
                //iPad screen
                bg = [CCSprite spriteWithFile:@"tournament_third_screen-ipad.jpg"];
            }
        }else{
            if ([UIScreen instancesRespondToSelector:@selector(scale)])
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                        //iphone 5
                        bg = [CCSprite spriteWithFile:@"tournament_third_screen-hd.jpg"];
                    }else{
                        //iphone retina screen
                        bg = [CCSprite spriteWithFile:@"tournament_third_screen-hd.jpg"];
                    }
                }else{
                    //iphone screen
                    bg = [CCSprite spriteWithFile:@"tournament_third_screen.jpg"];
                }
            }
        }
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
        [self addChild:[TournamentThirdLayer node] z:1];
    }
    return self;
}
@end

@implementation TournamentThirdLayer
- (id) init {
    self = [super init];
    if (self != nil) {
		self.isTouchEnabled = YES;
				
		
		spriteGoPlay = [CCSprite spriteWithSpriteFrameName:@"go_bt.png"];
		[spriteGoPlay setPosition:ADJUST_CCP(ccp(400,46))];
		[self addChild:spriteGoPlay z:101 tag:101];
		
		 // [OFAchievementService updateAchievement:@"242944" andPercentComplete:100.00 andShowNotification:YES];
				

    }
    return self;
}

-(void)goNextScreen{
	MenuScene * ms = [MenuScene node];
	[[CCDirector sharedDirector] replaceScene:ms];
}





- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [self convertTouchToNodeSpace: touch];//[touch locationInView: [touch view]];
	location = CGPointMake(location.x, location.y);
	
	if(CGRectContainsPoint(CGRectMake(spriteGoPlay.position.x- 0.5*spriteGoPlay.contentSize.width, spriteGoPlay.position.y- 0.5*spriteGoPlay.contentSize.height, spriteGoPlay.contentSize.width, spriteGoPlay.contentSize.height), location)) {
		[self goNextScreen];
	}
	//return kEventHandled;	
}
@end