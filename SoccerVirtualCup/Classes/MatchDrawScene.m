//
//  MatchDrawScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/12/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "MatchDrawScene.h"
#import "PenaltyScene.h"
#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "DeviceSettings.h"
#import "MKStoreManager.h"
#import "AppDelegate.h"
#import "RootViewController.h"


@implementation MatchDrawScene
	- (id) init {
		self = [super init];
		if (self != nil) {
            CCSprite * bg = nil;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                CGFloat scale = [[UIScreen mainScreen] scale];
                if (scale > 1.0){
                    //iPad retina screen
                    bg = [CCSprite spriteWithFile:@"match_draw-ipadhd.jpg"];
                }else{
                    //iPad screen
                    bg = [CCSprite spriteWithFile:@"match_draw-ipad.jpg"];
                }
            }else{
                if ([UIScreen instancesRespondToSelector:@selector(scale)])
                {
                    CGFloat scale = [[UIScreen mainScreen] scale];
                    if (scale > 1.0){
                        if([[ UIScreen mainScreen ] bounds ].size.height == 568){
                            //iphone 5
                            bg = [CCSprite spriteWithFile:@"match_draw-hd.jpg"];
                        }else{
                            //iphone retina screen
                            bg = [CCSprite spriteWithFile:@"match_draw-hd.jpg"];
                        }
                    }else{
                        //iphone screen
                        bg = [CCSprite spriteWithFile:@"match_draw.jpg"];
                    }
                }
            }

            [bg setPosition:ADJUST_CCP(ccp(240, 160))];
			[self addChild:bg z:0];
			[self addChild:[MatchDrawLayer node] z:1];
            
		}
		return self;
	}
@end

@implementation MatchDrawLayer
	- (id) init {
		self = [super init];
		if (self != nil) {
			self.isTouchEnabled = YES;
            /*
			spriteGoPlay = [CCSprite spriteWithSpriteFrameName:@"go_bt.png"];
			[spriteGoPlay setPosition:ADJUST_CCP(ccp(400,26))];
			[self addChild:spriteGoPlay z:101 tag:101];*/
            
            if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
                AppDelegate *app = (AppDelegate*) [[UIApplication sharedApplication] delegate];
                [(RootViewController*)[app navController] addInterstitial];
            }
            
            
            
		}
		return self;
	}
    - (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
        [self goNextScreen];
    }


	-(void)goNextScreen{
		PenaltyScene * ps = [PenaltyScene node];
		[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:ps]];
	}
/*
	- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
		UITouch *touch = [touches anyObject];
		CGPoint location = [touch locationInView: [touch view]];
		location = CGPointMake(location.y, location.x);
		if(CGRectContainsPoint(CGRectMake(spriteGoPlay.position.x- 0.5*spriteGoPlay.contentSize.width, spriteGoPlay.position.y- 0.5*spriteGoPlay.contentSize.height, spriteGoPlay.contentSize.width, spriteGoPlay.contentSize.height), location)) {
			[self goNextScreen];
		}
	}
 */
@end
