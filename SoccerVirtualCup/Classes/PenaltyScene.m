//
//  PenaltyScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//


#import "PenaltyScene.h"
#import "GlobalSingleton.h"
#import "GlobalCupSingleton.h"
#import "DeviceSettings.h"
@implementation PenaltyScene

@synthesize penaltyBallLayer;
-(void)dealloc{
	[penaltyBallLayer release];
	[super dealloc];
}

-(id)init{
	self = [super init];
	if(nil != self){
		//[[TextureMgr sharedTextureMgr] removeUnusedTextures];
		GlobalCupSingleton *myCupSingleton = [GlobalCupSingleton sharedInstance];
        
        [self setupBatchNode];
		
		PenaltyBallLayer* layer = [[PenaltyBallLayer alloc]init];
		self.penaltyBallLayer = layer;
		[layer release];
		[self addChild:penaltyBallLayer z:1];
        
		CCSprite* bg = nil;
		if([myCupSingleton matchWeather]==0){
			int r = arc4random()%4;
			if(r==0){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_1_penalty.jpg"];
			}else if(r==1){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2_penalty.jpg"];
			}else if(r==2){
                CCParticleRain* fireworks = [CCParticleRain node];
                fireworks.totalParticles = 1000.0;
                [fireworks setPosition:ADJUST_CCP(ccp(240,320))];
                [self addChild:fireworks z:1001];
                
				raining = [CCParticleRain node];
				raining.totalParticles = 1000.0;
				raining.startSize = 8;
				[self addChild:raining z:1000];
				bg = [CCSprite spriteWithSpriteFrameName:@"field_3_penalty.jpg"];
			}else if(r==3){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2_penalty.jpg"];
                [bg setColor:ccc3(180,180,255)];
			}else{
				bg = [CCSprite spriteWithSpriteFrameName:@"field_1_penalty.jpg"];	
			}
            NSLog(@"game field  = %i",r);
		}else if([myCupSingleton matchWeather]==1){
			int r = arc4random()%3;
			if(r==0){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_1_penalty.jpg"];
			}else if(r==1){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2_penalty.jpg"];
			}else if(r==2){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_3_penalty.jpg"];
                
			}else{
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2_penalty.jpg"];
                [bg setColor:ccc3(180,180,255)];
			}
		}else if([myCupSingleton matchWeather]==2){
            CCParticleRain* fireworks = [CCParticleRain node];
            fireworks.totalParticles = 1000.0;
            [fireworks setPosition:ADJUST_CCP(ccp(240,320))];
            [self addChild:fireworks z:1001];
            
			raining = [CCParticleRain node];
			raining.totalParticles = 1000.0;
			raining.startSize = 8;
			[self addChild:raining z:1000];
			bg = [CCSprite spriteWithSpriteFrameName:@"field_3_penalty.jpg"];
            [bg setColor:ccc3(180,180,255)];
		}else {
			int r = arc4random()%3;
			if(r==0){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_1_penalty.jpg"];
			}else if(r==1){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2_penalty.jpg"];
			}else if(r==2){
				bg = [CCSprite spriteWithSpriteFrameName:@"field_3_penalty.jpg"];
			}else{
				bg = [CCSprite spriteWithSpriteFrameName:@"field_2_penalty.jpg"];
                [bg setColor:ccc3(180,180,255)];
			}
		}
        
        [bg setPosition:ADJUST_CCP(ccp(240, 160))];
        [self addChild:bg z:0];
        [self addChild:[PenaltyLayer node] z:0];
	}
	return self;
}

- (void)setupBatchNode {
    
    
    NSString *fieldsPvrCcz = @"fields_penalty.pvr.ccz";
    NSString *fieldsPlist = @"fields_penalty.plist";
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0){
            //iPad retina screen
            fieldsPvrCcz = @"fields_penalty-ipadhd.pvr.ccz";
            fieldsPlist = @"fields_penalty-ipadhd.plist";
        }else{
            //iPad screen
            fieldsPvrCcz = @"fields_penalty-ipad.pvr.ccz";
            fieldsPlist = @"fields_penalty-ipad.plist";         }
    }else{
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0){
                fieldsPvrCcz = @"fields_penalty-hd.pvr.ccz";
                fieldsPlist = @"fields_penalty-hd.plist";
            }else{
                //iphone screen
                fieldsPvrCcz = @"fields_penalty.pvr.ccz";
                fieldsPlist = @"fields_penalty.plist";
            }
        }
    }
    
    
    
    
    
    
    
    
    _batchNodeFields = [CCSpriteBatchNode batchNodeWithFile:fieldsPvrCcz];
    [self addChild:_batchNodeFields z:-2];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:fieldsPlist];
    
}
@end
@implementation PenaltyLayer
- (id) init {
    self = [super init];
    if (self != nil) {
		self.isTouchEnabled = YES;
    }
    return self;
}
@end
