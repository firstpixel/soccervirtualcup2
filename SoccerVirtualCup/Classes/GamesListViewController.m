//
//  GamesListViewController.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/23/11.
//  Copyright (c) 2011 com.firstpixel. All rights reserved.
//

#import "GamesListViewController.h"
#import "CacheController.h"

@implementation GamesListViewController

@synthesize cancelButton;

CacheController *cacheController;

NSUserDefaults *appprefs;

NSMutableArray *announcementdataFromXML;
NSMutableDictionary *usernews;
NSXMLParser * XMLParser;
NSString * currentElement;
NSMutableString *currentId, *currentType, *currentTitle, *currentDesc,*currentMedia, *currentURL;
NSMutableDictionary *item;  //A temporary item; added to the "stories" array one at a time, and cleared for the next one

BOOL add;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	if ([stories count] == 0) {
        int random = 1+ arc4random() %(6);
        NSString *path = [[NSString alloc] initWithFormat:@"%@%i", @"http://www.firstpixel.com/soccervirtualcup/apns/news.xml?randomparam=", random];
        [self parseXMLFileAtURL:path];
	}
	
	cellSize = CGSizeMake([newsTable bounds].size.width, 80);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [stories count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MyIdentifier = @"MyIdentifier";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:MyIdentifier] autorelease];
	}
	
	// Set up the cell
	int storyIndex = [indexPath indexAtPosition: [indexPath length] - 1];
	[cell setText:[[stories objectAtIndex:storyIndex] objectForKey: @"title"]];
    [cell setBackgroundColor:[UIColor clearColor]];
    NSString* imageURL = [[stories objectAtIndex:storyIndex] objectForKey: @"media"];
    imageURL = [imageURL stringByReplacingOccurrencesOfString:@" " withString:@""];
    imageURL = [imageURL stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    imageURL = [imageURL stringByReplacingOccurrencesOfString:@"	" withString:@""];
	UIImage* images = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
    [cell setBackgroundView:[[UIImageView alloc] initWithImage:images]];
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic
    int storyIndex = [indexPath indexAtPosition:[indexPath length] - 1];
    
    NSString * storyLink = [[stories objectAtIndex: storyIndex] objectForKey: @"url"];
    
    // clean up the link - get rid of spaces, returns, and tabs...
    storyLink = [storyLink stringByReplacingOccurrencesOfString:@" " withString:@""];
    storyLink = [storyLink stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    storyLink = [storyLink stringByReplacingOccurrencesOfString:@"	" withString:@""];
    
    NSLog(@"link: %@", storyLink);
    // open in Safari
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:storyLink]];
}




-(IBAction)dismissForm{
    UIView *v = [self.view viewWithTag:10];
    v.hidden = YES;
    [self.view bringSubviewToFront:v];
    [v removeFromSuperview];
}





- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
		if ([currentElement isEqualToString:@"id"]) {
			[currentId appendString:string];
		}else if ([currentElement isEqualToString:@"type"]) {
			[currentType appendString:string];
		}else if ([currentElement isEqualToString:@"title"]) {
			[currentTitle appendString:string];
		}else if ([currentElement isEqualToString:@"desc"]) {
			[currentDesc appendString:string];
        }else if ([currentElement isEqualToString:@"media"]) {
			[currentMedia appendString:string];
		}else if ([currentElement isEqualToString:@"url"]) {
			[currentURL appendString:string];
		}
}



- (void)viewWillDisappear:(BOOL)animated {
}

- (void)viewDidDisappear:(BOOL)animated {
}




- (void)parserDidStartDocument:(NSXMLParser *)parser{	
	NSLog(@"found file and started parsing");
}

- (void)parseXMLFileAtURL:(NSString *)URL
{	
	stories = [[NSMutableArray alloc] init];
    //you must then convert the path to a proper NSURL or it won't work
    NSURL *xmlURL = [NSURL URLWithString:URL];
    // here, for some reason you have to use NSClassFromString when trying to alloc NSXMLParser, otherwise you will get an object not found error
    // this may be necessary only for the toolchain
    rssParser = [[NSXMLParser alloc] initWithContentsOfURL:xmlURL];
    // Set self as the delegate of the parser so that it will receive the parser delegate methods callbacks.
    [rssParser setDelegate:self];
    // Depending on the XML document you're parsing, you may want to enable these features of NSXMLParser.
    [rssParser setShouldProcessNamespaces:NO];
    [rssParser setShouldReportNamespacePrefixes:NO];
    [rssParser setShouldResolveExternalEntities:NO];
    [rssParser parse];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
	NSString * errorString = [NSString stringWithFormat:@"Unable to download story feed from web site (Error code %i )", [parseError code]];
	NSLog(@"error parsing XML: %@", errorString);
	
	UIAlertView * errorAlert = [[UIAlertView alloc] initWithTitle:@"Error loading content" message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[errorAlert show];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{			
    //NSLog(@"found this element: %@", elementName);
	currentElement = [elementName copy];
	if ([elementName isEqualToString:@"item"]) {
		// clear out our story item caches...
        item = [[NSMutableDictionary alloc] init];
		currentId =					[[NSMutableString alloc] init];
        currentType =				[[NSMutableString alloc] init];
        currentTitle =				[[NSMutableString alloc] init];
        currentDesc =				[[NSMutableString alloc] init];
        currentMedia =				[[NSMutableString alloc] init];
        currentURL =				[[NSMutableString alloc] init];
	}
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{     
	NSLog(@"ended element: %@", elementName);
	if ([elementName isEqualToString:@"item"]) {
        [stories addObject:[item copy]];
    }else{
		[item setObject:currentId forKey:@"id"];
		[item setObject:currentType forKey:@"type"];
		[item setObject:currentTitle forKey:@"title"];
		[item setObject:currentDesc forKey:@"desc"];
		[item setObject:currentMedia forKey:@"media"];
		[item setObject:currentURL forKey:@"url"];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser {
	
	NSLog(@"all done!");
	NSLog(@"stories array has %d items", [stories count]);
	[newsTable reloadData];
}






- (void)dealloc {
	
	[currentElement release];
	[rssParser release];
	[stories release];
	[item release];
    [currentId release];
    [currentType release];
	[currentTitle release];
	[currentDesc release];
	[currentMedia release];
	[currentURL release];
	
	[super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setTransform:CGAffineTransformMakeRotation((float)M_PI_2)];
    [self.view setFrame:self.view.frame];
    self.view.bounds=self.view.bounds;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.view.center=CGPointMake(768/2, 1024/2);
        newsTable.rowHeight = 200;
        
    }else{
        self.view.center=CGPointMake(320/2, 480/2);
        newsTable.rowHeight = 100;
        
    }
    newsTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    newsTable.backgroundColor = [UIColor clearColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return NO;//(interfaceOrientation == UIInterfaceOrientationLandscapeRight)
}

@end
