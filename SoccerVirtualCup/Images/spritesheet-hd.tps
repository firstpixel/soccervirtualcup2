<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>/Users/gilbeyruth/Documents/XCODE/soccervirtualcup2/SoccerVirtualCup/Images/spritesheet-hd.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>-ipadhd</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string>-hd</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.25</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>96</uint>
        <key>dataFormat</key>
        <string>cocos2d</string>
        <key>textureFileName</key>
        <filename>spritesheet{v}.pvr.ccz</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_BEST</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">FloydSteinbergAlpha</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>1</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">pvr2ccz</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>8192</int>
            <key>height</key>
            <int>8192</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <true/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">ContactPoint</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>spritesheet{v}.plist</filename>
            </struct>
            <key>java</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>spritesheet{v}.java</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">PVRTCI_4BPP_RGBA</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">PremultiplyAlpha</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">sprites-hd/alg.png</key>
            <key type="filename">sprites-hd/alg_bt.png</key>
            <key type="filename">sprites-hd/alg_p1.png</key>
            <key type="filename">sprites-hd/alg_p2.png</key>
            <key type="filename">sprites-hd/arg.png</key>
            <key type="filename">sprites-hd/arg_bt.png</key>
            <key type="filename">sprites-hd/arg_p1.png</key>
            <key type="filename">sprites-hd/arg_p2.png</key>
            <key type="filename">sprites-hd/aus.png</key>
            <key type="filename">sprites-hd/aus_bt.png</key>
            <key type="filename">sprites-hd/aus_p1.png</key>
            <key type="filename">sprites-hd/aus_p2.png</key>
            <key type="filename">sprites-hd/back_menu.png</key>
            <key type="filename">sprites-hd/ball-side.png</key>
            <key type="filename">sprites-hd/ball.png</key>
            <key type="filename">sprites-hd/ball_0000.png</key>
            <key type="filename">sprites-hd/ball_0001.png</key>
            <key type="filename">sprites-hd/ball_0002.png</key>
            <key type="filename">sprites-hd/ball_0003.png</key>
            <key type="filename">sprites-hd/ball_0004.png</key>
            <key type="filename">sprites-hd/ball_0005.png</key>
            <key type="filename">sprites-hd/ball_0006.png</key>
            <key type="filename">sprites-hd/ball_0007.png</key>
            <key type="filename">sprites-hd/ball_0008.png</key>
            <key type="filename">sprites-hd/ball_0009.png</key>
            <key type="filename">sprites-hd/ball_0010.png</key>
            <key type="filename">sprites-hd/ball_0011.png</key>
            <key type="filename">sprites-hd/ball_0012.png</key>
            <key type="filename">sprites-hd/ball_0013.png</key>
            <key type="filename">sprites-hd/ball_0014.png</key>
            <key type="filename">sprites-hd/ball_0015.png</key>
            <key type="filename">sprites-hd/ball_0016.png</key>
            <key type="filename">sprites-hd/ball_0017.png</key>
            <key type="filename">sprites-hd/ball_0018.png</key>
            <key type="filename">sprites-hd/ball_0019.png</key>
            <key type="filename">sprites-hd/ball_0020.png</key>
            <key type="filename">sprites-hd/ball_0021.png</key>
            <key type="filename">sprites-hd/ball_0022.png</key>
            <key type="filename">sprites-hd/ball_0023.png</key>
            <key type="filename">sprites-hd/ball_0024.png</key>
            <key type="filename">sprites-hd/ball_0025.png</key>
            <key type="filename">sprites-hd/ball_0026.png</key>
            <key type="filename">sprites-hd/ball_0027.png</key>
            <key type="filename">sprites-hd/ball_0028.png</key>
            <key type="filename">sprites-hd/ball_0029.png</key>
            <key type="filename">sprites-hd/ball_0030.png</key>
            <key type="filename">sprites-hd/ball_0031.png</key>
            <key type="filename">sprites-hd/ball_0032.png</key>
            <key type="filename">sprites-hd/ball_0033.png</key>
            <key type="filename">sprites-hd/ball_0034.png</key>
            <key type="filename">sprites-hd/ball_0035.png</key>
            <key type="filename">sprites-hd/ball_0036.png</key>
            <key type="filename">sprites-hd/ball_0037.png</key>
            <key type="filename">sprites-hd/ball_0038.png</key>
            <key type="filename">sprites-hd/ball_0039.png</key>
            <key type="filename">sprites-hd/ball_0040.png</key>
            <key type="filename">sprites-hd/ball_0041.png</key>
            <key type="filename">sprites-hd/ball_0042.png</key>
            <key type="filename">sprites-hd/ball_0043.png</key>
            <key type="filename">sprites-hd/ball_0044.png</key>
            <key type="filename">sprites-hd/ball_0045.png</key>
            <key type="filename">sprites-hd/ball_0046.png</key>
            <key type="filename">sprites-hd/ball_0047.png</key>
            <key type="filename">sprites-hd/ball_0048.png</key>
            <key type="filename">sprites-hd/ball_0049.png</key>
            <key type="filename">sprites-hd/ball_0050.png</key>
            <key type="filename">sprites-hd/ball_0051.png</key>
            <key type="filename">sprites-hd/ball_0052.png</key>
            <key type="filename">sprites-hd/ball_0053.png</key>
            <key type="filename">sprites-hd/ball_0054.png</key>
            <key type="filename">sprites-hd/ball_0055.png</key>
            <key type="filename">sprites-hd/ball_0056.png</key>
            <key type="filename">sprites-hd/ball_0057.png</key>
            <key type="filename">sprites-hd/ball_0058.png</key>
            <key type="filename">sprites-hd/ball_0059.png</key>
            <key type="filename">sprites-hd/ball_shadow.png</key>
            <key type="filename">sprites-hd/ball_volume.png</key>
            <key type="filename">sprites-hd/bel.png</key>
            <key type="filename">sprites-hd/bel_bt.png</key>
            <key type="filename">sprites-hd/bel_p1.png</key>
            <key type="filename">sprites-hd/bel_p2.png</key>
            <key type="filename">sprites-hd/bih.png</key>
            <key type="filename">sprites-hd/bih_bt.png</key>
            <key type="filename">sprites-hd/bih_p1.png</key>
            <key type="filename">sprites-hd/bih_p2.png</key>
            <key type="filename">sprites-hd/bra.png</key>
            <key type="filename">sprites-hd/bra_bt.png</key>
            <key type="filename">sprites-hd/bra_p1.png</key>
            <key type="filename">sprites-hd/bra_p2.png</key>
            <key type="filename">sprites-hd/bt-buy-USA.png</key>
            <key type="filename">sprites-hd/bt-buy-algeria.png</key>
            <key type="filename">sprites-hd/bt-buy-australia.png</key>
            <key type="filename">sprites-hd/bt-buy-brazil.png</key>
            <key type="filename">sprites-hd/bt-buy-croatia.png</key>
            <key type="filename">sprites-hd/bt-buy-england.png</key>
            <key type="filename">sprites-hd/bt-buy-france.png</key>
            <key type="filename">sprites-hd/bt-buy-italy.png</key>
            <key type="filename">sprites-hd/bt-buy-japan.png</key>
            <key type="filename">sprites-hd/bt-buy-mexico.png</key>
            <key type="filename">sprites-hd/bt-buy-netherlands.png</key>
            <key type="filename">sprites-hd/bt-buy-nigeria.png</key>
            <key type="filename">sprites-hd/bt-buy-portugal.png</key>
            <key type="filename">sprites-hd/bt-buy-russia.png</key>
            <key type="filename">sprites-hd/bt-buy-switzerland.png</key>
            <key type="filename">sprites-hd/bt-buy-uruguay.png</key>
            <key type="filename">sprites-hd/bt-buy-worldcup.png</key>
            <key type="filename">sprites-hd/bt-restore-all.png</key>
            <key type="filename">sprites-hd/bt_continue.png</key>
            <key type="filename">sprites-hd/bt_continue_over.png</key>
            <key type="filename">sprites-hd/bt_new_game.png</key>
            <key type="filename">sprites-hd/bt_new_game_over.png</key>
            <key type="filename">sprites-hd/charmap1.png</key>
            <key type="filename">sprites-hd/chi.png</key>
            <key type="filename">sprites-hd/chi_bt.png</key>
            <key type="filename">sprites-hd/chi_p1.png</key>
            <key type="filename">sprites-hd/chi_p2.png</key>
            <key type="filename">sprites-hd/civ.png</key>
            <key type="filename">sprites-hd/civ_bt.png</key>
            <key type="filename">sprites-hd/civ_p1.png</key>
            <key type="filename">sprites-hd/civ_p2.png</key>
            <key type="filename">sprites-hd/clock1.png</key>
            <key type="filename">sprites-hd/clock10.png</key>
            <key type="filename">sprites-hd/clock11.png</key>
            <key type="filename">sprites-hd/clock12.png</key>
            <key type="filename">sprites-hd/clock13.png</key>
            <key type="filename">sprites-hd/clock14.png</key>
            <key type="filename">sprites-hd/clock15.png</key>
            <key type="filename">sprites-hd/clock16.png</key>
            <key type="filename">sprites-hd/clock17.png</key>
            <key type="filename">sprites-hd/clock18.png</key>
            <key type="filename">sprites-hd/clock19.png</key>
            <key type="filename">sprites-hd/clock2.png</key>
            <key type="filename">sprites-hd/clock20.png</key>
            <key type="filename">sprites-hd/clock21.png</key>
            <key type="filename">sprites-hd/clock22.png</key>
            <key type="filename">sprites-hd/clock23.png</key>
            <key type="filename">sprites-hd/clock24.png</key>
            <key type="filename">sprites-hd/clock3.png</key>
            <key type="filename">sprites-hd/clock4.png</key>
            <key type="filename">sprites-hd/clock5.png</key>
            <key type="filename">sprites-hd/clock6.png</key>
            <key type="filename">sprites-hd/clock7.png</key>
            <key type="filename">sprites-hd/clock8.png</key>
            <key type="filename">sprites-hd/clock9.png</key>
            <key type="filename">sprites-hd/clockNumbers.png</key>
            <key type="filename">sprites-hd/clockSprite.png</key>
            <key type="filename">sprites-hd/clockSprite_blk.png</key>
            <key type="filename">sprites-hd/cmr.png</key>
            <key type="filename">sprites-hd/cmr_bt.png</key>
            <key type="filename">sprites-hd/cmr_p1.png</key>
            <key type="filename">sprites-hd/cmr_p2.png</key>
            <key type="filename">sprites-hd/col.png</key>
            <key type="filename">sprites-hd/col_bt.png</key>
            <key type="filename">sprites-hd/col_p1.png</key>
            <key type="filename">sprites-hd/col_p2.png</key>
            <key type="filename">sprites-hd/crc.png</key>
            <key type="filename">sprites-hd/crc_bt.png</key>
            <key type="filename">sprites-hd/crc_p1.png</key>
            <key type="filename">sprites-hd/crc_p2.png</key>
            <key type="filename">sprites-hd/cro.png</key>
            <key type="filename">sprites-hd/cro_bt.png</key>
            <key type="filename">sprites-hd/cro_p1.png</key>
            <key type="filename">sprites-hd/cro_p2.png</key>
            <key type="filename">sprites-hd/dnk.png</key>
            <key type="filename">sprites-hd/dnk_bt.png</key>
            <key type="filename">sprites-hd/dnk_p1.png</key>
            <key type="filename">sprites-hd/dnk_p2.png</key>
            <key type="filename">sprites-hd/drag_arrow.png</key>
            <key type="filename">sprites-hd/draw.png</key>
            <key type="filename">sprites-hd/ecu.png</key>
            <key type="filename">sprites-hd/ecu_bt.png</key>
            <key type="filename">sprites-hd/ecu_p1.png</key>
            <key type="filename">sprites-hd/ecu_p2.png</key>
            <key type="filename">sprites-hd/egy.png</key>
            <key type="filename">sprites-hd/egy_bt.png</key>
            <key type="filename">sprites-hd/egy_p1.png</key>
            <key type="filename">sprites-hd/egy_p2.png</key>
            <key type="filename">sprites-hd/eng.png</key>
            <key type="filename">sprites-hd/eng_bt.png</key>
            <key type="filename">sprites-hd/eng_p1.png</key>
            <key type="filename">sprites-hd/eng_p2.png</key>
            <key type="filename">sprites-hd/esp.png</key>
            <key type="filename">sprites-hd/esp_bt.png</key>
            <key type="filename">sprites-hd/esp_p1.png</key>
            <key type="filename">sprites-hd/esp_p2.png</key>
            <key type="filename">sprites-hd/fra.png</key>
            <key type="filename">sprites-hd/fra_bt.png</key>
            <key type="filename">sprites-hd/fra_p1.png</key>
            <key type="filename">sprites-hd/fra_p2.png</key>
            <key type="filename">sprites-hd/ger.png</key>
            <key type="filename">sprites-hd/ger_bt.png</key>
            <key type="filename">sprites-hd/ger_p1.png</key>
            <key type="filename">sprites-hd/ger_p2.png</key>
            <key type="filename">sprites-hd/get_ready.png</key>
            <key type="filename">sprites-hd/gha.png</key>
            <key type="filename">sprites-hd/gha_bt.png</key>
            <key type="filename">sprites-hd/gha_p1.png</key>
            <key type="filename">sprites-hd/gha_p2.png</key>
            <key type="filename">sprites-hd/go_bt.png</key>
            <key type="filename">sprites-hd/go_bt_click.png</key>
            <key type="filename">sprites-hd/gol1.png</key>
            <key type="filename">sprites-hd/gol2.png</key>
            <key type="filename">sprites-hd/gre.png</key>
            <key type="filename">sprites-hd/gre_bt.png</key>
            <key type="filename">sprites-hd/gre_p1.png</key>
            <key type="filename">sprites-hd/gre_p2.png</key>
            <key type="filename">sprites-hd/hon.png</key>
            <key type="filename">sprites-hd/hon_bt.png</key>
            <key type="filename">sprites-hd/hon_p1.png</key>
            <key type="filename">sprites-hd/hon_p2.png</key>
            <key type="filename">sprites-hd/icon.png</key>
            <key type="filename">sprites-hd/invisible.png</key>
            <key type="filename">sprites-hd/irn.png</key>
            <key type="filename">sprites-hd/irn_bt.png</key>
            <key type="filename">sprites-hd/irn_p1.png</key>
            <key type="filename">sprites-hd/irn_p2.png</key>
            <key type="filename">sprites-hd/isl.png</key>
            <key type="filename">sprites-hd/isl_bt.png</key>
            <key type="filename">sprites-hd/isl_p1.png</key>
            <key type="filename">sprites-hd/isl_p2.png</key>
            <key type="filename">sprites-hd/ita.png</key>
            <key type="filename">sprites-hd/ita_bt.png</key>
            <key type="filename">sprites-hd/ita_p1.png</key>
            <key type="filename">sprites-hd/ita_p2.png</key>
            <key type="filename">sprites-hd/its_your_turn.png</key>
            <key type="filename">sprites-hd/jpn.png</key>
            <key type="filename">sprites-hd/jpn_bt.png</key>
            <key type="filename">sprites-hd/jpn_p1.png</key>
            <key type="filename">sprites-hd/jpn_p2.png</key>
            <key type="filename">sprites-hd/kor.png</key>
            <key type="filename">sprites-hd/kor_bt.png</key>
            <key type="filename">sprites-hd/kor_p1.png</key>
            <key type="filename">sprites-hd/kor_p2.png</key>
            <key type="filename">sprites-hd/lost.png</key>
            <key type="filename">sprites-hd/mar.png</key>
            <key type="filename">sprites-hd/mar_bt.png</key>
            <key type="filename">sprites-hd/mar_p1.png</key>
            <key type="filename">sprites-hd/mar_p2.png</key>
            <key type="filename">sprites-hd/mex.png</key>
            <key type="filename">sprites-hd/mex_bt.png</key>
            <key type="filename">sprites-hd/mex_p1.png</key>
            <key type="filename">sprites-hd/mex_p2.png</key>
            <key type="filename">sprites-hd/ned.png</key>
            <key type="filename">sprites-hd/ned_bt.png</key>
            <key type="filename">sprites-hd/ned_p1.png</key>
            <key type="filename">sprites-hd/ned_p2.png</key>
            <key type="filename">sprites-hd/nga.png</key>
            <key type="filename">sprites-hd/nga_bt.png</key>
            <key type="filename">sprites-hd/nga_p1.png</key>
            <key type="filename">sprites-hd/nga_p2.png</key>
            <key type="filename">sprites-hd/no_.gif</key>
            <key type="filename">sprites-hd/ok_.gif</key>
            <key type="filename">sprites-hd/ok_no_.gif</key>
            <key type="filename">sprites-hd/pan.png</key>
            <key type="filename">sprites-hd/pan_bt.png</key>
            <key type="filename">sprites-hd/pan_p1.png</key>
            <key type="filename">sprites-hd/pan_p2.png</key>
            <key type="filename">sprites-hd/penalty_lost.png</key>
            <key type="filename">sprites-hd/penalty_scored.png</key>
            <key type="filename">sprites-hd/per.png</key>
            <key type="filename">sprites-hd/per_bt.png</key>
            <key type="filename">sprites-hd/per_p1.png</key>
            <key type="filename">sprites-hd/per_p2.png</key>
            <key type="filename">sprites-hd/player1.png</key>
            <key type="filename">sprites-hd/player2.png</key>
            <key type="filename">sprites-hd/pol.png</key>
            <key type="filename">sprites-hd/pol_bt.png</key>
            <key type="filename">sprites-hd/pol_p1.png</key>
            <key type="filename">sprites-hd/pol_p2.png</key>
            <key type="filename">sprites-hd/prt.png</key>
            <key type="filename">sprites-hd/prt_bt.png</key>
            <key type="filename">sprites-hd/prt_p1.png</key>
            <key type="filename">sprites-hd/prt_p2.png</key>
            <key type="filename">sprites-hd/rus.png</key>
            <key type="filename">sprites-hd/rus_bt.png</key>
            <key type="filename">sprites-hd/rus_p1.png</key>
            <key type="filename">sprites-hd/rus_p2.png</key>
            <key type="filename">sprites-hd/sau.png</key>
            <key type="filename">sprites-hd/sau_bt.png</key>
            <key type="filename">sprites-hd/sau_p1.png</key>
            <key type="filename">sprites-hd/sau_p2.png</key>
            <key type="filename">sprites-hd/sen.png</key>
            <key type="filename">sprites-hd/sen_bt.png</key>
            <key type="filename">sprites-hd/sen_p1.png</key>
            <key type="filename">sprites-hd/sen_p2.png</key>
            <key type="filename">sprites-hd/sound.png</key>
            <key type="filename">sprites-hd/srb.png</key>
            <key type="filename">sprites-hd/srb_bt.png</key>
            <key type="filename">sprites-hd/srb_p1.png</key>
            <key type="filename">sprites-hd/srb_p2.png</key>
            <key type="filename">sprites-hd/star.png</key>
            <key type="filename">sprites-hd/star_holder.png</key>
            <key type="filename">sprites-hd/sui.png</key>
            <key type="filename">sprites-hd/sui_bt.png</key>
            <key type="filename">sprites-hd/sui_p1.png</key>
            <key type="filename">sprites-hd/sui_p2.png</key>
            <key type="filename">sprites-hd/swe.png</key>
            <key type="filename">sprites-hd/swe_bt.png</key>
            <key type="filename">sprites-hd/swe_p1.png</key>
            <key type="filename">sprites-hd/swe_p2.png</key>
            <key type="filename">sprites-hd/time_10min.png</key>
            <key type="filename">sprites-hd/time_10min_bt.png</key>
            <key type="filename">sprites-hd/time_15min.png</key>
            <key type="filename">sprites-hd/time_15min_bt.png</key>
            <key type="filename">sprites-hd/time_3min.png</key>
            <key type="filename">sprites-hd/time_3min_bt.png</key>
            <key type="filename">sprites-hd/time_5min.png</key>
            <key type="filename">sprites-hd/time_5min_bt.png</key>
            <key type="filename">sprites-hd/time_8min.png</key>
            <key type="filename">sprites-hd/time_8min_bt.png</key>
            <key type="filename">sprites-hd/touchSprite.png</key>
            <key type="filename">sprites-hd/tun.png</key>
            <key type="filename">sprites-hd/tun_bt.png</key>
            <key type="filename">sprites-hd/tun_p1.png</key>
            <key type="filename">sprites-hd/tun_p2.png</key>
            <key type="filename">sprites-hd/uru.png</key>
            <key type="filename">sprites-hd/uru_bt.png</key>
            <key type="filename">sprites-hd/uru_p1.png</key>
            <key type="filename">sprites-hd/uru_p2.png</key>
            <key type="filename">sprites-hd/usa.png</key>
            <key type="filename">sprites-hd/usa_bt.png</key>
            <key type="filename">sprites-hd/usa_p1.png</key>
            <key type="filename">sprites-hd/usa_p2.png</key>
            <key type="filename">sprites-hd/versusScoreSprite.png</key>
            <key type="filename">sprites-hd/versusScoreSprite_blk.png</key>
            <key type="filename">sprites-hd/wait_playing.png</key>
            <key type="filename">sprites-hd/weather_fine.png</key>
            <key type="filename">sprites-hd/weather_fine_bt.png</key>
            <key type="filename">sprites-hd/weather_rainy.png</key>
            <key type="filename">sprites-hd/weather_rainy_bt.png</key>
            <key type="filename">sprites-hd/weather_random.png</key>
            <key type="filename">sprites-hd/weather_random_bt.png</key>
            <key type="filename">sprites-hd/win.png</key>
            <key type="filename">sprites-hd/you.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>sprites-hd</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
