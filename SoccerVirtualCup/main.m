//
//  main.m
//  SoccerVirtualCup2
//
//  Created by Gil Beyruth on 10/4/12.
//  Copyright Gil Beyruth 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
    [pool release];
    return retVal;
}
